//
//  WebServiceConstants.h
//
//
//  Created by 3Embed on 03/12/15.
//  Copyright (c) 2014 3Embed. All rights reserved.
//


// WEBSERVICE URL CONSTATNTS
#define iPhoneBaseURL      @"http://resellapp.io/api/"
#define mChatAPIBaseUrl    @"http://13.58.201.120:5010/"
#define mImageUrlForChat   @"http://13.58.201.120/chat/profilePics"
#define mMediaUploadForChat @"http://13.58.201.120:8009/"

// Cloudinary + Share Links
#define UrlForCloudinary @"https://res.cloudinary.com/deqvp8eep/image/upload/"
#define SHARE_LINK       @"http://resellapp.io/terms"
#define adminGalleryURL  @""
#define TERMS_CONDITIONS_LINK  @"http://resellapp.io/terms"
#define PRIVACY_LINK     @"http://resellapp.io/privacy"

//
// MQTT HOST & PORT

#define mMqttUsername    @"resell"
#define mMqttPassword    @"cTML6ayBRnGV8JYyHU"
#define mMQTTHost    @"13.58.201.120"
#define mPort        @"1883"  // Give Int values only inside sting

//


// AUTH Username / Passwords

#define authUsername   @"basicAuth"
#define authPassword   @"&jno-@8az=wSo*NHYVGpF^AQ?4yn36ZvW5ToUCUN+XGOuC?sz#SE$oxXVbwQGP|3WFyjcTAj2SIRQnLE|vo^-|-ATV5FZUf2*5A3Oiu|_EOMmG==&iApzQL3R7HHQj?jtb0mc2mT$Y%Isrgrxveld#Z^g3-ul^|0xAITganIuF23J0waSa6z6aP_+%De5LqtuY&ptx?qhZySECdyE^*4R^b*hFjQ-9?cCSJNfROzztEYbRyN=SqDyhhpzSmmP|Eb"

#define mAuthorization @"KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj"
//

// GOOGLE CONSOLE KEYS
#define mGoogleAPIKeyFCM        @"key=AIzaSyAw3JK0qz7w-jsEgWMlTwJt-VEASxfIfNI"
#define mGoogleServiceKey       @"AIzaSyD6JYYi4dKmRzKJvqlgKGn5Ltox8upeL9Y"
#define mGoogleAdsAppId         @""
#define mGoogleAdsUnitId        @""
#define mGoogleBannerAdsUnitId  @""
//

//  ENUMS FOR REQUESTS
typedef enum : NSUInteger {
    RequestTypeCheckPhoneNumber,
    RequestTypeVerify,
    RequestResendCode,
    RequestTypeLogin,
    RequestTypeLoginfaceBookContactSync,
    RequestTypeEmailCheck,
    RequestTypePhoneNumberCheck,
    RequestTypeUserNameCheck,
    RequestTypenewRegister,
    RequestTypeotpGeneration,
    RequestTypePhoneContactSync,
    RequestTypePost,
    RequestTypeFollow,
    RequestTypeFollowMultiple,
    RequestTypeunFollow,
    RequestTypeGetuserPosts,
    RequestTypemakePostRequest,
    RequestTypeGetHashTagsSuggestion,
    RequestTypeGetTagFriendsSuggestion,
    RequestTypeGetSearchPeople,
    RequestTypeGetExploreposts,
    RequestTypeGetSearchForPosts,
    RequestTypeGetFollowersList,
    RequestTypeGetFollowingList,
    RequestTypeCloudinaryCredintials,
    RequestTypegetPostsInHOmeScreen,
    RequestTypePostComment,
    RequestTypeGetPosts,
    RequestTypeFeedBack,
    RequestTypemakeUserProfileDetails,
    RequestTypemakeresetPassword,
    RequestTypeGetMemberFollowersList,
    RequestTypeGetMemberFollowingList,
    RequestTypeProfileDetails,
    RequestTypeDeletePost,
    RequestTypeSavingProfile,
    RequestTypeEmailCheckInEditProfile,
    RequestTypePhoneNumberCheckEditProfile,
    RequestTypeupdatePhoneNumber,
    RequestTypeEditProfile,
    RequestTypebasedonhashtag,
    RequestTypebasedonLoaction,
    RequestTypePhotosOfYou,
    RequestTypefollowingActivity,
    RequestTypeOwnActivity,
    RequestTypedeleteComments,
    RequestTypegetPhotosOfMember,
    RequestTypeLogout,
    RequestTypeGetCommentsOnPost,
    RequestTypeLikeAPost,
    RequestTypeUnlikeAPost,
    RequestTypeGetAllLikesOnPost,
    RequestTypeDiscoverPeople,
    RequestTypeGetReportReasonForPost,
    RequestTypereportPost,
    RequestTypegetUserSearchHistory,
    RequestTypeAddToSearchHistory,
    RequestTypesetPrivateProfile,
    RequestTypeaccceptFollowRequest,
    RequestTypehideFromDiscovery,
    RequestTypeGetFollowRequestForAccept,
    RequestTypeGetPostsDetailsForActivity,
    RequestTypeGetPostDetails,
    RequestTypeGetCategories,
    RequestTypeGetPostsByUsers,
    RequestTypeGetPostsForGuests,
    RequestTypeSearchProductsByFilters,
    RequestTypeEditPost,
    RequestTypePostsLikedByUser,
    RequestToLogUserDevice,
    RequestTypemarkSold,
    RequestTyperateForSeller,
    RequestToMarkAsSold,
    RequestToMarkAsSelling,
    RequestTypemakeOffer,
    RequestTypeacceptedOffers,
    RequestToGetWebContent,
    RequestTypesoldElseWhere,
    RequestTypeunseenNotificationCount,
    RequestToVerifyWithFacebook,
    RequestToVerifyWithGooglePlus,
    RequestUserCampaign,
    RequestGetCampaign,
    RequestToSavePaypal,
    RequestUserChatHistory,
    RequestUserDeleteChat,
    RequestToGetInsights,
    RequestToGetMonthInsights,
    RequestToGetCityInsights,
    RequestForPromoPlans,
    RequestForPurchasePlans
} RequestType;

//methods
#define mRequestTypeunseenNotificationCount                 @"unseenNotificationCount"
#define mRequestTypesoldElseWhere                           @"sold/elseWhere"
#define mRequestToGetUserChatHistory                        @"chatHistory"
#define mRequestToDeleteUser                                @"deleteChat"
#define mRequestToGetWebContent                             @"adminWebsitePagesURL"
#define mRequestTypeacceptedOffers                          @"acceptedOffers"
#define mRequestTypemakeOffer                               @"makeOffer"
#define mRequestTypemarkSold                                @"markSold"
#define mRequestTypeLogin                                   @"login"
#define mLogUserDevice                                      @"logDevice"
#define mLogGuestUserDevice                                 @"logGuest"
#define mRequestTypeEmailCheck                              @"emailCheck"
#define mRequestTypePhoneNumberCheck                        @"phoneNumberCheck"
#define mRequestTypeUserNameCheck                           @"usernameCheck"
#define mRequestTypenewRegister                             @"register"
#define mRequestTypeotpGeneration                           @"otp"
#define mRequestTypeLoginfaceBookContactSync                @"facebookContactSync"
#define mRequestTypePhoneContactSync                        @"phoneContacts"
#define mRequestTypePost                                    @"userPosts"
#define mRequestTypeFollow                                  @"follow/"
#define mRequestTypegetFollowRequestsForPrivateUsers        @"getFollowRequestsForPrivateUsers"
#define mRequestTypeUnFollow                                @"unfollow/"
#define mRequestTypeFollowMultiple                          @"followMultipleUsers"
#define mRequestTypeGetuserPosts                            @"userPosts"
#define mGetPostsByusers                                    @"getPostsById/users"
#define mGetPostsForGuests                                  @"getPostsById/guests"
#define mgetHashTagsSuggestion                              @"getHashTagSuggetion"
#define mgetUserNameSuggestion                              @"getUsersForTagging"
#define mgetPostsForUsers                                   @"allPosts/users/m"
#define mgetPostsForGuests                                  @"allPosts/guests/m"
#define mBusinessPostRequest                                @"product/v2"
#define mGetPostDetails                                     @"getPostsById"
#define mGetUserPosts                                       @"getUserPosts"
#define mGetCategories                                      @"getCategories"
#define mrequesttypesetPrivateProfile                       @"setPrivateProfile"
#define mRequestTypehideFromDiscovery                       @"hideFromDiscovery"
#define mfollowAction                                       @"action"
#define mRequestTypeaccceptFollowRequest                    @"accceptFollowRequest"
#define mFilterProducts                                     @"searchFilter/m"
#define mEditPostedProduct                                  @"product/v2"
#define mPostsLikedByUser                                   @"likedPosts"
#define mgetSearchForPosts                                  @"search/"
#define mgetFollowersList                                   @"getFollowers"
#define mgetFollwingList                                    @"getFollowing"
#define mgetCloudinaryCredintials                           @"getSignature"
#define mgetPostsInHOmeScreen                               @"home"
#define mPostComment                                        @"comments"
#define mUserProfile                                        @"profile"
#define mMemberProfile                                      @"profile/users"
#define mMemberProfileForGuest                              @"profile/guests"
#define mUserPosts                                          @"profile/posts"
#define mMemberSellingPosts                                 @"profile/posts/"
#define mMemberPostsForGuest                                @"profile/guests/posts"
#define mreportAProblem                                      @"reportAProblem"
#define mMembergetUserProfileBasics                         @"getUserProfileBasics"
#define mgetMemberFollowersList                             @"getMemberFollowers"
#define mgetMemberFollowingList                             @"getMemberFollowing"
#define mdeletePost                                         @"product/v2"
#define mRequestTypeSavingProfile                           @"saveProfile"
#define mRequestTypeEmailCheckInEditProfile                 @"check_mail"
#define mRequestTypePhoneNumberCheckInEditProfile           @"checkPhoneNumber"
#define mRequestTypeEditProfile                             @"editProfile"
#define mpostsbasedonhashtag                                @"getPostsOnHashTags"
#define mpostsbasedonLoaction                               @"getPostsByLocation"
#define mPhotosOfYou                                        @"getPhotosOfYou"
#define mRequestTypePhoneNumberUpdate                       @"updatePhoneNumber"
#define mRequestTypresetPassword                            @"resetPassword"
#define mMarkAsSelling                                      @"markSelling"
#define mMarkAsSold                                         @"markSold"
#define mSoldSomewhereElse                                  @"sold/elseWhere"
#define mGetCommentsOnPost                      @"getPostComments"
#define mLikeAPost                              @"like"
#define mUnlikeAPost                            @"unlike"
#define mGetAllLikesOnPost                      @"getAllLikes"
#define mRequestTyperateForSeller               @"rate/"
#define mqttID                                   @"mqttId"

#define payPalLink                             @"PaypalData"
#define mPayPalMe                               @"paypal/me"
#define mUserCampaign                           @"user/campaign"
#define mRuncampaign                            @"user/runcampaign"
#define mInsights                               @"insights"
#define mPromoPlans                             @"promotionPlans"
#define mPurchasePromoPlan                      @"promotePosts/"

#define mVerifyWithFacebook                     @"facebook/me"
#define mVerifyWithGoogle                       @"google/me"

#define mdiscoverPeople                         @"discover-people-website" 
#define mReportPostReason                       @"postReportReason"
#define mReportUserReason                       @"reportReason"
#define mRequestTypereportPost                  @"reportPost"
#define mReportUser                             @"report/"
#define mgetUserSearchHistory                   @"getUserSearchHistory"
#define maddToSearch                            @"searchHistory"

#define mRequestTypefollowingActivity           @"followingActivity"
#define mRequestTypeOwnActivity                 @"selfActivity"
#define mRequestTypedeleteComments              @"deleteCommentsFromPost"
#define mRequestTypegetPhotosOfMember           @"getPhotosOfMember"
#define mRequestTypegetPostsDetailsForActivity  @"getPostsDetailsForActivity"


#define mDevicePhoneNo                          @"Phone"
#define mDeviceBrand                            @"brand"
#define mContryCode                             @"CountryCode"
#define mOnlyNumber                             @"OnlyNumber"

//parameters
#define mLoginType                               @"loginType"
#define mEmail                                   @"email"
#define mphoneNumber                             @"phoneNumber"
#define mfbuniqueid                              @"facebookId"
#define mUserName                                @"username"
#define mUserId                                  @"userId"
#define mMqttId                                  @"mqttId"
#define mPswd                                    @"password"
#define mDeviceType                              @"deviceType"
#define mDeviceId                                @"deviceId"
#define mProfileUrl                              @"profilePicUrl"
#define mSignUpType                              @"signupType"
#define cloudinartyDetails                       @"cloudinaryDetails"
#define misPrivate                               @"isPrivate"
#define mdeviceToken                             @"deviceToken"
#define mGooglePlusAccessToken                   @"googleToken"
#define mCampaignId                              @"campaignId"

#define userDetailkeyWhileRegistration       @"userDetailWhileRegistration"
#define mpushToken                           @"pushToken"
#define mfaceBookId                          @"facebookId"
#define mauthToken                           @"token"
#define mcontacts                            @"contactNumbers"
#define mtype                                @"type"
#define mmailUrl                             @"mainUrl"
#define mthumbeNailUrl                       @"thumbnailUrl"
#define mpostCaption                         @"postCaption"
#define mhashTags                            @"hashTags"
#define mplace                               @"place"
#define musersTagged                         @"usersTagged"
#define muserNameTofollow                    @"userNameToFollow"
#define muserNameToUnFollow                  @"unfollowUserName"
#define mGooglePlusId                        @"googleId"
#define mPayPalUrl                           @"paypalUrl"



#define mfaceBookId                          @"facebookId"
#define mFacebookAccessToken                 @"accessToken"
#define macesstoken                          @"xaccesstoken"
#define mhashTag                             @"hashtag"
#define muserTosearch                        @"userToBeSearched"
#define msearchPeopleForGuest                @"guests/search/member"
#define mKeyToSearch                         @"keyToSearch"
#define mcomment                             @"comment"
#define mposttype                            @"postType"
#define mpostid                              @"postId"
#define mPromoPlanId                         @"planId"
#define mmemberName                          @"membername"
#define mMember                              @"member"
#define mDescription                         @"description"
#define mlocation                            @"location"
#define mlatitude                            @"latitude"
#define mlongitude                           @"longitude"
#define mPushTokenKey                        @"pushToken"
#define mfullName                            @"fullName"
#define mwebsite                             @"website"
#define mbio                                 @"bio"
#define mgender                              @"gender"
#define mlimit                               @"limit"
#define motp                                 @"otp"
#define mContainerHeight                     @"containerHeight"
#define mcontainerWidth                      @"containerWidth"
#define maddToSearchKey                      @"searchKey"
#define mcommentId                           @"commentId"
#define mmembername                          @"membername"
#define mhasAudio                            @"hasAudio"
#define mfeature                             @"feature"
#define mproblemExplaination                 @"problemExplaination"
#define mlogout                              @"logout/m"
#define mLimit                               @"limit"
#define moffset                              @"offset"
#define mSortby                              @"sortBy"
#define mdistanceOrder                       @"distanceOrder"
#define mPostedWithIn                        @"postedWithin"
#define mMinPrice                            @"minPrice"
#define mMaxPrice                            @"maxPrice"
#define mDistance                            @"distance"
#define mCurrLatt                            @"currentLatitude"
#define mCurrLongi                           @"currentLongitude"
#define mCountryShortName                    @"countrySname"
#define mCity                                @"city"
#define mReasonId                            @"reasonId"
#define mReportedUser                        @"reportedUser"
#define mDurationType                        @"durationType"




/**
 Param for posting a Product
 */
#define mPhotoType                                        @"0"
#define mProductName                                      @"productName"
#define mMainImgeUrl                                    @"mainUrl"
#define mMainImgeThumb                                  @"thumbnailImageUrl"
#define mMainImgeHeight                                  @"containerHeight"
#define mMainImgeWidth                                  @"containerWidth"
#define mImgHeight1                                      @"containerHeight1"
#define mImgHeight2                                      @"containerHeight2"
#define mImgHeight3                                      @"containerHeight3"
#define mImgHeight4                                      @"containerHeight4"
#define mImgWidth1                                       @"containerWidth1"
#define mImgWidth2                                       @"containerWidth2"
#define mImgWidth3                                       @"containerWidth3"
#define mImgWidth4                                       @"containerWidth4"
#define mImgUrl1                                         @"imageUrl1"
#define mImgUrl2                                         @"imageUrl2"
#define mImgUrl3                                         @"imageUrl3"
#define mImgUrl4                                         @"imageUrl4"
#define mThumbUrl1                                       @"thumbnailUrl1"
#define mThumbUrl2                                       @"thumbnailUrl2"
#define mThumbUrl3                                       @"thumbnailUrl3"
#define mThumbUrl4                                       @"thumbnailUrl4"
#define mSearchCategory                                  @"searchKey"
#define mCategory                                        @"category"
#define mSubCategory                                     @"subCategory"
#define mCondition                                       @"condition"
#define mCurrency                                        @"currency"
#define mPrice                                           @"price"
#define mImageCount                                      @"imageCount"
#define mFirmOnPrice                                     @"negotiable"
#define mTaggedProductCoordinates                        @"tagProductCoordinates"
#define mTagProductStrings                               @"tagProduct"
#define mcloudinaryPublicId                              @"cloudinaryPublicId"
#define mcloudinaryPublicId1                             @"cloudinaryPublicId1"
#define mcloudinaryPublicId2                             @"cloudinaryPublicId2"
#define mcloudinaryPublicId3                             @"cloudinaryPublicId3"
#define mcloudinaryPublicId4                             @"cloudinaryPublicId4"
#define mLabel                              @"label"



#define favDBdocumentID         @"favDBdocumentID"
#define contacDBDocumentID      @"contacDBDocumentID"

#define kController                         @"ControllerType"

