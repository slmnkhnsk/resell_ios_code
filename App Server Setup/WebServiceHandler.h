
//  WebServiceHandler.h
//
//  Created by 3Embed on 03/12/15.
//  Copyright (c) 2014 3Embed. All rights reserved.


#import <Foundation/Foundation.h>
#import "WebServiceConstants.h"
#import "AFNetworking/AFNetworking.h"

/*
 @protocol WebServiceHandlerDelegate
 @abstract protocol to be iplemented by the calling class to get the response
*/
@protocol WebServiceHandlerDelegate <NSObject>

/*
 @method didFinishLoadingRequest
 @abstract Method called when web service request is complete
 @param requestType - Request Type for this request
 @param response - id Response of this request. Can be nil in case of an error.
 @param error - NSError - error object in case of any error. Nil in case of success.
*/
@required
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error;
@optional
-(void)internetIsNotAvailable:(RequestType )requsetType;
@end

/*
 @class WebServiceHandler
 @abstract Class to handle request and response of web services
*/

@interface WebServiceHandler : NSObject

/*
 @method getPhotosWithDelegate
 @abstract Method to get photos from the service
 @param delegate - calling class object to recieve the response. Response is delivered WebServiceHandlerDelegate protocol method didFinishLoadingRequest
 @result void
*/

#pragma mark - Login Service
+ (void) logId:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Sign Up Service
+ (void) newRegistration:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Log Device Service
+ (void) logUserDevice:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) logGuestUserDevice:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Email/Phone/UserNameCheck
+ (void) emailCheck:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) phoneNumberCheck:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) userNameCheck:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Genrate OTP Service
+ (void) generateOtp:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) faceBookContactSync:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) phoneContactSync:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) postImageOrVideo:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) follow:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) unFollow:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getHashTagSuggestion:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getUserNameSuggestion:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getFollowingList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getFollowersList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getCloudinaryCredintials:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getPostsInHOmeScreen:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) commentOnPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getMemberPosts:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;//
+ (void) getUserPosts:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getMemberPostsForGuest:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;//
+ (void) getProfileDetailsOfMemberForGuest:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;//
+ (void) getMemberFollowingList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getMemberFollowersList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypeSavingProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypeEditProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypepostsbasedonhashtag:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypepostsOfYou:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypeEmailCheckEditProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypePhoneNumberCheckEditProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypeupdatePhoneNumber:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypepostsbasedonLocation:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getExplorePosts:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) deletePost:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getPostDetails:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - search People
+ (void) getSearchPeople:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getSearchPeopleForGuestUser:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) editPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) followMultipleUsers:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getUserProfileDetails:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getUserSearchHistory:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) addTosearchHistory:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
#pragma mark - Search Posts

+ (void) getSearchForPosts:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)rateForSeller:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getCommentsOnPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) likeAPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) unlikeAPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getAllLikesOnPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) discoverPeople:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) followingActivities:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) ownActivities:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) deleteComment:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getPhotosOfMember:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)  setPrivateProfile:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) accceptFollowRequest:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) hideFromDiscovery:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getFollowRequestsForPrivateUsers:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getExplorePostsForGuest:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)resetPassword:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)singlePost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) feedback:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) Logout:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getCategories:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)getPostsByusers:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)getPostsForGuests:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)searchProductsByFilters:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypePostsLikedByUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypeMarkAsSold:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) markAsSold:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) markAsSelling:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypeMakeOffer:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
#pragma mark - Report

+(void)getReportReasonForUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+(void)getReportReasonForPost:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)sendreportPost:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)sendreportUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void)acceptedOffers:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)soldSomeWhere:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

// request method for webContents
+(void)requestForWebContents :(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/**
 Get member  profile details.
 */
+ (void) getProfileDetailsOfUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/**
Get profile Details of Member.
*/
+ (void) getProfileDetailsOfMember:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

// get notification count  unseenNotificationCount

+ (void) getUsernNotificationCount:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - PayPal Url

+ (void) savePayPalLink:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - verifications
+ (void) verifyWithFacebook:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) verifyWithGooglePlus:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
#pragma mark - Run Campaign
+ (void) userCampaign:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) runCampaign:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Chat History
+(void)getUserchatHistory:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+(void)deletedUserChat:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Insights
+(void)getInsightsOfProduct:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+(void)getMonthInsightsOfProduct:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+(void)getInsightsCityWise:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Promotions Plans
+(void)getPromotionPlans:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+(void)PurchasePromoPlans:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
@end
