
//  WebServiceHandler.m
//  Created by 3Embed on 03/12/15.
//  Copyright (c) 2014 3Embed. All rights reserved.

#import "WebServiceHandler.h"
#import "AFNetworking/AFNetworking.h"
#import "Reachability.h"
#import "APIModels.h"
#import "UserDetails.h"
#import "ProductDetails.h"

#define Token_Expired               @"unauthorized (401)"
#define request_Timeout             @"The request timed out"

@implementation WebServiceHandler

#pragma mark - Public Methods

#pragma mark - Login Service

+ (void) logId:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeLogin path:mRequestTypeLogin params:params onComplition:^(NSDictionary *response, NSError *error, BOOL isSuccess)
     {
         if(isSuccess)
         {
        UserDetails *user = [[UserDetails alloc]initWithDictionary:response];
         [delegate didFinishLoadingRequest:RequestTypeLogin withResponse:(UserDetails *)user error:nil];
         }
         else
         {
             
         }
     }];
//    [APIModels makePostRequest:RequestTypeLogin path:mRequestTypeLogin params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
    
}

#pragma mark - SignUp Service

+ (void) newRegistration:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    
    [APIModels makePostRequest:RequestTypenewRegister path:mRequestTypenewRegister params:params onComplition:^(NSDictionary *response, NSError *error, BOOL isSuccess)
     {
         NSDictionary *newRegisterDictionary = [[NSDictionary alloc]init];
         newRegisterDictionary = @{@"code" : flStrForObj(response[@"code"]),
                                   @"message" :flStrForObj(response[@"message"]),
                                   mUserName : flStrForObj(response[@"response"][mUserName]),
                                   mUserId :flStrForObj(response[@"response"][mUserId]),
                                   mauthToken : flStrForObj(response[@"response"][@"authToken"]),
                                   mEmail : flStrForObj(response[@"response"][mEmail]),
                                   mMqttId :flStrForObj(response[@"response"][mMqttId]),
                                   mProfileUrl : flStrForObj(response[@"response"][mProfileUrl])
                                   
                                   };
         UserDetails *user = [[UserDetails alloc]initWithDictionary:newRegisterDictionary];
     
         [delegate didFinishLoadingRequest:RequestTypenewRegister withResponse:(UserDetails *)user error:nil];
         }];
  //  [APIModels makePostRequest:RequestTypenewRegister path:mRequestTypenewRegister params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
   
}

#pragma mark - Product Details

#pragma mark - For user

+ (void)getPostsByusers:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    
    [APIModels makePostRequest:RequestTypeGetPostsByUsers path:mGetPostsByusers params:params onComplition:^(NSDictionary *response, NSError *error, BOOL isSuccess)
     {
         NSMutableDictionary *productResponse = [[NSMutableDictionary alloc]init];
         if(response[@"data"])
         {
         productResponse = response[@"data"][0];
         }
         [productResponse setValue:response[@"code"] forKey:@"code"];
         [productResponse setValue:response[@"message"] forKey:@"message"];
        ProductDetails *product = [[ProductDetails alloc]initWithDictionary:productResponse];
         product.responseArray = response[@"data"][0];
        [delegate didFinishLoadingRequest:RequestTypeGetPostsByUsers withResponse:(ProductDetails *)product error:nil];
     }];
    
//     [APIModels makePostRequest:RequestTypeGetPostsByUsers path:mGetPostsByusers params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
#pragma mark - For Guest

+ (void)getPostsForGuests:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    
    [APIModels makePostRequest:RequestTypeGetPostsForGuests path:mGetPostsForGuests params:params onComplition:^(NSDictionary *response, NSError *error, BOOL isSuccess)
     {
         NSMutableDictionary *productResponse = [[NSMutableDictionary alloc]init];
         if(response[@"data"])
         {
         productResponse = response[@"data"][0];
         }
         [productResponse setValue:response[@"code"] forKey:@"code"];
         [productResponse setValue:response[@"message"] forKey:@"message"];
         ProductDetails *product = [[ProductDetails alloc]initWithDictionary:productResponse];
         product.responseArray = response[@"data"][0];
         [delegate didFinishLoadingRequest:RequestTypeGetPostsByUsers withResponse:(ProductDetails *)product error:nil];
     }];
    
//    [APIModels makePostRequest:RequestTypeGetPostsForGuests path:mGetPostsForGuests params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}



#pragma mark - Edit Profile
+ (void) RequestTypeSavingProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
    [APIModels makePostRequest:RequestTypeSavingProfile path:mRequestTypeSavingProfile params:params onComplition:^(NSDictionary *response, NSError *error, BOOL isSuccess)
     {
         NSDictionary *newRegisterDictionary = [[NSDictionary alloc]init];
         newRegisterDictionary = @{@"code" : response[@"code"],
                                   @"message" :flStrForObj(response[@"message"]),
                                   mfullName : flStrForObj(response[@"data"][mfullName]),
                                   mUserName :flStrForObj(response[@"data"][mUserName] ),
                                   mUserId :flStrForObj(response[@"data"][mUserId]),
                                   mauthToken : flStrForObj(response[mauthToken]),
                                   mEmail : flStrForObj(response[@"data"][mEmail]),
                                   mMqttId :[[NSUserDefaults standardUserDefaults]objectForKey:mMqttId],
                                   mProfileUrl : flStrForObj(response[@"data"][mProfileUrl]),
                                   mwebsite : flStrForObj(response[@"data"][mwebsite]),
                                   mbio :flStrForObj(response[@"data"][mbio])
                                   
                                   
                                   };
         UserDetails *user = [[UserDetails alloc]initWithDictionary:newRegisterDictionary];
         
         [delegate didFinishLoadingRequest:RequestTypeSavingProfile withResponse:(UserDetails *)user error:nil];
     }];

//    [APIModels makePostRequest:RequestTypeSavingProfile path:mRequestTypeSavingProfile   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


#pragma mark - Get Products
+ (void) getExplorePosts:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    
//    [APIModels makePostRequest:RequestTypeGetExploreposts path:mgetPostsForUsers params:params onComplition:^(NSDictionary *response, NSError *error, BOOL isSuccess)
//     {
//         
//     }];
    
    [APIModels makePostRequest:RequestTypeGetExploreposts path:mgetPostsForUsers params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) getExplorePostsForGuest:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetExploreposts path:mgetPostsForGuests params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) emailCheck:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    
    [APIModels makePostRequest:RequestTypeEmailCheck path:mRequestTypeEmailCheck params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) phoneNumberCheck:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypePhoneNumberCheck path:mRequestTypePhoneNumberCheck params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) userNameCheck:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
     [APIModels makePostRequest:RequestTypeUserNameCheck path:mRequestTypeUserNameCheck params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) faceBookContactSync:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeLoginfaceBookContactSync path:mRequestTypeLoginfaceBookContactSync params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) phoneContactSync:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypePhoneContactSync path:mRequestTypePhoneContactSync params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) postImageOrVideo:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypePost path:mBusinessPostRequest params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

#pragma  mark - Follow/Unfollow

+ (void) follow:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeFollow path:[NSString stringWithFormat:@"%@%@",mRequestTypeFollow,params[muserNameTofollow]] params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getFollowRequestsForPrivateUsers:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetFollowRequestForAccept path:mRequestTypegetFollowRequestsForPrivateUsers params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) followMultipleUsers:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeFollowMultiple path:mRequestTypeFollowMultiple params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) unFollow:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeFollow path:[NSString stringWithFormat:@"%@%@",mRequestTypeUnFollow,params[muserNameToUnFollow]] params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) generateOtp:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeotpGeneration path:mRequestTypeotpGeneration params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


#pragma mark- Get Search API

+ (void) getHashTagSuggestion:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetHashTagsSuggestion path:mgetHashTagsSuggestion params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getUserNameSuggestion:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetTagFriendsSuggestion path:mgetUserNameSuggestion params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getSearchPeople:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetSearchPeople path:@"searchUsers" params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) getSearchPeopleForGuestUser:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetSearchPeople path:msearchPeopleForGuest params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) getSearchForPosts:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makeGetRequest:RequestTypeGetSearchForPosts path:[NSString stringWithFormat:@"%@%@",mgetSearchForPosts,params[mProductName]] params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getFollowersList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetFollowersList path:mgetFollowersList params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getFollowingList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetFollowingList path:mgetFollwingList params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getCloudinaryCredintials:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeCloudinaryCredintials path:mgetCloudinaryCredintials params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getPostsInHOmeScreen:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypegetPostsInHOmeScreen path:mgetPostsInHOmeScreen params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) commentOnPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypePostComment path:mPostComment params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

/**
 Get  profile details Of User.
 */
 + (void) getProfileDetailsOfUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
 [APIModels makePostRequest:RequestTypeProfileDetails path:mUserProfile params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
 }

/**
 Get profile Details of Member.
 */
+ (void) getProfileDetailsOfMember:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeProfileDetails path:mMemberProfile params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


/**
 Get profile Details of Member For Guest.
 */
+ (void) getProfileDetailsOfMemberForGuest:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeProfileDetails path:mMemberProfileForGuest params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


/**
 Get member selling posts.
 */


+ (void) getMemberPosts:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeGetPosts path:[NSString stringWithFormat:@"%@%@",mMemberSellingPosts,params[mmemberName]]   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


/**
   Get user selling posts.
 */
+ (void) getUserPosts:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeGetPosts path:mUserPosts   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];   //
}



/**
 Get member selling posts for guest.
 */

+ (void) getMemberPostsForGuest:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeGetPosts path:mMemberPostsForGuest  params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


+ (void)feedback:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequestFor:RequestTypeFeedBack path:mreportAProblem   params:params bsaeUrl:@"http://159.203.143.251:3000/api/user" delegate:delegate];
}

+ (void) getUserProfileDetails:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypemakeUserProfileDetails path:mMembergetUserProfileBasics   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


+ (void)resetPassword:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypemakeresetPassword path:mRequestTypresetPassword   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getMemberFollowersList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetMemberFollowersList path:mgetMemberFollowersList params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getMemberFollowingList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
   [APIModels makePostRequest:RequestTypeGetMemberFollowingList path:mgetMemberFollowingList params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


+ (void) deletePost:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makeDeleteRequest:RequestTypeDeletePost path:mdeletePost params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) RequestTypeEditProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeEditProfile path:mRequestTypeEditProfile   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) RequestTypepostsbasedonhashtag:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypebasedonhashtag path:mpostsbasedonhashtag   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) RequestTypepostsbasedonLocation:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypebasedonLoaction path:mpostsbasedonLoaction   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


+ (void) RequestTypepostsOfYou:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypePhotosOfYou path:mPhotosOfYou   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) RequestTypeEmailCheckEditProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
    [APIModels makePostRequest:RequestTypeEmailCheckInEditProfile path:mRequestTypeEmailCheckInEditProfile   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) RequestTypePhoneNumberCheckEditProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
    [APIModels makePostRequest:RequestTypePhoneNumberCheckEditProfile path:mRequestTypePhoneNumberCheckInEditProfile   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

#pragma mark -
#pragma mark- Update PhoneNumber

+ (void) RequestTypeupdatePhoneNumber:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
    [APIModels makePostRequest:RequestTypeupdatePhoneNumber path:mRequestTypePhoneNumberUpdate   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


+ (void) getCommentsOnPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetCommentsOnPost path:mGetCommentsOnPost params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) likeAPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeLikeAPost path:mLikeAPost params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) unlikeAPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeUnlikeAPost path:mUnlikeAPost params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) getAllLikesOnPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeGetAllLikesOnPost path:mGetAllLikesOnPost params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) discoverPeople:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeDiscoverPeople path:mdiscoverPeople params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

#pragma mark - Report

+(void)getReportReasonForPost:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makeGetRequest:RequestTypeGetReportReasonForPost path:mReportPostReason params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+(void)getReportReasonForUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makeGetRequest:RequestTypeGetReportReasonForPost path:mReportUserReason params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void)sendreportPost:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypereportPost path:mRequestTypereportPost params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void)sendreportUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypereportPost path:[NSString stringWithFormat:@"%@%@",mReportUser,params[mReportedUser]] params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) getUserSearchHistory:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
        [APIModels makePostRequest:RequestTypegetUserSearchHistory path:mgetUserSearchHistory params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) addTosearchHistory:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeAddToSearchHistory path:maddToSearch params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) followingActivities:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypefollowingActivity path:mRequestTypefollowingActivity params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) ownActivities:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeOwnActivity path:mRequestTypeOwnActivity params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


+ (void) deleteComment:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypedeleteComments path:mRequestTypedeleteComments params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) getPhotosOfMember:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypegetPhotosOfMember path:mRequestTypegetPhotosOfMember params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void)  Logout:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeLogout path:mlogout params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void)  setPrivateProfile:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypesetPrivateProfile path:mrequesttypesetPrivateProfile params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) accceptFollowRequest:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypeaccceptFollowRequest path:mRequestTypeaccceptFollowRequest params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) hideFromDiscovery:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
     [APIModels makePostRequest:RequestTypehideFromDiscovery path:mRequestTypehideFromDiscovery params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void)singlePost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeGetPostsDetailsForActivity path:mRequestTypegetPostsDetailsForActivity params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void)getPostDetails:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeGetPostDetails path:mGetPostDetails params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void)getCategories:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makeGetRequest:RequestTypeGetCategories path:mGetCategories params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}



#pragma mark-
#pragma mark - searchFilter

+ (void)searchProductsByFilters:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypeSearchProductsByFilters path:mFilterProducts params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) editPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
{
    [APIModels makePutRequest:RequestTypeEditPost path:mEditPostedProduct params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
+ (void) RequestTypePostsLikedByUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestTypePostsLikedByUser path:mPostsLikedByUser   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];  //
}

+ (void) logUserDevice:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestToLogUserDevice path:mLogUserDevice   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];  //
}

+ (void) logGuestUserDevice:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestToLogUserDevice path:mLogGuestUserDevice   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];  //
}
+ (void) RequestTypeMarkAsSold:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
      [APIModels makePostRequest:RequestTypemarkSold path:mRequestTypemarkSold   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void)rateForSeller:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
    [APIModels makePostRequest:RequestTyperateForSeller path:[NSString stringWithFormat:@"%@%@",mRequestTyperateForSeller,params[@"seller"]]   params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

#pragma mark -
#pragma mark - Sold/Selling


+ (void) markAsSold:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestToMarkAsSold path:mMarkAsSold params:params bsaeUrl:iPhoneBaseURL delegate:delegate];  //
}

+ (void)soldSomeWhere:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestTypesoldElseWhere path:mRequestTypesoldElseWhere params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) markAsSelling:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate {
    [APIModels makePostRequest:RequestToMarkAsSelling path:mMarkAsSelling  params:params bsaeUrl:iPhoneBaseURL delegate:delegate];  //
}

+ (void) RequestTypeMakeOffer:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
     [APIModels makePostRequest:RequestTypemakeOffer path:mRequestTypemakeOffer params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void)acceptedOffers:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
    [APIModels makePostRequest:RequestTypeacceptedOffers path:mRequestTypeacceptedOffers params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
// request for webContent
+(void)requestForWebContents:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makeGetRequest:RequestToGetWebContent path:mRequestToGetWebContent params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) getUsernNotificationCount:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
     [APIModels makeGetRequest:RequestTypeunseenNotificationCount path:mRequestTypeunseenNotificationCount params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


#pragma mark - verifications


+ (void) verifyWithFacebook:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestToVerifyWithFacebook path:mVerifyWithFacebook params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) verifyWithGooglePlus:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestToVerifyWithGooglePlus path:mVerifyWithGoogle params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}
#pragma mark - PayPal Url

+ (void) savePayPalLink:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePutRequest:RequestToSavePaypal path:mPayPalMe params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

#pragma mark - Campaign

+ (void) userCampaign:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestUserCampaign path:mUserCampaign params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+ (void) runCampaign:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makeGetRequest:RequestGetCampaign path:mRuncampaign params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}


+(void)getUserchatHistory:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
    
    
    [APIModels makePostRequest:RequestUserChatHistory path:[NSString stringWithFormat:@"%@/%@",mRequestToGetUserChatHistory,params[@"userId"]] params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
    
}

+(void)deletedUserChat:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
    
    [APIModels makePostRequest:RequestUserDeleteChat path:[NSString stringWithFormat:@"%@/%@",mRequestToDeleteUser,params[@"userId"]] params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
    
}

#pragma mark -
#pragma mark - Get Insights

+(void)getInsightsOfProduct:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestToGetInsights path:mInsights params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+(void)getMonthInsightsOfProduct:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestToGetMonthInsights path:mInsights params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+(void)getInsightsCityWise:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@",mInsights,params[mpostid],params[mCountryShortName]];
    [APIModels makePostRequest:RequestToGetCityInsights path:path params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

#pragma mark -
#pragma mark - Promotions

+(void)getPromotionPlans:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makeGetRequest:RequestForPromoPlans path:mPromoPlans params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

+(void)PurchasePromoPlans:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [APIModels makePostRequest:RequestForPurchasePlans path:[NSString stringWithFormat:@"%@%@/%@",mPurchasePromoPlan,params[mPromoPlanId],params[mpostid]] params:params bsaeUrl:iPhoneBaseURL delegate:delegate];
}

@end
