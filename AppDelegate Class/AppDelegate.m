//
//  AppDelegate.m
//
//  Created by Ajay Thakur on 15/09/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.


#import "AppDelegate.h"
#import "PGTabBar.h"
#import "UIImageView+WebCache.h"
#import "WebServiceHandler.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "TinderGenericUtility.h"
#import "WebServiceConstants.h"
#import "AFNetworkReachabilityManager.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "WebViewForDetailsVc.h"
#import "Helper.h"
#import "AdsCampaignView.h"
#import "iRate.h"
#import "Resell-Swift.h"

#define storyBoard [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil]


@import UserNotifications;
@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
@class MQTT;
@class MQTTChatManager;


@import GoogleMaps;
@interface AppDelegate ()<SDWebImageManagerDelegate,WebServiceHandlerDelegate,AdsCampaignViewDelegate >
{
    NSTimer *timer , *adsTimer ,*adsCampaignTimer;
    NSTimer *reconnectTimer;
}
@property MQTTChatManager *mqttChatManager;

@end

@implementation AppDelegate

+ (void)initialize
{
    [iRate sharedInstance].applicationBundleID = @"com.salman.resell";
    [iRate sharedInstance].appStoreID = 1216265952 ;
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    [iRate sharedInstance].daysUntilPrompt = 2;
    [iRate sharedInstance].usesUntilPrompt = 5;
}

+(AppDelegate*)sharedAppDelegate{
    return  (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAdsCampaignWithData:) name:mTriggerCampaign object:nil];
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    self.mqttChatManager = [MQTTChatManager sharedInstance];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] registerForRemoteNotifications];
                    });
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    [GMSServices provideAPIKey:mGoogleServiceKey];
    [FIRApp configure];
    [GADMobileAds configureWithApplicationID:mGoogleAdsAppId];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    UIViewController* rootController = [[UIStoryboard storyboardWithName:mMainStoryBoard bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];
    self.window.rootViewController = rootController;
    
    [self requestForCloundinaryDetails];
    [NSTimer scheduledTimerWithTimeInterval:30.0*60
                                     target:self
                                   selector:@selector(requestForCloundinaryDetails)
                                   userInfo:nil
                                    repeats:NO];
    
    [Fabric with:@[[Crashlytics class]]];
    NSString *token = [[FIRInstanceID instanceID] token];
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:mdeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self handlePushNotificationWithLaunchOption:launchOptions];
    [self setNavigationBar];
    [self addNoInternetConnectionView];
    _internetConnectionErrorView.hidden = YES;
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
        if (status == AFNetworkReachabilityStatusNotReachable) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNetworkAvailable"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            _internetConnectionErrorView.hidden = NO;
            [_window bringSubviewToFront:_internetConnectionErrorView];
            
            NSDictionary *dict = @{
                                   @"message":@"NO"
                                   };
            [[NSNotificationCenter defaultCenter] postNotificationName:@"observeNetworkStatus" object:nil userInfo:dict];
        }
        else {
            MQTT *mqttModel = [MQTT sharedInstance];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [mqttModel createConnection];
            });
            _internetConnectionErrorView.hidden = YES;
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isNetworkAvailable"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSDictionary *dict = @{
                                   @"message":@"YES"
                                   };
            [[NSNotificationCenter defaultCenter] postNotificationName:@"observeNetworkStatus" object:nil userInfo:dict];        }
    }];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    /*
        adsTimer = [NSTimer scheduledTimerWithTimeInterval:240 target: self
                                                  selector: @selector(callAfterSixtySeconds:) userInfo: nil repeats: YES];
        self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:mGoogleAdsUnitId];
        self.interstitial.delegate = self;
        GADRequest *request = [GADRequest request];
        [self.interstitial loadRequest:request];
    */
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    return YES;
}

#pragma mark -
#pragma mark - Timer Action

-(void)callAfterSixtySeconds:(NSTimer *)timer {
    if(self.interstitial.hasBeenUsed) {
        self.interstitial = [self createAndLoadInterstitial];
    }
    [self.interstitial presentFromRootViewController:self.window.rootViewController];
}

#pragma mark -
#pragma mark - Google Ads Delegate Method

- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:mGoogleAdsUnitId];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial
{
    self.interstitial = [self createAndLoadInterstitial];
}

/// Tells the delegate an ad request failed.
- (void)interstitial:(GADInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]){
        self.interstitial = [self createAndLoadInterstitial];
    }
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}


-(void)requestForCloundinaryDetails{
    [WebServiceHandler getCloudinaryCredintials:@{@"":@""} andDelegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation] ||
    [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:YES];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[FIRMessaging messaging] disconnect];
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:YES];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:NO];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:NO];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pushToken"];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeUnknown];
    NSLog(@"FIR device token :%@",deviceToken);
    NSString *token = [[FIRInstanceID instanceID] token];
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"pushToken"];
}

-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions{
    NSDictionary *receivedDataFromPush = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    NSData *data = [receivedDataFromPush[@"body"] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonResponse = [[NSDictionary alloc]init];
    if(data) {
        jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
    }
    if([jsonResponse[@"type"]integerValue] == 73) {
        [[NSUserDefaults standardUserDefaults] setObject:jsonResponse forKey:mAdsCampaignKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return ;
    } else if (jsonResponse[@"receiverID"]) {
        [self openChatController:jsonResponse];
    }
    else if(receivedDataFromPush) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"openActivity"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    
    NSData *data = [notification.request.content.userInfo[@"body"] dataUsingEncoding:NSUTF8StringEncoding];
    if(data) {
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
        if ([jsonResponse[@"type"]integerValue] == 73) {
            [self openCustomNotification:jsonResponse];
            // this is for other notifications
        } else if (jsonResponse[@"receiverID"]) {
            //this is for chat
            UITabBarController *tabVC = (UITabBarController *)self.window.rootViewController;
            if(tabVC.tabBar.selectedItem.tag != 4)
            {
              completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge);
            }
            else
            {
              completionHandler(UNNotificationPresentationOptionSound);
            }
        }
        else {
//            UIApplication *app = (UIApplication *)[UIApplication sharedApplication];
//            if (app.applicationState == UIApplicationStateBackground  | app.applicationState == UIApplicationStateInactive) {
                completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge);
//            }
            }
        }
    }

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    [self handleRemoteNotification:[UIApplication sharedApplication] userInfo:response.notification.request.content.userInfo];
}

-(void) handleRemoteNotification:(UIApplication *) application userInfo:(NSDictionary *) remoteNotif {
    if (remoteNotif[@"body"]) {
        NSData *data = [remoteNotif[@"body"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
        if ([jsonResponse[@"type"]integerValue] == 73) {
            [[NSNotificationCenter defaultCenter] postNotificationName:mShowAdsCampaign object:jsonResponse];
        } else if (jsonResponse[@"receiverID"]) {
            [self openChatController:jsonResponse];
        }
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openActivityScreen" object:nil];
        }
    }
}

#pragma mark - Popup custom notification
-(void)openAdsCampaignWithData :(NSNotification *)noti
{
    [self openCustomNotification:noti.object];
}

-(void)openCustomNotification:(NSDictionary *)dict
{
    if(![[Helper userToken] isEqualToString:mGuestToken]) {
        AdsCampaignView *customView = [[AdsCampaignView alloc]init];
        customView.titleForAdsCampaign.text = flStrForObj(dict[@"title"]);
        customView.delegate = self;
        customView.messageForAdsCampaign.text = flStrForObj(dict[@"message"]);
        customView.url =  flStrForObj(dict[@"url"]);
        customView.userId = flStrForObj(dict[@"userId"]);
        customView.campaignId = flStrForObj(dict[@"campaignId"]);
        [customView.imageForAdsCampaign sd_setImageWithURL:[NSURL URLWithString:flStrForObj(dict[@"imageUrl"])] placeholderImage:[UIImage imageNamed:@"welcome_Image"]];
        UIWindow *windowAlert = [[UIApplication sharedApplication]keyWindow];
        [customView showOnWindow:windowAlert];
    }
}

-(void)openChatController:(NSDictionary *)chatData {
    // redirecting it to the next controller with the chat object data.
    // check for empty object.

    UIViewController *tabController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    if ([tabController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tbControler = (UITabBarController *)tabController;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[tbControler viewControllers] objectAtIndex:3] popToRootViewControllerAnimated: NO];
            [tbControler setSelectedIndex:3];
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OfferInitiated" object:nil userInfo:@{@"chatObj":chatData}];
        });
    }
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    [[NSUserDefaults standardUserDefaults] setObject:refreshedToken forKey:mdeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self connectToFcm];
}

- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

#pragma mark - push sound notification
-(void)playNotificationSound
{
    // play sound
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef = CFBundleCopyResourceURL(mainBundle, CFSTR("supcalling"), CFSTR("mp3"), NULL);
    CFRelease(soundFileURLRef);
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:NO];
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearDisk];
}

/*-------------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*------------------------------------------------------*/

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    if (error) {
        return ;
    }
    //storing the response from server to dictonary.
    NSDictionary *responseDict = (NSDictionary*)response;
    //checking the request type and handling respective response code.
    if (requestType == RequestTypeCloudinaryCredintials ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                // success response.
                [[NSUserDefaults standardUserDefaults] setObject:responseDict forKey:cloudinartyDetails];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
                break;
                
            default:
                break;
        }
    }
}

- (void)setNavigationBar
{
    if (@available(iOS 11.0, *)) {
        
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
        [[UITableView appearance] setContentInsetAdjustmentBehavior: UIScrollViewContentInsetAdjustmentNever] ;
    } else {
        // Fallback on earlier versions
        
    }
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setShadowImage:nil];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           mBaseColor, NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:RobotoMedium size:17.0], NSFontAttributeName, nil]];
    
}

#pragma mark - adding internet connection error view

- (void)addNoInternetConnectionView
{
    _internetConnectionErrorView = [[UIView alloc] initWithFrame:CGRectMake(0, 64,[UIScreen mainScreen].bounds.size.width, 20)];
    UILabel *internetConectionErrorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
    internetConectionErrorLabel.text = @"No internet connection";
    //    [Helper setToLabel:internetConectionErrorLabel Text:@"No internet connection" WithFont:LATO_BOLD FSize:14.0 Color:[UIColor whiteColor]];
    internetConectionErrorLabel.textAlignment = NSTextAlignmentCenter;
    [_internetConnectionErrorView addSubview:internetConectionErrorLabel];
    _internetConnectionErrorView.backgroundColor = [UIColor redColor];
    [self.window addSubview:_internetConnectionErrorView];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

-(void)knowmoreButtonClickedForUrl:(NSString *)url {
    NSURL *ur = [NSURL URLWithString:url];
    if (![[UIApplication sharedApplication] openURL:ur]) {
        UIAlertController *controller = [CommonMethods showAlertWithTitle:@"" message:@"It is not a valid url" actionTitle:@"OK"];
        [self.window.rootViewController presentViewController:controller animated:NO completion:nil];
    }
}

@end
