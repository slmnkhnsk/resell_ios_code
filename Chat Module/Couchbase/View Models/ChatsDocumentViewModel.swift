//
//  ChatsDocumentViewModel.Swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 01/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class ChatsDocumentViewModel: NSObject {
    
    
    public let couchbase: Couchbase
    
    init(couchbase: Couchbase) {
        self.couchbase = couchbase
    }
    
    var selfID :String? {
        return self.getUserID()
    }
    
    let chatDocID = ""
    
    /*
     "chatID":"",
     "initiated":"", // int
     "offerType":"",
     "productId":"",
     "productImage":"",
     "profilePic":"",
     "senderId":"",
     "recipientId":"",
     "productName":"",
     */
    
    /// Used for creating a document when there is no doc Available
    ///
    /// - Returns: string of chat doc ID.
    func createChatDoc(withRecieverName recieverName : String?,secretID : String?,message : [String : Any]?, recieverImage : String?, selfDocID: String?, selfUID:String?, withProductDetails productDetails: ProductDetails?) -> String? {
        
        var productName = ""
        var productPrice = ""
        var currency = ""
        var productImage = ""
        var profilePic = ""
        var memberMqttID = ""
        
        if let pName = productDetails?.productName, let pPrice = productDetails?.price, let currncy = productDetails?.currency, let pImage = productDetails?.mainUrl, let pPic = productDetails?.profilePicUrl, let memberID = productDetails?.memberMqttId {
            productPrice = pPrice
            productName = pName
            currency = currncy
            productImage = pImage
            profilePic = pPic
            memberMqttID = memberID
        }
        
        var pLat = "", pLong = "", pmemberName = "", pPlace = "", pReceiverImage = ""
        var pNegotiable = false
        
        if let lat = productDetails?.latitude, let long = productDetails?.longitude, let memberName = productDetails?.membername, let negotiable = productDetails?.negotiable, let place = productDetails?.place, let receiverImage = productDetails?.memberProfilePicUrl {
            pLat = "\(lat)"
            pLong = "\(long)"
            pmemberName = memberName
            pNegotiable = negotiable
            pPlace = place
            pReceiverImage = receiverImage
        }
        
        var offerType = "1" //have to add the check for offers accpted or not.
        if let selfDocID = selfDocID, let secretID = secretID, let selfUID = selfUID {
            if let message = message {
                if ((message["messageType"] as? String) != "15") {
                    // here comes the key for isSold and depending on that we have to decide that it is going to be accepted or not.
                    offerType = "2"
                } else {
                    offerType = message["offerType"] as! String
                }
                
                var isPorductSold = false
                if let productSold = message["productSold"] as? Bool {
                    isPorductSold = productSold
                } else if let productSold = message["productSold"] as? Int {
                    isPorductSold = (productSold as NSNumber).boolValue
                }
                
                var wasInvited = false
                if let userWasInvited = message["wasInvited"] as? Bool {
                    wasInvited = userWasInvited
                } else if let userWasInvited = message["wasInvited"] as? Int {
                    wasInvited = (userWasInvited as NSNumber).boolValue
                }
                
                var image = ""
                if let uImage = message["userImage"] as? String {
                    image = uImage
                } else if let uImage = message["profilePic"] as? String {
                    image = uImage
                }
                var chatIdnty = ""
                if let chatID =  message["chatId"] as? String {
                    chatIdnty = chatID
                } else {
                    chatIdnty = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
                }
                
                if let pName = message["productName"] as? String, let pImage = message["productImage"] as? String, let pPic = message["profilePic"] as? String {
                    productName = pName
                    productImage = pImage
                    profilePic = pPic
                }
                
                
                var senderId = ""
                var recipientId = ""
                if let senderid = message["senderId"] as? String, let recipientID = message["recipientId"] as? String, let selfID = Helper.getMQTTID() {
                    senderId = senderid
                    recipientId = recipientID
                    if selfID == senderid {
                        //                        if message["initiated"] as? Int == 1 {
                        //                            senderId = recipientID
                        //                        } else {
                        //                            recipientId = senderid        // new added code.
                        //                        }
                    }
                }
                var receiverName = ""
                if let userName = Helper.userName(), let messengerName = message["name"] as? String {
                    receiverName = messengerName
                    if message["initiated"] as? Int == 1 {
                        if receiverName != userName{
                            receiverName = messengerName
                        }
                    }
                }
                
                var totalUnread = 0
                var newMessage = false
                if let unreadCount =  message["totalUnread"] as? Int {
                    totalUnread = unreadCount
                } else if let unreadCount =  message["totalUnread"] as? String {
                    if let count = Int(unreadCount) {
                        totalUnread = count
                    }
                }
                if totalUnread>0 {
                    newMessage = true
                }
                
                if let initiated = message["initiated"] as? Int, let productID = message["productID"] as? String, let msg = message["message"] as? String {
                    guard let chatDocID = self.createChatDocument(withMessageArray: [message], hasNewMessage: newMessage, newMessage: msg, newMessageTime: "",newMessageDateInString: "", newMessageCount: totalUnread, lastMessageDate: "", recieverUIDArray: [], recieverDocIDArray: [], recieverName: receiverName, recieverImage: image, selfDocID: selfDocID, selfUID: selfUID, wasInvited: wasInvited, secretID: secretID, dTime: -1, productIsSold: isPorductSold, isSecretInviteVisibile: false, chatID: chatIdnty ,initiated: initiated, offerType: offerType, productId: productID, productImage: productImage, profilePic: profilePic, senderId: senderId, recipientId: recipientId, productName: productName, acceptedPrice: "", price: productPrice, currency: currency, lat: "", long: "", memberName: receiverName, place: "", negotiable: false) else { return nil }
                    return chatDocID
                }
                else {
                    guard let chatDocID = self.createChatDocument(withMessageArray: [message], hasNewMessage: newMessage, newMessage: "", newMessageTime: "",newMessageDateInString: "", newMessageCount: totalUnread, lastMessageDate: "", recieverUIDArray: [], recieverDocIDArray: [], recieverName: receiverName, recieverImage: image, selfDocID: selfDocID, selfUID: selfUID, wasInvited: wasInvited, secretID: secretID, dTime: -1, productIsSold: isPorductSold, isSecretInviteVisibile: false, chatID: chatIdnty, initiated: 0, offerType: offerType, productId: secretID, productImage: productImage, profilePic: profilePic, senderId: memberMqttID, recipientId: selfUID, productName: productName, acceptedPrice: "", price: productPrice, currency: currency, lat: "", long: "", memberName: receiverName, place: "", negotiable: false) else { return nil }
                    return chatDocID
                }
            }
            else { // this will execute when there is no chat initiated by server.
                let chatIdnty = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
                guard let chatDocID = self.createChatDocument(withMessageArray: [],hasNewMessage: false,newMessage: "",newMessageTime: "",newMessageDateInString: "",newMessageCount: 0,lastMessageDate: "",recieverUIDArray: [],recieverDocIDArray: [],recieverName: "",recieverImage: pReceiverImage,selfDocID: selfDocID,selfUID: selfUID,wasInvited: false,secretID: secretID,dTime: -1,productIsSold: false,isSecretInviteVisibile: false,chatID: chatIdnty,initiated: 1,offerType: offerType,productId: secretID,productImage: productImage,profilePic: profilePic,senderId: selfUID,recipientId: memberMqttID ,productName: productName,acceptedPrice: "",price: productPrice,currency: currency, lat: pLat, long: pLong, memberName: pmemberName, place: pPlace, negotiable: pNegotiable) else { return nil }
                return chatDocID
            }
        }
        return nil
    }
    
    
    fileprivate func getUserID() -> String? {
        guard let mqttID = Helper.getMQTTID() else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    /// This method will create individual Chats Document where all the deatails are going to be stored related to the message and chat.
    ///
    /// - Parameters:
    ///   - messageArray: All messages are going to be stored here with Any type
    ///   - hasNewMessage: Bool flag which will state that is there any new message available or not.
    ///   - newMessage: This is a string which will store the last new message.
    ///   - newMessageTime: String for time when the last message was recieved.
    ///   - newMessageDateInString: String for date when last mesage was recieved eg - Yesterday, today or local format when the message is recieved.
    ///   - newMessageCount: Integer format of unread messages.
    ///   - lastMessageDate: String format of time in GMT when the last message is recieved.
    ///   - recieverUIDArray: Array of string with the list of users IDs.
    ///   - recieverDocIDArray: Array of string with all the Documents as respect to the UserID.
    ///   - recieverName: Username of the receiver in String.
    ///   - recieverImage: receiving users image url in string.
    ///   - selfDocID: Senders own document ID for the current chat. Used for showing the last message.
    ///   - selfUID: senders UID for the current chat.
    ///   - wasInvited: Boolean flag for Whether invited or has invited someone else for secret chat.
    ///   - secretID: String ID for idnetifing each secret chat individually.
    ///   - dTime: destruction time in string for current message. By default it will be -1.
    ///   - isSecretInviteVisibile: Boolean flag for showing the pop up for secret invite.
    /// - Returns: Object of CBLDocument / Document
    
    func createChatDocument(withMessageArray messageArray: [Any], hasNewMessage : Bool, newMessage : String, newMessageTime : String, newMessageDateInString : String, newMessageCount : Int, lastMessageDate : String, recieverUIDArray : [String], recieverDocIDArray : [String], recieverName : String,recieverImage : String, selfDocID : String, selfUID : String, wasInvited : Bool, secretID :String, dTime : Float,productIsSold:Bool, isSecretInviteVisibile : Bool, chatID:  String,initiated : Int, offerType : String, productId : String, productImage : String, profilePic : String, senderId : String, recipientId : String, productName : String, acceptedPrice : String, price : String, currency : String, lat : String, long : String, memberName : String, place : String, negotiable : Bool ) -> String? {
        let params = ["messageArray":messageArray,
                      "hasNewMessage":hasNewMessage,
                      "newMessage":newMessage,
                      "newMessageTime":newMessageTime,
                      "newMessageDateInString":newMessageDateInString,
                      "newMessageCount":newMessageCount,
                      "lastMessageDate":lastMessageDate,
                      "receiver_uid_array":recieverUIDArray,
                      "receiver_docid_array":recieverDocIDArray,
                      "receiverName":recieverName,
                      "receiverImage":recieverImage,
                      "selfDocId":selfDocID,
                      "selfUid":selfUID,
                      "wasInvited":wasInvited,
                      "chatID":chatID,
                      "initiated":initiated,
                      "offerType":offerType,
                      "productId":productId,
                      "productImage":productImage,
                      "profilePic":profilePic,
                      "senderId":senderId,
                      "recipientId": recipientId,
                      "productName":productName,
                      "secretId":secretID,
                      "dTime":dTime,
                      "productIsSold":productIsSold,
                      "isAccepted":false,
                      "membername":memberName,
                      "latitude":lat,
                      "longitude":long,
                      "currency":currency,
                      "place":place,
                      "negotiable":negotiable as Any,
                      "price":price as Any,
                      "acceptedPrice":acceptedPrice,
                      "secretInviteVisibility":isSecretInviteVisibile] as [String:Any]
        
        if chatID.count == 0 {
            return nil
        }
        let chatDocID = couchbase.createDocument(withProperties: params)
        return chatDocID
    }
    
    /// Used for updating chat whenever a message will come
    ///
    /// - Parameters:
    ///   - data: Message data which will contains all the data
    ///   - msgObject: current message which is going to be added to the document.
    ///   - docID: current doc id where you wanted to update the data
    func updateChatData(withData data: [String: Any], msgObject : Any,inDocID docID : String, updateChatMsgs isUpdatingChat: Bool) {
        var chatData = data
        var msgArray:[Any] = chatData["messageArray"] as! [Any]
        guard let msgObject = msgObject as? [String : Any] else { return }
        guard let recieverName = msgObject["name"] as? String,
            let newMessageTime = msgObject["timestamp"] as? String,
            let newMessage = msgObject["payload"] as? String,
            let newMessageCount = data["newMessageCount"] as? Int, //
            let lastMessageDate = msgObject["timestamp"] as? String else { return }
        
        if let userName = Helper.userName(),let senderName = data["membername"] as? String {
            if userName != senderName {
                chatData["receiverName"] = senderName
            } else if userName != recieverName {
                chatData["receiverName"] = recieverName
            }
        }
        if let userImage = msgObject["userImage"] as? String {
            if (!userImage.isEmpty)
            {
                chatData["profilePic"] = userImage
                chatData["receiverImage"] = userImage
            }
        }
        
        chatData["newMessageTime"] = DateExtension().getDateString(fromTimeStamp: newMessageTime)
        
        if isUpdatingChat {
            if !msgArray.containsObject(object: msgObject) {
                let msgCount = newMessageCount+1
                msgArray.append(msgObject)
                chatData["newMessageCount"] = msgCount
                if msgCount>0 {
                    chatData["hasNewMessage"] = true
                } else {
                    chatData["hasNewMessage"] = false
                }
            }
        } else {
            chatData["initiated"] = true
        }
        chatData["newMessage"] = newMessage
        chatData["lastMessageDate"] = lastMessageDate
        chatData["selfDocId"] = docID
        chatData["newMessageDateInString"] =  DateExtension().getDateString(fromTimeStamp: newMessageTime)
        chatData["messageArray"] = msgArray
        couchbase.updateData(data: chatData, toDocID: docID)
    }
    
    /// Used for updating chat whenever a message will come
    ///
    /// - Parameters:
    ///   - data: Message data which will contains all the data
    ///   - msgObject: current message which is going to be added to the document.
    ///   - docID: current doc id where you wanted to update the data
    func updateChatDataFromChats(withData data: [String: Any], msgObject : Any,inDocID docID : String, isComingInitially : Bool) {
        var chatData = data
        guard let msgObject = msgObject as? [String : Any] else { return }
        guard let newMessageTime = msgObject["timestamp"] as? String,
            let newMessage = msgObject["payload"] as? String,
            let name = data["receiverName"] as? String,
            let lastMessageDate = msgObject["timestamp"] as? String else { return }
        
        if let userName = Helper.userName(),let senderName = data["membername"] as? String {
            if userName != senderName {
                chatData["receiverName"] = senderName
            } else if userName != name {
                chatData["receiverName"] = name
            }
            if let userImage = msgObject["userImage"] as? String {
                if(!userImage.isEmpty)
                {
                    chatData["receiverImage"] = userImage
                }
            }
        }
        
        var msgCount = 0
        if let unreadMsgCount = chatData["newMessageCount"] as? Int {
            msgCount = unreadMsgCount
            if msgCount > 0 {
                chatData["hasNewMessage"] = true
            }
        }
        
        if let offerType = msgObject["offerType"] as? String {
            if (chatData["offerType"] as? String) != "2" {
                chatData["offerType"] = offerType
            }
            if (offerType == "1"||offerType == "3") { // 1 - Offer created 2 - Offer Accepted 3 - Counter Offer
                var msgArray:[Any] = []
                msgArray = chatData["messageArray"] as! [Any]
                let msg = msgObject as Any
                if !msgArray.containsObject(object: msg) {
                    msgArray.append(msg)
                    if isComingInitially {
                        if let msgcont = chatData["newMessageCount"] as? Int {
                            msgCount = msgcont+1
                            chatData["hasNewMessage"] = true as Any
                        } else if let msgcont = chatData["newMessageCount"] as? String {
                            if let count = Int(msgcont) {
                                msgCount = count+1
                                chatData["hasNewMessage"] = true as Any
                            }
                        }
                    } else {
                        chatData["hasNewMessage"] = false
                    }
                }
                chatData["messageArray"] = msgArray
            } else {
                chatData["offerType"] = "2"   // offer Status is accepted .
                if (msgObject["isSelf"] as? Bool) == false {
                    let mesageObj = Message(forData: msgObject, withDocID: docID, isSelfMessage: false, andMessageobj: msgObject, offerType : "2", isMediaIncluded: false, includedMedia: nil, withImageURL : nil)
                    let messageVModelObj = MessageViewModal(withMessageObject: mesageObj)
                    if let price = msgObject["message"] as? String, let productID =  msgObject["productID"] as? String{
                        var msgArray = chatData["messageArray"] as! [Any]
                        if(msgArray.count > 0){
                            let lastMsg = msgArray[msgArray.count - 1] as! [String:Any]
                            if(lastMsg["offerType"] as? String != "4"){
                                let thankuMsgObj = messageVModelObj.getMessageDictionaryForAcceptedOffer(withProductID: productID, andPrice: price)
                                msgArray.append(thankuMsgObj)
                                chatData["messageArray"] = msgArray
                                if let msgcont = chatData["newMessageCount"] as? Int {
                                    msgCount = msgcont+1
                                    chatData["hasNewMessage"] = true as Any
                                } else if let msgcont = chatData["newMessageCount"] as? String {
                                    if let count = Int(msgcont) {
                                        msgCount = count+1
                                        chatData["hasNewMessage"] = true as Any
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            chatData["offerType"] = "2"
        }
        chatData["selfDocId"] = docID
        chatData["newMessageDateInString"] =  DateExtension().getDateString(fromTimeStamp: newMessageTime)
        chatData["newMessage"] = newMessage
        chatData["newMessageCount"] = msgCount
        chatData["lastMessageDate"] = lastMessageDate
        
        if let productSold = msgObject["productSold"] as? Bool {
            chatData["productSold"] = productSold
        }
        
        if let productName = msgObject["productName"] as? String, let productImage = msgObject["productImage"] as? String {
            chatData["productName"] = productName
            if productImage != "defaultUrl" {
                chatData["productImage"] = productImage
            }
        }
        
        chatData["newMessageTime"] = DateExtension().getDateString(fromTimeStamp: newMessageTime)
        couchbase.updateData(data: chatData, toDocID: docID)
        if ((msgObject["offerType"] as? String) == "2" || (msgObject["offerType"] as? String) == "3") {
            let name = NSNotification.Name(rawValue: "MessageNotification" + selfID!)
            NotificationCenter.default.post(name: name, object: self, userInfo: ["message": msgObject, "status":"0"])
        }
        let notificationName = NSNotification.Name(rawValue: "UpdateProductDetailsNotification")
        NotificationCenter.default.post(name: notificationName, object: self, userInfo: nil)
    }
    
    
    /// used for sending the delivered status after getting the message.
    ///
    /// - Parameter data: message data
    func sendMessageDeliveredStatus(withData data : [String : Any]) {
        guard let params = self.getMessageObjectForUpdatingStatus(withData: data, andStatus: "2") as? [String:Any] else { return }
        let ackRecieverID = params["from"] as! String
        MQTTChatManager.sharedInstance.sendAcknowledgment(toChannel: "\(AppConstants.MQTT.acknowledgementTopicName)\(ackRecieverID)", withMessage: params, withQOS: .atMostOnce)
    }
    
    
    /// For updating the document after receiving the message.
    ///
    /// - Parameters:
    ///   - data: message data
    ///   - topic: topic from the data is received.
    ///   - isComingInitially: if the data is coming initially
    func updateDocumentForMessageReceived(withMessageData data: [String : Any], atTopic topic : String, isComingInitially : Bool)  {
        var messageObj = data
        messageObj["deliveryStatus"] = "1"
        messageObj["isSelf"] = false
        //        let couchbaseManager = CouchbaseManager.sharedInstance
        //        couchbaseManager.showNotificationAlert(withMsg: data)
        let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbase)
        individualChatDocVMObject.updateIndividualChatDoc(withMsgObj: messageObj, toDocID: nil,isUpdatingChats : false, isComingInitially: isComingInitially)
        self.sendMessageDeliveredStatus(withData: messageObj)
        let name = NSNotification.Name(rawValue: "MessageNotification" + selfID!)
        NotificationCenter.default.post(name: name, object: self, userInfo: ["message": messageObj, "status":"0"])
    }
    
    /// Update document for chats getting from MQTT server.
    ///
    /// - Parameters:
    ///   - data: data fetched from MQTT server
    ///   - topic: current topic name.
    func updateDocumentForChatRecieved(withChatData data : [String:Any], atTopic topic : String, isComingInitially : Bool) {
        let name = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        self.updateChatsToCouchbase(withData: data, isComingInitially: isComingInitially)
        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
    }
    
    
    /// For deleting the document by using the passing properties.
    ///
    /// - Parameters:
    ///   - chatDocID: ChatDocID you want to delete from
    ///   - recieverID: receiver ID
    ///   - productID: ProductID/SecretID
    func deleteDocument(forChatDoc chatDocID: String?, withReciever recieverID : String?, AndProductID  productID: String?) {
        
    }
    
    /// For updating the chats to couchbase
    ///
    /// - Parameters:
    ///   - data: passing the data with the message.
    ///   - isComingInitially: if the data is comming initially.
    fileprivate func updateChatsToCouchbase(withData data:[String :Any], isComingInitially : Bool) {
        guard let chats = data["chats"] as? [Any] else { return }
        for chatObj in chats {
            guard let chatObj = chatObj as? [String :Any] else { return }
            guard let msgObj = getMessageObj(withData: chatObj) else { return }
            let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbase)
            individualChatDocVMObject.updateIndividualChatDoc(withMsgObj: msgObj, toDocID: nil, isUpdatingChats: true, isComingInitially: isComingInitially)
        }
    }
    
    
    /// For updating the document initially when chat is received.
    ///
    /// - Parameters:
    ///   - data: Passed data with the message.
    ///   - topic: topic from the chat is received.
    ///   - isComingInitially: if the data is comming initially.
    func updateDocumentForInitiatedChatReceived(withChatData data : [String:Any], atTopic topic : String, isComingInitially : Bool) {
        let name = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        guard let msgObj = getInitiatedChatObj(withData: data) else { return }
        let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbase)
        individualChatDocVMObject.updateIndividualChatDoc(withMsgObj: msgObj, toDocID: nil, isUpdatingChats: true, isComingInitially: isComingInitially)
        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
    }
    
    /// When receiving the initiated chat object.
    ///
    /// - Parameter data: message data
    /// - Returns: Modified message data to store in the couchbase
    fileprivate func getInitiatedChatObj(withData data : [String :Any]) -> [String : Any]?{
        guard let userID = self.getUserID() else { return nil }
        
        var params = [String : Any]()
        
        params["to"] = data["to"]
        params["from"] = data["from"]
        if (userID == (data["to"] as? String)) {
            params["isSelf"] = false
            if ((data["offerType"] as? String) == "1"){
                params["from"] = data["from"]
                params["to"] = data["to"]
                params["initiated"] = 0
            }
            params["recipientId"] = data["from"]
        } else {
            params["isSelf"] = true
            if ((data["offerType"] as? String) == "1"){
                params["from"] = data["to"]
                params["to"] = data["from"]
                params["initiated"] = 1
            }
            params["recipientId"] = data["to"]
        }
        params["message"] = data["payload"]
        params["messageType"] = data["type"]
        params["deliveryStatus"] = data["status"]
        params["id"] = data["id"]
        params["productID"] = data["productId"]
        params["downloadStatus"] = "0"
        params["dataSize"] = "1"
        params["chatId"] = data["id"]
        params["offerType"] = data["offerType"]
        params["productImage"] = data["productImage"]
        params["productName"] = data["productName"]
        params["profilePic"] = data["userImage"]
        params["receiverId"] = data["to"]
        params["senderId"] = data["from"]
        params["totalUnread"] = "1"
        params["userName"] = data["name"]
        params["name"] = data["name"]
        params["newMessageCount"] = "1"
        params["payload"] = data["payload"]
        params["userImage"] = data["profilePic"]
        
        if let timeStamp = data["timestamp"] as? intmax_t {
            params["timestamp"] = "\(timeStamp)"
        }
        else if let timeStamp = data["timestamp"] as? String {
            params["timestamp"] = timeStamp
        }
        return params
    }
    
    
    /// Used for getting the message Object for storing into database
    ///
    /// - Parameter data: message object dictionary
    /// - Returns: modified message objcet dictionary
    fileprivate func getMessageObj(withData data : [String :Any]) -> [String : Any]?{
        guard let userID = self.getUserID() else { return nil }
        if let isInitiated = data["initiated"] as? Bool {
            var params = [String : Any]()
            params["message"] = data["payload"]
            params["messageType"] = data["messageType"]
            params["isSelf"] = isInitiated
            params["from"] = data["senderId"]
            params["to"] = data["receiverId"]
            params["deliveryStatus"] = data["status"]
            params["id"] = data["messageId"]
            params["productID"] = data["productId"]
            params["downloadStatus"] = "1"
            params["dataSize"] = "0"
            params["chatId"] = data["chatId"]
            params["initiated"] = data["initiated"]
            params["offerType"] = data["offerType"]
            params["productImage"] = data["productImage"]
            params["productName"] = data["productName"]
            params["profilePic"] = data["profilePic"]
            params["receiverId"] = data["receiverId"]
            params["recipientId"] = data["recipientId"]
            params["senderId"] = data["senderId"]
            params["totalUnread"] = data["totalUnread"]
            params["userName"] = data["userName"]
            params["name"] = data["userName"]
            params["userImage"] = data["profilePic"]
            params["newMessageCount"] = data["totalUnread"]
            params["payload"] = data["payload"]
            params["productSold"] = data["productSold"]
            if let timeStamp = data["timestamp"] as? intmax_t {
                params["timestamp"] = "\(timeStamp)"
            }
            else if let timeStamp = data["timestamp"] as? String {
                params["timestamp"] = timeStamp
            }
            return params
        }
        return nil
    }
    
    
    /// For fetching the chat doc ID from message object.
    ///
    /// - Parameter message: received message.
    /// - Returns: Document ID
    func getChatDocID(fromMessage message :[String : Any]) -> String?{
        let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbase)
        let individualChatDocID = individualChatDocVMObject.getChatsDocID(fromMessage: message)
        return individualChatDocID
    }
    
    
    /// Updating message to the chat doc by using the data passed.
    ///
    /// - Parameters:
    ///   - msgObject: message to update
    ///   - docID: Document ID to update the message object
    func updateChatDoc(withMsgObj msgObject : Any,toDocID docID : String) {
        var chatData = couchbase.getData(fromDocID: docID)!
        var msgArray:[Any] = chatData["messageArray"] as! [Any]
        msgArray.append(msgObject)
        chatData["messageArray"] = msgArray
        self.couchbase.updateData(data: chatData, toDocID: docID)
    }
    
    
    /// updating Document For Message Delivered with message data
    ///
    /// - Parameter data: message data
    func updateDocumentForMessageDelivered(withMessageData data: [String : Any])  {
        self.updateDoc(withData: data)
        guard let selfuserID = selfID else { return }
        var name = NSNotification.Name(rawValue: "MessageNotification" + selfuserID)
        NotificationCenter.default.post(name: name, object: self, userInfo: ["message": data, "status":"2"])
        name = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
    }
    
    
    /// updating document for message read after getting read status with message data
    ///
    /// - Parameter data: message data
    func updateDocumentForMessageRead(withMessageData data: [String : Any])  {
        guard let selfuserID = selfID else { return }
        self.updateDoc(withData: data)
        var name = NSNotification.Name(rawValue: "MessageNotification" + selfuserID)
        NotificationCenter.default.post(name: name, object: self, userInfo: ["message": data, "status":"3"])
        name = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
    }
    
    
    /// update chat document with data
    ///
    /// - Parameter data: message data
    func updateDoc(withData data : Any) { //update chat doc with data
        guard let ackObj = data as? [String:Any] else { return }
        var msgData = ackObj
        msgData["deliveryStatus"] = ackObj["status"] as! String
        msgData["isSelf"] = false
        self.updateMessageStatus(withMessageObj: msgData)
    }
    
    
    /// update message status with message object
    ///
    /// - Parameter messageObj: message object
    func updateMessageStatus(withMessageObj messageObj : [String:Any]) {
        guard let docID = messageObj["doc_id"] as? String else { return }
        guard let chatDta = couchbase.getData(fromDocID: docID) else { return }
        var chatData = chatDta
        var msgArray = chatData["messageArray"] as! [[String:Any]]
        
        guard let msgIDArray = messageObj["msgIds"] as? [String] else { return }
        guard let index:Int = msgArray.index(where: {$0["id"] as! String == msgIDArray[0]}) else { return }
        
        var msgObj = msgArray[index]
        let status = messageObj["deliveryStatus"] as! String
        msgObj["deliveryStatus"] = messageObj["deliveryStatus"]
        if status == "3" {
            msgObj["deliveryStatus"] = "3"
        }
        msgArray[index] = msgObj
        chatData["messageArray"] = msgArray
        couchbase.updateData(data: chatData, toDocID: docID)
    }
    
    /// get chats from couchbase in Chat modal array format
    ///
    /// - Returns: array of chats
    func getChatsFromCouchbase() -> [Chat]? {
        let indexDocVMObj = IndexDocumentViewModel(couchbase: couchbase)
        guard let userID = self.getUserID() else { return [] }
        guard let indexID = indexDocVMObj.getIndexValue(withUserSignedIn: true) else { return [] }
        guard let indexData = couchbase.getData(fromDocID: indexID) else { return []  } //getting the index data here.
        guard let userIDArray = indexData["userIDArray"] as? [String] else { return []  }
        if userIDArray.contains(userID) {
            guard let userDocArray = indexData["userDocIDArray"] as? [String] else { return []  }
            if let index = userIDArray.index(of: userID) {
                let userDocID = userDocArray[index] // Fetching the docID respected to userID index.
                
                guard let userDocData = couchbase.getData(fromDocID: userDocID) else { return []  } //fetching user doc data from user doc id.
                if let individualChatDocID = userDocData["chatDocument"] as? String {
                    guard let individualChatData = couchbase.getData(fromDocID: individualChatDocID) else { return []  }
                    guard let chatUserIDArray = individualChatData["receiverUidArray"] as? [String] else { return []  }
                    guard let chatDocIDArray = individualChatData["receiverDocIdArray"] as? [String] else { return []  }
                    guard let chatSecretIDArray = individualChatData["secretIdArray"] as? [String] else { return []  }
                    if chatUserIDArray.count>0 {
                        let chatObjArray = self.getChatObject(fromRecieverDocIDArray: chatDocIDArray, withRecieverUIDArray: chatUserIDArray, secretIDArray: chatSecretIDArray)
                        return chatObjArray
                    } else {
                        return []
                    }
                }
            }
        }
        return []
    }
    
    
    /// for Getting chat object from recievers documentID array, recievers UsersID array and with secretID/ProductID array
    ///
    /// - Parameters:
    ///   - docIDArray: array of docIDs
    ///   - recieverUIDArray: array of receivers UIDs
    ///   - secretIDArray: array of secret UIDs
    /// - Returns: returns the chat modal objects array
    fileprivate func getChatObject(fromRecieverDocIDArray docIDArray: [String], withRecieverUIDArray recieverUIDArray : [String],secretIDArray : [String]) -> [Chat] {
        var chats = [Chat]()
        for chatDocID in docIDArray {
            guard let index = docIDArray.index(of: chatDocID) else { return [] }
            if (recieverUIDArray.count > index) {
                let chatUserID = recieverUIDArray[index]
                let productID = secretIDArray[index]
                if let chatObj = self.getChatObject(forChatDocID: chatDocID, chatUserID: chatUserID, productID: productID) {
                    chats.append(chatObj)
                }
            }
        }
        return chats
    }
    
    
    /// get chat object for chatdoc ID with chatusers ID and with productID
    ///
    /// - Parameters:
    ///   - chatDocID: chatdocID
    ///   - chatUserID: chatUserID
    ///   - productID: productID
    /// - Returns: Chat Object
    func getChatObject(forChatDocID chatDocID: String?, chatUserID : String?, productID : String?) -> Chat? {
        guard let chatDocID = chatDocID else { return nil }
        guard let chatData = couchbase.getData(fromDocID: chatDocID) else { return nil }
        var acceptedPrice = ""
        var productIsSold = false
        var wasInvited = true
        
        if let accptedPrice = chatData["acceptedPrice"] as? String {
            acceptedPrice = accptedPrice
        }
        
        if let isProductSold = chatData["productIsSold"] as? Int {
            if isProductSold == 0
            {
                productIsSold = false
            }
            else
            {
                productIsSold  = true
            }
            
        }
        
        if let userWasInvited = chatData["wasInvited"] as? Bool {
            wasInvited = userWasInvited
        }
        
        if let lastMsg = chatData["newMessage"] as? String, let name = chatData["receiverName"] as? String, let image = chatData["receiverImage"] as? String, let msgDeliveryTime = chatData["newMessageDateInString"] as? String, let msgCount = chatData["newMessageCount"] as? Int, let hasNewMessage = chatData["hasNewMessage"] as? Bool, let chatID = chatData["chatID"] as? String, let initiated = chatData["initiated"] as? Bool, let offerType = chatData["offerType"] as? String, let productId = chatData["productId"] as? String, let productImage = chatData["productImage"] as? String, let userImage = chatData["profilePic"] as? String, let senderID = chatData["senderId"] as? String,let lastMsgDate = chatData["lastMessageDate"] as? String, let recipientID = chatData["recipientId"] as? String
        {
            if chatID.count>0 {
                let chat = Chat(messageArray: [], hasNewMessage: hasNewMessage, newMessage: lastMsg.fromBase64(), newMessageTime: msgDeliveryTime, newMessageDateInString: msgDeliveryTime, newMessageCount: "\(msgCount)", lastMessageDate: lastMsgDate, recieverUIDArray: [], recieverDocIDArray: [], name: name, image: image, secretID: productID, userID: chatUserID, docID: chatDocID, wasInvited: wasInvited, destructionTime: -1, isSecretInviteVisible: false, chatID: chatID, initiated: initiated, offerType: offerType, productId: productId, productImage: productImage, userImage: userImage, senderId: senderID, recipientId: recipientID, productName: nil, membername: nil, currency: nil, latitude: nil, longitude: nil, mainUrl : nil, place : nil, profilePicUrl : nil, negotiable : nil, price : nil, acceptedPrice: acceptedPrice, isProductSold: productIsSold)
                
                if let productName = chatData["productName"] as? String,
                    let receiverImage = chatData["receiverImage"] as? String,
                    let productImage = chatData["productImage"] as? String,
                    let memberName = chatData["membername"] as? String{
                    chat.productName = productName
                    chat.membername = memberName
                    chat.productImage = productImage
                    chat.image = receiverImage
                }
                if let currency = chatData["currency"] as? String,
                    let lat = chatData["latitude"] as? Float,
                    let long = chatData["longitude"] as? Float,
                    let negotiable = chatData["negotiable"] as? Bool,
                    let place = chatData["place"] as? String {
                    if let price = chatData["price"] as? Int {
                        chat.price = price
                    } else if let price = chatData["price"] as? String {
                        chat.price = Int(price)
                    }
                    chat.currency = currency
                    chat.latitude = lat
                    chat.longitude = long
                    chat.negotiable = negotiable
                    chat.place = place
                }
                return chat
            }
        }
        return nil
    }
    
    /// get message object for updating status with data and status
    ///
    /// - Parameters:
    ///   - data: message data
    ///   - status: message status
    /// - Returns: message dictionary object.
    func getMessageObjectForUpdatingStatus(withData data : [String : Any], andStatus status: String) -> Any? {
        guard var fromID = data["from"] as? String, let msgIDs = data["id"] as? String,  var toID = data["to"] as? String  else { return nil }
        var docID : String
        if let doc = data["toDocId"] as? String
        {
            docID = doc
        }
        else
        {
            docID = ""
        }
        if toID == selfID {
            toID = fromID
            fromID = Helper.getMQTTID()
        }
        let params =
            ["from":fromID, //user ID of the sender
                "msgIds":[msgIDs], //array containing the individual message ids to be acknowladged in string.
                "doc_id":docID, // sender's docID which is recieved in the message
                "to":toID, // userID of the sender for whom the acknowledgement is to be sent.
                "status":status] //2 or 3 for message delivered and read respectively.
                as [String : Any]
        return params as Any
    }
    
    /// get messages from chatdocID
    ///
    /// - Parameter chatDocID: chatDocID
    /// - Returns: Array of message models
    func getMessagesFromChatDoc(withChatDocID chatDocID: String) -> [Message]{
        var messages = [Message]()
        if let docData = couchbase.getData(fromDocID: chatDocID) {
            if let msgArray = docData["messageArray"] as? [Any] {
                for msgObj in msgArray {
                    var offerType = ""
                    if let messageObj = msgObj as? [String: Any], let ExistingOfferType = docData["offerType"] as? String {
                        var msgtype = ""
                        var message = ""
                        if let type = messageObj["type"] as? String, let msg = messageObj["payload"] as? String {
                            msgtype = type
                            message = msg
                        } else if let type = messageObj["messageType"] as? String, let msg = messageObj["message"] as? String {
                            msgtype = type
                            message = msg
                        }
                        if let msgOfferType = messageObj["offerType"] as? String {
                            if(ExistingOfferType == "2" && (msgOfferType == "3" || msgOfferType == "1"))
                            {
                                offerType = ExistingOfferType
                            }
                            else
                            {
                                offerType = msgOfferType
                            }
                        } else{
                            offerType = ExistingOfferType
                        }
                        if (message.count == 0 && msgtype == "0"){
                        }else {
                            if let isSelf = messageObj["isSelf"] as? Bool {
                                let mesageObj = Message(forData: messageObj, withDocID: chatDocID, isSelfMessage: isSelf, andMessageobj: messageObj, offerType : offerType, isMediaIncluded: false, includedMedia: nil, withImageURL : nil)
                                messages.append(mesageObj)
                            }
                        }
                    }
                }
            }
        }
        return messages
    }
    
    /// update status for chat messages with message status and chatDocID
    ///
    /// - Parameters:
    ///   - status: message status
    ///   - chatDocID: chatDocID
    func updateStatusForChatMessages(withStatus status : String, chatDocID : String?) {
        guard let chatDocID = chatDocID else { return }
        guard let chatData = couchbase.getData(fromDocID: chatDocID) else { return }
        var chatDta = chatData
        var msgArray = chatDta["messageArray"] as! [[String:Any]]
        for (idx, dic) in msgArray.enumerated() {
            if ((dic["deliveryStatus"] as? String) != "3") && ((dic["isSelf"] as? Bool) == true){
                var msgData = dic
                msgData["deliveryStatus"] = status
                msgArray[idx] = msgData
            }
        }
        chatDta["messageArray"] = msgArray
        self.couchbase.updateData(data: chatDta, toDocID: chatDocID)
    }
    
    /// update messages with messages by chatID, receiverID and secretId
    ///
    /// - Parameters:
    ///   - messages: array of messages
    ///   - chatID: current chatId
    ///   - receiverID: receiverID
    ///   - secretId: secretId/{Product ID
    func updateMessages(withMessage messages : [[String :Any]]?, byChatID chatID : String?, receiverID : String?, secretId : String? ){
        guard let chatID = chatID, let receiverID = receiverID, let secretId = secretId, let messages = messages else { return }
        let individualChatVMObject = IndividualChatViewModel(couchbase: couchbase)
        guard let chatDocID = individualChatVMObject.getChatDocID(withRecieverID: receiverID, andSecretID: secretId, withContactObj: nil, messageData: nil, withProductDetails: nil) else { return }
        guard let data = couchbase.getData(fromDocID: chatDocID) else { return }
        var chatData = data
        if (chatData["chatID"] as? String) == chatID {
            guard let messagesArray = chatData["messageArray"] as? [[String : Any]] else { return }
            var messageArray = messagesArray
            guard let formatedMessages = self.getFormatedMessages(from: messages) else { return }
            messageArray.append(contentsOf:formatedMessages)
            let sortedThings = messageArray.sorted { msg1,msg2 in
                if let time0 = msg1["timestamp"] as? Int64, let time1 = msg2["timestamp"] as? Int64 {
                    return time0 < time1
                } else if let time0 = msg1["timestamp"] as? String, let time1 = msg2["timestamp"] as? String {
                    if let ts1 = Float(time0), let ts2 = Float(time1) {
                        return  ts1 < ts2
                    }
                }
                return false
            }
            chatData["messageArray"] = sortedThings
            couchbase.updateData(data: chatData, toDocID: chatDocID)
        }
    }
    
    /// get formated messages from messages
    ///
    /// - Parameter messages: array of messages
    /// - Returns: array of formated messages
    fileprivate func getFormatedMessages(from messages: [[String : Any]]) -> [[String : Any]]? {
        var formatedMessages = [[String : Any]]()
        for message in messages {
            var msg = [String : Any]()
            msg["message"] = message["payload"]
            msg["messageType"] = message["messageType"]
            msg["isSelf"] = false
            msg["from"] = message["senderId"]
            msg["to"] = message["receiverId"]
            msg["timestamp"] = message["timestamp"]
            msg["deliveryStatus"] = message["status"]
            msg["id"] = message["messageId"]
            msg["downloadStatus"] = "1"
            msg["dataSize"] = "0"
            msg["thumbnail"] = message["thumbnail"]
            msg["userImage"] = message["userImage"]
            msg["name"] = message["name"]
            
            guard let selfID = self.getUserID(), let senderId = message["senderId"] as? String  else { return nil }
            if selfID == senderId {
                msg["isSelf"] = true
            }
            formatedMessages.append(msg)
        }
        return formatedMessages
    }
    
    /// updating Lagel chat related data and chat object
    ///
    /// - Parameters:
    ///   - data: chat data
    ///   - chatObj: Chat Object
    func updateYeloChatRelatedData(withData data: [String : Any], andChatObj chatObj: Chat) {
        guard let chatID = chatObj.chatID, let chatDocID = chatObj.docID else { return }
        guard let dta = couchbase.getData(fromDocID: chatDocID) else { return }
        var chatData = dta
        if (chatData["chatID"] as? String) == chatID {
            chatData["membername"] = data["membername"]
            chatData["latitude"] = data["latitude"]
            chatData["longitude"] = data["longitude"]
            chatData["productImage"] = data["mainUrl"]
            chatData["currency"] = data["currency"]
            chatData["productName"] = data["productName"]
            chatData["place"] = data["place"]
            
            let memberName = data["membername"] as? String
            if Helper.userName() != memberName
            {
                chatData["receiverImage"] = data["memberProfilePicUrl"] // new added code.
            }
            
            chatData["negotiable"] = data["negotiable"]
            chatData["price"] = data["price"]
            chatData["wasInvited"] = data["wasInvited"]
            chatData["productIsSold"] = data["sold"]
            if let offerType = data["offerAccepted"] as? Int {
                if offerType == 0 {
                    
                } else {
                    if let acceptedPrice = data["offerPrice"] as? Int{
                        chatData["offerType"] = "2"
                        chatData["acceptedPrice"] = String(acceptedPrice)
                    }
                }
            }
        }
        couchbase.updateData(data: chatData, toDocID: chatDocID)
        var name = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
        name = NSNotification.Name(rawValue: "UpdateProductDetailsNotification")
        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
    }
    
    
    // In Buying Section This will be the member(productOwner) profile pic.
    func updateProfilePicFOrReceiver(productDetails:ProductDetails, andChatObj chatObj: Chat)
    {
        
        guard let chatID = chatObj.chatID, let chatDocID = chatObj.docID else { return }
        guard let dta = couchbase.getData(fromDocID: chatDocID) else { return }
        var chatData = dta
        if (chatData["chatID"] as? String) == chatID {
            chatData["receiverImage"] = productDetails.memberProfilePicUrl
            
        }
        couchbase.updateData(data: chatData, toDocID: chatDocID)
        
    }
}


