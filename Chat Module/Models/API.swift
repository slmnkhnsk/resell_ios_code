//
//  API.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 25/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

public class API : NSObject{
    
    //    typedef void (^completionBlockType) ([String : Any]?)
    
    
    typealias completionBlockType = ([String : Any]?) ->Void
    
    func initiatedChat(withdata data : [String : Any], completionBlock: @escaping completionBlockType) {
        let strURL = iPhoneBaseURL+"makeOffer"
         let header = (authUsername + ":" + authPassword).toBase64()
        let headerParams = ["authorization":"Basic \(header)"]
        AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
            guard let responseData = response.dictionaryObject else { return }
            completionBlock(responseData)
        },
                                 failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
                                    completionBlock(nil)
        })
    }
    
    func getPostDetailsByID(withID productID: String?, success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        let strURL = iPhoneBaseURL+"getPostsById/users"
         let header = (authUsername + ":" + authPassword).toBase64()
        let headerParams = ["authorization":"Basic \(header)"]
        guard let token = Helper.userToken() else { return }
        if token.count>1 {
            
            let params = ["token":token as Any,
                          "postId":productID as Any] as [String : Any]
            
            AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: params, success: { (response) in
                guard let responseData = response.dictionaryObject else { return }
                guard let dataArray = responseData["data"] as? [[String : Any]] else { return }
                success(dataArray[0])
            },
                                     failure: { (customErrorStruct) in
                                        print("Error",customErrorStruct)
                                        failure(customErrorStruct)
            })
        }
    }
    
    func getChats(withPageNo pageNo : String) {
        let strURL = AppConstants.getChats+"/0"
        guard let token = Helper.userToken() else { return }
        if token.count>1 {
            let headerParams = ["authorization":mAuthorization,
                                "token":token]
            
            AFWrapper.requestGETURL(serviceName: strURL, withHeader: headerParams,
                                    success: { (response) in
                                        guard let responseData = response.dictionaryObject else { return }
                                        print("Response",responseData)
            },
                                    failure: { (customErrorStruct) in
                                        print("Error",customErrorStruct)
            })
        }
    }
    
    func deleteChats(withSecretID secretID: String?, andRecipientID recipientID : String?, success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)  {
        guard let secretID = secretID, let recipientID = recipientID else { return }
        let strURL = AppConstants.getChats+"/"+recipientID+"/"+secretID
        guard let token = Helper.userToken() else { return }
        if token.count>1 {
            let headerParams = ["authorization":mAuthorization,
                                "token":token]
            AFWrapper.requestDeleteURL(serviceName: strURL, withHeader: headerParams,
                                       success: { (response) in
                                        guard let responseData = response.dictionaryObject else { return }
                                        print("Response",responseData)
                                        success(responseData)
            },
                                       failure: { (customErrorStruct) in
                                        print("Error",customErrorStruct)
                                        failure(customErrorStruct)
            })
        }
    }
    
    func getMessages(withUrl messageUrl : String) {
        guard let token = Helper.userToken() else { return }
        if token.count>1 {
            let headerParams = ["authorization":mAuthorization,
                                "token":token]
            AFWrapper.requestGETURL(serviceName: messageUrl, withHeader: headerParams,
                                    success: { (response) in
                                        guard let responseData = response.dictionaryObject else { return }
                                        print("Response",responseData)
            },
                                    failure: { (customErrorStruct) in
                                        print("Error",customErrorStruct)
            })
        }
    }
    
    func sendNotification(withText text: String, andTitle title: String, toTopic topic: String, andData data:[String : String]) {
        let url = "https://fcm.googleapis.com/fcm/send"
        let header = ["Authorization":mGoogleAPIKeyFCM]
        
        guard let receiverID = Helper.getMQTTID(), let secretID = data["secretID"] else { return }

        let userID = ["receiverID":receiverID,
        "secretID":secretID] as [String : String]
        
        let notificationParams = [ "body" : "\(text)",
            "title" : "\(title)", 
            "sound": "default"]
        let notificationData = ["body":userID]
        let params : [String : Any] = ["condition": "'\(topic)' in topics",
            "priority" : "high",
            "notification" : notificationParams,
            "data":notificationData
            ] as [String : Any]
        
        AFWrapper.requestPOSTURL(serviceName: url, withHeader: header, params: params, success:{ (response) in
            guard let responseData = response.dictionaryObject else { return }
            print("Response",responseData)
        },
                                 failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
        })
    }
}

