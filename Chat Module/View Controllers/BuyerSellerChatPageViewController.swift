//
//  BuyerSellerChatPageViewController.swift
//  Resell
//
//  Created by Rahul Sharma on 18/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

protocol IndexCangedDelegate {
    func currentIndexGotChanged(index :Int)
}

class BuyerSellerChatPageViewController: UIPageViewController {
    
    struct Constants {
        static let pages = 2
        static let PageIdentifier = "ChatsListViewController"
    }
    
    var currentIndex = 0
    var indexCangedDelegate : IndexCangedDelegate?
    var totalNumberOfPages = Constants.pages
    var chatlistModelObject : ChatListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        let vc = self.storyboard?.instantiateViewController(withIdentifier: Constants.PageIdentifier) as! ChatsListViewController
        vc.pageIndex = 0
        let buyerChats = self.chatlistModelObject?.chats
        vc.chatListViewModel = ChatListViewModel(withChatObjects: buyerChats)
        vc.getCurrentIndexDelgate = self
        setViewControllers([vc], // Has to be a single item array, unless you're doing double sided stuff I believe
            direction: .forward,
            animated: true,
            completion: nil)
        //self.reloadCurrentView()
        for view in self.view.subviews {
            if let view = view as? UIScrollView {
                view.isScrollEnabled = false
            }
        }
    }
    
    func reloadCurrentView() {
        if self.currentIndex < self.viewControllers!.count {
            if let currentViewController = self.viewControllers![self.currentIndex] as? ChatsListViewController {
                
                /*UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
                
                ActivityViewController *activityVC  = [story instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
                UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
                [self.navigationController presentViewController:nav animated:YES completion:nil];*/
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: Constants.PageIdentifier) as! ChatsListViewController
                vc.pageIndex = self.currentIndex
                self.setChatViewModel(toController: vc)
                vc.getCurrentIndexDelgate = self
                setViewControllers([vc],direction: .forward,animated: false,completion: nil)
                currentViewController.reloadTableView()
            }
        } else {
            if (self.view.viewWithTag(1) as? UITableView) != nil {
                if let controller = self.viewControllers?.last as? ChatsListViewController {
                    self.setChatViewModel(toController: controller)
                    controller.reloadTableView()
                }
            }
        }
    }
    
    func reloadController(withIndex index: Int, andChatVMObj chatListVMObj : ChatListViewModel) {
        if let controllers = self.viewControllers {
            if controllers.count > 1 {
                if let currentViewController = self.viewControllers![self.currentIndex] as? ChatsListViewController {
                    currentViewController.chatListViewModel = chatListVMObj
                    currentViewController.reloadTableView()
                }
            } else {
                if let currentViewController = self.viewControllers?.last as? ChatsListViewController {
                    currentViewController.chatListViewModel = chatListVMObj
                    currentViewController.reloadTableView()
                }
            }
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
            self.indexCangedDelegate?.currentIndexGotChanged(index: self.currentIndex)
    }
    
    func openController(withIndex index:Int){
        
        if (index == 1) {
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc :ActivityViewController = storyboard.instantiateViewController(withIdentifier:"ActivityControllerId") as! ActivityViewController
            vc.pageIndex = Int32(index)
            vc.indexDelegate = self
            self.setViewControllers([vc], direction: .reverse, animated: false, completion: nil)
            
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Constants.PageIdentifier) as! ChatsListViewController
            vc.pageIndex = index
            self.setChatViewModel(toController: vc)
            vc.getCurrentIndexDelgate = self
            self.setViewControllers([vc], direction: .reverse, animated: false, completion: nil)
            vc.reloadTableView()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in view.subviews {
            if view is UIPageControl {
                DispatchQueue.main.async {
                    view.backgroundColor = UIColor.clear
                }
            }
        }
    }
    
    func setChatViewModel(toController controller: ChatsListViewController){
        if controller.pageIndex == 0 {
            let buyerChats = self.chatlistModelObject?.chats
            controller.chatListViewModel = ChatListViewModel(withChatObjects: buyerChats)
        }
        else {
            let sellerChats = self.chatlistModelObject?.chats
            controller.chatListViewModel = ChatListViewModel(withChatObjects: sellerChats)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        DispatchQueue.main.async {
            self.view.setNeedsLayout()
        }
    }
}

extension BuyerSellerChatPageViewController : UIPageViewControllerDelegate, UIPageViewControllerDataSource{
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = (viewController as! ChatsListViewController).pageIndex {
            if index == 1 {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: Constants.PageIdentifier) as! ChatsListViewController
                controller.pageIndex = index-1
                self.setChatViewModel(toController: controller)
                controller.getCurrentIndexDelgate = self
                return controller
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = (viewController as! ChatsListViewController).pageIndex{
            if index == 0 {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller :ActivityViewController = storyboard.instantiateViewController(withIdentifier:"ActivityControllerId") as! ActivityViewController
                controller.indexDelegate = self
                controller.pageIndex = Int32(index) + 1
                //self.setViewControllers([vc], direction: .reverse, animated: false, completion: nil)
                
                /*let controller = self.storyboard?.instantiateViewController(withIdentifier: Constants.PageIdentifier) as! ChatsListViewController
                controller.getCurrentIndexDelgate = self
                controller.pageIndex = index+1
                self.setChatViewModel(toController: controller)*/
                return controller
            }
        }
        return nil
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension BuyerSellerChatPageViewController :GetCurrentIndexDelgate{
    func currentIndex(index: Int) {
        self.currentIndex = index
    }
}

extension BuyerSellerChatPageViewController :GetCurrIndexDelgate {
    func currentIndex(_ index: Int32) {
        self.currentIndex = Int(index)
    }
}
