//
//  BuyerSellerChatViewController.swift
//  Resell
//
//  Created by Rahul Sharma on 18/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Firebase


class BuyerSellerChatViewController: UIViewController {
    
    struct Constants {
        static let openChatSegue = "openChatSegue"
    }
    
    @IBOutlet weak var labelCountForNotifications: UILabel!
    @IBOutlet weak var searchBarOutlet: UISearchBar!
    @IBOutlet weak var buyngButtonOutlet: UIButton!
    @IBOutlet weak var buyingViewOutlet: UIView!
    @IBOutlet weak var buyingUnselectedViewOutlet: UIView!
    @IBOutlet weak var sellingUnselectedViewOutlet: UIView!
    @IBOutlet weak var sellingButtonOutlet: UIButton!
    @IBOutlet weak var sellingViewOutlet: UIView!
    @IBOutlet weak var topTabView: UIView!
    var productDetailsObject : ProductDetails!
    
    var chatListViewModel: ChatListViewModel!
    var controller:BuyerSellerChatPageViewController!
    let mqttChatManager = MQTTChatManager.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].title = NSLocalizedString("Chat", comment: "Chat")
        
        self.navigationItem.rightBarButtonItem = nil
        self.buyngButtonOutlet.setTitle("Messages", for: .normal)
        self.sellingButtonOutlet.setTitle("Notification", for: .normal)
        
        labelCountForNotifications.layer.borderColor = UIColor.white.cgColor
        
        controller = self.childViewControllers.first as! BuyerSellerChatPageViewController
        self.setBuyingButtonSelected()
        controller.indexCangedDelegate = self
        guard let mqttId = Helper.getMQTTID() else { return }
        if mqttId != mMqttId {
            mqttChatManager.subscribeGetChatTopic(withUserID: mqttId)
        }
        let name = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(BuyerSellerChatViewController.updateChatList), name: name, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BuyerSellerChatViewController.receivedChat(notification:)), name: NSNotification.Name(rawValue: "OfferInitiated"), object: nil)
        API().getChats(withPageNo:"0")
        
        self.buyngButtonOutlet.setTitleColor(AppConstants.chatColor.chatBaseColor, for: .selected)
        self.sellingButtonOutlet.setTitleColor(AppConstants.chatColor.chatBaseColor.withAlphaComponent(0.6), for: .normal)
        
        self.topTabView.backgroundColor = AppConstants.chatColor.chatTabTopBackgroundColor
        
    }
    
    func receivedChat(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        guard let chatObj = userInfo["chatObj"] as? [String : Any] else { return }
        guard let receiverID = chatObj["receiverID"] as? String, let secretID = chatObj["secretID"] as? String else { return }
        
        if let product = userInfo["productObj"] as? ProductDetails
        {
            productDetailsObject = product
        }
        
        let individualCVMObj = IndividualChatViewModel(couchbase: Couchbase())
        let toDocID = individualCVMObj.fetchIndividualChatDoc(withRecieverID: receiverID, andSecretID: secretID)
        if toDocID == nil {
            self.performSegue(withIdentifier: Constants.openChatSegue, sender: productDetailsObject)
            
        } else {
            let chatsDocVMObj = ChatsDocumentViewModel(couchbase: Couchbase())
            if let chat = chatsDocVMObj.getChatObject(forChatDocID: toDocID, chatUserID:receiverID, productID: secretID) {
                self.performSegue(withIdentifier: Constants.openChatSegue, sender: chat)
            }
        }
        self.updateChatListCount()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.openChatSegue{
            if let chatController = segue.destination as? ChatViewController {
                if let chatObj = sender as? Chat {
                    let chatVMObject = ChatViewModel(withChatData: chatObj)
                    chatController.chatViewModelObj = chatVMObject
                    chatController.productObj = self.productDetailsObject
                } else if let productObj = sender as? ProductDetails {
                    chatController.productObj = productObj
                }
            }
        }
    }
    
    func updateChatList() {
        let couchbaseObj = Couchbase.sharedInstance
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        guard let chats = chatsDocVMObject.getChatsFromCouchbase() else { return }
        let sortedChats = chats.sorted {
            guard let uniqueID1 = $0.lastMessageDate, let uniqueID2 = $1.lastMessageDate else { return false }
            return uniqueID1 > uniqueID2
        }
        self.chatListViewModel = ChatListViewModel(withChatObjects: sortedChats)
        self.title = self.chatListViewModel.userName ?? ""
        controller.chatlistModelObject = self.chatListViewModel
        self.chatListViewModel.setupNotificationSettings()
        self.chatListViewModel.authCheckForNotification()
        controller.reloadCurrentView()
        self.updateChatListCount()
    }
    
    func updateChatListCount() {
        self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].title = NSLocalizedString("Chat", comment: "Chat")
        let count = self.chatListViewModel.unreadChatsCounts()
        if count == 0 {
            self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].badgeValue = nil
        } else {
            self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].badgeValue = "\(count)"
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let notificationSharedObject = NotificationCount.sharedInstance()
        if notificationSharedObject?.notificationsCount == "0" {
            self.labelCountForNotifications.isHidden = true;
        }
        else
        {
            self.labelCountForNotifications.isHidden = false;
            self.labelCountForNotifications.text = notificationSharedObject?.notificationsCount ;
        }
        
        self.updateChatList()
        self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].title = NSLocalizedString("Chat", comment: "Chat")
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    @IBAction func refreshButtonAction(_ sender: Any) {
        /*API().getChats(withPageNo:"0")
        if (!MQTT.sharedInstance.isConnected) {
            MQTT.sharedInstance.createConnection()
        }
        let pIndicator = ProgressIndicator.sharedInstance()
        pIndicator?.showPI(on: self.view, withMessage: "Loading...")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            pIndicator?.hide()
        }*/
    }
    
    @IBAction func buyingButtonAction(_ sender: Any) {
        self.updateChatList()
        self.setBuyingButtonSelected()
    }
    
    @IBAction func sellingButtonActoin(_ sender: Any) {
        //self.updateChatList()
        self.setSellingButtonSelected()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].title = NSLocalizedString("Chat", comment: "Chat")
    }
    
    func setBuyingButtonSelected() {
        buyngButtonOutlet.isSelected = true
        sellingButtonOutlet.isSelected = false
        
        self.buyngButtonOutlet.setTitleColor(AppConstants.chatColor.chatBaseColor, for: .selected)
        self.sellingButtonOutlet.setTitleColor(AppConstants.chatColor.chatBaseColor.withAlphaComponent(0.6), for: .normal)
        
        controller.openController(withIndex: 0)
        
        self.buyingViewOutlet.backgroundColor = AppConstants.chatColor.chatBaseColor
        self.sellingViewOutlet.backgroundColor = UIColor.clear
        self.buyingUnselectedViewOutlet.isHidden = true
        self.sellingUnselectedViewOutlet.isHidden = false
        
    }
    
    func setSellingButtonSelected() {
        buyngButtonOutlet.isSelected = false
        sellingButtonOutlet.isSelected = true
        
        self.sellingButtonOutlet.setTitleColor(AppConstants.chatColor.chatBaseColor, for: .selected)
        self.buyngButtonOutlet.setTitleColor(AppConstants.chatColor.chatBaseColor.withAlphaComponent(0.6), for: .normal)
        
        controller.openController(withIndex: 1)
        self.buyingViewOutlet.backgroundColor = UIColor.clear
        self.sellingViewOutlet.backgroundColor = AppConstants.chatColor.chatBaseColor
        self.sellingUnselectedViewOutlet.isHidden = true
        self.buyingUnselectedViewOutlet.isHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func notificationButtonAction(_ sender: Any) {
        
        let notificationSharedObject = NotificationCount.sharedInstance()
        notificationSharedObject?.notificationsCount = "0"
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC :ActivityViewController = storyboard.instantiateViewController(withIdentifier:"ActivityControllerId") as! ActivityViewController
        let nav  = UINavigationController(rootViewController: newVC)
        self.navigationController?.present(nav, animated: true, completion: nil)
        
    }
    
}

extension BuyerSellerChatViewController : IndexCangedDelegate {
    func currentIndexGotChanged(index: Int) {
        switch index{
        case 0:
            self.setBuyingButtonSelected()
            
        default:
            self.setSellingButtonSelected()
        }
    }
}

extension BuyerSellerChatViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newString = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        getSearchData(withSearchText: newString)
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func getSearchData(withSearchText searchString: String) {
        let index = self.controller.currentIndex
        let chats = self.chatListViewModel.searchInChatModels(withString: searchString, withSelectedIndex: index)
        let chatListVMObj = ChatListViewModel(withChatObjects: chats)
        self.controller.reloadController(withIndex: index, andChatVMObj: chatListVMObj)
    }
}


