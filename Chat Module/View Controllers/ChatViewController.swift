//
//  ChatViewController.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Firebase
import Kingfisher


/// Handle all the methods related to the chat controller.
class ChatViewController: JSQMessagesViewController {
    
    
    /// Used Constants for default values.
    struct Constants {
        
        /// Default message size.
        static let messagePageSize = "20"
        
        /// To Counter Offer Controller Segue
        static let toCounterOfferSegue = "toCounterOfferSegue"
        
        /// To Location Picker Controller Segue
        static let toLocationPicker = "toLocationPicker"
        
        
        /// To Paypal View Controller Segue.
        static let toPaypalViewcontroller = "toPaypalViewcontroller"
        
        /// To Paypal Web View Controller Segue.
        static let paypalWebViewSegue = "paypalWebViewSegue"
        
        /// To image controller segue.
        static let imageControllerSegue = "imageControllerSegue"
        static let showLocationSegue = "showLocationSegue"
    }
    
    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var TitleLabelOutlet : UILabel!
    @IBOutlet weak var currentChatStatusLabelOutlet : UILabel!
    @IBOutlet var chatBackgroundView: UIView!
    
    let couchbaseObj = Couchbase.sharedInstance
    var refresher : UIRefreshControl!
    var staySafePopUpCount = 0
    var chatsDocVMObject : ChatsDocumentViewModel!
    
    
    /// Used for self ID.
    var selfID : String? {
        return self.getUserID()
    }
    
    
    /// Incoming bubble with the image and other properties.
    let incomingBubble = JSQMessagesBubbleImageFactory(bubble:#imageLiteral(resourceName: "IncomingChatTextBubble"), capInsets: .zero, layoutDirection: .leftToRight).incomingMessagesBubbleImage(with:AppConstants.chatColor.chatBaseColor2)
    
    
    /// Outgoing bubble with the image and other properties.
    let outgoingBubble = JSQMessagesBubbleImageFactory(bubble:#imageLiteral(resourceName: "OutgoingChatTextBubble"), capInsets: .zero, layoutDirection: .leftToRight).outgoingMessagesBubbleImage(with: AppConstants.chatColor.chatBaseColor)
    
    /// Chat view modal object
    var chatViewModelObj : ChatViewModel?
    
    /// Product Details Object.
    var productObj : ProductDetails?
    var chatDocID : String?
    
    /// Messages array.
    var messages = [Message]()
    var recieverID :String?
    var productID : String?
    
    var userName : String!
    var isTypingVisible = false
    
    /// MQTTChatManager shared instance.
    fileprivate let mqttChatManager = MQTTChatManager.sharedInstance
    
    //MARK: View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil 
        self.addRefresher()
        // CollectionView Background setup
        let backgroundSize: CGSize = (self.collectionView?.frame.size)!
        chatBackgroundView.frame = CGRect(x: 0, y: 0,width:backgroundSize.width, height: backgroundSize.height)
        self.collectionView?.backgroundView = self.chatBackgroundView
        self.chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        
        var name = NSNotification.Name(rawValue: "MessageNotification" + selfID!)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.receivedMessage(notification:)), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "LastSeen")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.receivedLastSeenStatus(notification:)), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "Typing")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.receivedTypingStatus(notification:)), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "PullToRefresh")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.stopRefresher(notification:)), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "reloadCollectinViewNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.reloadCollectionView(notification:)), name: name, object: nil)
        
        super.collectionView?.register(UINib(nibName: "CounterOfferPriceView", bundle: nil), forCellWithReuseIdentifier: "CounterOfferPriceCollectionViewCell")
        super.collectionView?.register(UINib(nibName: "OfferSentView", bundle: nil), forCellWithReuseIdentifier: "OfferSentPriceCollectionViewCell")
        super.collectionView?.register(UINib(nibName: "OfferReceivedView", bundle: nil), forCellWithReuseIdentifier: "OfferReceivedPriceCollectionViewCell")
        super.collectionView?.register(UINib(nibName: "Accepted", bundle: nil), forCellWithReuseIdentifier: "AcceptedPriceCollectionViewCell")
        super.collectionView?.register(UINib(nibName: "PaypalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "paypalView")
        super.collectionView?.register(UINib(nibName: "PayPalLinkSharedCell", bundle: nil), forCellWithReuseIdentifier: "PayPalLinkSharedCell")
        super.collectionView?.register(UINib(nibName: "SentLocationCell", bundle: nil), forCellWithReuseIdentifier: "SentLocationCell")
        super.collectionView?.register(UINib(nibName: "ReceivedLocationCell", bundle: nil), forCellWithReuseIdentifier: "ReceivedLocationCell")
        super.collectionView?.register(UINib(nibName: "ThankYouMessageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ThankYouMessageCollectionViewCell")
        super.collectionView?.register(UINib(nibName: "SentImageCell", bundle: nil), forCellWithReuseIdentifier: "SentImageCell")
        super.collectionView?.register(UINib(nibName: "ReceivedImageCell", bundle: nil), forCellWithReuseIdentifier: "ReceivedImageCell")
        
        
        let notificationName = NSNotification.Name(rawValue: "UpdateProductDetailsNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.recievedPostDetailsUpdate(notification:)), name: notificationName, object: nil)
        
        self.chatViewModelObj?.getProductDetails()
        self.setupProductNoLongerExist()
        self.setUpCollectionView()
    }
    
    /// Used for setting up product no longer exist .
    func setupProductNoLongerExist() {
        self.notAvailableTextOutlet?.text = "Product no longer exist"
        self.notAvailableTextOutlet?.textAlignment = .center
        self.productNoLongerExistView?.backgroundColor = AppConstants.chatColor.chatProductNotAvailableColor
        if let chatVMObj = self.chatViewModelObj {
            if (chatVMObj.isProductSold) == false {
                self.productDetailsVisibility = true
                self.producNoLongerAvailableVisibility = false
            } else {
                self.productDetailsVisibility = false
                self.producNoLongerAvailableVisibility = true
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let receiverID = self.recieverID {
            settingTypingStatus(withReceiverID: receiverID)
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.recieverID = self.getRecieverID()
        self.productID = self.getProductID()
        self.updateProductDetails()
        self.hidesBottomBarWhenPushed = true
        self.navigationController?.navigationBar.isTranslucent = false;
        self.navigationController?.navigationBar.shadowImage = nil;
        
        let name = NSNotification.Name(rawValue: "ChatDataUpdatedNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.reloadChatData(notification:)), name: name, object: nil)
    }
    
    
    
    
    /// Will call for the reloading collection view.
    ///
    /// - Parameter notification: Notification object.
    func reloadCollectionView(notification: NSNotification) {
        self.collectionView?.reloadData()
    }
    
    
    /// Used for showing a pull to refresh for refreshing/pulling the chat.
    func addRefresher() {
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
        self.productDetailsVisibility = true
    }
    
    func loadData() {
        var timeStamp = Int64(floor(Date().timeIntervalSince1970 * 1000))
        if let ts = self.messages.first?.uniquemessageId {
            timeStamp = ts
        }
        guard let mqttId = Helper.getMQTTID() else { return }
        if mqttId != mMqttId {
            mqttChatManager.subscribeToGetMessageTopic(withUserID: mqttId)
            self.fetchMessages(withTimeStamp: "\(timeStamp)")
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.refresher.endRefreshing()
        }
    }
    
    
    /// Calls when the notification received for getting chats.
    ///
    /// - Parameter notification: Notification with the chat details.
    func stopRefresher(notification: NSNotification) {
        self.refresher.endRefreshing()
        let userInfo = notification.userInfo as! [String: Any]
        if ((userInfo["chatId"] as? String) == (self.chatViewModelObj?.chatID)) {
            self.setUpCollectionView()
        }
    }
    
    func reloadChatData(notification: NSNotification) {
        DispatchQueue.main.async {
            self.setUpCollectionView()
            self.scrollToBottom(animated: true)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func getUserID() -> String? {
        guard let mqttID = Helper.getMQTTID() else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    fileprivate func fetchMessages(withTimeStamp timeStamp: String) {
        self.chatViewModelObj?.getMessages(withTimeStamp: timeStamp, andPageSize: Constants.messagePageSize)
    }
    
    
    /// Typing status send to the receiver ID.
    ///
    /// - Parameter receiverID: receivers id.
    fileprivate func settingTypingStatus(withReceiverID receiverID: String) {
        if let chatDocID = self.chatDocID {
            self.updateChatForReadMessage(toDocID: chatDocID, isControllerAppearing: true)
        }
        self.scrollToBottom(animated: false)
        if let selfID = selfID {
            mqttChatManager.subscribeTypingChannel(withUserID: selfID)
        }
        mqttChatManager.subscibeToLastSeenChannel(withUserID: receiverID)
    }
    
    /// After getting the product details it will update the product, with all the related data.
    func updateProductDetails() {
        if let productObj = self.productObj {
            
            if let chatDoc = self.chatDocID
            {
                if (!chatDoc.isEmpty)
                {
                    self.chatViewModelObj?.updateDataForReceiverProfilePic(product: productObj)
                }
            }
            if let memberImage = productObj.memberProfilePicUrl{
                if let imaegUrl = URL(string : memberImage) {
                    self.userImageOutlet.kf.setImage(with: imaegUrl, placeholder: #imageLiteral(resourceName: "itemUserDefault"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
                }
            }
            if let memberMqttId = productObj.memberMqttId{
                self.recieverID = memberMqttId
            }
            var userNme = productObj.membername
            if (self.chatViewModelObj?.initiated) == true {
                if let uName = self.chatViewModelObj?.memberName {
                    userNme = uName
                }
            } else {
                if let uName =  self.chatViewModelObj?.name {
                    userNme = uName
                }
            }
            self.userName = userNme
            self.TitleLabelOutlet.text = userNme
            if let productName = productObj.productName {
                self.productNameLabelOutlet?.text = productName
            }
            if let productimage = productObj.mainUrl{
                if let imaegUrl = URL(string : productimage) {
                    self.productImageViewOutlet?.kf.setImage(with: imaegUrl, placeholder: #imageLiteral(resourceName: "tagProduct__default"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
                }
            }
            if let price = productObj.price  {
                self.productPriceLabelOutlet?.text = price
            }
        }
        else  {
            if let username = self.getRecieverName() {
                self.userName = username
            }
            
            var userNme = ""
            if (self.chatViewModelObj?.initiated) == true {
                if let uName = self.chatViewModelObj?.memberName {
                    userNme = uName
                }
            } else {
                if let uName =  self.chatViewModelObj?.name {
                    userNme = uName
                }
            }
            self.TitleLabelOutlet.text = userNme
            
            if let imaegUrl = chatViewModelObj?.imageURL {
                self.userImageOutlet.kf.setImage(with: imaegUrl, placeholder: #imageLiteral(resourceName: "itemUserDefault"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
            }
            if let productName = chatViewModelObj?.productName{
                self.productNameLabelOutlet?.text = productName
            }
            if let productimage = chatViewModelObj?.productImage{
                self.productImageViewOutlet?.kf.setImage(with: productimage, placeholder: #imageLiteral(resourceName: "tagProduct__default"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
            }
            if let price = chatViewModelObj?.prductPrice  {
                self.productPriceLabelOutlet?.text = price
            }
        }
        if let receiverID = self.recieverID {
            self.settingTypingStatus(withReceiverID: receiverID)
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            if (self.chatViewModelObj?.wasInvited  == false) {
                if let chatDocID = self.chatDocID {
                    self.updateChatForAlertPopUp(toDocID: chatDocID)
                    let storyboard = UIStoryboard(name: "Chat", bundle: nil)
                    if let controller = storyboard.instantiateViewController(withIdentifier: "StaySafeViewController") as? StaySafeViewController {
                        self.staySafePopUpCount = self.staySafePopUpCount+1
                        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        self.view.endEditing(true)
                        self.present(controller, animated: false, completion: nil)
                    }
                }
            }
        }
        self.setUpCollectionView()
    }
    
    
    ///  Used for sending the typing status.
    func sendTypingStatus() {
        if let recieverID = recieverID, let productId = chatViewModelObj?.productId {
            mqttChatManager.sendTyping(toUser: recieverID, andProductID: productId)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let chatDocID = self.chatDocID {
            self.updateChatForReadMessage(toDocID: chatDocID, isControllerAppearing: false)
        }
        if let receiverID = self.recieverID, let selfID = selfID {
            mqttChatManager.unsubscribeTypingChannel(withUserID: selfID)
            mqttChatManager.unsubscibeToLastSeenChannel(withUserID: receiverID)
        }
        
        var name = NSNotification.Name(rawValue: "UpdateProductDetailsNotification")
        NotificationCenter.default.removeObserver(self, name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "ChatDataUpdatedNotification")
        NotificationCenter.default.removeObserver(self, name: name, object: nil)
    }
    
    
    /// For fetching senderID / OwnID / SelfID.
    
    /// - Remark:
    /// this method is used by JSQMessageController So don't remove it.
    ///
    /// - Returns: optional value of selfID.
    override func senderId() -> String {
        guard let selfID = selfID else { return "" }
        return selfID
    }
    
    
    /// For fetching the receiver ID. Depends upon the chatViewModelObj or productDetails Object whichever is available.
    ///
    /// - Returns: recevier ID.
    func getRecieverID() -> String? {
        if let recieverID = chatViewModelObj?.recieverID {
            return recieverID
        }
        else if let mqttID = self.productObj?.memberMqttId {
            return mqttID
        }
        return nil
    }
    
    
    /// For fetching the current productID/secretID
    ///
    /// - Returns: optional String value of productID/secretID
    func getProductID() -> String? {
        if let recieverID = chatViewModelObj?.productId {
            return recieverID
        }
        else if let mqttID = self.productObj?.postId {
            return mqttID
        }
        return nil
    }
    
    
    /// For fetching the receiver name, which is going to be used in the app.
    ///
    /// - Returns: receiver name String.
    func getRecieverName() -> String? {
        guard let recieverName = chatViewModelObj?.name else { return self.productObj?.membername }
        return recieverName
    }
    
    
    /// Chat document ID from messsage dictionary with a flag defining initially coming from controller.
    ///
    /// - Parameters:
    ///   - msgObj: message object for related data to fetch the doc id.
    ///   - isComingInitially: if it is coming initially.
    /// - Returns: Chat Document ID in the optional format.
    func getChatDocID(withMessageObj msgObj:[String : Any]?, isComingInitially : Bool) -> String? {
        if let chatDocID = chatViewModelObj?.docID {
            return chatDocID
        } else if let mqttID = self.productObj?.memberMqttId, let productID = self.productObj?.postId {
            let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbaseObj)
            if isComingInitially == true {
                return nil
            }
            guard let chatDocID = individualChatDocVMObject.getChatDocID(withRecieverID: mqttID, andSecretID: productID, withContactObj: self.chatViewModelObj?.chat, messageData: msgObj, withProductDetails: self.productObj) else {
                print("error in creating chat \(self)")
                return nil
            }
            return chatDocID
        }
        return nil
    }
    
    func setUpCollectionView() {
        self.recieverID = self.getRecieverID()
        self.chatDocID = self.getChatDocID(withMessageObj: nil, isComingInitially : true)
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: self.couchbaseObj)
        self.collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width:0.0, height:0.0)
        self.collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width:0.0, height:0.0)
        self.collectionView?.collectionViewLayout.springinessEnabled = false
        guard let chatDocID = self.chatDocID else { return }
        let unsortedMessages = chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
        let sortedMessages = unsortedMessages.uniq()
        self.messages = sortedMessages.sorted {
            guard let uniqueID1 = Int($0.messageId), let uniqueID2 = Int($1.messageId) else { return false }
            return uniqueID1 < uniqueID2
        }
        if self.messages.count == 0 {
            self.loadData()
        }
        self.collectionView?.reloadData()
        self.scrollToBottom(animated: true)
    }
    
    override func senderDisplayName() -> String {
        return (UIDevice.current.identifierForVendor?.uuidString)!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier ==  Constants.toCounterOfferSegue {
            guard let controller = segue.destination as? CounterOfferViewController, let message = sender as? Message else { return }
            controller.chatViewModelObj = chatViewModelObj
            controller.messageData = message
            controller.hidesBottomBarWhenPushed = true
            controller.userName = self.userName
        }
        else if segue.identifier ==  Constants.toLocationPicker {
            guard let controller = segue.destination as?  LocationSubmitTableViewController else { return }
            controller.locationSelectedDelegate = self
            
        }
        else if segue.identifier == Constants.paypalWebViewSegue {
            guard let controller = segue.destination as?  PayPalWebViewController, let url = sender as? URL else { return }
            controller.paypalLink = url
        }
        else if segue.identifier == Constants.imageControllerSegue {
            guard let controller = segue.destination as?  ImageViewerViewController, let url = sender as? String else { return }
            controller.imageUrl = url
        } else if segue.identifier == Constants.toPaypalViewcontroller {
            guard let controller = segue.destination as? PaypalViewController else { return }
            controller.paypalCallback = self
        }
        else if segue.identifier ==  Constants.showLocationSegue {
            guard let controller = segue.destination as?  ShowLocationViewController, let locationPoints = sender as? String else { return }
                controller.storeLocationStr = locationPoints
            
        }
    }
    
    @IBAction func leftButtonItemAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showUserProfileButtonAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC :UserProfileViewController = storyboard.instantiateViewController(withIdentifier:"userProfileStoryBoardId") as! UserProfileViewController
        
        // check for userName
        if let memberName = self.productObj?.membername {
            newVC.checkProfileOfUserNmae = memberName
        } else {
            if (self.chatViewModelObj?.initiated) == true {
                newVC.checkProfileOfUserNmae = self.chatViewModelObj?.memberName
            } else {
                newVC.checkProfileOfUserNmae = self.chatViewModelObj?.name
            }
        }
        newVC.checkingFriendsProfile = true
        newVC.productDetails = true
        newVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
}

extension ChatViewController {
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = (super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell)
        cell.backgroundColor = Color.clear
        let msg = self.messages[indexPath.item]
        if let messageMediaType:MessageTypes = msg.messageType {
            switch messageMediaType {
            case .image:
                if msg.isSelfMessage {
                    var cell : SentImageCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "SentImageCell", for: indexPath) as! SentImageCell
                    cell.msgObj = msg
                    cell.imageDelegate = self
                    cell = self.addLastDateAndTime(toCell: cell, withMessageObj: msg) as! SentImageCell
                    return cell
                } else {
                    //2 case for !isSelf and Offer = 2, is Accepted
                    var cell : ReceivedImageCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ReceivedImageCell", for: indexPath) as! ReceivedImageCell
                    cell.msgObj = msg
                    cell.imageDelegate = self
                    cell = self.addLastDateAndTime(toCell: cell, withMessageObj: msg) as! ReceivedImageCell
                    return cell
                }
                
            case .location:
                if msg.isSelfMessage {
                    var cell : SentLocationCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "SentLocationCell", for: indexPath) as! SentLocationCell
                    cell.backgroundColor = UIColor.clear
                    cell.msgObj = msg
                    cell.locationDelegate = self
                    cell = self.addLastDateAndTime(toCell: cell, withMessageObj: msg) as! SentLocationCell
                    return cell
                } else {
                    //2 case for !isSelf and Offer = 2, is Accepted
                    var cell : ReceivedLocationCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ReceivedLocationCell", for: indexPath) as! ReceivedLocationCell
                    cell.backgroundColor = UIColor.clear
                    cell.msgObj = msg
                    cell.locationDelegate = self
                    cell = self.addLastDateAndTime(toCell: cell, withMessageObj: msg) as! ReceivedLocationCell
                    return cell
                }
                
            case .order:
                if self.chatViewModelObj?.offerType == "2" { // If offer is Accepted
                    if msg.offerType == "4"{ // If Offer is accepted and self message.
                        if msg.isSelfMessage { // If offer is accepted and is self message and with Accpeted tag.
                            let cell : OfferCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "AcceptedPriceCollectionViewCell", for: indexPath) as! OfferCollectionViewCell
                            cell.backgroundColor = UIColor.clear
                            cell.chatVMObj = self.chatViewModelObj
                            cell.msgObj = msg
                            return cell
                        } else { // If offer is accpted but with the thank you tag.
                            let cell : ThankYouMessageCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ThankYouMessageCollectionViewCell", for: indexPath) as! ThankYouMessageCollectionViewCell
                            cell.backgroundColor = UIColor.clear
                            cell.chatVMObj = self.chatViewModelObj
                            cell.msgObj = msg
                            return cell
                        }
                    } else { // If offer is not selfs and received.
                        if msg.isSelfMessage { // If offer is accepted and is self message and with Accpeted tag.
                            let cell : OfferCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "OfferSentPriceCollectionViewCell", for: indexPath) as! OfferCollectionViewCell
                            cell.backgroundColor = UIColor.clear
                            cell.chatVMObj = self.chatViewModelObj
                            cell.msgObj = msg
                            return cell
                        } else { // FOr offer accepted
                            let cell : OfferCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "OfferReceivedPriceCollectionViewCell", for: indexPath) as! OfferCollectionViewCell
                            cell.backgroundColor = UIColor.clear
                            cell.chatVMObj = self.chatViewModelObj
                            cell.msgObj = msg
                            return cell
                        }
                    }
                } else {
                    if msg.isSelfMessage {
                        //1 case for isSelf and Offer = 1,3 is Not Accepted
                        let cell : OfferCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "OfferSentPriceCollectionViewCell", for: indexPath) as! OfferCollectionViewCell
                        cell.backgroundColor = UIColor.clear
                        cell.chatVMObj = self.chatViewModelObj
                        cell.msgObj = msg
                        return cell
                    } else {
                        //1 case for isSelf and Offer = 1,3  is Not Accepted
                        let cell : CounterOfferPriceCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "CounterOfferPriceCollectionViewCell", for: indexPath) as! CounterOfferPriceCollectionViewCell
                        cell.backgroundColor = UIColor.clear
                        cell.chatVMObj = self.chatViewModelObj
                        cell.msgObj = msg
                        cell.offerCellDelegate = self
                        return cell
                    }
                }
                
            case .audio:
                return cell
                
            case .contact:
                return cell
                
            case .doodle:
                return cell
                
            case .video:
                return cell
                
            case .sticker:
                return cell
                
            case .gif:
                return cell
                
            case .header:
                return cell
                
            case .payment:
                if msg.isSelfMessage {
                    //1 case for isSelf and Offer = 1,3 is Not Accepted
                    //PayPalLinkSharedCell
                    let cell : PayPalLinkSharedCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "PayPalLinkSharedCell", for: indexPath) as! PayPalLinkSharedCell
                    cell.backgroundColor = UIColor.clear
                    if let acceptedPrice = self.chatViewModelObj!.acceptedPrice, let currency = self.chatViewModelObj!.currency {
                        cell.priceText =  "\(currency) \(acceptedPrice)"
                    }
                    return cell
                } else {
                    //1 case for isSelf and Offer = 1,3  is Not Accepted
                    let cell : PaypalCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "paypalView", for: indexPath) as! PaypalCollectionViewCell
                    cell.msgObj = msg
                    cell.priceLabelOutlet.text = ""
                    if let acceptedPrice = self.chatViewModelObj!.acceptedPrice, let currency = self.chatViewModelObj!.currency {
                        cell.priceLabelOutlet.text = "\(currency) \(acceptedPrice)"
                    }
                    cell.paypalButtonDelegate = self
                    return cell
                }
            case .text:
                cell.timeLabelOutlet?.attributedText = self.setDateTime(fromMessageObj: msg)
                cell.currentStatusOutlet?.attributedText = self.setReadStatus(forMessage: msg)
                cell.dayLabelOutlet?.attributedText = self.setDay(fromMessageObj: msg)
                return cell
            }
        }
        return cell
    }
    
    func addLastDateAndTime(toCell cell: JSQMessagesCollectionViewCell, withMessageObj msgObj: Message) -> JSQMessagesCollectionViewCell {
        cell.timeLabelOutlet?.attributedText = self.setDateTime(fromMessageObj: msgObj)
        cell.currentStatusOutlet?.attributedText = self.setReadStatus(forMessage: msgObj)
        cell.dayLabelOutlet?.attributedText = self.setDay(fromMessageObj: msgObj)
        cell.backgroundColor = Color.clear
        return cell
    }
    
    func setDay(fromMessageObj message : Message)  -> NSAttributedString {
        guard let timestamp = message.timeStamp else { return NSAttributedString() }
        let dayStr = DateExtension().getDateString(fromTimeStamp: timestamp)
        let attribute = [NSForegroundColorAttributeName : UIColor.white]
        let str = NSAttributedString(string: dayStr, attributes: attribute)
        return str
    }
    
    func setDateTime(fromMessageObj message : Message)  -> NSAttributedString {
        guard let timestamp = message.timeStamp else { return NSAttributedString() }
        let lastmsgDate = DateExtension().getDateObj(fromTimeStamp: timestamp)
        let date = DateExtension().lastMessageInHours(date: lastmsgDate)
        let attribute = [NSForegroundColorAttributeName : UIColor.white]
        let str = NSAttributedString(string: date, attributes: attribute)
        return str
    }
    
    func setReadStatus(forMessage message : Message) -> NSAttributedString {
        let attribute = [NSForegroundColorAttributeName : UIColor.black]
        if message.isSelfMessage {
            var str = NSAttributedString(string: "✔︎", attributes: attribute)
            if message.messageStatus == "1" {
                let attribute = [NSForegroundColorAttributeName : UIColor.black]
                str = NSAttributedString(string: "✔︎", attributes: attribute)
                return str
            }
            if message.messageStatus == "2" {
                let attribute = [NSForegroundColorAttributeName : UIColor.black]
                str = NSAttributedString(string: "✔︎✔︎", attributes: attribute)
                return str
            }
            if message.messageStatus == "3" {
                let attribute = [NSForegroundColorAttributeName : UIColor.white]
                str = NSAttributedString(string: "✔︎✔︎", attributes: attribute)
                return str
            }
            return str
        }
        let str = NSAttributedString(string: "", attributes: attribute)
        return str
    }
    
    //MARK: Number of items in view
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    //MARK: Data on cell
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
        let data = self.messages[indexPath.row]
        return data
    }
    
    //MARK: To configure outgong or incomming bubble
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource? {
        let message:Message = messages[indexPath.item]
        if (self.senderId() == message.senderId) {
            return outgoingBubble
        } else {
            return incomingBubble
        }
    }
    
    //MARK: To show timestamp above message
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every 3rd message
         */
        if(indexPath.item == 0){
            let message = self.messages[indexPath.item]
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        else {
            let currentMessage = self.messages[indexPath.item]
            let previousMessage = self.messages[indexPath.item - 1]
            if compareDates(currentDate: currentMessage.date, previousDate: previousMessage.date){
                return nil
            }else{
                let message = self.messages[indexPath.item]
                return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
            }
        }
    }
    
    /// Used to get message object array as respect to the passed data.
    ///
    /// - Parameter messageData: data for the current message.
    /// - Returns: Array of Object with containg all the data of messages.
    func getMessageObject(fromData data: [String:Any], withStatus status: String, isSelf : Bool,fileSize : Double) -> [String:Any]? {
        
        guard let messageType = data["type"] as? String, let payload = data["payload"], let from = data["from"], let timeStamp = data["timestamp"], let id = data["id"], let to = data["to"] else { return nil }
        var params = [String : Any]()
        params = ["message":payload as Any,
                  "messageType":messageType as Any,
                  "isSelf":isSelf as Any,
                  "from": from as Any,
                  "to": to as Any,
                  "timestamp":timeStamp as Any,
                  "deliveryStatus":status as Any,
                  "id":id as Any,
                  "downloadStatus":"1" as Any,
                  "dataSize":fileSize as Any,
                  "thumbnail":"" as Any]
        if messageType == "1" {
            params["dataSize"] = data["dataSize"]
            params["thumbnail"] = data["thumbnail"]
        }
        
        return params
    }
    
    func compareDates(currentDate:Date, previousDate:Date)->Bool{
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: currentDate )
        let currentMonth = calendar.component(.month, from: currentDate)
        let currentDay = calendar.component(.day, from: currentDate)
        
        let previousYear = calendar.component(.year, from: previousDate)
        let previousMonth = calendar.component(.month, from: previousDate)
        let previousDay = calendar.component(.day, from: previousDate)
        
        if currentYear == previousYear && currentMonth == previousMonth && currentDay == previousDay{
            return true
        }
        else{
            return false
        }
    }
    
    //MARK: Height of cell to show timestamp
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        /**
         *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
         */
        
        /**
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         *  The other label height delegate methods should follow similarly
         *
         *  Show a timestamp for every 3rd message
         */
        
        if(indexPath.item == 0){
            return 25
        }
        else{
            let currentMessage = self.messages[indexPath.item]
            let previousMessage = self.messages[indexPath.item - 1]
            if compareDates(currentDate: currentMessage.date, previousDate: previousMessage.date) {
                return 0.0
            } else {
                return 25
            }
        }
    }
    
    //MARK:To show sender name
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         */
        /**
         *  iOS7-style sender name labels
         */
        let currentMessage = self.messages[indexPath.item]
        if currentMessage.senderId == self.senderId() {
            return 0.0
        }
        if indexPath.item - 1 > 0 {
            let previousMessage = self.messages[indexPath.item - 1]
            if previousMessage.senderId == currentMessage.senderId {
                return 0.0
            }
        }
        return 25
    }
    
    //MARK: Responding to collection view tap events
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapMessageBubbleAt indexPath: IndexPath) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        let message = self.messages[indexPath.row]
        if let messageType = message.messageType {
            if messageType == .image {
                self.performSegue(withIdentifier: "imageControllerSegue", sender: message.imageUrl)
            }
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellBottomLabelAt indexPath: IndexPath) -> CGFloat {
        return 5
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapCellAt indexPath: IndexPath, touchLocation: CGPoint) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
    }
    
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date) {
        self.updateMessageUIAfterSendingMessage(withText:text)
        self.finishSendingMessage()
    }
    
    /// This methods do the task for sending message from MQTT and update the couchbase also. With the passed text.
    ///
    /// - Parameter text: text in the string format.
    func updateMessageUIAfterSendingMessage(withText text: String) {
        guard let message = makeMessageForSendingBetweenServers(withText: text, andType: "0"), let receiverID = self.recieverID else { return }
        mqttChatManager.sendMessage(toChannel: "\(receiverID)", withMessage: message, withQOS: .atLeastOnce) //sending message here.
        guard let MsgObjForDB = self.getMessageObject(fromData: message, withStatus: "0", isSelf: true, fileSize: 0) else { return }
        var msgObj = MsgObjForDB
        msgObj["name"] = Helper.userName() as Any
        guard let dateString = DateExtension().getDateString(fromDate: Date()) else { return }
        msgObj["sentDate"] = dateString as Any
        msgObj["payload"] = msgObj["message"] as Any
        if messages.count == 0 {
            guard let chatDocID = self.chatDocID else { return }
            if let chatDta = couchbaseObj.getData(fromDocID: chatDocID) {
                let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
                chatsDocVMObject.updateChatData(withData: chatDta, msgObject : msgObj as Any, inDocID  : chatDocID, updateChatMsgs: false)
            }
        }
        if let chatDocID = self.chatDocID {
            self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
            let msgObj = Message(forData: msgObj, withDocID: chatDocID, isSelfMessage: true, andMessageobj: message, offerType: self.chatViewModelObj?.offerType, isMediaIncluded: false, includedMedia: nil, withImageURL: nil)
            messages.append(msgObj)
        } else {
            self.chatDocID = self.getChatDocID(withMessageObj: msgObj, isComingInitially: false)
            if let chatDocID = self.chatDocID {
                self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
                let mesageObj = Message(forData: msgObj, withDocID: chatDocID, isSelfMessage: true, andMessageobj: message, offerType : self.chatViewModelObj?.offerType, isMediaIncluded: false, includedMedia: nil, withImageURL: nil)
                messages.append(mesageObj)
            }
        }
        DispatchQueue.main.async {
            self.setUpCollectionView()
            self.scrollToBottom(animated: true)
            self.finishSendingMessage(animated: true)
        }
    }
    
    func makeMessageForSendingBetweenServers(withText text : String, andType type: String) -> [String:Any]? {
        let chatDocumentID = self.getChatDocID(withMessageObj:nil, isComingInitially: false)
        self.chatDocID = chatDocumentID
        self.chatViewModelObj?.chat.docID = chatDocumentID
        guard let productID = self.productID, let chatDocID = chatDocumentID, let receiverID = self.recieverID , let userName = Helper.userName(), let imageURL = Helper.userProfileImageUrl() else { return nil }
        let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
        var params = [String :Any]()
        params["receiverIdentifier"] = "" as Any
        params["from"] = self.senderId() as Any
        params["to"] = receiverID as Any
        params["payload"] = text.toBase64() as Any
        params["toDocId"] = chatDocID as Any
        params["timestamp"] = "\(timeStamp)" as Any
        params["id"] = "\(timeStamp)" as Any
        params["type"] = type as Any
        params["name"] = userName as Any
        params["secretId"] = productID as Any
        params["userImage"] = imageURL as Any
        return params
    }
    
    func makeMessageForSendingBetweenServers(withImageData imageData : String,withImageSize dataSize:Int, andImageURL imgUrl: String, withtimeStamp timestamp : String? ) -> [String:Any]? {
        guard let productID = self.productID else { return nil }
        var timeStamp = ""
        if let tStamp = timestamp {
            timeStamp = tStamp
        } else {
            timeStamp = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
        }
        var params = [String :Any]()
        params["receiverIdentifier"] = "" as Any
        params["from"] = self.senderId() as Any
        params["to"] = self.recieverID as Any
        params["payload"] = imgUrl.toBase64() as Any
        params["toDocId"] = chatDocID as Any
        params["timestamp"] = timeStamp as Any
        params["id"] = timeStamp as Any
        params["type"] = "1" as Any
        params["name"] = Helper.userName() as Any
        params["secretId"] = productID as Any
        params["userImage"] = Helper.userProfileImageUrl() as Any
        params["thumbnail"] = imageData
        params["dataSize"] = dataSize as Any
        return params
    }
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        /*
        let photoAction = UIAlertAction(title: "Photo Library", style: .default) { (action : UIAlertAction)
            in
            /**
             *  Opening gallery to select image
             */
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.mediaTypes = ["public.image"]
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        let locationAction = UIAlertAction(title: "Send Location", style: .default) { (action : UIAlertAction) in
            /**
             *  Adding location
             */
            //            let locationItem = self.buildLocationItem()
            self.performSegue(withIdentifier: Constants.toLocationPicker, sender: nil)
        }
        
        let cameraActrion = UIAlertAction(title: "Open camera", style: .default) {(action: UIAlertAction) in
            if(!UIImagePickerController.isSourceTypeAvailable(.camera)){
                
                let alert = UIAlertController.init(title: "Message", message: "Camera is not available in your device", preferredStyle: .alert)
                let action = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        */
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
      //  sheet.addAction(photoAction) // for picking the photos action
        //        sheet.addAction(videoAction)
        sheet.addAction(cancelAction) // cancel button action
       // sheet.addAction(cameraActrion) // camera open action
        //        sheet.addAction(audioAction)
      //  sheet.addAction(locationAction) // location picker action
        
        // PayPal link commented
        if let initiated = self.chatViewModelObj?.chat.initiated, let offerType = self.chatViewModelObj?.offerType {
            if initiated == false {
                sheet.addAction(UIAlertAction(title: "Share Paypal Link", style: .default, handler: {(action: UIAlertAction) in
                    if offerType != "2" {
                        let alert = UIAlertController.init(title: "Message", message: "Please accept the order first", preferredStyle: .alert)
                        let action = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                        return
                    } else {
                        if let paypalStr = Helper.getPayPalLink() {
                            if paypalStr.count > 0 {
                                self.updateMessageUIAfterSendingPaypalLink(withText: paypalStr)
                            }
                            else {
                                self.performSegue(withIdentifier: Constants.toPaypalViewcontroller, sender: nil)
                            }
                        } else {
                            self.performSegue(withIdentifier: Constants.toPaypalViewcontroller, sender: nil)
                        }
                    }
                }))
            }
        }
        self.present(sheet, animated: true, completion: nil)
    }
}

extension ChatViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let mediaType = info["UIImagePickerControllerMediaType"] as? String else { return }
        let msgObj = self.updateImage(withURL:"")
        if let msg = msgObj {
            messages.append(msg)
            self.collectionView?.reloadData()
            self.scrollToBottom(animated: true)
            if(mediaType == "public.image") {
                guard let imageToSend = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
                let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
                let name  = arc4random_uniform(900000) + 100000
                AFWrapper.updloadPhoto(withPhoto: imageToSend, andName: "\(name)\(timeStamp)", success: { (response) in
                    let fileName = "\(name)\(timeStamp).jpeg"
                    let url = "\(AppConstants.imageURL)/\(fileName)"
                    self.sendImageMessage(withImage: imageToSend, andUploadedUrl: url, withMsgObj: msg)
                    msg.imageUrl = url
                }, failure: { (error) in
                    print(error.localizedDescription)
                    self.setUpCollectionView()
                    self.showAlert("Failure", message: "Failed to upload the image")
                })
                self.dismiss(animated: true, completion: nil)
            }
            else{
                guard let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL else { return }
                let strURL = AppConstants.constantURL + AppConstants.uploadPhoto
                AFWrapper.updateVideoByUsingMultipart(serviceName: strURL, videoURL: videoURL, success: { (response) in
                    print(response)
                }, failure: { (error) in
                    print(error)
                })
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    /// This will update the JSQPhotoMediaItem into the messages and also to the database with sending it through the MQTT.
    ///
    /// - Parameter mediaItem: JSQ Photo media item.
    /// - Returns: message object.
    func updateImage(withURL url : String) -> Message? {
        //1 . Creating message Object with media item.
        guard let msgData = self.makeMessageForSendingBetweenServers(withImageData: "", withImageSize: 0, andImageURL: url,withtimeStamp:nil) else { return nil }
        guard let MsgObjForDB = self.getMessageObject(fromData: msgData, withStatus: "0", isSelf: true, fileSize: 0) else { return nil }
        if let chatDocID = self.chatDocID {
            let mesageObj = Message(forData: MsgObjForDB, withDocID: chatDocID, isSelfMessage: true, andMessageobj: msgData, offerType : self.chatViewModelObj?.offerType, isMediaIncluded: false,includedMedia : nil, withImageURL : url)
            //2. Updating message object to message array.
            return mesageObj
        } else {
            self.chatDocID = self.getChatDocID(withMessageObj: MsgObjForDB, isComingInitially: false)
            if let chatDocID = self.chatDocID {
                let mesageObj = Message(forData: MsgObjForDB, withDocID: chatDocID, isSelfMessage: true, andMessageobj: msgData, offerType : self.chatViewModelObj?.offerType, isMediaIncluded: false, includedMedia : nil, withImageURL : url)
                //2. Updating message object to message array.
                return mesageObj
            }
        }
        return nil
    }
    
    func sendImageMessage(withImage image: UIImage, andUploadedUrl imageURL : String, withMsgObj mssageObj : Message) {
        // 4. sending message as an Image.
        guard let imageData = self.createThumbnail(forImage: image) else { return }
        let imgData: NSData = NSData(data: UIImageJPEGRepresentation((image), 1)!)
        let imageSize: Int = imgData.length
        guard let msgData = self.makeMessageForSendingBetweenServers(withImageData: imageData, withImageSize: imageSize, andImageURL: imageURL, withtimeStamp : mssageObj.timeStamp) else { return }
        mqttChatManager.sendMessage(toChannel: "\(self.recieverID!)", withMessage: msgData, withQOS: .atLeastOnce)
        guard let MsgObjForDB = self.getMessageObject(fromData: msgData, withStatus: "0", isSelf: true, fileSize: 0) , let receiverName = Helper.userName()  else { return }
        var msgObj = MsgObjForDB
        msgObj["name"] = receiverName as Any
        guard let dateString = DateExtension().getDateString(fromDate: Date()) else { return }
        msgObj["sentDate"] = dateString as Any
        msgObj["payload"] = msgObj["message"] as Any
        if messages.count == 0 {
            guard let chatDocID = self.chatDocID else { return }
            if let chatDta = couchbaseObj.getData(fromDocID: chatDocID) {
                let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
                chatsDocVMObject.updateChatData(withData: chatDta, msgObject : msgObj as Any, inDocID  : chatDocID, updateChatMsgs: false)
            }
        }
        if let chatDocID = self.chatDocID {
            self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
        }
        DispatchQueue.main.async {
            self.setUpCollectionView()
            self.scrollToBottom(animated: true)
            self.finishSendingMessage(animated: true)
        }
    }
    
    func createThumbnail(forImage image : UIImage) -> String?{
        // Define thumbnail size
        let size = CGSize(width: 70, height: 70)
        
        // Define rect for thumbnail
        let scale = max(size.width/image.size.width, size.height/image.size.height)
        let width = image.size.width * scale
        let height = image.size.height * scale
        let x = (size.width - width) / CGFloat(2)
        let y = (size.height - height) / CGFloat(2)
        let thumbnailRect = CGRect(x: x, y: y, width: width, height: height)
        
        // Generate thumbnail from image
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: thumbnailRect)
        guard let thumbnail = UIGraphicsGetImageFromCurrentImageContext() else { return  nil }
        UIGraphicsEndImageContext()
        let imageData = ImageExtension.convertImageToBase64(image: thumbnail)
        return imageData
    }
    
    /*
     func buildVideoItem(videoURL : NSURL) -> JSQVideoMediaItem {
     let videoItem = JSQVideoMediaItem(fileURL: videoURL as URL, isReadyToPlay: true, thumbnailImage: thumbnail(sourceURL: videoURL))
     return videoItem
     }
     
     func thumbnail(sourceURL:NSURL) -> UIImage {
     let asset = AVAsset(url: sourceURL as URL)
     let imageGenerator = AVAssetImageGenerator(asset: asset)
     let time = CMTime(seconds: 5, preferredTimescale: 1)
     
     do {
     let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
     return UIImage(cgImage: imageRef)
     } catch {
     //            print(error)
     return UIImage(named: "play")!
     }
     }
     
     func buildAudioItem() -> JSQAudioMediaItem {
     let sample = Bundle.main.path(forResource: "jsq_messages_sample", ofType: "m4a")
     let audioData = NSData(contentsOf: NSURL.fileURL(withPath: sample!))
     let audioItem = JSQAudioMediaItem(data: audioData! as Data)
     return audioItem
     }
     */
}

extension ChatViewController {
    func receivedMessage(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        let status = userInfo["status"] as! String
        guard let content = userInfo["message"] as? [String : Any] else { return }
        switch status {
        case "1","2", "3" :
            self.chatsDocVMObject.updateStatusForChatMessages(withStatus: status, chatDocID: self.getChatDocID(withMessageObj: content, isComingInitially: false))
            self.updateChatStatus()
        default:
            self.updateCurrentChatMessages(withmessage: content)
        }
    }
    
    func updateChat(withAcceptedMessage message: [String : Any]) -> Message? {
        guard let chatDociD = self.chatDocID else { return nil }
        let messageObj = Message(forData: message, withDocID: chatDociD, isSelfMessage: true, andMessageobj: message, offerType: "4", isMediaIncluded: false, includedMedia: nil, withImageURL : nil)
        return messageObj
    }
    
    func receivedLastSeenStatus(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        guard let message = userInfo["message"] as? [String : Any] else { return }
        if ((message["userId"] as? String) == self.recieverID) {
            if ((message["status"] as? Int) == 1) { // online
                self.currentChatStatusLabelOutlet.text = "Online"
            } else if ((message["status"] as? Int) == 0), let timeStamp = message["timestamp"] as? String { //offline
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                    guard let date = DateExtension().getDate(forLastSeenTimeString: timeStamp) else { return }
                    let timeStampTime = "last seen \(DateExtension().lastSeenTime(date: date))"
                    self.currentChatStatusLabelOutlet.text = timeStampTime
                })
            }
        }
    }
    
    func recievedPostDetailsUpdate(notification: NSNotification) {
        let chatsDocVMObj = ChatsDocumentViewModel(couchbase: self.couchbaseObj)
        if let chatObject = chatsDocVMObj.getChatObject(forChatDocID: self.chatViewModelObj?.docID, chatUserID: self.chatViewModelObj?.userID, productID: self.getProductID()) {
            self.chatViewModelObj = ChatViewModel(withChatData: chatObject)
            self.updateProductDetails()
            
        }
        self.setupProductNoLongerExist()
    }
    
    func receivedTypingStatus(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        guard let message = userInfo["message"] as? [String : Any], let toId =  message["to"] as? String, let secretID =  message["secretId"] as? String else { return }
        if self.selfID == toId && self.chatViewModelObj?.productId == secretID {
            self.currentChatStatusLabelOutlet.text = "Typing..."
            if !isTypingVisible {
                self.isTypingVisible = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.isTypingVisible = false
                    self.currentChatStatusLabelOutlet.text = "Online"
                })
            }
        }
    }
    
    func didRecieve(withMessage message: Any, inTopic topic: String) {
        guard let msgServerObj = message as? [String:Any] else { return }
        if let fromID = msgServerObj["from"] as? String {
            if fromID == selfID! {
            } else {
                if let chatDocID = self.chatDocID {
                    self.messages = chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
                    self.finishReceivingMessage()
                } else {
                    self.chatDocID = self.getChatDocID(withMessageObj: msgServerObj, isComingInitially: false)
                    if let chatDocID = self.chatDocID {
                        self.messages = chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
                        self.finishReceivingMessage()
                    }
                }
            }
        }
    }
}

extension ChatViewController {
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        self.sendTypingStatus()
    }
    
    func updateCurrentChatMessages(withmessage message: [String : Any]) {
        let state = UIApplication.shared.applicationState
        if state == .background {
            return
        }
        self.chatsDocVMObject.updateMessageStatus(withMessageObj: message)
        if let chatDocID = self.chatDocID {
            self.messages = self.chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
            self.finishReceivingMessage()
            self.sendReadAcknowledgment(withMessage: message)
        } else {
            self.chatDocID = self.getChatDocID(withMessageObj: message, isComingInitially: false)
            if let chatDocID = self.chatDocID {
                self.messages = self.chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
                self.finishReceivingMessage()
                self.sendReadAcknowledgment(withMessage: message)
            }
        }
    }
    
    func sendReadAcknowledgment(withMessage message : [String : Any]) {
        guard let params = self.chatsDocVMObject.getMessageObjectForUpdatingStatus(withData: message, andStatus: "3") as? [String:Any] else { return }
        mqttChatManager.sendAcknowledgment(toChannel: "\(AppConstants.MQTT.acknowledgementTopicName)\(self.recieverID!)", withMessage: params, withQOS: .atMostOnce)
    }
    
    func updateChatForReadMessage(toDocID docID : String,isControllerAppearing : Bool) {
       if var chatData = couchbaseObj.getData(fromDocID: docID)
       {
        if ((chatData["hasNewMessage"] as? Bool) == true){
            // here we are sending the recieved acknowledgment for last message.
            if isControllerAppearing{
                self.fetchLastMessageFromSenderAndSendAcknowledgment(withChatData: chatData)
            }
        }
        chatData["hasNewMessage"] = false
        chatData["newMessageCount"] = 0
        couchbaseObj.updateData(data: chatData, toDocID: docID)
        }
    }
    
    func updateChatForAlertPopUp(toDocID docID : String) {
        if var chatData = couchbaseObj.getData(fromDocID: docID) {
            self.chatViewModelObj?.chat.wasInvited = true
            chatData["wasInvited"] = 1 as Any
            couchbaseObj.updateData(data: chatData, toDocID: docID)
        }
    }
    
    func fetchLastMessageFromSenderAndSendAcknowledgment(withChatData chatData: [String:Any]) {
        let msgArray = chatData["messageArray"] as! [[String:Any]]
        for (_, messageData) in msgArray.enumerated() {
            if ((messageData["timestamp"] as? String) == (chatData["lastMessageDate"] as? String)){
                self.sendReadAcknowledgment(withMessage: messageData)
            }
        }
    }
    
    func updateChatStatus() {
        guard let chatDocID = self.chatDocID else {  return }
        self.messages = self.chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
        DispatchQueue.main.async {
            self.setUpCollectionView()
            self.scrollToBottom(animated: true)
            self.finishReceivingMessage()
        }
    }
}

extension ChatViewController : OfferButtonPressedDelegate {
    func acceptButtonPressed(withMessageDetails messageObj: Message) {
        guard let price = messageObj.messagePayload, let productID = self.productID, let chatDociD = self.chatDocID else { return }
        self.chatViewModelObj?.offerAccepted(withMessageObj: messageObj, andUserName: self.userName, completionBlock: { _ in
            let messageVMObj = MessageViewModal(withMessageObject: messageObj)
            let acceptedMessage = messageVMObj.getAcceptedMessageObject()
            let msgDict = messageVMObj.getMessageDictionaryForAcceptedOffer(withProductID: productID, andPrice: price)
            self.chatsDocVMObject.updateChatDoc(withMsgObj: msgDict, toDocID: chatDociD)
            self.messages.append(acceptedMessage)
            DispatchQueue.main.async {
                self.setUpCollectionView()
                self.scrollToBottom(animated: true)
                self.finishSendingMessage(animated: true)
            }
        })
    }
    
    func counterOfferButtonPressed(withMessageDetails messageObj: Message) {
        print(messageObj)
        self.performSegue(withIdentifier: Constants.toCounterOfferSegue, sender: messageObj)
    }
}

extension ChatViewController : LocationFetchedDelegate {
    func locationSelected(_ locationObj: [AnyHashable : Any]!) {
        if let location = locationObj as? [String : Any] {
            if let latlong = location["latlog"] as? String, let name = location["name"] as? String, let address = location["address"] as? String {
                let msgStr = "(\(latlong))@@\(name)@@\(address)"
                self.updateUIAfterSendingLocation(withMsg: msgStr)
            }
        }
    }
    
    func updateUIAfterSendingLocation(withMsg message: String) {
        guard let message = makeMessageForSendingBetweenServers(withText: message, andType: "3") else { return }
        mqttChatManager.sendMessage(toChannel: "\(self.recieverID!)", withMessage: message, withQOS: .atLeastOnce)
        guard let MsgObjForDB = self.getMessageObject(fromData: message, withStatus: "0", isSelf: true, fileSize: 0) else { return }
        var msgObj = MsgObjForDB
        msgObj["name"] = Helper.userName()
        guard let dateString = DateExtension().getDateString(fromDate: Date()) else { return }
        msgObj["sentDate"] = dateString as Any
        msgObj["payload"] = msgObj["message"] as Any
        if messages.count == 0 {
            guard let chatDocID = self.chatDocID else { return }
            if let chatDta = couchbaseObj.getData(fromDocID: chatDocID) {
                let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
                chatsDocVMObject.updateChatData(withData: chatDta, msgObject : msgObj as Any, inDocID  : chatDocID, updateChatMsgs: false)
            }
        }
        if let chatDocID = self.chatDocID {
            self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
            let mesageObj = Message(forData: msgObj, withDocID: chatDocID, isSelfMessage: true, andMessageobj: message, offerType : self.chatViewModelObj?.offerType, isMediaIncluded: false, includedMedia: nil, withImageURL : nil)
            messages.append(mesageObj)
            DispatchQueue.main.async {
                self.setUpCollectionView()
                self.scrollToBottom(animated: true)
                self.finishSendingMessage(animated: true)
            }
        } else {
            self.chatDocID = self.getChatDocID(withMessageObj: msgObj, isComingInitially: false)
            if let chatDocID = self.chatDocID {
                self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
                let mesageObj = Message(forData: msgObj, withDocID: chatDocID, isSelfMessage: true, andMessageobj: message, offerType : self.chatViewModelObj?.offerType, isMediaIncluded: false, includedMedia: nil, withImageURL : nil)
                messages.append(mesageObj)
                DispatchQueue.main.async {
                    self.setUpCollectionView()
                    self.scrollToBottom(animated: true)
                    self.finishSendingMessage(animated: true)
                }
            }
        }
    }
}

extension ChatViewController : PaypalButtonPressedDelegate {
    func paypalButtonPressed(withMessageDetails message: Message) {
        print(message)
        var msgStr = ""
        if let msg = message.messagePayload?.fromBase64() {
            msgStr = msg
        } else {
            msgStr = message.messagePayload!
        }
        guard let paypalURL = URL(string : msgStr) else { return }
        self.performSegue(withIdentifier: Constants.paypalWebViewSegue, sender: paypalURL)
    }
    
    func updateMessageUIAfterSendingPaypalLink(withText text: String) {
        guard let message = makeMessageForSendingBetweenServers(withText: text, andType: "16") else { return }
        mqttChatManager.sendMessage(toChannel: "\(self.recieverID!)", withMessage: message, withQOS: .atLeastOnce)
        guard let MsgObjForDB = self.getMessageObject(fromData: message, withStatus: "0", isSelf: true, fileSize: 0) , let receiverName = Helper.userName()  else { return }
        var msgObj = MsgObjForDB
        msgObj["name"] = receiverName as Any
        guard let dateString = DateExtension().getDateString(fromDate: Date()) else { return }
        msgObj["sentDate"] = dateString as Any
        msgObj["payload"] = msgObj["message"] as Any
        if messages.count == 0 {
            guard let chatDocID = self.chatDocID else { return }
            if let chatDta = couchbaseObj.getData(fromDocID: chatDocID) {
                let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
                chatsDocVMObject.updateChatData(withData: chatDta, msgObject : msgObj as Any, inDocID  : chatDocID, updateChatMsgs: false)
            }
        }
        if let chatDocID = self.chatDocID {
            self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
            let mesageObj = Message(forData: msgObj, withDocID: chatDocID, isSelfMessage: true, andMessageobj: message, offerType : self.chatViewModelObj?.offerType, isMediaIncluded: false, includedMedia: nil, withImageURL : nil)
            messages.append(mesageObj)
            DispatchQueue.main.async {
                self.setUpCollectionView()
                self.scrollToBottom(animated: true)
                self.finishSendingMessage(animated: true)
            }
        } else {
            self.chatDocID = self.getChatDocID(withMessageObj: msgObj, isComingInitially: false)
            if let chatDocID = self.chatDocID {
                self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
                let mesageObj = Message(forData: msgObj, withDocID: chatDocID, isSelfMessage: true, andMessageobj: message, offerType : self.chatViewModelObj?.offerType, isMediaIncluded: false, includedMedia: nil, withImageURL : nil)
                messages.append(mesageObj)
                DispatchQueue.main.async {
                    self.setUpCollectionView()
                    self.scrollToBottom(animated: true)
                    self.finishSendingMessage(animated: true)
                }
            }
        }
    }
    
    // MARK : details button action
    override func detailsButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC :ProductDetailsViewController = storyboard.instantiateViewController(withIdentifier:mInstaTableVcStoryBoardId) as! ProductDetailsViewController
        newVC.hidesBottomBarWhenPushed = true;
        newVC.postId = self.getProductID()
        newVC.noAnimation = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
}

extension ChatViewController : payPalDelegate {
    func paypalLinkIsSaved(_ paypalLink: String!) {
        if let paypalStr = Helper.getPayPalLink() {
            if paypalStr.count > 0 {
                self.updateMessageUIAfterSendingPaypalLink(withText: paypalStr)
            }
        }
    }
}

extension JSQMessagesInputToolbar {
    override open func didMoveToWindow() {
        super.didMoveToWindow()
        if #available(iOS 11.0, *) {
            if self.window?.safeAreaLayoutGuide != nil {
                self.bottomAnchor.constraintLessThanOrEqualToSystemSpacingBelow((self.window?.safeAreaLayoutGuide.bottomAnchor)!, multiplier: 1.0).isActive = true
            }
        }
    }
}

extension ChatViewController : SentImageCellDelegate, ReceivedImageCellDelegate{
    func tapOnSentImageCellDelegate(messageObj: Message) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        self.goToImageViewer(Message: messageObj)
    }
    
    func tapOnReceivedImageCellDelegate(messageObj: Message) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        self.goToImageViewer(Message: messageObj)
    }
    
    func goToImageViewer(Message: Message) {
        if let messageType = Message.messageType {
            if messageType == MessageTypes.image {
                if (Message.imageUrl != nil) {
                    self.performSegue(withIdentifier: "imageControllerSegue", sender: Message.imageUrl)
                }
                else {
                    self.performSegue(withIdentifier: "imageControllerSegue", sender: Message.messagePayload)
                }
            }
        }
    }
}

extension ChatViewController {
    func addThankYouMessageToChat(withMessage messageObj : Message, productID : String, chatDocID : String, price : String ) {
        let msgCopy = messageObj.copy() as! Message
        let messageVMObj = MessageViewModal(withMessageObject: msgCopy)
        let acceptedMessage = messageVMObj.getThankYouMessageObject()
        let msgDict = messageVMObj.getMessageDictionaryForAcceptedOffer(withProductID: productID, andPrice: price)
        self.chatsDocVMObject.updateChatDoc(withMsgObj: msgDict, toDocID: chatDocID)
        self.messages.append(acceptedMessage)
        self.finishSendingMessage(animated: true)
    }
    
    func addAcceptedMessageToChat(withMessage messageObj : Message, productID : String, chatDocID : String, price : String ) {
        let msgCopy = messageObj.copy() as! Message
        let messageVMObj = MessageViewModal(withMessageObject: msgCopy)
        let acceptedMessage = messageVMObj.getAcceptedMessageObject()
        let msgDict = messageVMObj.getMessageDictionaryForAcceptedOffer(withProductID: productID, andPrice: price)
        self.chatsDocVMObject.updateChatDoc(withMsgObj: msgDict, toDocID: chatDocID)
        self.messages.append(acceptedMessage)
        self.finishSendingMessage(animated: true)
    }
}


extension ChatViewController : ReceivedLocationCellDelegate, SentLocationCellDelegate{
    
    func tapOnReceivedLocationDelegate(messageObj: Message) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        self.goToShowLocationViewController(message: messageObj)
    }
    
    func tapOnSentLocationDelegate(messageObj: Message) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        self.goToShowLocationViewController(message: messageObj)
    }
    
    func goToShowLocationViewController(message: Message) {
        if let messageType = message.messageType {
            if messageType == MessageTypes.location {
                if (message.messagePayload != nil) {
                    self.performSegue(withIdentifier: Constants.showLocationSegue, sender: message.messagePayload)
                }
            }
        }
    }
}

extension Sequence where Iterator.Element: Hashable {
    func uniq() -> [Iterator.Element] {
        var seen = Set<Iterator.Element>()
        return filter { seen.update(with: $0) == nil }
    }
}

func == (lhs: Message, rhs: Message) -> Bool {
    guard let lhsTimeStamp = Int(lhs.messageId), let rhsTimeStamp = Int(rhs.messageId) else { return false }
    return (lhsTimeStamp == rhsTimeStamp)
}

