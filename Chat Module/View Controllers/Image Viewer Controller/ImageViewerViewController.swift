//
//  ImageViewerViewController.swift
//  Resell
//
//  Created by Sachin Nautiyal on 16/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class ImageViewerViewController: UIViewController {

    @IBOutlet weak var progressViewOutlet: UIProgressView!
    @IBOutlet weak var imageViewOutlet: UIImageView!
    
    var imageUrl : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let imageurl = URL(string : imageUrl)
        self.imageViewOutlet.kf.setImage(with: imageurl, placeholder:UIImage(named:"itemProdDefault"), options:  [.transition(ImageTransition.fade(1))], progressBlock:
            { receivedSize, totalSize in
                let value = receivedSize/totalSize
                self.progressViewOutlet.isHidden = false
                print(value)
                self.progressViewOutlet.progress = Float(value)
        }) { (image, error, cacheType, imageUrl) in
            print("finished")
            self.progressViewOutlet.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
