//
//  LocationSubmitTableViewController.h
//
//  Created by Rahul Sharma on 3/10/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol LocationFetchedDelegate <NSObject>
-(void)locationSelected:(NSDictionary *)locationObj;
@end

@interface LocationSubmitTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *navtitleBtn;
@property (weak, nonatomic) id <LocationFetchedDelegate> locationSelectedDelegate;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnNavRightRefresh;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnNavLeftCancel;


- (IBAction)navCancelBtnCliked:(id)sender;
- (IBAction)navtitleBtnCliked:(id)sender;
- (IBAction)navRefreshBtn:(id)sender;

@end
