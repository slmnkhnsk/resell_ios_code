//
//  LocationSubmitTableViewController.m
//
//  Created by Rahul Sharma on 3/10/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import "LocationSubmitTableViewController.h"
#import "LocationCellTableViewCell.h"
#import "PickUpaddressTableViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Helper.h"


//AIzaSyBgP0zBlOZIs2_-5ZKUadAAdR6-m_WCMGM

#define kGOOGLE_API_KEY @"AIzaSyBgP0zBlOZIs2_-5ZKUadAAdR6-m_WCMGM"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

@interface LocationSubmitTableViewController ()<MKMapViewDelegate,CLLocationManagerDelegate,gotAddress,UIAlertViewDelegate>
{
    
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentCentre;
    int currenDist;
    NSMutableArray *cityArr;
    NSMutableArray *placeNameArr;
    NSMutableArray *latLogArr;
    NSMutableArray *iconArr;
    
}
@property (nonatomic,weak) UISearchBar *searchBar;

@end

@implementation LocationSubmitTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _navtitleBtn.layer.cornerRadius = 5.0;
    _navtitleBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _navtitleBtn.layer.borderWidth = 0.5;
    _navtitleBtn.clipsToBounds = YES;
    
    // _mapView.showsCompass = YES;
    
    [self.btnNavLeftCancel setTintColor:mBaseColor];
    [self.btnNavRightRefresh setTintColor:mBaseColor];
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    
    //Set some parameters for the location object.
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        _mapView.delegate = self;
        _mapView.showsUserLocation =YES;
        [locationManager  requestWhenInUseAuthorization];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Service Disabled"
                                                        message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.delegate = self;
        [alert show];
    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSNumber *val =  [[NSUserDefaults standardUserDefaults] objectForKey:@"serachbuttoncliked"];
    if (val.boolValue) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"serachbuttoncliked"];
        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    int count =0;
    if (placeNameArr.count ==0) {
        count =  2;
    }
    else{
        count = (int)[placeNameArr count]+1;;
    }
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LocationCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];
    if (indexPath.row ==0) {
        
        cell.titleLabel.text = @"Send Your Location";
        cell.titleLabel.textColor = [UIColor blueColor];
        cell.subtitleLabel.textColor = [UIColor colorWithRed:(147.0/255.0) green:(147.0/255.0) blue:(147.0/255.0) alpha:1];
        cell.subtitleLabel.text = @"current";
        cell.imageViewicon.image = [UIImage imageNamed:@"send_your_location_location_icon"];
        
    }else{
        
        if (placeNameArr.count ==0) {
            
            if (indexPath.row == 1) {
                
                cell.titleLabel.text =@"Searching....";
                cell.subtitleLabel.text = @"";
                
            }
            
        }else{
            
            
            cell.titleLabel.text = [placeNameArr objectAtIndex:indexPath.row-1];
            cell.subtitleLabel.text = [cityArr objectAtIndex:indexPath.row -1];
            cell.subtitleLabel.textColor = [UIColor colorWithRed:(147.0/255.0) green:(147.0/255.0) blue:(147.0/255.0) alpha:1];
            cell.titleLabel.textColor = [UIColor blackColor];
            cell.imageViewicon.image = [UIImage imageNamed:@"contacts_info_image_frame"];
            [cell.imageViewicon sd_setImageWithURL:[NSURL URLWithString:[iconArr objectAtIndex:indexPath.row-1]]
                                  placeholderImage:[UIImage imageNamed:@"contacts_info_image_frame"]];
            // NSString *url = [iconArr objectAtIndex:indexPath.row -1];
            // NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
            //[cell.imageView setImage:[UIImage imageWithData:data]];
        }
        
        
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row ==0) {
        
        NSString *lat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        NSString *log = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        //  NSArray *Arr = @[lat,log];
        // NSString *latlog = [NSString stringWithFormat:@"%@",Arr];
        NSString *latlog = [NSString stringWithFormat:@"%@,%@",lat,log];
        
        NSDictionary *dict = @{
                               @"name":@"",
                               @"address":@"current location",
                               @"latlog":latlog
                               
                               };
        
        if([_locationSelectedDelegate respondsToSelector:@selector(locationSelected:)]) {
            [_locationSelectedDelegate locationSelected:dict];
        }
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        if (placeNameArr != nil) {
            NSString *name = [NSString stringWithFormat:@"%@",[placeNameArr objectAtIndex:indexPath.row -1]];
            NSString *address = [NSString stringWithFormat:@"%@",[cityArr objectAtIndex:indexPath.row -1]];
            NSString *latlog = [NSString stringWithFormat:@"%@",[latLogArr objectAtIndex:indexPath.row -1]];
            
            NSDictionary *dict = @{
                                   @"name":name,
                                   @"address":address,
                                   @"latlog":latlog
                                   };
            if([_locationSelectedDelegate respondsToSelector:@selector(locationSelected:)]) {
                [_locationSelectedDelegate locationSelected:dict];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}


-(void)queryGooglePlaces:(NSString *)googleType{
    //currenDist  =500;
    
    //  NSLog(@"lat =%f log =%f",locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude);
    
    //  NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=%@&types=%@&sensor=true&key=%@", locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude, [NSString stringWithFormat:@"%i",currenDist], googleType, kGOOGLE_API_KEY];
    
    
    NSString *url = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/search?client_id=OOZHXARNKFGUATVDGV2A3QMY5IWZPBCOMYH3PV1GYVH0LN5Y&client_secret=SAZX0KD50HLQ2RSIPXR0UQLNVWOEBJTI2YSSD2H0SD4SKVOX&v=20130815&ll=%f,%f&query=%@",locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude,@""
                     ];
    
    
    //  NSString *url = @"https://maps.googleapis.com/maps/api/place/search/json?location=-33.8670522,151.1957362&radius=500&types=food&sensor=flase&key=AIzaSyBgP0zBlOZIs2_-5ZKUadAAdR6-m_WCMGM";
    
    NSURL *urlReq = [NSURL URLWithString:url];
    
    dispatch_async(kBgQueue, ^{
        NSData *data =[NSData dataWithContentsOfURL:urlReq];
        [self performSelectorOnMainThread:@selector(fetchData:) withObject:data waitUntilDone:YES];
    });
    
}

-(void)fetchData:(NSData*)responsedata{
    
    
    NSError *error ;
    NSDictionary *json  =[NSJSONSerialization JSONObjectWithData:responsedata options:kNilOptions error:&error];
    
    cityArr = [NSMutableArray new];
    placeNameArr =[NSMutableArray new];
    latLogArr = [NSMutableArray new];
    iconArr = [NSMutableArray new];
    
    
    NSDictionary *dict = json[@"response"];
    
    NSArray* places = [dict objectForKey:@"venues"];
    NSString *icon;
    
    for (NSDictionary *dict in places) {
        
        if(dict[@"categories"]){
            NSArray *arr = dict[@"categories"];
            if (arr.count>0) {
                icon = [NSString stringWithFormat:@"%@bg_32%@",arr[0][@"icon"][@"prefix"],arr[0][@"icon"][@"suffix"]];
            }else{
                icon = [NSString stringWithFormat:@"contacts_info_sup_icon_off"];
            }
            
        }else{
            
            icon = [NSString stringWithFormat:@"contacts_info_sup_icon_off"];
        }
        
        NSDictionary *temp = dict[@"location"];
        
        if (temp[@"address"]) {
            [cityArr addObject:[NSString stringWithFormat:@"%@",temp[@"address"]]];
        }else{
            [cityArr addObject:[NSString stringWithFormat:@"%@",@""]];
        }
        
        if (dict[@"name"]) {
            [placeNameArr addObject:[NSString stringWithFormat:@"%@",dict[@"name"]]];
        }else{
            [placeNameArr addObject:[NSString stringWithFormat:@"%@",@""]];
        }
        
        if (temp[@"lat"] && temp[@"lng"]) {
            [latLogArr addObject:[NSString stringWithFormat:@"%@,%@",temp[@"lat"],temp[@"lng"]]];
            [iconArr addObject:[NSString stringWithFormat:@"%@",icon]];
        }else{
            [latLogArr addObject:[NSString stringWithFormat:@"%@,%@",@"0",@"0"]];
            [iconArr addObject:[NSString stringWithFormat:@"%@",icon]];
        }
        
        
    }
    
    [self.tableView reloadData];
    
    
}

#pragma MKMapKit delegates

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views {
    MKCoordinateRegion region;
    region = MKCoordinateRegionMakeWithDistance(locationManager.location.coordinate,800,800);
    [mv setRegion:region animated:YES];
    //@"address" //locality
    
    NSString *lat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    NSString *log = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    [[NSUserDefaults standardUserDefaults]setObject:lat forKey:@"latitude"];
    [[NSUserDefaults standardUserDefaults] setObject:log forKey:@"logitute"];
    [self queryGooglePlaces:@"address"];
    
}



-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    //Get the east and west points on the map so you can calculate the distance (zoom level) of the current map view.
    MKMapRect mRect = self.mapView.visibleMapRect;
    MKMapPoint eastMapPoint = MKMapPointMake(MKMapRectGetMinX(mRect), MKMapRectGetMidY(mRect));
    MKMapPoint westMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), MKMapRectGetMidY(mRect));
    
    //Set your current distance instance variable.
    currenDist = MKMetersBetweenMapPoints(eastMapPoint, westMapPoint);
    
    //Set your current center point on the map instance variable.
    currentCentre = self.mapView.centerCoordinate;
    
    
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    
}

- (IBAction)navCancelBtnCliked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)navtitleBtnCliked:(id)sender {
    
    UIStoryboard *Story =[UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
    PickUpaddressTableViewController *pick = [Story instantiateViewControllerWithIdentifier:@"PickUpaddressTableViewController"];
    pick.delegate = self;
    
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:pick];
    
    [self presentViewController:nav animated:NO completion:nil];
    
}

- (IBAction)navRefreshBtn:(id)sender {
    
    [self queryGooglePlaces:@"address"];
    
}

-(void)arrayOFaddress:(NSMutableArray *)places{
    
    // NSLog(@"palace =%@",places);
    
}

-(void)addressSelectedWithData:(NSDictionary *)data {
    if([_locationSelectedDelegate respondsToSelector:@selector(locationSelected:)]) {
        [_locationSelectedDelegate locationSelected:data];
    }
}

#pragma alertView delegetes

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}


@end
