//
//  PayPalWebViewController.swift
//  Resell
//
//  Created by Sachin Nautiyal on 06/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import WebKit

class PayPalWebViewController: UIViewController {
    
    @IBOutlet weak var webViewOutlet: UIView!
    @IBOutlet weak var progressIndicatorView: UIProgressView!
    var webView : WKWebView!
    var paypalLink : URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createNavLeftButton()
        self.title = "Paypal Payment"
        if let paypalURL = paypalLink {
            let urlRequest = URLRequest(url: paypalURL)
            webView = WKWebView(frame: self.webViewOutlet.frame)
            webView.navigationDelegate = self
            webView.load(urlRequest)
            self.webViewOutlet.addSubview(webView)
            let pgIndicator = ProgressIndicator.sharedInstance()
            pgIndicator?.showMessage("Loading..", on: self.view)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createNavLeftButton() {
        
        self.navigationController?.navigationBar.isTranslucent = false;
        self.navigationController?.navigationBar.shadowImage = nil;
        let navButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 40))
        navButton.rotateButton()
        let image = UIImage(named: "tagProduct__back")
        navButton.setImage(image, for: .normal)
        navButton.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
        
        let button1 = UIBarButtonItem.init(customView: navButton)
        self.navigationItem.leftBarButtonItem  = button1
    }
    
    func backButtonClicked() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PayPalWebViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        ProgressIndicator.sharedInstance().hide()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ProgressIndicator.sharedInstance().hide()
    }
}
