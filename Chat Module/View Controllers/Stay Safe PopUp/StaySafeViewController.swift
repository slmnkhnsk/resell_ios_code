//
//  StaySafeViewController.swift
//  Resell
//
//  Created by Sachin Nautiyal on 20/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class StaySafeViewController: UIViewController {
    
    @IBOutlet weak var okButtonView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.okButtonView.backgroundColor = AppConstants.chatColor.chatBaseColor
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
