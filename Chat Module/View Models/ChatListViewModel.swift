//
//  ChatListViewModel.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 01/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import UserNotifications

class ChatListViewModel: NSObject {
    
    public var chats: [Chat]
    
    struct  Constants {
        static let cellIdentifier = "ChatListTableViewCell"
        static let errorMsg = "Something went wrong"
        static let notificationNotAllowedError = "Notifications not allowed"
    }
    
    let couchbaseObj = Couchbase.sharedInstance
    
    var userName : String? {
        guard let userName = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userName) as? String else { return nil }
        return userName
    }
    
    var buyerChats : [Chat]?{
        var buyers = [Chat]()
        for buyerChat in self.chats {
            if let isInitiated = buyerChat.initiated {
                if isInitiated {
                    buyers.append(buyerChat)
                }
            }
        }
        return buyers
    }
    
    var sellerChats : [Chat]?{
        var sellers = [Chat]()
        for sellerChat in self.chats {
            if let isInitiated = sellerChat.initiated {
                if !isInitiated {
                    sellers.append(sellerChat)
                }
            }
        }
        return sellers
    }
    
    
    init(withChatObjects chats : [Chat]?) {
        if let chats = chats{
            self.chats = chats
        }else {
            self.chats = []
        }
    }
    
    func setupNotificationSettings() {
        // Specify the notification types.
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            let options: UNAuthorizationOptions = [.alert, .sound, .badge];
            center.requestAuthorization(options: options) {
                (granted, error) in
                if !granted {
                    print(Constants.errorMsg)
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func authCheckForNotification () {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings { (settings) in
                if settings.authorizationStatus != .authorized {
                    // Notifications not allowed
                    print(Constants.notificationNotAllowedError)
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func getNumberOfRows() -> Int {
        return self.chats.count
    }
    
    func deleteChat(fromIndexPath indexPath :IndexPath) {
        self.chats.remove(at: indexPath.row)
    }
    
    func deleteRow(fromIndexPath indexPath :IndexPath, success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        let dataObject = self.chats[indexPath.row]
        let chatVMObj = ChatViewModel(withChatData: dataObject)
        chatVMObj.deleteChat(success: { response in
            success(response)
        }) { error in
            failure(error)
        }
    }
    
    func setUpTableViewCell(indexPath : IndexPath, tableView : UITableView) -> UITableViewCell {
        let dataObject = self.chats[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier) as! ChatListTableViewCell
        cell.chatVMObj = ChatViewModel(withChatData: dataObject)
        return cell
    }
    
    func unreadChatsCounts() -> Int {
        var unreadChats = [Chat]()
        for chat in self.chats {
            if let hasNewMessage = chat.hasNewMessage {
                if hasNewMessage {
                    unreadChats.append(chat)
                }
            }
        }
        return unreadChats.count
    }
    
    /// Used for searching in inside the chats model
    ///
    /// - Parameters:
    ///   - string: Search String
    ///   - index: current table index
    /// - Returns: array of related chats.
    func searchInChatModels(withString string : String, withSelectedIndex index:Int) -> [Chat]? {
        var chatArray:[Chat]!
        if index == 0 {
            guard let chats = self.buyerChats else { return nil }
            chatArray = chats
        } else if index == 1 {
            guard let chats = self.sellerChats else { return nil }
            chatArray = chats
        }
        
        let filtered = chatArray.filter {
            if index == 0, let name = $0.productName {
                return name.range(of: string, options: .caseInsensitive) != nil
            } else if index == 1, let name = $0.membername {
                return name.range(of: string, options: .caseInsensitive) != nil
            } else { return false }
        }
        if filtered.count == 0 || string == "" {
            if index == 0 {
                guard let chats = self.buyerChats else { return nil }
                return chats
            } else if index == 1 {
                guard let chats = self.sellerChats else { return nil }
                return chats
            }
        }
        return filtered
    }
}
