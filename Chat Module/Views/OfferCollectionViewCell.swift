//
//  OfferCollectionViewCell.swift
//  Resell
//
//  Created by Rahul Sharma on 05/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class OfferCollectionViewCell: JSQMessagesCollectionViewCell {
    
    @IBOutlet weak var offerSentViewOutlet: UIView!
    @IBOutlet weak var offerPriceViewOutlet: UIView!
    
    @IBOutlet weak var acceptedTextView: UIView!
    @IBOutlet weak var mainViewOutlet: UIView!
    @IBOutlet weak var mainTitleLabelOutlet: UILabel!
    @IBOutlet weak var priceOutlet: UILabel!
    var chatVMObj : ChatViewModel!
    var msgObj  : Message! {
        didSet {
            if (chatVMObj != nil) {
                if let currency = chatVMObj.currency, let msg = msgObj.messagePayload {
                    self.priceOutlet.text = currency+" "+msg
                } else {
                    self.priceOutlet.text = msgObj.messagePayload
                }
                if msgObj.isSelfMessage {
                    self.mainViewOutlet.backgroundColor = AppConstants.chatColor.chatBaseColor
                       self.setAcceptedOfferViewCorners()
                } else {
                    self.mainViewOutlet.backgroundColor = AppConstants.chatColor.chatBaseColor2
                    self.setOfferPriceViewCorners()
                }
            }
        }
    }
    
    func setAcceptedOfferViewCorners() {
        self.mainViewOutlet.roundCorners(corners: [.topLeft,.bottomRight, .bottomLeft], radius: 10)
        if(self.acceptedTextView != nil)
        {
            self.acceptedTextView.roundCorners(corners:[.topLeft], radius: 10)
        }
        if(self.offerSentViewOutlet != nil)
        {
            self.offerSentViewOutlet.roundCorners(corners:[.topLeft], radius: 10)
        }
    }
    
    func setOfferPriceViewCorners()
    {
        self.mainViewOutlet.roundCorners(corners: [.topRight,.bottomRight, .bottomLeft], radius: 10)
        if(self.offerPriceViewOutlet != nil)
        {
            self.offerPriceViewOutlet.roundCorners(corners:[.topRight], radius: 10)
        }
    }
}





class OfferSentMediaItem : NSObject, JSQMessageMediaData {
    
    let OfferSentCell = OfferCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 180, height: 100))
    
    func mediaView() -> UIView? {
        return OfferSentCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return OfferSentCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 100)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}

class OfferRecievedMediaItem : NSObject, JSQMessageMediaData {
    
    var OfferReceivedCell = OfferCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 180, height: 100))
    
    func mediaPlaceholderView() -> UIView {
        return OfferReceivedCell
    }
    
    func mediaView() -> UIView? {
        return OfferReceivedCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 100)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}

class AcceptedMediaItem : NSObject, JSQMessageMediaData {
    
    var AcceptedMediaCell = OfferCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 180, height: 100))
    
    func mediaPlaceholderView() -> UIView {
        return AcceptedMediaCell
    }
    
    func mediaView() -> UIView? {
        return AcceptedMediaCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 100)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}

extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
