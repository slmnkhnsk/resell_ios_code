//
//  PaypalCollectionViewCell.swift
//  Resell
//
//  Created by Rahul Sharma on 24/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController

protocol PaypalButtonPressedDelegate {
    func paypalButtonPressed(withMessageDetails :Message)
}

class PaypalCollectionViewCell: JSQMessagesCollectionViewCell {
    
    @IBOutlet weak var priceLabelOutlet: UILabel!
    
    var paypalButtonDelegate : PaypalButtonPressedDelegate? = nil
    var msgObj  : Message! 

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func payPalButtonAction(_ sender: Any) {
        paypalButtonDelegate?.paypalButtonPressed(withMessageDetails: self.msgObj)
    }
}

class PaypalCellMediaItem : NSObject, JSQMessageMediaData {
    
    let paypalCell = PaypalCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 200, height: 115))
    
    func mediaView() -> UIView? {
        return paypalCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return paypalCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 200, height: 115)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}
