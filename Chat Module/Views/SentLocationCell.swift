//
//  SentLocationCell.swift
//  Resell
//
//  Created by Sachin Nautiyal on 13/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController

protocol SentLocationCellDelegate {
    
    func tapOnSentLocationDelegate(messageObj: Message)
}


// Used for the outlets of the SentLocationCell
class SentLocationCell: JSQMessagesCollectionViewCell {
    
    var locationDelegate : SentLocationCellDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var locationImageOutlet: UIImageView!
    
    /// model object of message. see didSet for more details.
    var msgObj  : Message!{
        didSet {
            self.setValues(withMsgObj : msgObj)
        }
    }
        
    /// used fo setting values for location inside the cell.
    ///
    /// - Parameter msgObj: Message Object.
    func setValues(withMsgObj msgObj : Message) {
        var msgStr = ""
        if let msg = msgObj.messagePayload?.fromBase64() {
            msgStr = msg
        } else {
            msgStr = msgObj.messagePayload!
        }
        self.locationImageOutlet.kf.indicatorType = .activity
        self.locationImageOutlet.kf.indicator?.startAnimatingView()
        DispatchQueue.global().async {
            if let latStr = msgStr.slice(from: "(", to: ","), let longStr = msgStr.slice(from: ",", to: ")") {
                let str = "http://maps.googleapis.com/maps/api/staticmap?markers=color:red|\(latStr),\(longStr)&zoom=15&size=180x220&sensor=true&key=\(mGoogleServiceKey)"
                if let utfStr = str.addingPercentEscapes(using: String.Encoding.utf8) {
                    if let mapUrl = URL(string: utfStr) {
                        do {
                            let image = try UIImage(data: Data(contentsOf: mapUrl))
                            DispatchQueue.main.async {
                                self.locationImageOutlet.image = image
                                self.locationImageOutlet.kf.indicator?.stopAnimatingView()
                            }
                        } catch let error {
                            print(error)
                        }
                    }
                    
                }
            }
        }
    }
    
    @IBAction func tapOnSentLocationCell(_ sender: Any) {
        self.locationDelegate?.tapOnSentLocationDelegate(messageObj: msgObj)
    }
    
}

/// SentLocationMediaItem inherites the JSQMessageMediaData properties
class SentLocationMediaItem : NSObject, JSQMessageMediaData {
    
    /// Used for the instance of SentLocationCell
    let locationCell = SentLocationCell(frame:CGRect(x: 0, y: 0, width: 180, height: 220))
    
    func mediaView() -> UIView? {
        return locationCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return locationCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 220)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}

