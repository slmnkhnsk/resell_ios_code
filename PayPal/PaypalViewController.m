//
//  PaypalViewController.m


//  Created by Imma Web Pvt Ltd on 09/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PaypalViewController.h"
#import "paypalButtonTableViewCell.h"
#import "PaypalTableViewCell.h"
#import <CoreText/CoreText.h>
#import <WebKit/WebKit.h>
#import "PayPalSectionHeaderCell.h"
@interface PaypalViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,WebServiceHandlerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
@property (weak, nonatomic) IBOutlet UIButton *saveButtonoutlet;
@property (weak, nonatomic) IBOutlet UIView *unlinkView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewbuttomConstrain;

@end

@implementation PaypalViewController
{
    PaypalTableViewCell * cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    self.navigationItem.title = @"Connect Paypal";
    
    // Do any additional setup after loading the view.
    [self createNavigatLeftButton];
    self.navigationController.navigationBar.shadowImage = nil ;
    
    [self.saveButtonoutlet setBackgroundColor:mBaseColor];
}

- (void)createNavigatLeftButton
{
    
    UIButton *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton rotateButton];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]forState:UIControlStateSelected];
    [navCancelButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0,0,40,40)];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

// hiding navigation bar and changing to previous controloler.

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
  
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if(self.paypalLink.length > 0)
    {
        NSString *newString ;
        @try
        {
            // Attempt access to an empty array
            newString = [self.paypalLink substringFromIndex:22];
            
        }
        @catch (NSException *exception)
        {
            cell.nameTextFieldOutlet.text = @"";
            [self.saveButtonoutlet setTitle:@"Unlink Paypal Link" forState:UIControlStateNormal];
            return;
        }
        @finally
        {
            // Cleanup, in both success and fail cases
        }
        cell.nameTextFieldOutlet.text = newString;
        [self.saveButtonoutlet setTitle:@"Unlink Paypal Link" forState:UIControlStateNormal];
    }
    else
        [self.saveButtonoutlet setTitle:@"Save" forState:UIControlStateNormal];
}


- (void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)saveButtonAction:(id)sender {
    
    NSString *valueToSave;
    if(self.paypalLink.length > 0)
    {
         valueToSave = @"";
        
        if(_paypalCallback)
        {
            [self.paypalCallback PaypalLinkIsSaved:valueToSave];

        }
        
        NSDictionary *requestDic = @{mauthToken : [Helper userToken],
                                     mPayPalUrl : valueToSave
                                     };
        
        [WebServiceHandler savePayPalLink:requestDic andDelegate:self];
        
        
        [self.navigationController popViewControllerAnimated:YES];
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:payPalLink];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }else
    {
    NSString *link = cell.nameTextFieldOutlet.text;
    if(link.length > 0)
    {
    valueToSave = [NSString stringWithFormat:@"https://www.paypal.me/%@",cell.nameTextFieldOutlet.text];
    
    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:payPalLink];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [cell.nameTextFieldOutlet resignFirstResponder];
        if(_paypalCallback)
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:mSavePayPalNotify object:valueToSave];
            [self.paypalCallback PaypalLinkIsSaved:valueToSave];
            
        }
        
    [self.navigationController popViewControllerAnimated:YES];
        NSDictionary *requestDic = @{mauthToken : [Helper userToken],
                                     mPayPalUrl : valueToSave
                                     };
        
        [WebServiceHandler savePayPalLink:requestDic andDelegate:self];
        
        
        
    }
    else
    {
        UIAlertController *controller = [CommonMethods showAlertWithTitle:nil message:@"Enter your Paypal link." actionTitle:@"Ok"];
        mPresentAlertController;
    }
    }

}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [cell.nameTextFieldOutlet resignFirstResponder];

   
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
   
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    //when keyboard appears this will  notifiy.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyboard movements

- (void)keyboardWasHidden:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int height = MIN(keyboardSize.height,keyboardSize.width);
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.viewbuttomConstrain.constant = height;
                         [self.view layoutIfNeeded];
                     }];
}
- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int height = MIN(keyboardSize.height,keyboardSize.width);
    self.viewbuttomConstrain.constant = height;
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.viewbuttomConstrain.constant = 0;
                         [self.view layoutIfNeeded];
                     }];
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 50;
    }
    else
    {
        return 94;
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        static NSString *CellIdentifier = @"paypalSectionHeaderCell";
        PayPalSectionHeaderCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:headerView.exampleLabel.text];
        [attrString addAttribute:(NSString*)kCTUnderlineStyleAttributeName
                                   value:[NSNumber numberWithInt:kCTUnderlineStyleSingle]
                                   range:(NSRange){0,[attrString length]}];
        headerView.exampleLabel.attributedText = attrString ;
        return headerView;
        
    }
    return  nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 50;
    else
        return 10;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"labelCell" forIndexPath:indexPath];
        return cell;
    }
    else
    {
        paypalButtonTableViewCell *buttonCell = [tableView dequeueReusableCellWithIdentifier:@"buttonCell" forIndexPath:indexPath];
        
        [buttonCell.paypalButtonOutlet addTarget:self action:@selector(savePaypal:) forControlEvents:UIControlEventTouchUpInside];
        [[buttonCell.paypalButtonOutlet  layer] setBorderWidth:2.0f];
        [[buttonCell.paypalButtonOutlet  layer] setBorderColor:[UIColor lightGrayColor].CGColor];
        buttonCell.paypalButtonOutlet.layer.cornerRadius = buttonCell.paypalButtonOutlet.frame.size.height /2;
        buttonCell.paypalButtonOutlet.clipsToBounds = YES;
        
        return buttonCell;
    }
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableViewOutlet deselectRowAtIndexPath:indexPath animated:NO];
}



-(void)savePaypal:(UIButton*)sender
{
    [self performSegueWithIdentifier:@"WebViewShow" sender:nil];

}


-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    
}

@end
