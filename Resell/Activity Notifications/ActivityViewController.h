//
//  ActivityViewController.h

//
//  Created by Rahul Sharma on 4/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GetCurrIndexDelgate <NSObject>

-(void)currentIndex:(int)index;

@end

@interface ActivityViewController : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *followingViewButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *YouViewOutlet;

- (IBAction)youButtonAction:(id)sender;
- (IBAction)followingButtonAction:(id)sender;

@property (strong,nonatomic)id<GetCurrIndexDelgate>indexDelegate;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *movableDividerLeadingConstraintOutlet;

//@property (nonatomic) int pageIndex;
@property int pageIndex;
@property (weak, nonatomic) IBOutlet UIView *topTabView;

@property (strong, nonatomic) IBOutlet UITableView *followingActivityTable;
@property (strong, nonatomic) IBOutlet UITableView *selfActivityTable;
@property (strong, nonatomic) IBOutlet UIView *defaultSelfActivityTable;
@property BOOL showOwnActivity;

#define imageForFollowButton @"activity_you_following_icon_unselector"
#define imageForFollowingButton @"activity_you_following_icon_selector"
#define imageForRequestedButton @"activity_you_requested_btn"
@end
