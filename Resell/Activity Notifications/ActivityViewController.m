    //
//  ActivityViewController.m

//  Created by Rahul Sharma on 4/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ActivityViewController.h"
#import "YouTableViewCell.h"
#import "FollowTableViewCell.h"
#import "WebServiceConstants.h"
#import "WebServiceHandler.h"
#import "TinderGenericUtility.h"
#import "UIImageView+WebCache.h"
#import "FontDetailsClass.h"
#import "UserProfileViewController.h"
#import "ProductDetailsViewController.h"
#import "Helper.h"
#import "PGDiscoverPeopleViewController.h"
#import "ScreenConstants.h"
#import "CommonMethods.h"
#import "StarRatingActiTableViewCell.h"
#import "StarScreenViewController.h"
#import "UIButton+FollowButtonCustomization.h"

@interface ActivityViewController ()<WebServiceHandlerDelegate,FollowTableViewCellDelegate,YouTableViewCellDelegate,UIGestureRecognizerDelegate> {
    
    NSMutableArray *arrayOfFollowingActivity;
    NSMutableArray *arrayOfSelfActivity;
    UIImage *productImage ;
    
    int index;
    int followingIndex;
    
    UIRefreshControl *refreshControl;
    UIRefreshControl *refreshControlForFollowing;
    
    UIActivityIndicatorView *avForSelfActivity;
    UIActivityIndicatorView *avForFollowingActivity;
}
@property bool classIsAppearing;
@end

@implementation ActivityViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    index = 0;
    followingIndex = 0;
    
    [self addingRefreshControl];
    
    arrayOfFollowingActivity =[[NSMutableArray alloc] init];
    arrayOfSelfActivity =[[NSMutableArray alloc] init];
    
    [self createNavLeftButton];
    [self getAllActivities];
    [self makingBackGroundOfTablev];
    [self updateFollowStatus];
    
    [self.topTabView setBackgroundColor:mNavigationBarColor];
    
}

-(void)createNavLeftButton {
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mCloseButton normalState:mCloseButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

-(void)navLeftButtonAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)updateFollowStatus {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollwoStatus:) name:@"updatedFollowStatus" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    _classIsAppearing = NO;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
 [self.mainScrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0) animated:NO];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.indexDelegate currentIndex:self.pageIndex];
    self.navigationItem.title = NSLocalizedString(navTitleForActivity, navTitleForActivity);
    self.navigationController.navigationBar.shadowImage = nil;
    _classIsAppearing = YES;
    
}

-(void)makingBackGroundOfTablev {
    avForSelfActivity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    avForSelfActivity.frame =CGRectMake(self.view.frame.size.width/2 -12.5, self.view.frame.size.height/2 -50, 25,25);
    avForSelfActivity.tag  = 1;
    [self.selfActivityTable addSubview:avForSelfActivity];
    [avForSelfActivity startAnimating];
    
    avForFollowingActivity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    avForFollowingActivity.frame =CGRectMake(self.view.frame.size.width/2 -12.5, self.view.frame.size.height/2 -50, 25,25);
    avForFollowingActivity.tag  = 1;
    [self.followingActivityTable addSubview:avForFollowingActivity];
    [avForFollowingActivity startAnimating];
}

-(void)addingRefreshControl {
    refreshControlForFollowing = [[UIRefreshControl alloc]init];
    [self.followingActivityTable addSubview:refreshControlForFollowing];
    [refreshControlForFollowing addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventValueChanged];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [self.selfActivityTable addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshSelfTable:) forControlEvents:UIControlEventValueChanged];
}

-(void)refreshTable:(id)sender {
    //reload table
    [refreshControlForFollowing beginRefreshing];
    followingIndex = 0;
    [self getFollowingActivity];
}

-(void)refreshSelfTable:(id)sender {
    //reload table
    [refreshControl beginRefreshing];
    index =0;
    [self getOwnersActivity];
}

- (IBAction)followingButtonAction:(id)sender {
    CGRect frame = self.mainScrollView.bounds;
    frame.origin.x = 0;
    [self.mainScrollView scrollRectToVisible:frame animated:YES];
    self.followingViewButtonOutlet.selected = YES;
    self.YouViewOutlet.selected = NO;
    
    [self.followingViewButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.YouViewOutlet setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.6] forState:UIControlStateNormal];
    
}

- (IBAction)youButtonAction:(id)sender {
    CGRect frame = self.mainScrollView.bounds;
    frame.origin.x = CGRectGetWidth(self.view.frame);
    [self.mainScrollView scrollRectToVisible:frame animated:YES];
    self.followingViewButtonOutlet.selected = NO;
    self.YouViewOutlet.selected = YES;
    
    [self.YouViewOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.followingViewButtonOutlet setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.6] forState:UIControlStateNormal];
}

/*--------------------------------------------------*/
#pragma mark - Scrollview Delegate
/*--------------------------------------------------*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.mainScrollView]) {
        CGPoint offset = scrollView.contentOffset;
        self.movableDividerLeadingConstraintOutlet.constant = scrollView.contentOffset.x/2;
        if(offset.x <= CGRectGetWidth(self.view.frame) /2 ) {
            // Followig selected
            self.followingViewButtonOutlet.selected = YES;
            self.YouViewOutlet.selected = NO;
            
            [self.followingViewButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
            [self.YouViewOutlet setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.6] forState:UIControlStateNormal];
            
        }
        else {
            //you button selected
            self.followingViewButtonOutlet.selected = NO;
            self.YouViewOutlet.selected = YES;
            
            [self.YouViewOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
            [self.followingViewButtonOutlet setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.6] forState:UIControlStateNormal];
            
        }
        // Set offset to adjusted value
        scrollView.contentOffset = offset;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.mainScrollView]) {
        CGPoint offset = scrollView.contentOffset;
        if(offset.x <= CGRectGetWidth(self.view.frame) /2 ) {
            // Followig selected
            self.movableDividerLeadingConstraintOutlet.constant = 0;
        }
        else {
            //you button selected
            self.movableDividerLeadingConstraintOutlet.constant = CGRectGetWidth(self.view.frame) /2;
        }
    }
}

/*---------------------------------------------------------------------------*/
#pragma
#pragma mark - tableview delegates and data source.
/*--------------------------------------------------------------------------*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 100) {
        return [arrayOfFollowingActivity count];
    }
    else
    {
       return  [arrayOfSelfActivity count];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 200) {
        if([flStrForObj(arrayOfSelfActivity[indexPath.row][@"notificationType"]) integerValue] == 8) {
            return 100;
        }
        else {
            return 60;
        }
    }
    else {
           return 60;
    }
}

-(void)rateButton:(UIButton*)sender {
    //uncoment this to rate seller
    StarScreenViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"starScreenViewController"];
    newView.activityResponse = arrayOfSelfActivity[sender.tag];
    newView.ratiingForSeller =  YES;
    [self.navigationController pushViewController:newView animated:YES];
}

-(void)sellerButton:(UIButton*)sender {
}

-(void)productButton:(UIButton*)sender {
}

//Custom Header (it contains profile image of the posted person and his/her username and time label and location if available.)
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView.tag == 200)
    {
        
        YouTableViewCell *youtablecell;
        StarRatingActiTableViewCell *rateUser;
        if([flStrForObj(arrayOfSelfActivity[indexPath.row][@"notificationType"]) integerValue] == 8) {
            rateUser = [tableView dequeueReusableCellWithIdentifier:@"startTableCell"
                                                       forIndexPath:indexPath];
        }
        else {
            youtablecell = [tableView dequeueReusableCellWithIdentifier:@"youTableViewCell"
                                                           forIndexPath:indexPath];
            [youtablecell.particularPostImageView setHidden:YES];
            [youtablecell.postButtonOutlet setHidden:YES];
            [youtablecell.followButtonOutlet setHidden:YES];
            youtablecell.delegate = self;
            youtablecell.userdetails = arrayOfSelfActivity[indexPath.row];
            youtablecell.actitvtyUserName = flStrForObj(arrayOfSelfActivity[indexPath.row][@"username"]);
            youtablecell.postID = flStrForObj(arrayOfSelfActivity[indexPath.row][@"postId"]);
        }
        NSString *activityStmt;
        NSString *activitytime = [Helper convertEpochToNormalTimeInshort:flStrForObj(arrayOfSelfActivity[indexPath.row][@"createdOn"])];
        
        
        NSInteger switchCase = [arrayOfSelfActivity[indexPath.row][@"notificationType"] integerValue];
        
        switch (switchCase) {
            case 0:
            {
                [youtablecell.particularPostImageView setHidden:NO];
                [youtablecell.postButtonOutlet setHidden:NO];
                activityStmt = [NSString stringWithFormat:@"%@ tagged you in their post.  %@",flStrForObj(arrayOfSelfActivity[indexPath.row][@"membername"]),activitytime];
                youtablecell.actitvtyUserName = flStrForObj(arrayOfSelfActivity[indexPath.row][@"membername"]);
                [youtablecell.particularPostImageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfSelfActivity[indexPath.row][@"thumbnailImageUrl"])] placeholderImage:[UIImage imageNamed:@""]];
                [youtablecell.followButtonOutlet setHidden:YES];
            }
                break;
            case 1:
            {
                [youtablecell.particularPostImageView setHidden:NO];
                [youtablecell.postButtonOutlet setHidden:NO];
                activityStmt = [NSString stringWithFormat:@"%@ %@:%@",flStrForObj(arrayOfSelfActivity[indexPath.row][@"membername"]),NSLocalizedString(review, review),flStrForObj(arrayOfSelfActivity[indexPath.row][@"b"][@"properties"][@"postCaption"])];
                [youtablecell.particularPostImageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfSelfActivity[indexPath.row][@"thumbnailImageUrl"]]) placeholderImage:[UIImage imageNamed:@""]];
                 [youtablecell.followButtonOutlet setHidden:YES];
                 }
                 break;
                 
                 
                 case 2:
                 {
                     [youtablecell.particularPostImageView setHidden:NO];
                     [youtablecell.postButtonOutlet setHidden:NO];
                     activityStmt = [NSString stringWithFormat:@"%@ %@  %@",flStrForObj(arrayOfSelfActivity[indexPath.row][@"membername"]),NSLocalizedString(likedYourPostStatement, likedYourPostStatement),activitytime];
                     [youtablecell.particularPostImageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfSelfActivity[indexPath.row][@"thumbnailImageUrl"])] placeholderImage:[UIImage imageNamed:@""]];
                     [youtablecell.followButtonOutlet setHidden:YES];
                 }
                 break;
                 
                 case 3:
                 {
                     [youtablecell.followButtonOutlet setHidden:NO];
                     activityStmt = [NSString stringWithFormat:@"%@ %@  %@",flStrForObj(arrayOfSelfActivity[indexPath.row][@"membername"]),NSLocalizedString(startedFollowingYouStatement, startedFollowingYouStatement),activitytime];
                 }
                 break ;
                 case 4:
                 {
                     [youtablecell.followButtonOutlet setHidden:NO];
                     activityStmt = [NSString stringWithFormat:@"%@ requested to follow you.  %@",flStrForObj(arrayOfSelfActivity[indexPath.row][@"membername"]),activitytime];
                 }
                 break;
                 
                 
                 case 5:
                 {
                     [youtablecell.particularPostImageView setHidden:NO];
                     [youtablecell.postButtonOutlet setHidden:NO];
                     activityStmt = [NSString stringWithFormat:@"%@ %@  %@",flStrForObj(arrayOfSelfActivity[indexPath.row][@"membername"]),NSLocalizedString(leftReviewOnYourPostStatement, leftReviewOnYourPostStatement),activitytime];
                     [youtablecell.particularPostImageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfSelfActivity[indexPath.row][@"thumbnailImageUrl"])] placeholderImage:[UIImage imageNamed:@""]];
                     [youtablecell.followButtonOutlet setHidden:YES];
                 }
                 break ;
                 
                 case 6:
                 {
                     [youtablecell.particularPostImageView setHidden:NO];
                     [youtablecell.postButtonOutlet setHidden:NO];
                     activityStmt = [NSString stringWithFormat:@" %@ %@ %@",(arrayOfSelfActivity[indexPath.row][@"membername"]),NSLocalizedString(sendOfferStatement, sendOfferStatement),activitytime];
                     [youtablecell.particularPostImageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfSelfActivity[indexPath.row][@"thumbnailImageUrl"])] placeholderImage:[UIImage imageNamed:@""]];
                     
                     NSString *memberProPic = flStrForObj(arrayOfSelfActivity[indexPath.row][@"memberProfilePicUrl"]);
                     memberProPic = [memberProPic stringByReplacingOccurrencesOfString:@"upload/v1501227756/" withString:@"upload/h_80/w_80/"];
                     
                     [youtablecell.FriendProfileImageView sd_setImageWithURL:[NSURL URLWithString:memberProPic] placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
                     [youtablecell.followButtonOutlet setHidden:YES];
                 }
                 break ;
                 
                 case 8:
                 {
                     NSString *ratingMessage= [NSString stringWithFormat:NSLocalizedString(youJustBoughtStatement, youJustBoughtStatement),@" %@ %@ %@ . %@ %@",arrayOfSelfActivity[indexPath.row][@"productName"],NSLocalizedString(from, from),arrayOfSelfActivity[indexPath.row][@"membername"],NSLocalizedString(rateSellerStatement, rateSellerStatement),activitytime];
                     
                     [rateUser.rateUserAction setTitle:NSLocalizedString(rateButtonTitle, rateButtonTitle) forState:UIControlStateNormal];
                     [rateUser.rateUserAction setBackgroundColor:mBaseColor];
                     
                     [rateUser.dicriptionLabel setAttributedText:[Helper customisedActivityStmt:flStrForObj(arrayOfSelfActivity[indexPath.row][@"productName"])  seconUserName:flStrForObj(arrayOfSelfActivity[indexPath.row][@"membername"]) timeForPost:activitytime :ratingMessage]];
                     
                     //roundImage
                     rateUser.rateUserAction.tag = indexPath.row;
                     [rateUser.rateUserAction addTarget:self action:@selector(rateButton:) forControlEvents:UIControlEventTouchUpInside];
                     [rateUser.buyerNameAction addTarget:self action:@selector(sellerButton:) forControlEvents:UIControlEventTouchUpInside];
                     [rateUser.productNameAction addTarget:self action:@selector(productButton:) forControlEvents:UIControlEventTouchUpInside];
                     
                     [rateUser.sellerPic sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfSelfActivity[indexPath.row][@"memberProfilePicUrl"]]) placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
                      return rateUser;
                      }
                      break;
                      
                      
                      }
                      
                      
                      if(activityStmt) {
                          [youtablecell.descrptionLabel setAttributedText:[Helper customisedActivityStmt:flStrForObj(arrayOfSelfActivity[indexPath.row][@"membername"])  seconUserName:flStrForObj(arrayOfSelfActivity[indexPath.row][@"user2_username"]) timeForPost:activitytime :activityStmt]];
                      }
                      [youtablecell.FriendProfileImageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfSelfActivity[indexPath.row][@"memberProfilePicUrl"]]) placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
                       
                       [self updateFollowButtonTitle:indexPath.row and:youtablecell.followButtonOutlet andFollowStatus:flStrForObj(arrayOfSelfActivity[indexPath.row][@"followRequestStatus"])];
                       
                       youtablecell.followButtonOutlet.tag = 1000 + indexPath.row;
                       return youtablecell;
                       }
                       
                       else
                       {
                           FollowTableViewCell *followCell;
                           followCell = [tableView dequeueReusableCellWithIdentifier:@"followTableviewcell"
                                                                        forIndexPath:indexPath];
                           followCell.delegate = self;
                           followCell.postID = flStrForObj(arrayOfFollowingActivity[indexPath.row][@"postId"]);
                           followCell.postType = @"0";
                           followCell.actitvtyUserName = flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user2_username"]);
                           followCell.nameButton = [UIButton buttonWithType:UIButtonTypeCustom];
                           followCell.nameButton.tag = indexPath.row;
                           [followCell.nameButton addTarget:self action:@selector(nameAction:) forControlEvents:UIControlEventTouchUpInside];
                           followCell.nameButton.backgroundColor = [UIColor clearColor];
                           [followCell.descriptionLabel addSubview:followCell.nameButton];
                           followCell.userdetails = arrayOfFollowingActivity[indexPath.row];
                           followCell.postdetails = arrayOfFollowingActivity[indexPath.row][@"postDetails"];
                           NSString *activityStmt;
                           NSString *activitytime = [Helper convertEpochToNormalTimeInshort:flStrForObj(arrayOfFollowingActivity[indexPath.row][@"createdOn"])];
                           
                           NSInteger switchCase = [arrayOfFollowingActivity[indexPath.row][@"notificationType"]integerValue];
                           
                           switch (switchCase) {
                               case 0:
                               {
                                   [followCell.postImage setHidden:NO];
                                   activityStmt = [NSString stringWithFormat:@"%@ tagged %@ in post.  %@",flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user1_username"]),flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user2_username"]),activitytime];
                                   [followCell.postImage sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfFollowingActivity[indexPath.row][@"thumbnailImageUrl"])] placeholderImage:[UIImage imageNamed:@""]];
                               }
                                   break;
                               case 1:
                               {
                                   [followCell.postImage setHidden:NO];
                                   activityStmt = [NSString stringWithFormat:@"%@ %@   %@",flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user1_username"]),NSLocalizedString(mentionedInReviewStatement, mentionedInReviewStatement), activitytime];
                                   [followCell.postImage sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfSelfActivity[indexPath.row][@"thumbnailImageUrl"])] placeholderImage:[UIImage imageNamed:@""]];
                               }
                                   break;
                               case 2:
                               {
                                   [followCell.postImage setHidden:NO];
                                   
                                   
                                   activityStmt = [NSString stringWithFormat:@"%@ %@ %@ post.  %@",flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user1_username"]),NSLocalizedString(likedStatement, likedStatement),flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user2_username"]),activitytime];
                                   [followCell.postImage sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfFollowingActivity[indexPath.row][@"thumbnailImageUrl"])] placeholderImage:[UIImage imageNamed:@""]];
                               }
                                   break;
                               case 3:
                               {
                                   activityStmt = [NSString stringWithFormat:@"%@ %@ %@.  %@",flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user1_username"]),NSLocalizedString(startedFollowingStatement, startedFollowingStatement), flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user2_username"]),activitytime];
                                   [followCell.postImage setHidden:YES];
                               }
                                   break;
                               case 4:
                               {
                                   [followCell.postImage setHidden:NO];
                                   activityStmt = [NSString stringWithFormat:@"%@ requested to follow %@.  %@",flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user1_username"]),flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user2_username"]), activitytime];
                               }
                                   break;
                               case 5:
                               {
                                   [followCell.postImage setHidden:NO];
                                   [followCell.postImage sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfFollowingActivity[indexPath.row][@"thumbnailImageUrl"])] placeholderImage:[UIImage imageNamed:@""]];
                                   activityStmt = [NSString stringWithFormat:@"%@ %@ %@ post.  %@",flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user1_username"]),NSLocalizedString(leftReviewStatement, leftReviewStatement) ,flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user2_username"]),activitytime];
                               }
                                   break;
                           }
                           
                           if(activityStmt) {
                               [followCell.descriptionLabel setAttributedText:[Helper customisedActivityStmt:flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user1_username"])  seconUserName:flStrForObj(arrayOfFollowingActivity[indexPath.row][@"user2_username"]) timeForPost:activitytime :activityStmt]];
                           }
                           
                           followCell.nameButton.frame = CGRectMake(0, 10, 80, 15);
                           [followCell.friendProfileImage sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfFollowingActivity[indexPath.row] [@"user1_profilePicUrl"] ]) placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
                            return followCell;
                            
                            }
                            }
                            

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
if (tableView.tag == 200 )
{
    if(arrayOfSelfActivity.count >20) {
        if (indexPath.row == [arrayOfSelfActivity count] - 1 ) {
            ++index;
            [self getOwnersActivity];
        }
    }
}
else {
    if(arrayOfFollowingActivity.count >20) {
        if (indexPath.row == [arrayOfFollowingActivity count] - 1 ) {
            ++followingIndex;
            [self getFollowingActivity];
        }
    }
}
}
                
#pragma mark -FriendsCellDelegate
-(void)cell:(FollowTableViewCell *)cell button:(UIButton *)button withObject:(NSDictionary *)object{
    [self openProfileOfUserName:object[@"user1_username"]];
}
                       
#pragma mark -SelfCellDelegate
                       
-(void)ownActivitycell:(YouTableViewCell *)cell button:(UIButton *)buttonN withObject:(NSDictionary *)userobject{
    [self openProfileOfUserName:userobject[@"membername"]];
}
                       
-(void)openProfileOfUserName:(NSString *)selectedUserName {
    if(selectedUserName) {
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
        
        UserProfileViewController *newView = [story instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
        newView.checkProfileOfUserNmae = selectedUserName;
        newView.checkingFriendsProfile = YES;
        [self.navigationController pushViewController:newView animated:YES];
    }
}
                
-(void)cell:(FollowTableViewCell *)cell postbutton:(UIButton *)button ofpostType:(NSString *)posttype withpostid:(NSString *)id andUserName:(NSString *)name
{
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
    
    ProductDetailsViewController *newView = [story instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
    newView.movetoRowNumber = 0;
    newView.postId = id;
    newView.activityUser = name;
    newView.postType = posttype;
    newView.navigationBarTitle = name;
    newView.noAnimation = YES;

    
    [self.navigationController pushViewController:newView animated:YES];
}
                       
-(void)selfCell:(YouTableViewCell *)cell postbutton:(UIButton *)button ofpostType:(NSString *)posttype withpostid:(NSString *)id andUserName:(NSString *)name {
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
    
      ProductDetailsViewController *newView = [story instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
      newView.movetoRowNumber = 0;
      newView.postId = id;
      newView.activityUser = name;
      newView.postType = posttype;
      newView.navigationBarTitle = name;
      newView.noAnimation = YES;
      [self.navigationController pushViewController:newView animated:YES];
}
                       
-(void)updateFollowButtonTitle:(NSInteger )row and:(id)sender andFollowStatus:(NSString *)followstatus{
     UIButton *reeceivedButton = (UIButton *)sender;
    //  if follow status is 1 --->    "Following"
    //  if follow status is nil --->  "Follow"
    
    if ([followstatus  isEqualToString:@"1"]) {
        [reeceivedButton makeButtonAsFollowing];
    }
    else {
          [reeceivedButton makeButtonAsFollow];
    }
    
    [reeceivedButton addTarget:self
                        action:@selector(FollowButtonAction:)
              forControlEvents:UIControlEventTouchUpInside];
}
           
-(void)FollowButtonAction:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    NSIndexPath *selectedCellForLike = [self.selfActivityTable indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
    //  if follow status is 1 --->    "Following"
    //  if follow status is nil --->  "Follow"
    
    NSString *followStatus = flStrForObj(arrayOfSelfActivity[selectedCellForLike.row][@"followRequestStatus"]);
    if ([followStatus isEqualToString:@"1"])  {
        [self unfollowRequest:flStrForObj(arrayOfSelfActivity[selectedCellForLike.row][@"membername"])];
        [selectedButton makeButtonAsFollow];
        arrayOfSelfActivity[selectedCellForLike.row][@"followRequestStatus"] = @"2";
        [self sendNewFollowStatusThroughNotification:flStrForObj(arrayOfSelfActivity[selectedCellForLike.row][@"membername"]) andNewStatus:@"2"];
    }
    else {
        [self followRequest:flStrForObj(arrayOfSelfActivity[selectedCellForLike.row][@"membername"])];
        [selectedButton makeButtonAsFollowing];
        arrayOfSelfActivity[selectedCellForLike.row][@"followRequestStatus"] = @"1";
        [self sendNewFollowStatusThroughNotification:flStrForObj(arrayOfSelfActivity[selectedCellForLike.row][@"membername"]) andNewStatus:@"1"];
    }
}
           
-(void)followRequest :(NSString *)usernameToFollow {
    NSDictionary *requestDict = @{muserNameTofollow     : flStrForObj(usernameToFollow),
                                  mauthToken            :[Helper userToken],
                                  };
    [WebServiceHandler follow:requestDict andDelegate:self];
}
                       
-(void)unfollowRequest:(NSString *)usernameToUnfollow {
    NSDictionary *requestDict = @{muserNameToUnFollow     : flStrForObj(usernameToUnfollow),
                                  mauthToken            :[Helper userToken],
                                  };
    [WebServiceHandler unFollow:requestDict andDelegate:self];
}
                       
-(void)sendNewFollowStatusThroughNotification:(NSString *)userNamer andNewStatus:(NSString *)newFollowStatus {
    NSDictionary *newFollowDict = @{@"newFollowStatus"     :flStrForObj(newFollowStatus),
                                    @"userName"            :flStrForObj(userNamer),
                                    };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedFollowStatus" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"newUpdatedFollowData"]];
}
                       
-(void)updateFollwoStatus:(NSNotification *)noti {
    if (!_classIsAppearing) {
        //check the postId and Its Index In array.
        NSString *userNamer = flStrForObj(noti.object[@"newUpdatedFollowData"][@"userName"]);
        NSString *foolowStatusRespectToUser = noti.object[@"newUpdatedFollowData"][@"newFollowStatus"];
        
        for (int i=0; i <arrayOfSelfActivity.count;i++) {
            if ([flStrForObj(arrayOfSelfActivity[i][@"membername"]) isEqualToString:userNamer]) {
                arrayOfSelfActivity[i][@"followRequestStatus"] = foolowStatusRespectToUser;
                NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:i inSection:0];
                NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
                [self.selfActivityTable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
        }
    }
}
                       
-(void)getAllActivities {
     [self getOwnersActivity];
     [self getFollowingActivity];
}
                       
#pragma mark - WebService
                       
           /*
            * Method to Request Following Activities
            *
            */
-(void)getFollowingActivity {
    NSDictionary *requestDict = @{
                                  mauthToken      :[Helper userToken],
                                  moffset         :flStrForObj([NSNumber numberWithInteger:followingIndex*40]),
                                  mlimit          :flStrForObj([NSNumber numberWithInteger:40])
                                  };
    [WebServiceHandler followingActivities:requestDict andDelegate:self];
}
/*
 * Method to Request Own Activities
 *
*/
-(void)getOwnersActivity {
    NSDictionary *requestDict = @{
                                  mauthToken      :flStrForObj([Helper userToken]),
                                  moffset         :flStrForObj([NSNumber numberWithInteger:index*40]),
                                  mlimit          :flStrForObj([NSNumber numberWithInteger:40])
                                  };
    [WebServiceHandler ownActivities:requestDict andDelegate:self];
}
                       
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    if(requestType ==  RequestTypefollowingActivity) {
        [avForFollowingActivity stopAnimating];
        self.followingActivityTable.backgroundView = nil;
        [refreshControlForFollowing endRefreshing];
    }
    if(requestType == RequestTypeOwnActivity) {
        [avForSelfActivity stopAnimating];
        self.selfActivityTable.backgroundView = nil;
        [refreshControl endRefreshing];
    }
    
    if (error) {
      if(requestType ==  RequestTypefollowingActivity) {
          if(arrayOfSelfActivity.count ==0) {
              self.followingActivityTable.backgroundView = [self showErrorMessage:[error localizedDescription]];
          }
        }
        
        if(requestType == RequestTypeOwnActivity) {
            if(arrayOfFollowingActivity.count ==0) {
                self.selfActivityTable.backgroundView = [self showErrorMessage:[error localizedDescription]];
            }
        }
        return;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    if (requestType == RequestTypefollowingActivity ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                NSMutableArray *temp = [[NSMutableArray alloc] init];
                temp =responseDict[@"data"];
                if(followingIndex == 0) {
                    [arrayOfFollowingActivity removeAllObjects];
                }
                
                for(int i= 0;i<temp.count;i++) {
                    if(![flStrForObj(temp[i][@"notificationType"]) isEqualToString:@""])
                        [arrayOfFollowingActivity addObject:temp[i]];
                }
                
                if(arrayOfFollowingActivity.count == 0) {
                    [self viewWhenNodataAvailabeleForFollowingActivity];
                }
                else {
                    self.followingActivityTable.backgroundView =nil;
                }
                
                [self.followingActivityTable reloadData];
            }
                break;
                //failure responses.
                default:
                break;
        }
    }
    
    NSDictionary *responseDict1  = (NSDictionary*)response;
    if (requestType == RequestTypeOwnActivity ) {
        switch ([responseDict1[@"code"] integerValue]) {
            case 200: {
                //successs response.
                NSMutableArray *tempOwn = [[NSMutableArray alloc] init];
                tempOwn =responseDict1[@"data"];
                if(index == 0)
                {
                    [arrayOfSelfActivity removeAllObjects];
                }
                
                for(int i = 0; i< tempOwn.count;i++) {
                    if(![flStrForObj(tempOwn[i][@"notificationType"]) isEqualToString:@""])
                    {
                             [arrayOfSelfActivity addObject:tempOwn[i]];
                    }
                }
                
                self.selfActivityTable.backgroundView =nil;
                if(arrayOfSelfActivity.count == 0) {
                    [self viewWhenNodataAvailabeleForActivity];
                }
                
                [self.selfActivityTable reloadData];
            }
                //failure responses.
                break;
            default:
                break;
        }
    }
}

                       
                       
-(void)nameAction:(UIButton *)sender
{
    
}
                       
-(UIView *)showErrorMessage:(NSString *)errorMessage {
    UIView *noDataAvailableMessageView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [noDataAvailableMessageView setCenter:self.view.center];
    UILabel *message = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 -100, self.view.frame.size.height/2 -20, 200, 60)];
    message.textAlignment = NSTextAlignmentCenter;
    message.numberOfLines = 0;
    message.text = errorMessage;
    [noDataAvailableMessageView addSubview:message];
                           
    return noDataAvailableMessageView;
}
                       
-(void)viewWhenNodataAvailabeleForActivity {
    UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width, self.view.frame.size.height)];
    UIImageView *imag = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 -60,80,120,120)];
    imag.contentMode = UIViewContentModeScaleAspectFit ;
    imag.image = [UIImage imageNamed:@"empty self activity"];
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(60,180,self.view.frame.size.width-120,75)];
    messageLabel.text = NSLocalizedString(noSelfActivityLabelText, noSelfActivityLabelText);
    messageLabel.font = [UIFont fontWithName:RobotoRegular size:13];
    messageLabel.numberOfLines =0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    
    UIButton *startSelling = [[UIButton alloc] initWithFrame:CGRectMake(20,260,self.view.frame.size.width - 40,50)];
    startSelling.backgroundColor = mBaseColor2;
    startSelling.layer.cornerRadius = 5;
    [startSelling setTitle:NSLocalizedString(startSellingButtonTitle, startSellingButtonTitle) forState:UIControlStateNormal];
    [startSelling setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [startSelling.titleLabel setFont:[UIFont fontWithName:RobotoMedium size:15]];
    [startSelling addTarget:self action:@selector(startSelling:) forControlEvents:UIControlEventTouchUpInside];
    
    [mainView addSubview:messageLabel];
    //    [mainView addSubview:titleLabel];
    [mainView addSubview:imag];
    [mainView addSubview:startSelling];
    self.selfActivityTable.backgroundView = mainView;
}
                       
-(void)viewWhenNodataAvailabeleForFollowingActivity {
    UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width, self.view.frame.size.height)];
    UIImageView *imag = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 -60,80,120,120)];
    imag.contentMode = UIViewContentModeScaleAspectFit ;
    imag.image = [UIImage imageNamed:@"empty followers"];
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(60,180,self.view.frame.size.width-120,75)];
    messageLabel.text = NSLocalizedString(noFollowingActivityLabelText, noFollowingActivityLabelText);
    messageLabel.font = [UIFont fontWithName:RobotoRegular size:13];
    messageLabel.numberOfLines =0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    
    UIButton *followButton = [[UIButton alloc] initWithFrame:CGRectMake(20,260,self.view.frame.size.width - 40,50)];
    followButton.backgroundColor = mBaseColor2;
    followButton.layer.cornerRadius = 5;
    [followButton setTitle:NSLocalizedString(findPeopleButtonTitle, findPeopleButtonTitle) forState:UIControlStateNormal];
    [followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [followButton.titleLabel setFont:[UIFont fontWithName:RobotoMedium size:15]];
    [followButton addTarget:self action:@selector(followButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [mainView addSubview:messageLabel];
    [mainView addSubview:imag];
    [mainView addSubview:followButton];
    
    self.followingActivityTable.backgroundView = mainView;
}
           
-(void)followButtonClicked:(id)sender  {
    PGDiscoverPeopleViewController *postsVc = [self.storyboard instantiateViewControllerWithIdentifier:mDiscoverPeopleVcSI];
    [self.navigationController pushViewController:postsVc animated:YES];
}

                            
-(void)startSelling:(id)sender
 {
   [self dismissViewControllerAnimated:YES completion:nil];
 }
                            
           
@end
