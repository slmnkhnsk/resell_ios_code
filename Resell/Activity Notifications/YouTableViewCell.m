//
//  YouTableViewCell.m
//
//  Created by Rahul Sharma on 7/22/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "YouTableViewCell.h"

@implementation YouTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self layoutIfNeeded];
    self.followButtonOutlet.layer.cornerRadius = 2.0f;
    self.followButtonOutlet.clipsToBounds = YES;
    self.usernameBtn.userInteractionEnabled = YES;
    self.accessoryType = UITableViewCellAccessoryNone;
    
    self.FriendProfileImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    tapGesture1.numberOfTapsRequired = 1;
    [tapGesture1 setDelegate:self];
    [self.FriendProfileImageView addGestureRecognizer:tapGesture1];
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [_usernameBtn addTarget:self action:@selector(userNameAction:) forControlEvents:UIControlEventTouchUpInside];
   return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (IBAction)followButtonAction:(id)sender {

}
- (IBAction)userNameAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(ownActivitycell:button:withObject:)]) {
        [self.delegate ownActivitycell:self button:sender withObject:self.userdetails];
      }
}

- (IBAction)postButtonAction:(id)sender {
    if (self.delegate && [_delegate respondsToSelector:@selector(selfCell:postbutton:ofpostType:withpostid:andUserName:)]) {
        [self.delegate selfCell:self postbutton:sender ofpostType:@"0" withpostid:self.postID andUserName:self.actitvtyUserName];
    }
}

- (void) tapGesture: (id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ownActivitycell:button:withObject:)]) {
        [self.delegate ownActivitycell:self button:sender withObject:self.userdetails];
    }
}

@end
