

//
//  CategoriesListTableViewController.m

//
//  Created by Rahul Sharma on 14/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CategoriesListTableViewController.h"
#import "cellForListOfOptions.h"

@interface CategoriesListTableViewController ()<WebServiceHandlerDelegate , UITableViewDelegate,UITableViewDataSource>

@end

@implementation CategoriesListTableViewController

#pragma 
#pragma mark- ViewController LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    if([self.navigationItem.title isEqualToString:NSLocalizedString(navTitleForProductCategory, navTitleForProductCategory)]){
        self.tableViewForList.separatorColor = [UIColor clearColor];
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];  [HomePI showPIOnView:self.view withMessage:@"Loading..."];
    if([self.showResultsFor isEqualToString:@"categories"])
    [self requestForListWithoutInfinteRefresh];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 Create left navbar button
 */
-(void)createNavLeftButton
{
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,20,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButton) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

-(void)navLeftButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Request ForPost

/**
 Method call first time to get likes details upto 20
 */
-(void)requestForListWithoutInfinteRefresh {
    self.currentIndex = 0;
    [self requestListWithIndex:self.currentIndex];
}


/**
 WebService Call with Request params.     
 */
-(void)requestListWithIndex :(NSInteger )offsetIndex {
    NSDictionary *requestDict = @{
                                  mLimit : mLimitValue,
                                  moffset :[NSNumber numberWithInteger:20 * offsetIndex]
                                  }; 
    [WebServiceHandler getCategories:requestDict andDelegate:self];
}

-(void)requestForListBasedOnRequirement {
    
    //for Home Screen.
    //requestingForPosts.
    __weak CategoriesListTableViewController *weakSelf = self;
    // setup infinite scrollinge
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        if (self.dataArray.count >19) {
            [weakSelf requestListWithIndex:weakSelf.currentIndex];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
        return self.dataArray.count;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    cellForListOfOptions *cell = [tableView dequeueReusableCellWithIdentifier:@"cellForList" forIndexPath:indexPath];
    
    
    if ([self.showResultsFor isEqualToString:@"categories"]) {
        [cell updateCell:self.dataArray withKey:@"categoryName" IndexPath:indexPath andPreviousSelection:self.previousSelection];
        
    }
    else {
        [cell updateCell:self.dataArray withKey:self.key IndexPath:indexPath andPreviousSelection:self.previousSelection];
    }
    return cell;
}


#pragma mark - Table view delegate method

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     if ([self.showResultsFor isEqualToString:@"categories"])
    self.callBack(self.dataArray[indexPath.row]);
    else if([self.showResultsFor isEqualToString:@"conditions"])
     self.callBack(self.dataArray[indexPath.row]);
    else
    self.callBack(self.dataArray[indexPath.row][@"subcategoryname"]);
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Web Service Delegate method
-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Error" message:[error localizedDescription] actionTitle:@"Ok"];
        mPresentAlertController;
        return;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    if (requestType == RequestTypeGetCategories) {
        
        
        //success response(200 is for success code).
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                //                self.currentIndex = 0;
                [self handlingResponseOfExplorePosts:response];
            }
                break;
            case 204:{
                
                   }
                break;
                
            default:
                break;
        }
    }
    
}

/**
 Handling the response comes from API call.

 @param response Dictionary.
 */
-(void)handlingResponseOfExplorePosts :(NSDictionary *) response
{
    self.dataArray = [NSMutableArray new];
    self.tableViewForList.separatorColor = mTableViewCellSepratorColor;
    
    if(self.currentIndex == 0) {
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:response[@"data"]];
    }
    else {
        self.currentIndex++;
        [self.dataArray  addObjectsFromArray:response[@"data"]];
    }
    [self.tableViewForList reloadData];
    
    if(self.dataArray.count >19)
        [self requestForListBasedOnRequirement];
}





@end
