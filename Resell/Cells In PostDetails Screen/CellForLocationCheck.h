//
//  CellForLocationCheck.h

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface CellForLocationCheck : UITableViewCell
@property (weak, nonatomic) IBOutlet GMSMapView *viewForMap;
@property (weak, nonatomic) IBOutlet UILabel *labelForLoc;
-(void)setUpMapView:(double)latitude andLongitude:(double )longi;
@end
