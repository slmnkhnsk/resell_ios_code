//
//  CellForLocationCheck.m

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForLocationCheck.h"
#import <GoogleMaps/GoogleMaps.h>
@implementation CellForLocationCheck

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUpMapView:(double )latitude andLongitude:(double )longi{
    

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude                                                                 longitude:longi zoom:16];
    [self.viewForMap animateToCameraPosition:camera];
    self.viewForMap.myLocationEnabled = NO;
    self.viewForMap.mapType = kGMSTypeNormal;
    self.viewForMap.settings.myLocationButton = NO;
    self.viewForMap.settings.zoomGestures = YES;
    self.viewForMap.settings.tiltGestures = NO;
    self.viewForMap.settings.rotateGestures = NO;
    self.viewForMap.userInteractionEnabled=NO;
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.icon = [UIImage imageNamed:@"itemMapIcon"];
    marker.position = CLLocationCoordinate2DMake(latitude, longi);
    marker.map = self.viewForMap;
  
}

@end
