//
//  ChatSafeView.h

//
//  Created by Imma Web Pvt Ltd on 07/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChatSaveXibDelegate <NSObject>

-(void)hideAlert;

@end

@interface ChatSafeView : UIView < UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *okButtonOutlet;
@property(nonatomic,weak)id<ChatSaveXibDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *alertContentView;

-(void)onWindow:(UIWindow *)onWindow;

@end
