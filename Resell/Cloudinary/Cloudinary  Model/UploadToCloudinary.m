//
//  UploadToCloudinary.m

//
//  Created by Rahul Sharma on 14/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "UploadToCloudinary.h"
#import "PostingScreenModel.h"

@implementation UploadToCloudinary 
static UploadToCloudinary *sharedInstance = nil;
static int imageNumber;
+(instancetype)sharedInstance
{
    if(sharedInstance == nil)
        sharedInstance = [UploadToCloudinary new];
    return sharedInstance;
}

-(id)initWithArrayOfImagePaths :(NSArray *)arrayOfPaths 
{  self = [super init];
    if(!self)return nil;
    self.arrayOfimagePaths = arrayOfPaths;
    return self;
}

-(void)uploaderProgress:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite context:(id)context {
    
}
-(void)uploaderError:(NSString *)result code:(NSInteger)code context:(id)context {
    
     [self.cloudObj.delegate cloudinaryError:result andCheck:YES ];
}

/*-------------------------------------------------------------------------------*/
#pragma mark - Uploading Image to Cloudinary.
/*--------------------------------------------------------------------------------*/

-(void)uploadingImageToCloudinaryWithArrayOfPaths{
    
    imageNumber = 0;
    self.cloundinaryCreditinals =[[NSUserDefaults standardUserDefaults]objectForKey:cloudinartyDetails];
    if([self.arrayOfimagePaths[imageNumber][@"imageType"] isEqualToString:@"storingImagePath"])
    {
        NSString *postImagePath = flStrForObj(self.arrayOfimagePaths[0][@"imageValue"]);
        postImagePath = [self reduceImageQuality:postImagePath];
        CLCloudinary *mobileCloudinary = [[CLCloudinary alloc] init];
        [mobileCloudinary.config setValue:self.cloundinaryCreditinals[@"response"][@"cloudName"] forKey:@"cloud_name"];
        
        CLUploader* mobileUploader = [[CLUploader alloc] init:mobileCloudinary delegate:[UploadToCloudinary sharedInstance]];
        
        if(self.cloundinaryCreditinals){
        [mobileUploader upload:postImagePath options:@{
                                                       @"signature":self.cloundinaryCreditinals[@"response"][@"signature"],
                                                       @"timestamp":self.cloundinaryCreditinals[@"response"][@"timestamp"],
                                                       @"api_key": self.cloundinaryCreditinals[@"response"][@"apiKey"],
                                                       }];
        }
        else
        {
         [self.delegate cloudinaryError:nil andCheck:YES ];

        }
        
    }
    else
    {
        self.mainImageUrl = self.arrayOfimagePaths[0][@"imageValue"];
        NSString *stringToReplace = @"upload/fl_progressive:steep/";
        NSString * newString = @"upload/w_150,h_150/";
        self.mainThumbnailUrl = [self.mainImageUrl stringByReplacingOccurrencesOfString:stringToReplace withString:newString];
        self.cloudinaryPublicId = self.arrayOfimagePaths[0][@"publicId"];
        [self uploadNextImageTocloud];
        
    }
}

/*-----------------------------------------------------*/
#pragma mark - cloudinary delegate
/*----------------------------------------------------*/

- (void)uploaderSuccess:(NSDictionary*)result context:(id)context {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString* publicId = [result valueForKey:@"public_id"];
        NSString *appendString = @"w_150,h_150/";
        
        NSString * newString = [result[@"secure_url"] stringByReplacingOccurrencesOfString:UrlForCloudinary withString:@""];
        
        NSString *reduceImageQuality = @"upload/fl_progressive:steep/";
        reduceImageQuality = [result[@"secure_url"] stringByReplacingOccurrencesOfString:@"upload/" withString:reduceImageQuality];
        
        NSString *thumbNailUrl =[[UrlForCloudinary stringByAppendingString:[NSString stringWithFormat:@"%@",appendString]] stringByAppendingString:newString];
       
        switch(imageNumber )
        {
            case 0: self.cloudObj.mainThumbnailUrl = thumbNailUrl;
                self.cloudObj.mainImageUrl =reduceImageQuality;
                self.cloudObj.cloudinaryPublicId = publicId ;
                break;
            case 1:
                self.cloudObj.ImageUrl1 = reduceImageQuality;
                self.cloudObj.cloudinaryPublicId1 = publicId ;
                break;
            case 2:
                self.cloudObj.ImageUrl2 =reduceImageQuality;
                self.cloudObj.cloudinaryPublicId2 = publicId ;
                break;
            case 3:
                self.cloudObj.ImageUrl3 =reduceImageQuality;
                self.cloudObj.cloudinaryPublicId3 = publicId ;
                break;
            case 4:
                self.cloudObj.ImageUrl4 = reduceImageQuality;
                self.cloudObj.cloudinaryPublicId4 = publicId ;
                break;
        }
        [self uploadNextImageTocloud];
        
    });
}

-(void)uploadNextImageTocloud {
    UploadToCloudinary *cloud = [UploadToCloudinary sharedInstance];
    self.cloundinaryCreditinals =[[NSUserDefaults standardUserDefaults]objectForKey:cloudinartyDetails];
    
    imageNumber++;
    
    if(imageNumber != cloud.cloudObj.arrayOfimagePaths.count){
        if([cloud.cloudObj.arrayOfimagePaths[imageNumber][@"imageType"] isEqualToString:@"storingImagePath"])
        {
            NSString *postImagePath = flStrForObj(cloud.cloudObj.arrayOfimagePaths[imageNumber][@"imageValue"]);
            postImagePath  = [self reduceImageQuality:postImagePath];
            CLCloudinary *mobileCloudinary = [[CLCloudinary alloc] init];
            [mobileCloudinary.config setValue:self.cloundinaryCreditinals[@"response"][@"cloudName"] forKey:@"cloud_name"];
            CLUploader* mobileUploader = [[CLUploader alloc] init:mobileCloudinary delegate:[UploadToCloudinary sharedInstance]];
            if(self.cloundinaryCreditinals){
            [mobileUploader upload:postImagePath options:@{
                                                           @"signature":self.cloundinaryCreditinals[@"response"][@"signature"],
                                                           @"timestamp":self.cloundinaryCreditinals[@"response"][@"timestamp"],
                                                           @"api_key": self.cloundinaryCreditinals[@"response"][@"apiKey"],
                                                           }];
            }
            else{
                [self.cloudObj.delegate cloudinaryError:nil andCheck:YES ];
            }
        }
        else {
            switch(imageNumber)
            {
                case 0:
                    self.mainImageUrl = cloud.cloudObj.arrayOfimagePaths[0][@"imageValue"];
                    self.cloudinaryPublicId = cloud.cloudObj.arrayOfimagePaths[0][@"publicId"] ;
                    break;
                case 1:
                    self.ImageUrl1 = cloud.cloudObj.arrayOfimagePaths[1][@"imageValue"];
                    self.cloudinaryPublicId1 = cloud.cloudObj.arrayOfimagePaths[1][@"publicId"] ;;
                    break;
                case 2:
                    self.ImageUrl2 = cloud.cloudObj.arrayOfimagePaths[2][@"imageValue"];
                    self.cloudinaryPublicId2 = cloud.cloudObj.arrayOfimagePaths[2][@"publicId"] ; ;
                    break;
                case 3:
                    self.ImageUrl3 = cloud.cloudObj.arrayOfimagePaths[3][@"imageValue"];
                    self.cloudinaryPublicId3 = cloud.cloudObj.arrayOfimagePaths[3][@"publicId"] ; ;
                    break;
                case 4:
                    self.ImageUrl4 = cloud.cloudObj.arrayOfimagePaths[4][@"imageValue"];
                    self.cloudinaryPublicId4 = cloud.cloudObj.arrayOfimagePaths[4][@"publicId"] ;  ;
                    break;
                    
            }
            [self uploadNextImageTocloud];
            
        }
    }
    
    if(imageNumber == cloud.cloudObj.arrayOfimagePaths.count)
    {
        imageNumber ++ ;
        PostingScreenModel *post = [PostingScreenModel sharedInstance];
        [post getUrlsOfImagePaths:cloud];
        NSDictionary *request = [post createParamDictionaryForAPICall:post.modelObj];
        [cloud.cloudObj.delegate getDictionaryFromCloudinaryModelClass:request];
    }
}


-(NSString *)reduceImageQuality:(NSString *)originalImagePath {
    NSString *filePath = originalImagePath;
    NSData *imgData = [NSData dataWithContentsOfFile:filePath];
    UIImage *tempImage =  [[UIImage alloc] initWithData:imgData];
    NSData *thumbImgData = UIImageJPEGRepresentation(tempImage,0.2);

    NSString *thumbNailimageName = [NSString stringWithFormat:@"%@%@.png",@"reducedImage",[self getCurrentTime]];
    //to get the image path.
    NSArray* Thumbnailpaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* thumabNaildocumentsDirectory = [Thumbnailpaths objectAtIndex:0];
    NSString* ThumbnailimagePath = [thumabNaildocumentsDirectory stringByAppendingPathComponent:thumbNailimageName];
    [thumbImgData writeToFile:ThumbnailimagePath atomically:NO];
    return ThumbnailimagePath;
}

- (UIImage*)getImageFromLocalImagePath:(NSString *)imagePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"test.png" ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

-(NSString*)getCurrentTime
{
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormated;
    
}
@end
