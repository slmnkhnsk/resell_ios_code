//
//  HomeViewCommentsViewController.m

//
//  Created by Rahul Sharma on 4/4/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CommentsViewController.h"
#import "UIImage+GIF.h"
#import "FontDetailsClass.h"
#import "UserProfileViewController.h"
#import "UITextView+Placeholder.h"

@interface CommentsViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,WebServiceHandlerDelegate,UITextViewDelegate> {
    NSMutableArray *responseArray;
    UIRefreshControl *refreshControl;
    CGFloat heightOfRow;
    UIView *customizedTableviewHeader;
    NSInteger *offset;
    NSInteger index;
    NSString *laststring;
    BOOL onlyFirstTimeMoveToBottom;
    BOOL needToShowHeaderForMorePosts;
    BOOL PostsAreMoreThanTwenty;
    CGFloat tableviewCurrentYoffset;
}

@end

@implementation CommentsViewController

/*---------------------------------------*/
#pragma mark
#pragma mark - ViewCOntroller LifeCycle
/*---------------------------------------*/
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(navTitleForComments, navTitleForComments);
    [self createNavLeftButton];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveAccKeybaordHeightForReview:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveToOriginalPostionForReview) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextChanged:) name:UITextFieldTextDidChangeNotification object:self.commentTextView];
    self.bottomCommentView.layer.borderWidth = 0.5;
    self.bottomCommentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    onlyFirstTimeMoveToBottom = YES;
    needToShowHeaderForMorePosts = YES;
    PostsAreMoreThanTwenty = NO;
    responseArray =[[NSMutableArray alloc] init];
    self.sendButtonOutlet.enabled = NO;
    
    [self.sendButtonOutlet setBackgroundColor:mBaseColor];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    [self setHidesBottomBarWhenPushed:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setHidesBottomBarWhenPushed:NO];
    self.navigationController.navigationBar.shadowImage = nil ;
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UITextFieldTextDidChangeNotification];
}


#pragma mark
#pragma mark -TextField Delgates.

- (BOOL)checkForMandatoryField {
    if (self.commentTextView.text.length != 0 ) {
        return YES;
    }
    return NO;
}


-(void)textViewDidChange:(UITextView *)textView
{
    if ([self checkForMandatoryField]) {
        [self.sendButtonOutlet setEnabled:YES];
    }
    else {
        [self.sendButtonOutlet setEnabled:NO];
    }
    //From here you can get the last text entered by the user
    NSArray *stringsSeparatedBySpace = [textView.text componentsSeparatedByString:@" "];
    
    //then you can check whether its one of your userstrings and trigger the event
    laststring = [stringsSeparatedBySpace lastObject];
   bool isValidForTagFriends = [laststring containsString:@"@"];
    
    if(isValidForTagFriends) {
        if (laststring.length > 2) {
            [self requestForTagFriends];
            self.commentsTableViewOutlet.hidden = YES;
        }
        else {
          self.commentsTableViewOutlet.hidden = NO;
        }
    }
    else {
        self.commentsTableViewOutlet.hidden = NO;
    }

    CGRect newFramae = self.commentTextView.frame;
    newFramae.size.height = self.commentTextView.contentSize.height;
    
    if(newFramae.size.height + 20 > 54 )
    {
        
        if (newFramae.size.height < 108) {
            self.textViewSuperViewHeightConstraint.constant = newFramae.size.height + 20;
            self.commentTextViewHeightConstraint.constant = newFramae.size.height + 2;
        }
        
    }
    else {
        
        self.textViewSuperViewHeightConstraint.constant = 54;
        self.commentTextViewHeightConstraint.constant = 34;
   
    }
    
    if (self.commentsTableViewOutlet.contentSize.height > self.commentsTableViewOutlet.frame.size.height)
    {
        CGPoint newPosition = CGPointMake(0, self.commentsTableViewOutlet.contentSize.height - self.commentsTableViewOutlet.frame.size.height );
        [self.commentsTableViewOutlet setContentOffset:newPosition animated:NO];
    }
    [self adjustContentSize:self.commentTextView];
}

-(void)requestForTagFriends {
    NSString  *stringToSearchForTagFriend = [laststring substringFromIndex:1];
    NSDictionary *requestDict = @{
                                  muserTosearch :stringToSearchForTagFriend,
                                  mauthToken :[Helper userToken],
                                  };
    [WebServiceHandler getUserNameSuggestion:requestDict andDelegate:self];
    [self.commentsTableViewOutlet reloadData];
}


-(void)adjustContentSize:(UITextView*)tv{
    CGFloat deadSpace = ([tv bounds].size.height - [tv contentSize].height);
    CGFloat inset = MAX(0, deadSpace/2.0);
    tv.contentInset = UIEdgeInsetsMake(inset, tv.contentInset.left, inset, tv.contentInset.right);
}

-(void)textFieldTextChanged:(id)sender {
    if ([self checkForMandatoryField]) {
        [self.sendButtonOutlet setEnabled:YES];
    }
    else {
        [self.sendButtonOutlet setEnabled:NO];
    }
}

-(void)changeThePositionOfTableview {
    [self.timerIvar invalidate];
    self.timerIvar =  nil;
    
    if (self.commentsTableViewOutlet.contentSize.height > self.commentsTableViewOutlet.frame.size.height)
    {
        CGPoint newPosition = CGPointMake(0, self.commentsTableViewOutlet.contentSize.height - self.commentsTableViewOutlet.frame.size.height );
        [self.commentsTableViewOutlet setContentOffset:newPosition animated:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    if (onlyFirstTimeMoveToBottom) {
        if (responseArray.count == 0) {
            ProgressIndicator *PI = [ProgressIndicator sharedInstance];
            [PI showPIOnView:self.view withMessage:@"Loading..."];
            [self getPostComments:index];

        }
    }
}

/*-----------------------------------------------------------------------*/
#pragma mark
#pragma mark - tableview delegates and data source.
/*-----------------------------------------------------------------------*/

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 2) {
        return NO;
    }
    else {
        if (indexPath.section == 1) {
            if ([responseArray[indexPath.row][@"username"] isEqualToString:[Helper userName]]) {
                return YES;
            }
            else {
                return NO;
            }
        }
        else {
            return NO;
        }
    }
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    //username and commented username same then user can delete d comment otherwise he can do direct message to commented user.
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        //insert your deleteAction here.
        NSDictionary *requestDict = @{
                                      mauthToken:flStrForObj([Helper userToken]),
                                      mpostid:_postId,
                                      mLabel:@"Photo",
                                      mcommentId:responseArray[indexPath.row][@"commentId"],
                                      mtype:@"0",
                                      };
        [WebServiceHandler deleteComment:requestDict andDelegate:self];
        
        [responseArray removeObjectAtIndex:indexPath.row];
        [_commentsTableViewOutlet deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    }];
    
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(tableView.tag == 2) {
         return 1;
    }
    else {
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        if (section == 0) {
            if ([self.postCaption isEqualToString:@"null"]) {
                return 0;
            }
            else {
                // if  there is no caption then no need to show the above posted user name with post caption row.
                return 1;
            }
        }
        else {
            return responseArray.count;
        }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        CommentsTableViewCell *commentcell = [tableView dequeueReusableCellWithIdentifier:@"commentsCell" forIndexPath:indexPath];
        [commentcell layoutIfNeeded];
        commentcell.profileImageViewOutlet.layer.cornerRadius = commentcell.profileImageViewOutlet.frame.size.height/2;
        commentcell.profileImageViewOutlet.clipsToBounds = YES;
        
        if (indexPath.section == 0) {
            [commentcell.userNameButtonOutlet setTitle:flStrForObj(_userNameOfPostedUser) forState:UIControlStateNormal];
            [commentcell.profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:flStrForObj(self.imageUrlOfPostedUser)]
                                                  placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
            
            commentcell.timeLabelOutlet.text = @"";
            
            if ([flStrForObj(self.postCaption) isEqualToString:@"null"]) {
                commentcell.commentLabelOutlet.text = @"";
                heightOfRow = 50;
            }
            else {
                commentcell.commentLabelOutlet.text = flStrForObj(self.postCaption);
                //[self heightOfLabel:commentcell];
                [commentcell changeHeightOfCommentLabel:flStrForObj(self.postCaption) andFrame:self.view.frame];
                
            }
        }
        else
        {
            [commentcell.profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:flStrForObj(responseArray[indexPath.row][@"profilePicUrl"])]
                                                  placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
            [commentcell.userNameButtonOutlet setTitle: flStrForObj(responseArray[indexPath.row][@"username"]) forState:UIControlStateNormal];
            
            commentcell.commentLabelOutlet.text = flStrForObj(responseArray[indexPath.row][@"commentBody"]);
            //[self heightOfLabel:commentcell];
            [commentcell changeHeightOfCommentLabel:flStrForObj(responseArray[indexPath.row][@"commentBody"]) andFrame:self.view.frame];
            commentcell.timeLabelOutlet.text = [self convertTimeFormat:responseArray[indexPath.row][@"commentedOn"]];
        }
        commentcell.commentLabelOutlet.userInteractionEnabled = YES;
        [self handlinguserName:commentcell];
        return commentcell;
}


-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if(tableView.tag == 2) {
        return 50;
    } else {
        CommentsTableViewCell *cell = (CommentsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"commentsCell"];
        
        NSString *commentWithUserName;
        
        if (indexPath.section == 0) {
            if ([flStrForObj(self.postCaption) isEqualToString:@"null"]) {
                commentWithUserName = @"";
                 return 0;
            }
            else {
                commentWithUserName = @"";
                return 0;
               // commentWithUserName = flStrForObj(self.postCaption);
            }
        }
        else
        {
            commentWithUserName = flStrForObj(responseArray[indexPath.row][@"commentBody"]);
        }
        
        
        UILabel*captionlbl=[[UILabel alloc]initWithFrame:cell.commentLabelOutlet.bounds];
        captionlbl.font=cell.commentLabelOutlet.font;
        
        // NSString *commentWithUserName =   flStrForObj(responseArray[indexPath.row][@"commentBody"]);
        
        NSCharacterSet *ws = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        commentWithUserName = [commentWithUserName stringByTrimmingCharactersInSet:ws];
        
        
        CGRect frame=captionlbl.frame;
        frame.size.width= self.view.frame.size.width - 65 ;
        captionlbl.frame=frame;
        
        captionlbl.text =commentWithUserName;
        
        CGFloat heightOfCaption;
        
        //claculating the height of text and if the text is empty directly making the respective label or button height as zero otherwise calculating height of text by using measureHieightLabel method.
        //+5 ids for spacing for the labels.
        
        if ([captionlbl.text  isEqualToString:@""]) {
            heightOfCaption = 0;
        }
        else {
            heightOfCaption = [Helper measureHieightLabel:captionlbl] + 5;
        }
        
        // 20 --- > for posted time label height.
        CGFloat totalHeightOfRow =  heightOfCaption + 35;
        
        return totalHeightOfRow;
    }
}

-(void)heightOfLabel:(id)sender {
    CommentsTableViewCell *receivedCell = (CommentsTableViewCell *)sender;
    
    CGFloat dynamicHeightOfCommentLabel =  [Helper measureHieightLabel:receivedCell.commentLabelOutlet];
    receivedCell.commentLabelHeightConstraint.constant = dynamicHeightOfCommentLabel + 5;
    heightOfRow  =  dynamicHeightOfCommentLabel  + 40;
}

-(NSString *)convertTimeFormat:(NSString *)str {
    NSTimeInterval seconds = [str doubleValue];
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:(seconds/1000)];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
    [dateFormatte setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    NSTimeInterval secondsBetween = [todayDate timeIntervalSinceDate:epochNSDate];
    NSString *timeStamp = [self PostedTimeSincePresentTime:secondsBetween];
  
    return timeStamp;
}


/* ---------------------------------------------------------------------*/
#pragma mark
#pragma mark - TimeConverting  From EpochValue
/* --------------------------------------------------------------------*/

//converting seconds into minutes or hours or  days or weeks based on number of seconds.

-(NSString *)PostedTimeSincePresentTime:(NSTimeInterval)seconds {
    if(seconds < 60)
    {
        NSInteger time = round(seconds);
        //showing timestamp in seconds.
        if(seconds < 3)
        {
            return @"now";
        }
        else {
            NSString *secondsInstringFormat = [NSString stringWithFormat:@"%ld", (long)time];
            NSString *secondsWithSuffixS = [secondsInstringFormat stringByAppendingString:@"s"];
            return secondsWithSuffixS;
        }
    }
    
    else if (seconds >= 60 && seconds <= 60 *60) {
        //showing timestamp in minutes.
        NSInteger numberOfMinutes = seconds / 60;
        
        NSString *minutesInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfMinutes];
        NSString *minutesWithSuffixM = [minutesInstringFormat stringByAppendingString:@"m"];
        return minutesWithSuffixM;
    }
    else if (seconds >= 60 *60 && seconds <= 60*60*24) {
        //showing timestamp in hours.
        NSInteger numberOfHours = seconds /(60*60);
        
        NSString *hoursInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfHours];
        NSString *hoursWithSuffixH = [hoursInstringFormat stringByAppendingString:@"h"];
        return hoursWithSuffixH;
    }
    else if (seconds >= 24 *60 *60 && seconds <= 60*60*24*7) {
        //showing timestamp in days.
        NSInteger numberOfDays = seconds/(60*60*24);
        
        NSString *daysInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfDays];
        NSString *daysWithSuffixD = [daysInstringFormat stringByAppendingString:@"d"];
        return daysWithSuffixD;
    }
    else if (seconds >= 60*60*24*7) {
        //showing timestamp in weeks.
        NSInteger numberOfWeeks = seconds /(60*60*24*7);
        NSString *weeksInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfWeeks];
        NSString *weeksWithSuffixS = [weeksInstringFormat stringByAppendingString:@"w"];
        return weeksWithSuffixS;
    }
    return @"";
}


#pragma mark - navigation bar buttons

-(void)createNavLeftButton
{
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,20,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

- (void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.
 
 @param notification post by Notificationcentre.
 */
-(void)veiwMoveAccKeybaordHeightForReview :(NSNotification *)notification {
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    //Given size may not account for screen rotation
    int keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    self.textFieldSuperViewBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.textFieldSuperViewBottomConstraint.constant = keyboardHeight;
                         [self.view layoutIfNeeded];
                     }];
}


/**
 This method will move view back to original frame.
 */
-(void)veiwMoveToOriginalPostionForReview
{
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.textFieldSuperViewBottomConstraint.constant = 0;
                         [self.commentTextView resignFirstResponder];
                         [self.view layoutIfNeeded];
                     }];
}


#pragma mark
#pragma mark - Send Button Action

- (IBAction)sendButtonAction:(id)sender {
    self.commentsTableViewOutlet.backgroundView = nil ;
    [self.commentsTableViewOutlet reloadData];
    self.commentsTableViewOutlet.hidden = NO;
        if (self.commentTextView.text.length) {
            [self commentOnPost:self.commentTextView.text];
            self.commentTextView.text = @"";
            [self.sendButtonOutlet setEnabled:NO];
            self.textViewSuperViewHeightConstraint.constant = 54;
    }
}

/*----------------------------------------*/
#pragma mark
#pragma mark - WebServiceDelegate
/*----------------------------------------*/

//service request

-(void)commentOnPost :(NSString *)comment
{
   NSArray * words = [self.commentTextView.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableArray *WordsForHashTags = [NSMutableArray new];
    for (NSString * word in words){
        if ([word length] > 1 && [word characterAtIndex:0] == '#'){
            NSString * editedWord = [word substringFromIndex:1];
            [WordsForHashTags addObject:editedWord];
        }
    }
    
    NSString *hashTagsString = [[WordsForHashTags valueForKey:@"description"] componentsJoinedByString:@","];
    NSString *hashTagsInLowerCase = [hashTagsString lowercaseString];
    
    
    
    NSDictionary *requestDict = @{
                                  mauthToken:flStrForObj([Helper userToken]),
                                  mcomment:flStrForObj(comment),
                                  mpostid:flStrForObj(self.postId),
                                  mtype:@"0",
                                  mhashTags:flStrForObj(hashTagsInLowerCase),
                                  };
    [WebServiceHandler commentOnPost:requestDict andDelegate:self];
}

-(void)getPostComments :(NSInteger )offse
{
    NSDictionary *requestDict = @{
                                  mauthToken:flStrForObj([Helper userToken]),
                                  mpostid:flStrForObj(self.postId),
                                  moffset:flStrForObj([NSNumber numberWithInteger:index*20]),
                                  mlimit:flStrForObj([NSNumber numberWithInteger:20])
                                  };
    
    [WebServiceHandler getCommentsOnPost:requestDict andDelegate:self];
}

-(void)getSomeComments :(NSInteger )offse
{
    NSDictionary *requestDict = @{
                                  mauthToken:flStrForObj([Helper userToken]),
                                  mpostid:flStrForObj(self.postId),
                                  moffset:flStrForObj([NSNumber numberWithInteger:0]),
                                  mlimit:flStrForObj([NSNumber numberWithInteger:20*offse])
                                  };
    [WebServiceHandler getCommentsOnPost:requestDict andDelegate:self];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 0) {
        if (PostsAreMoreThanTwenty ) {
            // creating custom header view
            if (needToShowHeaderForMorePosts) {
                customizedTableviewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
                customizedTableviewHeader.backgroundColor =[UIColor whiteColor];
                
                UIView *boarderLine = [[UIView alloc] initWithFrame:CGRectMake( 50,customizedTableviewHeader.frame.size.height - 1,tableView.frame.size.width,1)];
                boarderLine.backgroundColor =[UIColor colorWithRed:0.5003 green:0.5002 blue:0.5003 alpha:0.3];
                
                /* Create custom view to display section header... */
                
                //creating user name label
                UILabel *messageLabel = [[UILabel alloc] init];
                messageLabel.text = NSLocalizedString(loadMoreCommentsText, loadMoreCommentsText);
                [messageLabel setFont:[UIFont fontWithName:RobotoMedium size:14]];
                messageLabel.textColor =[UIColor colorWithRed:0.1176 green:0.1176 blue:0.1176 alpha:1.0];
                messageLabel.frame=CGRectMake(0, 0, self.view.frame.size.width , 40);
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.backgroundColor = [UIColor clearColor];
                
                //creating  total  header  as button
                UIButton *headerButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [headerButton addTarget:self
                                 action:@selector(headerButtonClicked:)
                       forControlEvents:UIControlEventTouchUpInside];
                headerButton.backgroundColor =[UIColor clearColor];
                headerButton.frame = CGRectMake(0, 0, tableView.frame.size.width,40);
                
                // adding  headerButton,UserImageView,timeLabel,UserNamelabel to the customized tableView  Section Header.
                
                [customizedTableviewHeader addSubview:headerButton];
                [customizedTableviewHeader addSubview:messageLabel];
                [customizedTableviewHeader addSubview:boarderLine];
                return customizedTableviewHeader;
            }
            else {
                UIView *emptyview= [[UIView alloc] init];
                emptyview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
                return emptyview;
            }
        }
        else{
            UIView *emptyview= [[UIView alloc] init];
            emptyview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
            return emptyview;
        }
    }
        else {
            UIView *emptyview= [[UIView alloc] init];
            emptyview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
            return emptyview;
        }
}

-(CGFloat )tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        if (PostsAreMoreThanTwenty && needToShowHeaderForMorePosts) {
            return 40.0;
        }
        else {
            return 0;
        }
    }
    else {
        return 0;
    }
}

-(void)headerButtonClicked:(id)sender {
    UIView *footerView = [[UIView alloc] init];
    [footerView setBackgroundColor:[UIColor whiteColor]];
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = CGRectMake(self.view.frame.size.width/2-20,0, 40, 40.0);
    [footerView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
    NSArray *subviewsfff = [[sender superview] subviews];
    [subviewsfff[0] removeFromSuperview];
    [subviewsfff[1] removeFromSuperview];
    [customizedTableviewHeader addSubview:footerView];

     index++;
    [self getPostComments:index];
}

//handling response.

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
        return;
    }
    NSMutableDictionary *responseDict = (NSMutableDictionary *)response;
    if (requestType == RequestTypePostComment ) {
     customizedTableviewHeader.backgroundColor =[UIColor whiteColor];
        switch ([responseDict[@"code"] integerValue]) {
            case 200:
            {
                [responseArray addObject: @{
                                            @"commentBody" : flStrForObj(response[@"data"][0][@"commentData"][0][@"commentBody"]),
                                            @"commentedOn":  flStrForObj(response[@"data"][0][@"commentData"][0][@"commentedOn"]),
                                            @"username":flStrForObj([Helper userName]),
                                            @"commentId":flStrForObj(response[@"data"][0][@"commentData"][0][@"commentId"]),
                                            @"profilePicUrl": flStrForObj(response[@"data"][0][@"profilePicUrl"]),
                                            }];
                [self addnewRow:responseArray.count-1];

            }
                break;
            default:
                break;
        }
    }
    else if (requestType == RequestTypedeleteComments ) {
    }
    else if (requestType == RequestTypeGetCommentsOnPost) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                NSArray *newData = responseDict[@"result"];
                if (index >= 1) {
                    responseArray=[[[responseArray reverseObjectEnumerator] allObjects] mutableCopy];
                    [responseArray addObjectsFromArray:newData];
                    responseArray=[[[responseArray reverseObjectEnumerator] allObjects] mutableCopy];
                }
                else {
                    newData=[[[newData reverseObjectEnumerator] allObjects] mutableCopy];
                    [responseArray addObjectsFromArray:newData];
                }
                
                if([responseDict[@"result"] count]) {
                    
                    if ([responseDict[@"result"] count] < 19) {
                        PostsAreMoreThanTwenty =NO;
                    }
                    else {
                        PostsAreMoreThanTwenty =YES;
                    }
                    
                    //[self.commentsTableViewOutlet reloadData];
                    
                    if (onlyFirstTimeMoveToBottom) {
                        onlyFirstTimeMoveToBottom =  NO;
                        self.timerIvar  =   [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(changeThePositionOfTableview) userInfo:nil repeats:YES];
                        [self.commentTextView becomeFirstResponder];
                        self.commentsTableViewOutlet.backgroundView = nil ;
                        [self.commentsTableViewOutlet reloadData];
                    }
                    else {
                          [self.commentsTableViewOutlet reloadData];
                    }
                }
                else {
                    needToShowHeaderForMorePosts = NO;
                    [self showingMessageForTableViewBackground];
                     [self.commentsTableViewOutlet reloadData];
                    
                }
            }
                break;
            default:
                break;
        }
    }
}

-(void)addnewRow:(NSInteger )atIndex {
    //here adding new data (single comment details )to old data.
    [_commentsTableViewOutlet beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:atIndex inSection:1];
    [self.commentsTableViewOutlet insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationBottom];
    [_commentsTableViewOutlet endUpdates];
  
    if (self.commentsTableViewOutlet.contentSize.height > self.commentsTableViewOutlet.frame.size.height) {
        
        CGRect frame = [self.commentsTableViewOutlet rectForRowAtIndexPath:indexPath];
        NSLog(@"row height : %f", frame.size.height);
        
        
        CGPoint newPosition = CGPointMake(0, self.commentsTableViewOutlet.contentSize.height - self.commentsTableViewOutlet.frame.size.height + frame.size.height );
        [self.commentsTableViewOutlet setContentOffset:newPosition animated:YES];
    }
}




/*-----------------------------------------------------------------------------*/
#pragma
#pragma mark - Handling Hashtags,URL and UserNames.
/*------------------------------------------------------------------------------*/

-(void)handlinguserName:(id)sender {
    
    CommentsTableViewCell *receivedCell = (CommentsTableViewCell *)sender;
    // Attach a block to be called when the user taps a user handle.
    
    receivedCell.commentLabelOutlet.userHandleLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        [self goToProfileOfTheUserName:string];
    };
}

-(void)goToProfileOfTheUserName:(NSString *)string {
    UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
    
    //removing @ from string.
    NSString *stringWithoutspecialCharacter = [string
                                               stringByReplacingOccurrencesOfString:@"@" withString:@""];
    newView.checkProfileOfUserNmae = stringWithoutspecialCharacter;
    newView.checkingFriendsProfile = YES;
    newView.ProductDetails = YES;
    [self.navigationController pushViewController:newView animated:NO];
}


-(void)handlingURLLink :(id)sender {
    CommentsTableViewCell *receivedCell = (CommentsTableViewCell *)sender;
    // Attach a block to be called when the user taps a URL
    receivedCell.commentLabelOutlet.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        NSLog(@"URL tapped %@", string);
    };
}



#pragma mark
#pragma mark - tapTodismissKeyboard


- (IBAction)userNameButtonAction:(id)sender {
    
    UIButton *selectedButton = (UIButton *)sender;
    NSString *nameOfUser = selectedButton.titleLabel.text;
    [self goToProfileOfTheUserName:nameOfUser];
}


-(void)showingMessageForTableViewBackground{
    
    self.backgroundViewForTable.frame = CGRectMake(0, 0, self.commentsTableViewOutlet.bounds.size.width, self.commentsTableViewOutlet.bounds.size.height);
    self.commentsTableViewOutlet.backgroundView = self.backgroundViewForTable;
}

@end
