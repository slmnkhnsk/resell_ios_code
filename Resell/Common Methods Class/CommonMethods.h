//
//  CommonMethods.h

//
//  Created by Rahul Sharma on 04/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CBObjects.h"
@interface CommonMethods : NSObject



+ (UIButton *) createNavButtonsWithselectedState:(NSString *)selectedStateImage normalState :(NSString *)normalStateImage;
+ (UIAlertController *) showAlertWithTitle :(NSString *)title message :(NSString *)message actionTitle:(NSString *)actionTitle;
+ (UINavigationController *) presentLoginScreenController;
+ (UIBarButtonItem *)getNegativeSpacer;
+ (CGFloat)measureWidthLabel: (UILabel *)label;
+ (NSDictionary *)updateDeviceDetailsForAdmin;
+(void)setNegativeSpacingforNavigationItem :(UINavigationItem *)navItem  andExtraBarItem:(UIBarButtonItem *)extraItem ;
+(void)authTokenExpired_UserLoggedInOtherDeviceForview :(UIView *)view andTaBbar :(UITabBarController *)tabBarController;
@end
