//
//  EditProfileViewController.m
//  Pods
//
//  Created by Rahul Sharma on 5/5/16.
//
#import "EditProfileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "WebServiceConstants.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"
#import "TinderGenericUtility.h"
#import "TWPhotoPickerController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "FBLoginHandler.h"
#import "UIImageView+WebCache.h"
#import  "FontDetailsClass.h"
#import "Cloudinary.h"
#import "Helper.h"

@interface EditProfileViewController ()<FBLoginHandlerDelegate,CLUploaderDelegate,WebServiceHandlerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {

 
    NSArray *pickerArray;
    CLCloudinary *cloudinary;
    NSString *oldprofilePicUrl;
    UIActivityIndicatorView *av;
    NSDictionary *cloundinaryCreditinals;
    BOOL  isUploadImageToCloudinary;
    BOOL needToShowAlertForSavingChanges;
    NSString *newProfilePicImagePath;
     NSInteger count;
}

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    count = 1 ;
    [self createNavLeftButton];
    [self createTapGestureForView];
    [self createTapGestureScrollDownView];
    [self createTapGestureForProfileImageAndLabel];
    [self convertProfileImageToRoundedShape];
    [self navBarCustomization];
    [self gettingCloudinaryCredntials];
    oldprofilePicUrl = _profilepicurl;
    isUploadImageToCloudinary = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedPhoneNumberdataReceived:) name:mUpdateNumberNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedEmaildataReceived:) name:mUpdateEmailNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextChanged:) name:UITextFieldTextDidChangeNotification object:self.userNameTextfield];
    self.bioTextViewOutlet.delegate =self;
    self.bioTextViewOutlet.autocorrectionType = UITextAutocorrectionTypeNo;
}


-(void)textFieldTextChanged:(id)sender {
    NSString *convertString = self.userNameTextfield.text;
    self.userNameTextfield.text = [convertString lowercaseString];
    if ([convertString containsString:@" "]) {
        if ([self.userNameTextfield.text length] > 0) {
            self.userNameTextfield.text = [self.userNameTextfield.text substringToIndex:[self.userNameTextfield.text length] - 1];
            NSString *addUnderScore = [self.userNameTextfield.text stringByAppendingString:@"_"];
            self.userNameTextfield.text = addUnderScore;
        }
    }
}

-(void) gettingCloudinaryCredntials{
    cloundinaryCreditinals =[[NSUserDefaults standardUserDefaults]objectForKey:cloudinartyDetails];
}
-(void)navBarCustomization {
    self.navigationItem.title = NSLocalizedString(navTitleForEditProfile, navTitleForEditProfile);
    self.navigationController.navigationBar.translucent = NO ;
}


-(void)updatedPhoneNumberdataReceived:(NSNotification *)noti {
    _phoneNumberTextField.text =  [NSString stringWithFormat:@"%@", noti.object[@"updatedPhoneNumber"]];
}

-(void)updatedEmaildataReceived:(NSNotification *)noti {
    _emailTextField.text = noti.object[@"updatedEmailId"];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    if (_necessarytocallEditProfile) {
        // Requesting For Post Api.(passing "token" as parameter)
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      };
        [WebServiceHandler RequestTypeEditProfile:requestDict andDelegate:self];
        
        
        //showing activity view.
        self.viewForActivityIndicator.hidden =NO;
        self.viewForDetails.hidden = YES;
        

        [self createActivityViewInNavbar];
        
        _necessarytocallEditProfile=NO;
    }
}


//method for creating activityview in  navigation bar right.
- (void)createActivityViewInNavbar {
    av = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [av setFrame:CGRectMake(0,0,50,30)];
    [self.view addSubview:av];
    av.tag  = 1;
    [av startAnimating];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:av];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}


#pragma mark
#pragma mark - rounded image.

-(void)convertProfileImageToRoundedShape {
    [self.view layoutIfNeeded];
    _profileImageViewOutlet.image = _profilepic;
    _profileImageViewOutlet.layer.cornerRadius = _profileImageViewOutlet.frame.size.height/2;
    _profileImageViewOutlet.clipsToBounds = YES;
}

#pragma mark
#pragma mark - navigation bar buttons

//method for creating navigation bar left button.
- (void)createNavLeftButton {
    UIButton *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setTitle:NSLocalizedString(alertCancel, alertCancel) forState:UIControlStateNormal];
    
    [navCancelButton setTitleColor:mBaseColor forState:UIControlStateNormal];
    [navCancelButton addTarget:self action:@selector(CancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    navCancelButton.titleLabel.font = [UIFont fontWithName:RobotoRegular size:17];
    [navCancelButton setFrame:CGRectMake(0,0,60,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

//method for creating navigation bar right button.
- (void)createNavRightButton {

    UIButton *navDoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navDoneButton setTitle:NSLocalizedString(doneButtonTitle, doneButtonTitle) forState:UIControlStateNormal];
     
    [navDoneButton setTitleColor:mBaseColor forState:UIControlStateNormal];
    navDoneButton.titleLabel.font = [UIFont fontWithName:RobotoRegular size:17];
    [navDoneButton setFrame:CGRectMake(0,0,60,40)];
    [navDoneButton addTarget:self action:@selector(DoneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
     
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navDoneButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

//action for navigation bar items (buttons).

- (void)CancelButtonAction:(UIButton *)sender {
    if(needToShowAlertForSavingChanges) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(unsavedChanges, unsavedChanges) message:NSLocalizedString(unsavedChangesMessage, unsavedChangesMessage) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *noButton = [UIAlertAction actionWithTitle:NSLocalizedString(alertNo, alertNo) style:UIAlertActionStyleDefault handler:nil];
        UIAlertAction *yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(alertYes, alertYes)  style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    { [self goBack];}];
        
        [alertController addAction:noButton];
        [alertController addAction:yesButton];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        [self goBack];
    }
}


- (void)DoneButtonAction:(UIButton *)sender {
    [self hideKeyboard];
    
    // if there is no user name just need to show the alert.
    //   username is mandatory.
    
    if ([_userNameTextfield.text isEqualToString:@""]) {
        [self showAlert:NSLocalizedString(emptyUserName, emptyUserName)];
    }
    else if ([_userNameTextfield.text length] > 30 ) {
        [self showAlert:NSLocalizedString(UsernameTextLimitsWarningMessage, UsernameTextLimitsWarningMessage)] ; }
    else if ([_fullNameTextField.text length] > 30 ) {
        [self showAlert:NSLocalizedString(NameTextLimitsWarningMessage, NameTextLimitsWarningMessage)] ;
    }
    else {
        
        if (![self.websiteTextField.text isEqualToString:@""]) {
            if (![self validateUrl:self.websiteTextField.text]) {
                [Helper showAlertWithTitle:NSLocalizedString(alertMessage, alertMessage) Message:NSLocalizedString(invalidWebUrl, invalidWebUrl) viewController:self];
            }
            else {
                if (isUploadImageToCloudinary) {
                    ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
                    [loginPI showPIOnView:self.view withMessage:NSLocalizedString(updateDetailsIndicatorTitle, updateDetailsIndicatorTitle)];
                    [self uploadingImageToCloudinary:newProfilePicImagePath];
                }
                else {
                    [self requestForSavingProfile];
                }
            }
        }
        else {
            //checking is necceassry to upload image to cloudinary.
            if (isUploadImageToCloudinary) {
                ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
                [loginPI showPIOnView:self.view withMessage:NSLocalizedString(updateDetailsIndicatorTitle, updateDetailsIndicatorTitle)];
                [self uploadingImageToCloudinary:newProfilePicImagePath];
            }
            else {
                [self requestForSavingProfile];
            }
        }
        
    }
}


-(void)requestForSavingProfile {
    ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
    [loginPI showPIOnView:self.view withMessage:@"Updating Details"];
    
    NSString *bioText = flStrForObj(self.bioTextViewOutlet.text);
    if ([self.bioTextViewOutlet.text isEqualToString:NSLocalizedString(bio, bio)]) {
        bioText = @"";
    }
    
    // Requesting For Post Api.(passing "token" as parameter)
    NSDictionary *requestDict = @{
                                 mauthToken :flStrForObj([Helper userToken]),
                                  mUserName:flStrForObj(self.userNameTextfield.text),
                                  mfullName :flStrForObj(self.fullNameTextField.text),
//                                  mbio:flStrForObj(self.bioTextField.text),
                                  mbio:flStrForObj(bioText),
                                  mwebsite:flStrForObj(self.websiteTextField.text),
                                  mgender:flStrForObj(self.genderLabel.text),
                                  mProfileUrl:flStrForObj(_profilepicurl)
                                  };
    [WebServiceHandler RequestTypeSavingProfile:requestDict andDelegate:self];
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx = @"((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*)+)+(/)?(\\?.*)?";

    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    
    if ([urlTest evaluateWithObject:candidate]) {
        if ([candidate containsString:@".."]) {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return false;
    }
}


-(void)createTapGestureForProfileImageAndLabel {
    //tapGesture hiding keyBoard.
    UITapGestureRecognizer *tapForImage = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(showactionShett)];
    tapForImage.delegate = self;
    [self.profileImageViewOutlet addGestureRecognizer:tapForImage];
    
    UITapGestureRecognizer *tapForLabel =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showactionShett)];
    tapForLabel.delegate = self ;
    [self.editLabel addGestureRecognizer:tapForLabel];
    
}

-(void)createTapGestureForView {
    //tapGesture hiding keyBoard.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(showPickerView)];
    tap.delegate = self;
    [self.genderView addGestureRecognizer:tap];
}

-(void)createTapGestureScrollDownView {
    //tapGesture hiding keyBoard.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(scrollDown)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

-(void)scrollDown {
   }

-(void)showPickerView {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil  message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(NotSpecified, NotSpecified)  style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.genderLabel setText:NSLocalizedString(NotSpecified, NotSpecified)];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(Male, Male) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.genderLabel setText:NSLocalizedString(Male, Male)];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(female, female) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.genderLabel setText:NSLocalizedString(female, female)];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
-(void)showactionShett {
    [self hideKeyboard];
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:nil];
    // choose From Library
    UIAlertAction *chooseFromLib = [UIAlertAction actionWithTitle:NSLocalizedString(chooseFromLibrary, chooseFromLibrary) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        TWPhotoPickerController *photoPickr = [[TWPhotoPickerController alloc] init];
        photoPickr.viewFromProfileSelector = @"itisForProfilePhoto";
        photoPickr.cropBlock = ^(UIImage *image) {
            [self.profileImageViewOutlet setImage:image];
            self.profileImageViewOutlet.contentMode = UIViewContentModeScaleAspectFit ;
            
            NSString *imageName = [NSString stringWithFormat:@"%@%@.png",@"Image",[self getCurrentTime]];
            NSData *imgData1 = UIImageJPEGRepresentation(image, 0.5f);
            //to get the image path.
            NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString* documentsDirectory = [paths objectAtIndex:0];
            NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
            [imgData1 writeToFile:imagePath atomically:NO];
            isUploadImageToCloudinary = YES;
            newProfilePicImagePath = imagePath;
            _profilepicurl = @"wait need to upload image to cloudinary";
            [self.view layoutIfNeeded];
            _profileImageViewOutlet.layer.cornerRadius = _profileImageViewOutlet.frame.size.height/2;
            _profileImageViewOutlet.clipsToBounds = YES;
        };
        [self presentViewController:photoPickr animated:YES completion:NULL];
    }];
    
    // import From Facebook
    UIAlertAction *importFromFacebook = [UIAlertAction actionWithTitle:NSLocalizedString(importProfilePicFromFacebook, importProfilePicFromFacebook) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        NSDictionary *fbdetails = [[NSUserDefaults standardUserDefaults]objectForKey:@"userFbDetails"];
        
        if (fbdetails) {
              NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=400&heigth=400",fbdetails[@"id"]]];
            self.profilepicurl = pictureURL.absoluteString;
            
            [_profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:self.profilepicurl]];
            [self.view layoutIfNeeded];
            _profileImageViewOutlet.layer.cornerRadius = _profileImageViewOutlet.frame.size.height/2;
            _profileImageViewOutlet.clipsToBounds = YES;
            
            newProfilePicImagePath = nil;
            isUploadImageToCloudinary = NO;
        }
        else {
            //request for fb.
            FBLoginHandler *handler = [FBLoginHandler sharedInstance];
            [handler loginWithFacebook:self];
            [handler setDelegate:self];
        }
    }];
    
    // Take Photo
    UIAlertAction *takePhoto = [UIAlertAction actionWithTitle:NSLocalizedString(takePhotoFromCamera, takePhotoFromCamera) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
#if TARGET_IPHONE_SIMULATOR
        UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Warning" message:@"IN SIMULATOR CAMERA IS NOT AVAILABLE" actionTitle:@"OK"];
        mPresentAlertController;
#else
        self.imgpicker = [[UIImagePickerController alloc] init];
        self.imgpicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.imgpicker.delegate =self;
        [self presentViewController:self.imgpicker animated:YES completion:nil];
#endif
        
    }];
    
    // Remove Photo
    UIAlertAction *removeCurrentPhoto = [UIAlertAction actionWithTitle:NSLocalizedString(removeCurrentProfilePic, removeCurrentProfilePic) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        _profileImageViewOutlet.image = [UIImage imageNamed:@"defaultpp"];
        _profilepicurl = @"defaultUrl";
        isUploadImageToCloudinary = NO;
    }];
    
    [controller addAction:cancel];
    [controller addAction:chooseFromLib];
    [controller addAction:importFromFacebook];
    [controller addAction:takePhoto];
    if(![_profilepicurl isEqualToString:@"defaultUrl"])
    [controller addAction:removeCurrentPhoto];
    [self presentViewController:controller animated:YES completion:nil];
    
}

-(void)hideKeyboard {
    [self.emailTextField resignFirstResponder];
    [self.phoneNumberTextField resignFirstResponder];
    [self.userNameTextfield resignFirstResponder];
    [self.fullNameTextField resignFirstResponder];
    [self.bioTextField resignFirstResponder];
    [self.bioTextViewOutlet resignFirstResponder];
    [self.websiteTextField resignFirstResponder];
}

#pragma mark
#pragma mark - TextView Delegate

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([self.bioTextViewOutlet.text  isEqual: NSLocalizedString(bio, bio)]) {
        [textView setText:@""];
        textView.textColor = [UIColor blackColor];
    }
    
    textView.selectedRange = NSMakeRange(textView.text.length, 0);
}



-(void)adjustContentSize:(UITextView*)tv{
    CGFloat deadSpace = ([tv bounds].size.height - [tv contentSize].height);
    CGFloat inset = MAX(0, deadSpace/2.0);
    tv.contentInset = UIEdgeInsetsMake(inset, tv.contentInset.left, inset, tv.contentInset.right);
}

-(void)textViewDidChange:(UITextView *)textView
{
   
    needToShowAlertForSavingChanges = YES;
//    [self adjustContentSize:self.bioTextViewOutlet];
    
    CGRect newFramae = self.bioTextViewOutlet.frame;
    newFramae.size.height = self.bioTextViewOutlet.contentSize.height;
    
    if(newFramae.size.height  < 40 ) {
        self.topViewHeightConstraint.constant = 200;
        
    }
    else {
        if (newFramae.size.height < 120) {
           self.topViewHeightConstraint.constant = 165 + newFramae.size.height;
        }
    }
}



-(void)updateHeightOfTextView{
    CGRect newFramae = self.bioTextViewOutlet.frame;
    newFramae.size.height = self.bioTextViewOutlet.contentSize.height;
    
    if(newFramae.size.height  < 40 ) {
        self.topViewHeightConstraint.constant = 200;
        
    }
    else {
        if (newFramae.size.height < 120) {
            self.topViewHeightConstraint.constant = 165 + newFramae.size.height;
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
    }
     else if([[textView text] length] > 149 )
    {
        [self showAlert:NSLocalizedString(bioTextLimitsWarningMessage, bioTextLimitsWarningMessage)];
        return NO;
    }
    
    return YES;
}

-(void)textViewDidChangeSelection:(UITextView *)textView
{
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if (self.bioTextViewOutlet.text.length == 0) {
        [textView setText:NSLocalizedString(bio, bio)];
        textView.textColor = [UIColor lightGrayColor];
    }
}

/*-----------------------------------*/
#pragma mark
#pragma mark - textfield Delegates.
/*-----------------------------------*/

/**
 *  giving implementation to key board return button.
 *
 */

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _fullNameTextField) {
        [ _userNameTextfield becomeFirstResponder];
    }
    
    else if (textField ==_userNameTextfield) {
        [ _websiteTextField becomeFirstResponder];
    }
    else if (textField == _websiteTextField) {
        [_bioTextField becomeFirstResponder];
        [_bioTextViewOutlet becomeFirstResponder];
    }
    else if(textField == _bioTextField){
        [_bioTextField resignFirstResponder];
    }
    return YES;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == _websiteTextField || textField == _bioTextField || textField == _fullNameTextField || textField == _userNameTextfield) {
         needToShowAlertForSavingChanges = YES;
    }
    
    if(textField == _userNameTextfield) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if ([string isEqualToString:filtered]) {
            textField.tintColor = [UIColor blueColor];
        }
        else {
            textField.tintColor = [UIColor redColor];
        }
        return [string isEqualToString:filtered];
    }
    return  YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    self.userNameTextfield.tintColor = [UIColor blueColor];
}

/*-------------------------------------------------------------*/
  #pragma
  #pragma mark - ScrollView Delegate(For Update Posts).(PAGING)
/*-------------------------------------------------------------*/

- (void)scrollViewDidScroll: (UIScrollView *)scroll {
    CGFloat currentOffset = scroll.contentOffset.y;
    NSLog(@"%f",currentOffset);
    if (currentOffset < -64) {
        [UIView animateWithDuration:0.2 animations:^{
            self.viewTopConstraint.constant = 20;
            [self.view layoutIfNeeded];
            self.pickerView.hidden = YES;
        }];
    }
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    return [pickerArray count];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
     [self.genderLabel setText:[pickerArray objectAtIndex:row]];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.viewTopConstraint.constant = 20;
        [self.view layoutIfNeeded];
        self.pickerView.hidden = YES;
    }];
   
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
    return [pickerArray objectAtIndex:row];
}

/*--------------------------------------------*/
#pragma mark
#pragma mark - imageResizing.
/*--------------------------------------------*/

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(NSString*)getCurrentTime {
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // Set the dateFormatter format
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    [dateFormatter setDateFormat:@"EEEMMddyyyyHHmmss"];
    // Get the date time in NSString
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    //  //NSLog(@"%@", dateInStringFormated);
    return dateInStringFormated;
    // Release the dateFormatter
    //[dateFormatter release];
}

/*-----------------------------------------*/
#pragma mark
#pragma mark - image picker(camera photo)
/*-----------------------------------------*/

//image picker
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary*)info {
    NSData *dataimage = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerOriginalImage"], 1);
    
    UIImage *selectedProfileImage = [Helper resizeImage:[[UIImage alloc] initWithData:dataimage]];
    
    NSString *imageName = [NSString stringWithFormat:@"%@%@.png",@"Image",[self getCurrentTime]];
    NSData *imgData1 = UIImageJPEGRepresentation(selectedProfileImage, 0.5f);
    //to get the image path.
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
    [imgData1 writeToFile:imagePath atomically:NO];
    UIImage *storingImage = [[UIImage alloc] initWithData:dataimage]; // if u want to store original captured image then keep dataimage instead of data.
    UIImageWriteToSavedPhotosAlbum(storingImage, nil, nil, nil);
    isUploadImageToCloudinary = YES;
    _profilepicurl = @"wait need to upload image to cloudinary";
    newProfilePicImagePath = imagePath;
    [self.profileImageViewOutlet setImage:selectedProfileImage];
    [self.imgpicker dismissViewControllerAnimated:YES completion:nil];
}



/*---------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*---------------------------------*/

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [av stopAnimating];
    [self createNavRightButton];
    
    //hiding activity view.
    self.viewForActivityIndicator.hidden = YES;
    self.viewForDetails.hidden = NO;
    
    if (error) {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
        return;
    }
    
    //storing the response from server to dictonary.
    NSDictionary *responseDict = (NSDictionary*)response;
    //checking the request type and handling respective response code.
    if (requestType == RequestTypeSavingProfile ) {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        UserDetails *user = response ;
        switch (user.code) {
            case 200: {
                [Helper storeUserLoginDetails:user];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfilePic" object:user];
                [self goBack];
                
            }
                break;
                default:
                [Helper showAlertWithTitle:nil Message:NSLocalizedString(profileChangesFailed, profileChangesFailed) viewController:self];
                break;
        }
    }
    
    //checking the request type and handling respective response code.
    if (requestType == RequestTypeEditProfile ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                 if (flStrForObj(responseDict[@"data"][@"name"])) {
                     self.fullNameTextField.text = flStrForObj(responseDict[@"data"][@"fullName"]);
                 }
                 if (flStrForObj(responseDict[@"data"][@"username"])) {
                     self.userNameTextfield.text = flStrForObj(responseDict[@"data"][@"username"]);
                 }
                 if (flStrForObj(responseDict[@"data"][@"phoneNumber"])) {
                     self.phoneNumberTextField.text = flStrForObj(responseDict[@"data"][@"phoneNumber"]);
                 }
                 if (flStrForObj(responseDict[@"data"][@"email"])) {
                     self.emailTextField.text = flStrForObj(responseDict[@"data"][@"email"]);
                 }
                 if (flStrForObj(responseDict[@"data"][@"bio"])) {
                     self.bioTextField.text = flStrForObj(responseDict[@"data"][@"bio"]);
                     self.bioTextViewOutlet.text =  flStrForObj(responseDict[@"data"][@"bio"]);
                     if ([self.bioTextViewOutlet.text isEqualToString:@""]) {
                         self.bioTextViewOutlet.text = NSLocalizedString(bio, bio);
                         self.bioTextViewOutlet.textColor = [UIColor lightGrayColor];
                     }
                     
                     
                     [self updateHeightOfTextView];
                 }
                
               
                 if (flStrForObj(responseDict[@"data"][@"websiteUrl"])) {
                     self.websiteTextField.text = flStrForObj(responseDict[@"data"][@"websiteUrl"]);
                 }
                 if (flStrForObj(responseDict[@"data"][@"profilePicUrl"])) {
                     _profilepicurl= flStrForObj(responseDict[@"data"][@"profilePicUrl"]);
                     [_profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:_profilepicurl]];
                     [_profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:_profilepicurl] placeholderImage:[UIImage imageNamed:@"defaultpp"]];
                     [self.view layoutIfNeeded];
                     self.profileImageViewOutlet.layer.cornerRadius = self.profileImageViewOutlet.frame.size.height/2;
                     _profileImageViewOutlet.clipsToBounds = YES;
                 }
                 if (flStrForObj(responseDict[@"data"][@"gender"])) {
                     self.genderLabel.text = flStrForObj(responseDict[@"data"][@"gender"]);
                     if ([self.genderLabel.text isEqualToString:@""]) {
                         self.genderLabel.text = NSLocalizedString(NotSpecified, NotSpecified);
                     }
                 }
            }
                break;
            default:
                break;
        }
        
        if (requestType == RequestTypeCloudinaryCredintials ) {
            
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    // success response.
                    [[NSUserDefaults standardUserDefaults] setObject:responseDict forKey:cloudinartyDetails];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self gettingCloudinaryCredntials];
                }
                    break;
                    
                default:
                    break;
            }
        }

    }
}


- (IBAction)emailAddressButtonAction:(id)sender {
    [self performSegueWithIdentifier:@"updateEmailVcSegue" sender:nil];
}

- (IBAction)phoneNumberButtonAction:(id)sender {
       [self performSegueWithIdentifier:@"editProfileToPhoneNumberVerification" sender:nil];
}



/*------------------------------------------------*/
#pragma mark
#pragma mark - facebook handler
/*-------------------------------------------------*/

/**
 *  Facebook login is success
 *  @param userInfo Userdict
 */

- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo {

      NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=400&heigth=400",userInfo[@"id"]]];
     self.profilepicurl = pictureURL.absoluteString;
  
    [_profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:self.profilepicurl]];
     [self.view layoutIfNeeded];
    _profileImageViewOutlet.layer.cornerRadius = _profileImageViewOutlet.frame.size.height/2;
    _profileImageViewOutlet.clipsToBounds = YES;
    
    newProfilePicImagePath = nil;
    isUploadImageToCloudinary = NO;
}

/**
 *  Login failed with error
 *
 *  @param error error
 */
- (void)didFailWithError:(NSError *)error {
    [Helper showAlertWithTitle:NSLocalizedString(alertMessage, alertMessage) Message:NSLocalizedString(mFacebookLoginErrorMessage, mFacebookLoginErrorMessage) viewController:self];
}

/**
 *  User cancelled
 */

- (void)didUserCancelLogin {
    NSLog(@"USER CANCELED THE LOGIN");
}


/*--------------------------------------------*/
#pragma mark
#pragma mark - cloudinaryImageUploadingDelegates.
/*--------------------------------------------*/

- (void) uploaderSuccess:(NSDictionary*)result context:(id)context {
    NSString* publicId = [result valueForKey:@"public_id"];
    NSLog(@"Upload success. Public ID=%@, Full result=%@", publicId, result);
    _profilepicurl = result[@"secure_url"];
    [self requestForSavingProfile];
    isUploadImageToCloudinary = NO;
}

-(void)uploaderError:(NSString*)result code:(NSInteger )code context:(id)context {
    NSLog(@"Upload error: %@, %ld", result, (long)code);
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if(count == 1)
    {
        UIAlertController *controller  = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(unableToPostYourListings, unableToPostYourListings) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:
                                 ^(UIAlertAction *action)
                                 {
                                     [WebServiceHandler getCloudinaryCredintials:@{@"":@""} andDelegate:self];
                                     
                                 }];
        
        [controller addAction:action];
        [self presentViewController:controller animated:YES completion:nil];
        count++ ;
    }
    else
    {
        [self requestForSavingProfile];
    }
}

- (void) uploaderProgress:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite context:(id)context {
    NSLog(@"Upload progress: %ld/%ld (+%ld)", (long)totalBytesWritten, (long)totalBytesExpectedToWrite, (long)bytesWritten);
}




-(void)uploadingImageToCloudinary:(NSString *)imagePath {
    
    CLCloudinary *mobileCloudinary = [[CLCloudinary alloc] init];
    [mobileCloudinary.config setValue:cloundinaryCreditinals[@"response"][@"cloudName"] forKey:@"cloud_name"];
    CLUploader* mobileUploader = [[CLUploader alloc] init:mobileCloudinary delegate:self];
    if(cloundinaryCreditinals){
    [mobileUploader upload:imagePath options:@{
                                                                   @"signature":cloundinaryCreditinals[@"response"][@"signature"],
                                                                   @"timestamp": cloundinaryCreditinals[@"response"][@"timestamp"],
                                                                   @"api_key": cloundinaryCreditinals[@"response"][@"apiKey"],
                                                                   }];
    }
    else{
        [self uploaderError:nil code:0 context:nil];
    }
}

//image need to upload when take photo or select from library
//for fb i will get link so no need to upload in cloudinary
//for remove photo default url is there
//no need to call cloudinary  if user not changed d profil pic.

-(void)goBack {
    if ([_pushingVcFrom isEqualToString:@"ProfileScreen"]) {
         [self dismissViewControllerAnimated:NO completion:nil];
    }
    else{
          [self.navigationController popViewControllerAnimated:YES];
    }
}



/**
 Alert method

 @param message message to display.
 */
-(void)showAlert:(NSString *)message
{
    [Helper showAlertWithTitle:@"" Message:message viewController:self];
    }

#pragma mark-
#pragma mark - Tap Gesture

- (IBAction)tapToDismissKeyboard:(id)sender {
    
    [self.view endEditing:YES];
    
}
@end
