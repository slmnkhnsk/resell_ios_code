//
//  ForgotPasswordViewController.m

//
//  Created by Rahul_Sharma on 04/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "FontDetailsClass.h"

@interface ForgotPasswordViewController ()<WebServiceHandlerDelegate,UITextFieldDelegate>

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = NO ;
    self.navigationItem.title = @"Forgot Password";
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,40,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButton) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
    self.sendLoginLinkButtonOutlet.backgroundColor = mLoginButtonDisableBackgroundColor;
   }

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveAccKeybaord:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveToOriginal) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
    self.navigationController.navigationBar.hidden = YES ;
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification ];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden {
    return NO;
}


#pragma mark
#pragma mark - phone number validation

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self sendLoginLinkButtonAction:self];
    return YES;
}



- (IBAction)sendLoginLinkButtonAction:(id)sender {
    
   
         //username or email address.
        if (![self emailValidationCheck:[_emailTextField text]]) {
            
            [self errrAlert:@"Please enter a valid email"];
        }
        else {
            [self.activityIndicator startAnimating];
            NSDictionary *requestDict = @{@"type" :@"0",
                                          mEmail    : _emailTextField.text
                                          };
            [WebServiceHandler resetPassword:requestDict andDelegate:self];
        }
}

- (BOOL)emailValidationCheck:(NSString *)emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

- (void)errrAlert:(NSString *)message {
    //[self dismissKeyboard];
    //creating alert for error message
 
    [UIView animateWithDuration:0.2 animations:^{
        [self.emailTextField resignFirstResponder];
        CGRect frameOfView = self.view.frame;
        frameOfView.origin.y = 0;
        frameOfView.origin.x=0;
        self.view.frame = frameOfView;
    }  completion:^(BOOL finished) {
        UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Message" message:message actionTitle:@"Ok"];
        mPresentAlertController;
          }];
}

#pragma mark
#pragma mark - phone number validation

- (BOOL)validatePhone:(NSString *)enteredphoneNumber {
    NSString *phoneNumber = enteredphoneNumber;
    NSString *phoneRegex = @"[2356789][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    return matches;
}


#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.
 
 @param notification post by Notificationcentre.
 */
-(void)veiwMoveAccKeybaord :(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    //Given size may not account for screen rotation
    NSInteger HeightOfkeyboard = MIN(keyboardSize.height,keyboardSize.width);
    float maxY = CGRectGetMaxY(_sendLoginLinkButtonOutlet.frame) + HeightOfkeyboard +3;
    float reminder = CGRectGetHeight(self.view.frame) - maxY;
    if (reminder < 0) {
        [UIView animateWithDuration:0.4 animations: ^ {
            CGRect frameOfView = self.view.frame;
            frameOfView.origin.y = reminder;
            self.view.frame = frameOfView;}];
    }
}

/**
 This method will move view back to original frame.
 */
-(void)veiwMoveToOriginal
{
    [UIView animateWithDuration:0.4 animations: ^ {
        CGRect frameOfView = self.view.frame;
        frameOfView.origin.y = 0;
        self.view.frame = frameOfView;}];
}

#pragma mark
#pragma mark - WebServiceDelegate

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    //handling response.
    
    [self.activityIndicator stopAnimating];
    if (error) {
        [self errrAlert:[error localizedDescription]];
        return;
    }
    //storing the response from server to dictonary(responseDict).
    NSDictionary *responseDict = (NSDictionary*)response;
    if (requestType == RequestTypemakeresetPassword) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
                UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:responseDict[@"message"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self.navigationController popViewControllerAnimated:YES];
                    ;
                }];
                [controller addAction:action];
                [self presentViewController:controller animated:YES completion:nil];
              
            }
                break;
                // all these responses are error messages.
            case 204: {
                [self errrAlert:responseDict[@"message"]];
            }
                break;
            default:
                [self errrAlert:responseDict[@"message"]];
                break;
        }
    }
}

- (void) navLeftButton{
    [self.navigationController popViewControllerAnimated:YES];
 }

- (IBAction)tapToDismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)emailTextfieldValueIsChanged:(id)sender {
    if (_emailTextField.text.length < 2 ){
            self.sendLoginLinkButtonOutlet.enabled = NO;
            _sendLoginLinkButtonOutlet.backgroundColor = mLoginButtonDisableBackgroundColor;
        }
        else {
            self.sendLoginLinkButtonOutlet.enabled = YES;
            self.sendLoginLinkButtonOutlet.backgroundColor = mLoginButtonEnableBackgroundColor;
        }
}
@end
