
//
//  GetDirectionController.m
//  Homappy
//
//  Created by Rahul Sharma on 28/07/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "GetCurrentLocation.h"

@interface GetCurrentLocation ()<CLLocationManagerDelegate>
{
    GMSGeocoder *geocoder_;
    CLLocationManager *locationManager;
}
@end

@implementation GetCurrentLocation

static GetCurrentLocation *share;

+ (instancetype)sharedInstance
{
    if (!share)
    {
        share  = [[self alloc] init];
    }
    return share;
}

/**
 *  All Directions Method
 */

/*---------------------------------------*/
#pragma mark - CLLocation Delegate Method
/*---------------------------------------*/

- (void)getLocation
{
    //check location services is enabled
    
    if ([CLLocationManager locationServicesEnabled])
    {
        if (!locationManager) {
            
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if  ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
            {
                [locationManager requestWhenInUseAuthorization];
            }
        }
        [locationManager startUpdatingLocation];
        
    }
    else {
    }

}

/*
 To Get Updated lattitude & longitude
 @return nil.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSString *latitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    self.lastLatLong = location.coordinate;

         /*****************************************************/
        
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (!(error))
             {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 
                 self.currentCity = [placemark locality];
                 self.countryShortCode = [placemark ISOcountryCode];

                 if (self.delegate && [self.delegate respondsToSelector:@selector(updatedAddress:)]) {
                     [self.delegate updatedAddress:[[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "]];
                 }
                 
                 NSLog(@"'Get Current Location Class'- Current Location:\nAddress: %@\nLat: %@\nLong :%@\nCity: %@\nZipCode: %@",[[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "],latitude,longitude,placemark.locality,placemark.postalCode);
             }
             else
             {
                 NSLog(@"Failed to update location : %@",error);
//                 if (self.delegate && [self.delegate respondsToSelector:@selector(didFailLocation)]) {
//                     [self.delegate didFailLocation];
//                 }
             }
         }];
        
        [locationManager stopUpdatingLocation];
        
    if (self.delegate && [self.delegate respondsToSelector:@selector(updatedLocation:and:)]) {
        [self.delegate updatedLocation:location.coordinate.latitude and:location.coordinate.longitude];
        }

//    }
}

/*
 To print error msg of location manager
 @param error msg.
 @return nil.
 */
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"locationManager failed to update location : %@",[error localizedDescription]);
    
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied)
    {
        // The user denied your app access to location information.
        [self gotoLocationServicesMessageViewController];
    }
    else  if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorNetwork)
    {
        //Network-related error
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        [self gotoLocationServicesMessageViewController];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)//kCLAuthorizationStatusAuthorized
    {
       
            
    }

}

/**
 *  Showing Location Service Message Controller (By Pushing)
 */
-(void)gotoLocationServicesMessageViewController
{
    
}

@end
