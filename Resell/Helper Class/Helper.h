//
//  Helper.h

//
//  Created by Rahul Sharma on 9/3/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceConstants.h"
#import "UserDetails.h"
@interface Helper : NSObject
+(NSString *)PostedTimeSincePresentTime:(NSTimeInterval)seconds;
+(NSString *)convertEpochToNormalTime :(NSString *)epochTime;
+(NSMutableAttributedString*)customisedActivityStmt:(NSString*)username :(NSString*)statment;
+(NSMutableAttributedString*)customisedActivityStmt:(NSString*)username seconUserName:(NSString *)secondUserName  timeForPost:(NSString *)time : (NSString*)statment;

+(void)storeUserLoginDetails:(UserDetails *)user;
+ (CGFloat)measureHieightLabel: (UILabel *)label;
+(NSString *)userName;
+(NSString *)userToken;
+(NSString *)getMQTTID;
+(NSString *)userProfileImageUrl;


+ (NSString*)getWebLinkForFeed:(NSString *)postDic;
+(CGFloat )heightOfText:(UILabel *)label;
+(NSString *)deviceToken;
+(void)twitterSharing:(NSMutableArray *)postDetails;
+ (void)makeFBPostWithParams:(NSMutableArray*)params;
+(void)chkTwitterLogin;

#pragma mark -
#pragma mark - Check Facebook Login.
/**
 Check Facebook login from anyviewController.

 @param VCReferenceObject viewController reference object.
 */
+(void)checkFbLoginforViewController :(UIViewController *)VCReferenceObject;
+(NSString *)instagramSharing:(NSString *)param;
+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message viewController :(UIViewController *)viewControllerRefrence ;
+(void)setToLabel:(UILabel*)lbl Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size Color:(UIColor*)color;
+(void)setButton:(UIButton*)btn Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size TitleColor:(UIColor*)t_color ShadowColor:(UIColor*)s_color;

+(NSString *)convertEpochToNormalTimeInshort :(NSString *)epochTime;

+(BOOL)emailValidationCheck:(NSString *)emailToValidate;
+(NSString *)makeWebPostLink:(NSString *)postId andUserName:(NSString *)userName;

+ (NSString *) nonNullStringForString:(NSString *) string;
+(NSString *)getPayPalLink;
+(void)storePaypalLinkWithPaypalLink : (NSString *)paypalLink;
+(UIView *)showMessageForNoInternet :(BOOL)show forView : (UIView *)view ;
+ (UIImage *)resizeImage:(UIImage *)image ;

#pragma mark -
#pragma mark - Show Unfollow Alert

/**
 This method show alert for asking permission to unfollow the user.
 
 @param profieImage     profileImage object of UIImage.
 @param profileName     profileName Of user in string Format.
 @param vcReference     reference of viewController.
 @param completionBlock Bolck will call method only in case of unfollowaction.

 */
+ (void)showUnFollowAlert:(UIImage *)profieImage
                      and:(NSString *)profileName 
  viewControllerReference:(UIViewController *)vcReference
             onComplition:(void (^)(BOOL isUnfollow))completionBlock;


+ (void)presentActivityController:(UIActivityViewController *)controller forViewController:(UIViewController *)refrenceVC ;


#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."

#define mInstaTableVcStoryBoardId       @"instaTableViewController"
#define mDiscoverPeopleVcSI             @"discoverPeopleStoryBoardId"
#define numberOfFbFriendFoundInPicogram @"numberOfYourFbFriendFoundInPicogram"
#define numberOfContactsFoundInPicogram @"numberOfContactsFoundInPicogram"
#define mpaypalView                     @"paypalViewController"





@end
