//
//  CellForFilteredItem.h

//
//  Created by Rahul Sharma on 06/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellForFilteredItem : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UIButton *buttonToDisplayFilter;

@end
