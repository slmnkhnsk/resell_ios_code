//
//  SortCategoryVC.h
//  Resell
//
//  Created by pc1 on 12/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SortDelegate <NSObject>

-(void)getSorting:(NSString *)selectedSorting;
@end

@interface SortCategoryVC: UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UITableView *tblSortBy;
    
}

@property(weak,nonatomic) NSString *sortBy;
@property BOOL checkArrayOfSorting;

@property(nonatomic,weak)id<SortDelegate>delegate;

@end
