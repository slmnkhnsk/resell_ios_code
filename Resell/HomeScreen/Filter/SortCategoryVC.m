//
//  SortCategoryVC.m
//  Resell
//
//  Created by pc1 on 12/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SortCategoryVC.h"
#import "CellForSorting.h"

#define mDefaultColor   [UIColor colorWithRed:72/255.0f green:72/255.0f blue:72/255.0f alpha:1.0f]

@interface SortCategoryVC () {
    
    NSArray *arrayOfSortingBy;
    NSInteger i;
    
}

@end

@implementation SortCategoryVC

#pragma mark - View LifeCycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayOfSortingBy=[[NSArray alloc]initWithObjects:sortingByNewestFirst,sortingByClosestFirst,sortingByPriceLowToHigh,sortingByPriceHighToLow,nil];
    
    [tblSortBy setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [tblSortBy setSeparatorColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1]];
    tblSortBy.delegate=self;
    tblSortBy.dataSource=self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.sortBy == nil) {
        self.sortBy = sortingByNewestFirst;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark-
#pragma mark - TableView DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayOfSortingBy.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:{
            CellForSorting *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForSort"];
            if([[arrayOfSortingBy objectAtIndex:indexPath.row] isEqualToString:self.sortBy ])
            {
                cell.imageMark.hidden=NO;
                cell.labelForSortBy.textColor = mBaseColor;
            }
            else{
                cell.imageMark.hidden=YES;
                cell.labelForSortBy.textColor = mDefaultColor;
                
            }
            
            cell.labelForSortBy.text = NSLocalizedString([arrayOfSortingBy objectAtIndex:indexPath.row],[arrayOfSortingBy objectAtIndex:indexPath.row]);
            return cell;
        }
            break;
           
        default:
        {
            UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
            return cell1;
        }
            break;
            
    }
}

#pragma mark-
#pragma mark - TableView Delegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CellForSorting *cell=[tableView cellForRowAtIndexPath:indexPath];
    for(i=0;i<arrayOfSortingBy.count;i++)
    {
        NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:0 ];
        CellForSorting *cell=[tableView cellForRowAtIndexPath:ind];
        if(i!=indexPath.row)
        {
            cell.imageMark.hidden=YES;
            cell.labelForSortBy.textColor = mDefaultColor;
        }
    }
    if(cell.imageMark.hidden){
        cell.imageMark.hidden=NO;
        cell.labelForSortBy.textColor = mBaseColor;
        self.sortBy =[arrayOfSortingBy objectAtIndex:indexPath.row];
    }
    else
    {
        self.sortBy = @"";
        cell.imageMark.hidden=YES;
        cell.labelForSortBy.textColor = mDefaultColor;
    }
    
    [self.delegate getSorting:self.sortBy];
    [self dismissViewControllerAnimated:true completion:nil];
    
}


@end
