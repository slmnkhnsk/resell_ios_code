//
//  SortPriceVC.h
//  Resell
//
//  Created by pc1 on 12/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SortPriceDelegate <NSObject>

-(void)getSortPrice:(NSString *)min maxPrice:(NSString *)max;
@end

@interface SortPriceVC : UIViewController <UITextFieldDelegate> {
    
    __weak IBOutlet UIView *minView;
    __weak IBOutlet UITextField *txtMin;
    __weak IBOutlet UIView *maxView;
    __weak IBOutlet UITextField *txtMax;

}

@property(weak,nonatomic) NSString *strMin, *strMax;

@property(nonatomic,weak)id<SortPriceDelegate>delegate;

@end
