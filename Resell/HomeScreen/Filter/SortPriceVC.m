//
//  SortPriceVC.m
//  Resell
//
//  Created by pc1 on 12/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SortPriceVC.h"

@interface SortPriceVC ()

@end

@implementation SortPriceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    txtMin.delegate = self;
    txtMax.delegate = self;
    
    txtMin.becomeFirstResponder;
    
    [self setUpDesign];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.strMin == nil) {
        self.strMin = @"";
    }
    
    if (self.strMin == nil) {
        self.strMax = @"";
    }
    
    txtMin.text = self.strMin;
    txtMax.text = self.strMax;

}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.view endEditing:YES];
    [self.delegate getSortPrice:txtMin.text maxPrice:txtMax.text];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark-
#pragma mark - SetView Design

-(void)setUpDesign {
    minView.layer.borderWidth = 1.0;
    minView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    minView.layer.cornerRadius = 2.0;
    minView.layer.masksToBounds = YES;
    
    maxView.layer.borderWidth = 1.0;
    maxView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    maxView.layer.cornerRadius = 2.0;
    maxView.layer.masksToBounds = YES;
}

#pragma mark-
#pragma mark - TextField Delegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    UIToolbar *toolbar = [[UIToolbar alloc]init];
    toolbar.frame = CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, 60);
    [toolbar setBarStyle:UIBarStyleDefault];
    [toolbar setTranslucent:NO];
    toolbar.backgroundColor = [UIColor whiteColor];
    
    UIButton *btnDone =[[UIButton alloc] initWithFrame:(CGRectMake(0, 7, [[UIScreen mainScreen] bounds].size.width - 10, 35))];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setFont:[UIFont fontWithName:@"Roboto-medium" size:15.0]];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDone setBackgroundColor:mBaseColor];
    [btnDone addTarget:self action:@selector(resignKeyboard) forControlEvents:UIControlEventTouchUpInside];
    btnDone.layer.cornerRadius = 3.0;
    btnDone.layer.masksToBounds = YES;
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
    [toolbar setItems:@[doneBtn]];
    [toolbar setUserInteractionEnabled:YES];
    [toolbar sizeToFit];
    [txtMin setDelegate:self];
    [txtMin setInputAccessoryView:toolbar];
    [txtMax setDelegate:self];
    [txtMax setInputAccessoryView:toolbar];
    
    return YES;
    
}

- (void)resignKeyboard {
    
    [self.view endEditing:YES];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
