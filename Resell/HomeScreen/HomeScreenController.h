//
//  HomeScreenController

//
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterViewController.h"
#import "PostingScreenModel.h"
#import <CoreLocation/CLLocationManager.h>



@interface HomeScreenController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate,filterDelegate, UIPopoverPresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource>
{
        PostingScreenModel *post;
        NSString *categoryInStringFormat ;
        UIRefreshControl *refreshControl;
        NSMutableArray  *explorePostsresponseData , *arrayOfCategoryImagesUrls, *arrayOfcategory,*temp ,*productsArray;
        UIActivityIndicatorView  *avForCollectionView;
        UICollectionView *filteredCV;
        NSString  *minPrice,*maxPrice, *currencyCode, *currencySymbol, *sortBy,*postedWithin;;
        float  dist;
        UILabel *errorMessageLabelOutlet;
        CGPoint lastScrollContentOffset, searchLastScrollContentOffset;
        GetCurrentLocation *getLocation;
    __weak IBOutlet UIButton *btnSortBy;
    __weak IBOutlet UIButton *btnPriceSort;
        UISearchBar *vSearchBar;
        UIView *navTitleView;
        CGFloat yOrigin, navTitleViewWidth;
    
}

#pragma mark -
#pragma mark - Non IB Properties-

@property (nonatomic, strong) CLLocationManager * locationManager;
@property (nonatomic)BOOL isFilter, flagForLocation, noPostsAreAvailable ,isScreenDidAppear;
@property (nonatomic)double latForFilter,longiForFilter ,currentLat, currentLong;
@property (nonatomic,strong) NSString *locationName;
@property (nonatomic,strong) NSString *DefaultSort, *strSortMin, *strSortMax;
@property int currentIndex,paging;
@property NSInteger cellDisplayedIndex , leadingConstraint;
@property NSString *postedImagePath, *postedthumbNailImagePath;
@property (nonatomic, retain) UIDocumentInteractionController *dic;
@property (nonatomic, strong) ZoomInteractiveTransition * transition;
@property(assign, nonatomic) CGFloat currentOffset;

@property (weak, nonatomic) IBOutlet UIView *searchMainViewContainer;
@property (strong, nonatomic) IBOutlet UIButton *postButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *peopleButtonOutlet;
@property (strong, nonatomic) IBOutlet UIView *baseViewOutlet;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollViewOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (strong, nonatomic) IBOutlet UIView *movingDividerOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *movingDividerLeadingConstraint;
@property (strong, nonatomic) IBOutlet UITableView *postTableView;
@property (strong, nonatomic) IBOutlet UITableView *peopleTableView;
@property (weak, nonatomic) IBOutlet UIView *topTabView;

- (IBAction)postsButtonAction:(id)sender;
- (IBAction)peopleButtonAction:(id)sender;

#pragma mark -
#pragma mark - UIView Outlets-
@property (strong, nonatomic) IBOutlet UIView *emtyFiltersView;


#pragma mark -
#pragma mark - Label Outlets-
@property (strong, nonatomic) IBOutlet UILabel *labelNotificationsCount;
@property (strong, nonatomic) IBOutlet UILabel *emptyFilterTextLabel;

#pragma mark -
#pragma mark - Button Outlets-

@property (weak, nonatomic) IBOutlet UIView *sellStuffButtonView;
@property (strong, nonatomic) IBOutlet UIButton *filterButtonOutlet;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightBarButtonItem;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *searchButtonOutlet;


#pragma mark -
#pragma mark - CollectionView Outlets

@property (weak, nonatomic) IBOutlet UICollectionView *collectionviewFilterItem;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOutlet;

#pragma mark -
#pragma mark - Button Actions-
- (IBAction)searchButtonAction:(id)sender;
- (IBAction)notificationButtonACtion:(id)sender;
- (IBAction)filterButtonAction:(id)sender;
- (IBAction)sellStuffButton:(id)sender;
- (IBAction)removeProductFilterButtonAcion:(id)sender;
- (IBAction)btnSortByTapped:(UIButton *)sender;
- (IBAction)btnSortPriceTapped:(UIButton *)sender;


#pragma mark -
#pragma mark - NSConstraints Outlets

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *FilterViewTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintOfSellButton;



















@end
