
//  HomeScreenController.h
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//
#import "HomeScreenController.h"
#import "ListingCollectionViewCell.h"
#import "SVPullToRefresh.h"
#import "ProductDetailsViewController.h"
#import "CellForFilteredItem.h"
#import "FilterViewController.h"
#import "RFQuiltLayout.h"
#import "ActivityViewController.h"
#import "CameraViewController.h"
#import "SearchPostsViewController.h"
#import "ZoomInteractiveTransition.h"
#import "ZoomTransitionProtocol.h"
#import "StartBrowsingViewController.h"
#import "AskPermissionViewController.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "ProductDetails.h"
#import "SortCategoryVC.h"
#import "SortPriceVC.h"
#import "UserProfileViewController.h"
#import "TopTableViewCell.h"
#import "PeopleTableViewCell.h"
#define FiltersViewHeight 60


@import Firebase;

@interface HomeScreenController ()<WebServiceHandlerDelegate,RFQuiltLayoutDelegate,GetCurrentLocationDelegate,ZoomTransitionProtocol , SDWebImageManagerDelegate , AskPermissionDelegate, CLLocationManagerDelegate ,ProductDetailsDelegate,UICollectionViewDataSourcePrefetching, SortDelegate, SortPriceDelegate, UISearchBarDelegate >
{
    
    NSMutableArray *topResponseData,*peopleData;
    
}

@end

@implementation HomeScreenController

/*--------------------------------------*/
#pragma mark
#pragma mark - ViewController LifCycle
/*--------------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self.navigationController.interactivePopGestureRecognizer setDelegate:nil];
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem = self.rightBarButtonItem;
    self.searchButtonOutlet = nil;
    
    topResponseData = peopleData = [NSMutableArray new];
    self.postButtonOutlet.selected = YES;
    
     self.transition = [[ZoomInteractiveTransition alloc] initWithNavigationController:self.navigationController];
    [CommonMethods setNegativeSpacingforNavigationItem:self.navigationItem andExtraBarItem:self.searchButtonOutlet];
    minPrice = @""; maxPrice = @""; postedWithin = @""; sortBy = @"";
    dist = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    self.leadingConstraint = 40;
    self.FilterViewTopConstraint.constant= 0;
    
    
    if ([[UIScreen mainScreen] bounds].size.height == 812.0) {
        yOrigin = 0.0;
    } else {
        yOrigin = 0.0;
    }
    
    if ([[UIScreen mainScreen] bounds].size.height == 736.0) {
        navTitleViewWidth = [[UIScreen mainScreen] bounds].size.width -  75;
    } else {
        navTitleViewWidth = [[UIScreen mainScreen] bounds].size.width -  65;
    }
    
    navTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, yOrigin, navTitleViewWidth, 44)];
    
    vSearchBar = [[UISearchBar alloc] init];
    [vSearchBar setTintColor:mBaseColor];
    vSearchBar.frame=CGRectMake(0, 2,navTitleView.bounds.size.width , 38); // (SCREEN_WIDTH-(searchWidth))/2

    [vSearchBar setBackgroundImage:[UIImage new]];
    vSearchBar.delegate=self;
    //[[UITextField appearanceWhenContainedIn:[vSearchBar class], nil] setFont:SPFont(FONT_NAME_MONTSERRAT, FONT_SIZE_SearchBarText)];
    [navTitleView addSubview:vSearchBar];
    [self customiseSearchBar];
    self.navigationItem.titleView.frame = CGRectMake(0, 0, navTitleViewWidth, 44);
    self.navigationItem.titleView = navTitleView;
    
    
    self.DefaultSort = sortingByNewestFirst;
    [btnSortBy setTitle:sortingByNewestFirst forState:UIControlStateNormal];
    [btnPriceSort setTitle:@"Price: Any" forState:UIControlStateNormal];
    
    currencyCode = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
    currencySymbol = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey: NSLocaleCurrencySymbol]];
    arrayOfcategory = [[NSMutableArray alloc]init];
    explorePostsresponseData = [[NSMutableArray alloc] init];
    arrayOfCategoryImagesUrls = [[NSMutableArray alloc]init];
    productsArray = [[NSMutableArray alloc]init];
    [self addingActivityIndicatorToCollectionViewBackGround];
    [self RFQuiltLayoutIntialization];
    [self addingRefreshControl];
    [self notficationObservers];
    [self getCategoriesListFromServer];
    [self checkPermissionForAllowLocation];
    
    if([[Helper userToken] isEqualToString:mGuestToken]){
        NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
        [WebServiceHandler logGuestUserDevice:param andDelegate:self];
    }
    
    self.collectionViewOutlet.prefetchDataSource = self ;
    self.collectionViewOutlet.prefetchingEnabled = YES ;
    
    [self.searchMainViewContainer setHidden:YES];
    [self setViewDesign];
    
 }


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.isScreenDidAppear = NO;
    self.sellStuffButtonView.hidden = NO;
    self.sellStuffButtonView.alpha = 0 ;
    self.bottomConstraintOfSellButton.constant = 60;
    NSDictionary *checkAdsCampaign  = [[NSUserDefaults standardUserDefaults] objectForKey:mAdsCampaignKey];
    if(checkAdsCampaign)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:mAdsCampaignKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:mTriggerCampaign object:checkAdsCampaign];
    }
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"openActivity"]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"openActivity"];
        [self.tabBarController setSelectedIndex:0];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
        
        ActivityViewController *activityVC  = [story instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [self getnotificationCount];
    [self showWelcomeMessageForBrowsing];
    
    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
    [prefetcher prefetchURLs:arrayOfCategoryImagesUrls progress:nil completed:^(NSUInteger completedNo, NSUInteger skippedNo) {
    }];
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"recent_login"])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"recent_login"];
        [self requesForProductListings];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"Instagram_Share"])
    {
        [self ShareOnInstagram:[[NSUserDefaults standardUserDefaults]valueForKey:@"postUrl"]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"postUrl"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Instagram_Share"];
    }
    [UIView transitionWithView:self.sellStuffButtonView
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.sellStuffButtonView.alpha = 0.0 ;
                    }
                    completion:NULL];
    
    self.isScreenDidAppear = YES ;
    
}


/**
 Clear cache memory on warning.
 */
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}

/**
 Deallocate memory again.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:mDeletePostNotifiName];
    [[NSNotificationCenter defaultCenter] removeObserver:mSellingAgainNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:mUpdatePostDataNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:@"openActivityScreen"];
    [[NSNotificationCenter defaultCenter] removeObserver:mSellingPostNotifiName];
    [[NSNotificationCenter defaultCenter] removeObserver:mShowAdsCampaign];
}


/*------------------------------------*/
#pragma mark
#pragma mark - Set View Design
/*------------------------------------*/

- (void)setViewDesign
{
    [self.postButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.peopleButtonOutlet setTitleColor:[mBaseColor colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    [self.movingDividerOutlet setBackgroundColor:mBaseColor];
    [self.topTabView setBackgroundColor:mNavigationBarColor];
}

-(void)openActivityScreen{
    [self openActivityScreenForpush];
    
}

- (void) customiseSearchBar {
    vSearchBar.placeholder=@"Resell";
    
    for (UIView *subView in vSearchBar.subviews) {
        for(id field in subView.subviews){
            if ([field isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)field;
                //[textField setBackgroundColor:SPCOLOR(kColorSearchBarBG)];
                [textField setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:248.0/255.0 blue:250.0/255.0 alpha:1.0]];
                [textField setTextColor:mBaseColor];
                [textField setFont:[UIFont fontWithName:@"Roboto-medium" size:17.0]];
                UIView *backgroundView = textField.subviews.firstObject;
                if (backgroundView) {
                    backgroundView.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:248.0/255.0 blue:250.0/255.0 alpha:1.0];
                    backgroundView.layer.cornerRadius = 8;
                    backgroundView.clipsToBounds = true;
                    
                }
            }
        }
    }
    
    //[[UITextField appearanceWhenContainedIn:[vSearchBar class], nil] setFont:SPFont(FONT_NAME_MONTSERRAT, FONT_SIZE_SearchBarText)];
}


#pragma mark -
#pragma mark - Notification Observer

-(void)notficationObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostFromNotification:) name:mDeletePostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sellingPostAgain:) name:mSellingAgainNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(UpdatePostOnEditing:) name:mUpdatePostDataNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshData:) name:mUpdatePromotedPost object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addNewPost:)name:mSellingPostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openActivityScreen)name:@"openActivityScreen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postNotificationToAppdelegate:)name:mShowAdsCampaign object:nil];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Instagram_Share"];
}



#pragma mark
#pragma mark - Location Permission & Delegate -

-(void)checkPermissionForAllowLocation
{
    BOOL locationServiceEnable ;
    if ([CLLocationManager locationServicesEnabled]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            locationServiceEnable = YES;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
            
            AskPermissionViewController *askVC = [story instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
            askVC.permissionDelegate = self;
            askVC.locationPermission = YES ;
            askVC.locationEnable = locationServiceEnable ;
            [self.navigationController presentViewController:askVC animated:NO completion:nil];
        }
        else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            locationServiceEnable = NO;
            UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
            
            AskPermissionViewController *askVC = [story instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
            askVC.permissionDelegate = self;
            askVC.locationPermission = YES ;
            askVC.locationEnable = locationServiceEnable ;
            [self.navigationController presentViewController:askVC animated:NO completion:nil];
        }
        else {
            [self allowPermission:YES];
        }
    }
    else
    {
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
        
        AskPermissionViewController *askVC = [story instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
        askVC.permissionDelegate = self;
        askVC.locationPermission = YES ;
        askVC.LocationPrivacy = YES ;
        [self.navigationController presentViewController:askVC animated:NO completion:nil];
    }
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {

    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self allowPermission:YES];
        
    }
    else
    {
        [self allowPermission:NO];
    }
}

- (void)updatedLocation:(double)latitude and:(double)longitude
{
    self.latForFilter = latitude;
    self.longiForFilter = longitude;
    self.currentLat = latitude ;
    self.currentLong = longitude ;
    if(self.flagForLocation)
    {
        self.flagForLocation  = NO;
        [self requesForProductListings];
    }
}

- (void)updatedAddress:(NSString *)currentAddress
{
    

}

#pragma mark -
#pragma mark - RFQuiltLayout Intialization -

/**
 This method will create an object for RFQuiltLayout.
 */
-(void)RFQuiltLayoutIntialization
{
    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
    layout.direction = UICollectionViewScrollDirectionVertical;
    if(self.view.frame.size.width == 375)
    {
      layout.blockPixels = CGSizeMake( 37,31);
    }
    
    else if(self.view.frame.size.width == 414)
    {
        layout.blockPixels = CGSizeMake( 102,31);
    }
    else
    {
       layout.blockPixels = CGSizeMake( 79,31);
    }
    
    _collectionViewOutlet.collectionViewLayout = layout;
    layout.delegate=self;
    
}



#pragma mark
#pragma mark - Product Filter -

/**
 This is Delegate method of Filter VC.
 
 */
-(void)getFilteredItems:(NSMutableDictionary *)dicOfFilters miniPrice:(NSString *)min maxPrice:(NSString *)max curncyCode:(NSString *)currCode curncySymbol:(NSString *)currSymbol anddistance:(int)distnce
{
    minPrice = min;
    maxPrice = max;
    dist = distnce;
    currencyCode = currCode;
    currencySymbol = currSymbol;
    arrayOfcategory = dicOfFilters[@"category"];
    temp = [[NSMutableArray alloc] init];
    for (int i = 0 ;i <arrayOfcategory.count;i++) {
        NSString *typeOfFilter = @"category";
        NSString *valueForTheFilter = flStrForObj(arrayOfcategory[i]);
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:typeOfFilter forKey:@"typeOfFilter"];
        [cbmc setValue:valueForTheFilter forKey:@"value"];
        [temp addObject:cbmc];
    }
    
    if ([dicOfFilters valueForKey:@"sortBy"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"sortBy" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"sortBy"] forKey:@"value"];
        [temp addObject:cbmc];
        
        NSString *tempString = [dicOfFilters valueForKey:@"sortBy"];
        [self handleSortByString:tempString];
        
        
    }
    
    if ([dicOfFilters valueForKey:@"postedWithin"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"postedWithin" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"postedWithin"] forKey:@"value"];
        [temp addObject:cbmc];
         NSString *tempString = [dicOfFilters valueForKey:@"postedWithin"];
        [self handleSortByString:tempString];

    }
    
    if ([dicOfFilters valueForKey:@"price"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"price" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"price"] forKey:@"value"];
        [temp addObject:cbmc];
    }
    
    if ([dicOfFilters valueForKey:@"distance"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"distance" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"distance"] forKey:@"value"];
        [temp addObject:cbmc];
    }
      self.leadingConstraint = [[dicOfFilters valueForKey:@"leadingConstraint"]integerValue];
    
    if([dicOfFilters valueForKey:@"locationName"]){
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"locationName" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"locationName"] forKey:@"value"];
        [temp addObject:cbmc];
        self.locationName = [dicOfFilters valueForKey:@"locationName"];
        self.latForFilter = [[dicOfFilters valueForKey:@"lat"]doubleValue];
        self.longiForFilter = [[dicOfFilters valueForKey:@"long"]doubleValue];
    }
    
    [explorePostsresponseData removeAllObjects];
    [self reloadCollectionView] ;
    [avForCollectionView startAnimating];

    if(temp.count>0){
        //self.FilterViewTopConstraint.constant=0;
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
    else{
        [self requesForProductListings];
        //self.FilterViewTopConstraint.constant = -FiltersViewHeight;
        currencyCode = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
        currencySymbol = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey: NSLocaleCurrencySymbol]];
    }
    [_collectionviewFilterItem reloadData];
    

}


-(void)handleSortByString:(NSString *)checkString
{
    
    if([checkString containsString: NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst ) ])
    {
        sortBy = @"postedOnDesc";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByClosestFirst, sortingByClosestFirst) ])
    {
        sortBy  = @"distanceAsc";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceLowToHigh, sortingByPriceLowToHigh) ])
    {
        sortBy = @"priceAsc" ;
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceHighToLow, sortingByPriceHighToLow) ])
    {
        sortBy = @"priceDsc" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast24h, postedWithLast24h) ])
    {
        postedWithin = @"1" ;
    }
    if([checkString containsString:NSLocalizedString(postedWithLast7d, postedWithLast7d) ])
    {
        postedWithin = @"7" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast30d,postedWithLast30d) ])
    {
        postedWithin = @"30" ;
    }
    
    
}


-(void)handleRemoveFilters:(NSString *)checkString
{
    if([checkString containsString:NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst )])
    {
        sortBy = NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst );
    }
    
    if([checkString containsString:NSLocalizedString(sortingByClosestFirst, sortingByClosestFirst)])
    {
        sortBy  = NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst );
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceLowToHigh, sortingByPriceLowToHigh)])
    {
        sortBy = NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst ) ;
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceHighToLow, sortingByPriceHighToLow)])
    {
        sortBy = NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst ) ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast24h, postedWithLast24h)])
    {
        postedWithin = @"" ;
    }
    if([checkString containsString:NSLocalizedString(postedWithLast7d, postedWithLast7d)])
    {
        postedWithin = @"" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast30d,postedWithLast30d)])
    {
        postedWithin = @"" ;
    }
    
    
}






#pragma mark -
#pragma mark - Apply Filters
/**
 Apply search on Products by applying selected Filters.
 */
-(void)applyFiltersOnProductsWithIndex :(NSInteger)checkIndex
{
    
    if(self.locationName.length < 1)
    {
        self.locationName = getLocation.currentCity;
        self.latForFilter = self.currentLat;
        self.longiForFilter = self.currentLong;
    }
    
    categoryInStringFormat = [arrayOfcategory componentsJoinedByString:@","]; /** string with all categories**/
    
    NSDictionary *requestDic = @{mauthToken :flStrForObj([Helper userToken]),
                                 mlimit :@"20",
                                 moffset : flStrForObj([NSNumber numberWithInteger:checkIndex *20]),
                                 mSearchCategory  : flStrForObj(categoryInStringFormat),
                                 mSortby :   sortBy,
                                 mCurrency:  currencyCode,
                                 mMinPrice  : minPrice,
                                 mMaxPrice  : maxPrice,
                                 mPostedWithIn : postedWithin,
                                 mDistance  : [NSString stringWithFormat:@"%f",dist],
                                 mlocation  : flStrForObj(self.locationName),
                                 mlatitude  : [NSString stringWithFormat:@"%lf",self.latForFilter],
                                 mlongitude : [NSString stringWithFormat:@"%lf",self.longiForFilter],
                                 };
    
    [WebServiceHandler searchProductsByFilters:requestDic andDelegate:self];
}

#pragma mark -
#pragma mark - Remove Filters
/**
 Remove filter For Product.
 
 @param sender delete Filter button.
 */
- (IBAction)removeProductFilterButtonAcion:(id)sender {
    UIButton *btn=(UIButton *)sender;
    NSInteger tag= [btn tag]%100;
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"category"])
        [arrayOfcategory removeObjectAtIndex:tag];
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"distance"])
    {
        dist = 0;
        self.leadingConstraint = 40;
    }
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"price"])
    {
        minPrice = @"";
        maxPrice = @"";
        
    }
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"sortBy"] || [temp[tag][@"typeOfFilter"] isEqualToString:@"postedWithin"])
    {
        [self handleRemoveFilters:temp[tag][@"value"]];
    }
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"locationName"])
    {
    self.locationName = @"";
    self.latForFilter = 0;
    self.longiForFilter = 0;
    }


    
    
    [temp removeObjectAtIndex:tag];
    
    [explorePostsresponseData removeAllObjects];
     [self reloadCollectionView] ;
    [avForCollectionView startAnimating];

    
    if (temp.count ==0) {
        [UIView animateWithDuration:0.4 animations:^{
            //self.FilterViewTopConstraint.constant = -FiltersViewHeight;
            [self.view layoutIfNeeded];
        }];
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self requestForExplorePosts:self.currentIndex];
        currencyCode = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
        currencySymbol = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey: NSLocaleCurrencySymbol]];
    }
    else{
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
        [self.collectionviewFilterItem reloadData];
 
   }


#pragma mrk -
#pragma mark - Pull To refresh -

-(void)addingRefreshControl {
    refreshControl = [[UIRefreshControl alloc]init];
    [self.collectionViewOutlet addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
}
-(void)refreshData:(id)sender {
    [self getnotificationCount];
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    if(temp.count==0){
       [self allowPermission:YES];
    }
    else{
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
}
- (void)stopAnimation {
    __weak HomeScreenController *weakSelf = self;
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.collectionViewOutlet.pullToRefreshView stopAnimating];
        [weakSelf.collectionViewOutlet.infiniteScrollingView stopAnimating];
    });
}

/*-------------------------------------------------------------------------*/
#pragma mark - tableview Delegate and data source.
/*------------------------------------------------------------------------*/


// top tableview  (posts)         -       10(tag)
//peopleTableView      -       20(tag)



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 10)
        return topResponseData.count;
    else if (tableView.tag == 20)
        return peopleData.count;
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 10 ) {
        TopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:mTopTableViewCellID  forIndexPath:indexPath];
        [cell setValuesInCellWithUsername:flStrForObj(topResponseData[indexPath.row][@"productName"]) fullName:flStrForObj(topResponseData[indexPath.row][@"category"]) andProfileImage:flStrForObj( topResponseData[indexPath.row][@"thumbnailImageUrl"])];
        return cell;
    }
    else  if (tableView.tag == 20 )
    {
        PeopleTableViewCell *peopleCell = [tableView dequeueReusableCellWithIdentifier:mPeopleTableViewCellID forIndexPath:indexPath];
        [peopleCell setValuesInCellWithUsername:flStrForObj(peopleData[indexPath.row][@"username"]) fullName:flStrForObj(peopleData[indexPath.row][@"fullName"]) andProfileImage:flStrForObj(peopleData[indexPath.row][@"profilePicUrl"])];
        
        return peopleCell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [vSearchBar setShowsCancelButton:NO];
    if (tableView.tag == 10) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ProductDetailsViewController *newView = [story instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
        newView.hidesBottomBarWhenPushed = YES;
        newView.postId = flStrForObj(topResponseData[indexPath.row][@"postId"]);
        NSArray *data = [NSArray new];
        data = topResponseData[indexPath.row];
        newView.movetoRowNumber = indexPath.item;
        newView.noAnimation = YES;
        newView.hidesBottomBarWhenPushed = YES ;
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UserProfileViewController *newView = [story instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
        newView.checkProfileOfUserNmae = flStrForObj(peopleData[indexPath.row][@"username"]);
        newView.checkingFriendsProfile = YES;
        newView.ProductDetails = YES;
        [self.navigationController pushViewController:newView animated:YES];
        
    }
    
}


#pragma mark -
#pragma mark - collectionview delegates -


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if(collectionView==_collectionviewFilterItem)
    {
        return temp.count;
        
    }
    return  explorePostsresponseData.count;
    
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row +12 == explorePostsresponseData.count && self.paging!= self.currentIndex && self.cellDisplayedIndex != indexPath.row) {
        self.cellDisplayedIndex = indexPath.row ;
        if(temp.count)
        {
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
        }
        else
        {
        [self requestForExplorePosts:self.currentIndex];
        }
    }
    
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView==_collectionviewFilterItem)
    {
        CellForFilteredItem *cell=[collectionView dequeueReusableCellWithReuseIdentifier:mProductFiltersCellId forIndexPath:indexPath];
       [ cell.buttonToDisplayFilter setTitle: flStrForObj(temp[indexPath.row][@"value"]) forState:UIControlStateNormal];
        cell.deleteButton.tag = 100 + indexPath.row;
        return cell;
    }
    ListingCollectionViewCell  *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:mSearchCollectioncellIdentifier forIndexPath:indexPath];
    if (collectionViewCell == nil) {
        collectionViewCell.postedImageOutlet = nil ;
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [collectionViewCell ShowFeaturedLabel:explorePostsresponseData forIndexPath:indexPath];
            NSString *thumbimgUrl = flStrForObj(explorePostsresponseData[indexPath.item][@"mainUrl"]);
            [collectionViewCell loadImageForCell:[thumbimgUrl stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit,h_500,q_40,w_500/"]];

        });
    }
    return collectionViewCell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CellForFilteredItem *cell=(CellForFilteredItem*)[collectionView cellForItemAtIndexPath:indexPath];
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,cell.buttonToDisplayFilter.frame.size.height)];
    lbl.font=cell.buttonToDisplayFilter.titleLabel.font;
    if(temp.count)
    {
    lbl.text=flStrForObj(temp[indexPath.row][@"value"]);
    }
    CGFloat labelWidth = [CommonMethods measureWidthLabel:lbl];
    CGFloat cellWidth= labelWidth + 50;
    if(cellWidth >200){
        cellWidth = 200;
        return CGSizeMake(cellWidth,50 );
    }
    return CGSizeMake(cellWidth,50 );
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ListingCollectionViewCell *cell = (ListingCollectionViewCell *)[self.collectionViewOutlet cellForItemAtIndexPath:indexPath];
    if([collectionView isEqual:self.collectionViewOutlet]) {
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
        
        ProductDetailsViewController *newView = [story instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
        newView.indexPath = indexPath ;
        newView.productDelegate = self ;
        newView.hidesBottomBarWhenPushed = YES;
        newView.postId = flStrForObj(explorePostsresponseData[indexPath.row][@"postId"]);
        newView.dataFromHomeScreen = YES;
        newView.currentCity = getLocation.currentCity;
        newView.countryShortName = getLocation.countryShortCode ;
        newView.currentLattitude = [NSString stringWithFormat:@"%lf",self.currentLat];
        newView.currentLongitude = [NSString stringWithFormat:@"%lf",self.currentLong];
        newView.movetoRowNumber = indexPath.item;
        newView.imageFromHome = cell.postedImageOutlet.image;
        newView.product = [[ProductDetails alloc]initWithDictionary:explorePostsresponseData[indexPath.row]];
        [self.navigationController pushViewController:newView animated:YES];
    }
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5,5,5,5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section;
{
    return 0;
}

#pragma mark -
#pragma mark – RFQuiltLayoutDelegate -

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger width,width2;
    NSInteger height,height2;
    if(self.view.frame.size.width == 375){
    width = 5;
    height2 = [flStrForObj(explorePostsresponseData[indexPath.row][@"containerHeight"]) integerValue]/30;
    width2 =  [flStrForObj(explorePostsresponseData[indexPath.row][@"containerWidth"]) integerValue]/37;
        
        if (width2 == 0 ) {
            width2 = 10;
        }
        
        if (height2 == 0 ) {
            height2 = 10;
        }
        
        
    height = (width * height2)/width2;
    }
    else{
    width = 2;
    height2 = [flStrForObj(explorePostsresponseData[indexPath.row][@"containerHeight"]) integerValue]/30;
    if(self.view.frame.size.width == 414)
    {
       width2 =  [flStrForObj(explorePostsresponseData[indexPath.row][@"containerWidth"]) integerValue]/102;
    }
    else
    {
        width2 =  [flStrForObj(explorePostsresponseData[indexPath.row][@"containerWidth"]) integerValue]/79;
    }
        
        if (width2 == 0 ) {
            width2 = 10;
        }
        
        if (height2 == 0 ) {
            height2 = 20;
        }
        
        
    height = (width * height2)/width2;
    }
    return CGSizeMake(width,height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,0,0);
}

#pragma mark - Searchbar Deleagate

-(void) hideKeyboard {
    [vSearchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    searchBar.showsCancelButton = NO;
    [self hideKeyboard];
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [self.searchMainViewContainer setHidden:NO];
    [searchBar setShowsCancelButton:YES animated:YES];
    [self searchEnableframe];
    
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [self hideKeyboard];
    
    /*if (![searchBar.text isEqualToString:@""]) {
        UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
        SearchPostsViewController *searchVC = [story instantiateViewControllerWithIdentifier:mSearchPostsID];
        searchVC.searchString = searchBar.text;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
        [self.navigationController presentViewController:navigationController  animated:YES completion:nil];
    }*/
    
    /*searchBar.text = @"";
    searchBar.showsCancelButton = NO;*/
    
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSString *encodedString =  [searchText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    
    if(self.peopleButtonOutlet.selected){
        if (![searchText isEqualToString:@""]) {
            [self requestForPeople:encodedString];
        }
        else {
            [peopleData removeAllObjects];
            [self.peopleTableView reloadData];
        }
    }
    else
    {
        
        if(![searchText isEqualToString:@""]) {
            [self requestForPosts:encodedString];
        }
        else {
            [topResponseData removeAllObjects];
            [self.postTableView reloadData];
        }
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) aSearchBar {
    
    aSearchBar.text = @"";
    [self hideKeyboard];
    aSearchBar.showsCancelButton = NO;
    [self searchDisableframe];
    [self.searchMainViewContainer setHidden:YES];
}

-(void)searchDisableframe {
    self.navigationItem.rightBarButtonItem = self.rightBarButtonItem;
    navTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, yOrigin, navTitleViewWidth, 44)];
    vSearchBar.frame=CGRectMake(0, 2,navTitleView.bounds.size.width , 38); // (SCREEN_WIDTH-(searchWidth))/2
    [vSearchBar setBackgroundImage:[UIImage new]];
    [self customiseSearchBar];
    self.navigationItem.titleView.frame = CGRectMake(0, 0, navTitleViewWidth, 44);
}

- (void)searchEnableframe {
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.rightBarButtonItems = nil;
    navTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, yOrigin, navTitleViewWidth + 35, 44)];
    [navTitleView setBackgroundColor:[UIColor whiteColor]];
    vSearchBar.frame=CGRectMake(0, 2,navTitleView.bounds.size.width , 38); // (SCREEN_WIDTH-(searchWidth))/2
    [vSearchBar setBackgroundImage:[UIImage new]];
    [self customiseSearchBar];
    [self.navigationItem.titleView setBackgroundColor:[UIColor whiteColor]];
    self.navigationItem.titleView.frame = CGRectMake(0, 0, navTitleViewWidth + 35 , 44);
}

-(void)requestForPeople:(NSString *)searchText {
    
    if([[Helper userToken]isEqualToString:mGuestToken])
    {
        NSDictionary *requestDict = @{
                                      mMember :searchText,
                                      moffset:@"0",
                                      mlimit:@"40"
                                      };
        [WebServiceHandler getSearchPeopleForGuestUser:requestDict andDelegate:self];
        
    }
    else{
        NSDictionary *requestDict = @{
                                      @"userNameToSearch" :searchText,
                                      mauthToken :flStrForObj([Helper userToken]),
                                      moffset:@"0",
                                      mlimit:@"40"
                                      };
        [WebServiceHandler getSearchPeople:requestDict andDelegate:self];
    }
}

-(void)requestForPosts:(NSString *)searchText {
    NSDictionary *requestDict = @{
                                  mauthToken :flStrForObj([Helper userToken]),
                                  mProductName:searchText,
                                  moffset:@"0",
                                  mlimit:@"20"
                                  
                                  };
    [WebServiceHandler getSearchForPosts:requestDict andDelegate:self];
}

/*------------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*--------------------------------------------------------*/

- (void)errorAlert:(NSString *)message {
    //alert for failure response.
    UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Message" message:message actionTitle:@"Ok"];
    [self presentViewController:controller animated:YES completion:nil];
}



-(void)hadnlingPeopleApiResponse:(NSDictionary *)peopleDic {
    if (peopleDic) {
        peopleData =[[NSMutableArray alloc] init];
        peopleData = peopleDic[@"data"];
        [self.peopleTableView reloadData];
    }
    else {
        peopleData = nil;
        [self.peopleTableView reloadData];
    }
}


-(void)handlingPostsResponseData:(NSDictionary *)postsData {
    if (postsData) {
        topResponseData =[[NSMutableArray alloc] init];
        topResponseData = postsData[@"data"];
        [self.postTableView reloadData];
    }
    else {
        topResponseData = nil;
        [self.postTableView reloadData];
    }
}

#pragma mark -
#pragma mark - Request For Listings -

-(void)requesForProductListings
{
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    [self requestForExplorePosts:0];
}

-(void)requestForExplorePosts:(NSInteger )receivedindex {
    
    NSString *currentLattitudeParam,*currentLongitudeParam ;
    currentLattitudeParam = [NSString stringWithFormat:@"%lf",self.currentLat];
    currentLongitudeParam = [NSString stringWithFormat:@"%lf",self.currentLong];
    if(self.currentLat == 0)
    {
        currentLattitudeParam = @"";
        currentLongitudeParam = @"";
    }
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:mdeviceToken];
    if (token.length>0) {
        if ([[Helper userToken] isEqualToString:mGuestToken]) {
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex * mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue]),
                                          mPushTokenKey : token,
                                          mlatitude : currentLattitudeParam,
                                          mlongitude : currentLongitudeParam
                                          };
            [WebServiceHandler getExplorePostsForGuest:requestDict andDelegate:self];
        }
        else{
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex* mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger: mPagingValue]),
                                          mlatitude : currentLattitudeParam,
                                          mlongitude: currentLongitudeParam,
                                          mPushTokenKey : token
                                          };
            [WebServiceHandler getExplorePosts:requestDict andDelegate:self];
        }

    } else{
    
    if ([[Helper userToken] isEqualToString:mGuestToken]) {
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      moffset:flStrForObj([NSNumber numberWithInteger:receivedindex * mPagingValue]),
                                      mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue]),
                                      mlatitude : currentLattitudeParam,
                                      mlongitude : currentLongitudeParam
                                      };
        [WebServiceHandler getExplorePostsForGuest:requestDict andDelegate:self];
    }
    else{
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      moffset:flStrForObj([NSNumber numberWithInteger:receivedindex* mPagingValue]),
                                      mlimit:flStrForObj([NSNumber numberWithInteger: mPagingValue]),
                                      mlatitude : currentLattitudeParam,
                                      mlongitude: currentLongitudeParam
                                      };
        [WebServiceHandler getExplorePosts:requestDict andDelegate:self];
    }
    }
}


#pragma mark -
#pragma mark - WebServiceDelegate -



-(void)internetIsNotAvailable:(RequestType)requsetType {
    [avForCollectionView stopAnimating];
    [refreshControl endRefreshing];
    if(!explorePostsresponseData.count)
    {
        self.collectionViewOutlet.backgroundView = [Helper showMessageForNoInternet:YES forView:self.view];
    }
    
}

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [Helper showMessageForNoInternet:NO forView:self.view];
    
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage , mCommonServerErrorMessage) viewController:self];
        [avForCollectionView stopAnimating];
        [refreshControl endRefreshing];
        return;
    }
    else
    {
        [avForCollectionView stopAnimating];
        [refreshControl endRefreshing];
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    switch (requestType) {
        case RequestTypeGetExploreposts:
        {
            [avForCollectionView stopAnimating];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    
                   // productsArray = [ProductDetails arrayOfProducts:responseDict[@"data"]];
                    
                    
                    [self handlingResponseOfExplorePosts:response];
                }
                    break;
                case 204: {
                    if(!explorePostsresponseData.count)
                    {
                        [self showingMessageForCollectionViewBackgroundForType:0];
                    }
                }
                    break;
                    
                    
            }
        }
            break;
        case RequestTypeunseenNotificationCount:
        {
            [avForCollectionView stopAnimating];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    NSString *unseenCount = flStrForObj(response[@"data"]);
                    if ([unseenCount isEqualToString:@"0"]) {
                        self.labelNotificationsCount.hidden = YES;
                    }
                    else
                    {
                        self.labelNotificationsCount.hidden = NO;
                        self.labelNotificationsCount.text = unseenCount ;
                    }
                }
                    break;
                    
            }
        }
            break ;
        
        case RequestTypeSearchProductsByFilters:
        {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self handlingResponseOfExplorePosts:response];
                }
                    break;
                case 204:{
                    if(self.cellDisplayedIndex == -1)
                    {
                        [explorePostsresponseData removeAllObjects];
                         [self reloadCollectionView] ;
                        [self showingMessageForCollectionViewBackgroundForType:1];
                    }
                }
                    break;
            }
            
        }
            break ;
            
        case RequestTypeGetCategories:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    NSDictionary *categoryData = [[NSDictionary alloc]init];
                    categoryData = response[@"data"];
                    arrayOfCategoryImagesUrls = [[NSMutableArray alloc]init];
                    
                    for(NSDictionary *dic in categoryData)
                    {
                        [arrayOfCategoryImagesUrls addObject:dic[@"activeimage"]];
                        [arrayOfCategoryImagesUrls addObject:dic[@"deactiveimage"]];
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"data"] forKey:mKeyForSavingCategoryList];
                }
                    break;
                default:
                    break;
            }
        }
            break ;
        case RequestTypeGetSearchForPosts:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self handlingPostsResponseData:responseDict];
                }
                    break;
                case 19031:
                case 19032: {
                    [self errorAlert:responseDict[@"message"]];
                }
            }
        }
            break;
        case RequestTypeGetSearchPeople:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self hadnlingPeopleApiResponse:responseDict];
                }
                    break;
                case 19031:
                case 19032: {
                    [self errorAlert:responseDict[@"message"]];
                }
            }
        }
            break;
        case RequestTypegetUserSearchHistory:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    // NSLog(@"searched history is :%@",responseDict);
                }
                    break;
            }
        }
            break;
        case RequestTypeAddToSearchHistory:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    // NSLog(@"added key to searched history is :%@",responseDict);
                }
                    break;
            }
        }
            break;
        default:
            break;
    }
}



#pragma mark -
#pragma mark - Handling Response -



-(void)handlingResponseOfExplorePosts:(NSMutableDictionary *)receivedData {
    self.collectionViewOutlet.backgroundView = nil;
    [refreshControl endRefreshing];
    if(self.currentIndex == 0) {
        [explorePostsresponseData removeAllObjects];
        [explorePostsresponseData  addObjectsFromArray:receivedData[@"data"]];
    }
    else {
        [explorePostsresponseData  addObjectsFromArray:receivedData[@"data"]];
    }
    
    self.currentIndex ++;
    self.paging ++;
    [self stopAnimation];
     [self reloadCollectionView] ;
}


-(void)addingActivityIndicatorToCollectionViewBackGround
{
    avForCollectionView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    avForCollectionView.frame =CGRectMake(self.view.frame.size.width/2 -12.5, self.view.frame.size.height/2 - 100, 25,25);
    avForCollectionView.tag  = 1;
    [self.collectionViewOutlet addSubview:avForCollectionView];
    [avForCollectionView startAnimating];
}


#pragma mark -
#pragma mark - Button Actions -


- (IBAction)sellStuffButton:(id)sender{
    if([[Helper userToken]isEqualToString:mGuestToken])
    {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
        
    }
    else{
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
        
        CameraViewController *cameraVC=[story instantiateViewControllerWithIdentifier:@"CameraStoryBoardID"];
        cameraVC.sellProduct = TRUE;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraVC];
        //baseVC.sellCamera=TRUE;
        
        [self presentViewController:nav animated:NO completion:nil];
    }
}

- (IBAction)searchButtonAction:(id)sender {
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
    SearchPostsViewController *searchVC = [story instantiateViewControllerWithIdentifier:mSearchPostsID];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
    [self.navigationController presentViewController:navigationController  animated:YES completion:nil];
}

- (IBAction)notificationButtonACtion:(id)sender {
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        UINavigationController *nav = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    else{
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
        
        ActivityViewController *activityVC  = [story instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

-(void)openActivityScreenForpush{
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        
    }
    else{
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
        
        ActivityViewController *activityVC  = [story instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        activityVC.showOwnActivity = YES ;
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

- (IBAction)filterButtonAction:(id)sender {
    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
    [prefetcher prefetchURLs:arrayOfCategoryImagesUrls progress:nil completed:^(NSUInteger completedNo, NSUInteger skippedNo) {
    }];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
    
    FilterViewController *Fvc = [story instantiateViewControllerWithIdentifier:@"FilterItemsStoryboardID"];
    Fvc.hidesBottomBarWhenPushed = YES;
    Fvc.delegate=self;
    Fvc.checkArrayOfFilters = YES;
    Fvc.minPrce = minPrice;
    Fvc.maxPrce = maxPrice;
    Fvc.distnce = dist;
    Fvc.arrayOfSelectedFilters = temp;
    Fvc.lattitude = self.latForFilter;
    Fvc.longitude = self.longiForFilter;
    Fvc.currencyCode = currencyCode ;
    Fvc.currencySymbol = currencySymbol;
    Fvc.leadingConstraint = self.leadingConstraint ;
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:Fvc];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
}

- (IBAction)btnSortByTapped:(UIButton *)sender {
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
    SortCategoryVC *newView = [story instantiateViewControllerWithIdentifier:@"SortCategoryVC"];
    newView.modalPresentationStyle = UIModalPresentationPopover;
    newView.preferredContentSize = CGSizeMake(225,160);
    newView.checkArrayOfSorting = YES;
    newView.delegate = self;
    newView.sortBy = self.DefaultSort;
    
    UIPopoverPresentationController *popover= newView.popoverPresentationController;
    popover.delegate = self;
    popover.permittedArrowDirections = UIPopoverArrowDirectionUp;
    popover.sourceView = sender;
    popover.sourceRect = sender.bounds;
    
    [self presentViewController:newView animated:YES completion:nil];
    
    
}

- (IBAction)btnSortPriceTapped:(UIButton *)sender {
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
    SortPriceVC *newView = [story instantiateViewControllerWithIdentifier:@"SortPriceVC"];
    newView.modalPresentationStyle = UIModalPresentationPopover;
    newView.preferredContentSize = CGSizeMake(300,60);
    newView.delegate = self;
    newView.strMax = self.strSortMax;
    newView.strMin = self.strSortMin;

    UIPopoverPresentationController *popover= newView.popoverPresentationController;
    popover.delegate = self;
    popover.permittedArrowDirections = UIPopoverArrowDirectionUp;
    popover.sourceView = sender;
    popover.sourceRect = sender.bounds;
    
    [self presentViewController:newView animated:YES completion:nil];
    
}

- (IBAction)postsButtonAction:(id)sender {
    
    self.postButtonOutlet.selected = YES;
    self.peopleButtonOutlet.selected = NO;
    
    [self.postButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.peopleButtonOutlet setTitleColor:[mBaseColor colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    
    self.movingDividerLeadingConstraint.constant = self.mainScrollViewOutlet.contentOffset.x/2;
    CGRect frame = self.mainScrollViewOutlet.bounds;
    frame.origin.x = 0 * CGRectGetWidth(self.view.frame);
    [self.mainScrollViewOutlet scrollRectToVisible:frame animated:YES];
    
}

- (IBAction)peopleButtonAction:(id)sender {
    
    self.postButtonOutlet.selected = NO;
    self.peopleButtonOutlet.selected = YES;
    
    [self.postButtonOutlet setTitleColor:[mBaseColor colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    [self.peopleButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    
    CGRect frame = self.mainScrollViewOutlet.bounds;
    frame.origin.x = 1 * CGRectGetWidth(self.view.frame);
    [self.mainScrollViewOutlet scrollRectToVisible:frame animated:YES];
    self.movingDividerLeadingConstraint.constant = self.mainScrollViewOutlet.contentOffset.x/2;
    
}

#pragma mark -
#pragma mark - UIPopoverPresentationController Delegate
- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
                                                               traitCollection:(UITraitCollection *)traitCollection {
    return UIModalPresentationNone;
}


#pragma mark -
#pragma mark - No Posts View

-(void)showingMessageForCollectionViewBackgroundForType :(NSInteger )type{
    
    if(type == 0)
    {
        self.emptyFilterTextLabel.text = NSLocalizedString(noPostsNearYouText, noPostsNearYouText) ;
    }
    else if(type == 1)
    {
        self.emptyFilterTextLabel.text = NSLocalizedString(noPostsForAppliedFilter, noPostsForAppliedFilter) ;
    }
    UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.collectionViewOutlet.frame.size.width,self.collectionViewOutlet.frame.size.height)];
    self.emtyFiltersView.frame = backGroundViewForNoPost.frame ;
    [backGroundViewForNoPost addSubview:self.emtyFiltersView];
    self.collectionViewOutlet.backgroundView = backGroundViewForNoPost;
}

#pragma mark -
#pragma mark - ZoomTransitionProtocol

-(UIView *)viewForZoomTransition:(BOOL)isSource
{
    NSIndexPath *selectedIndexPath = [[self.collectionViewOutlet indexPathsForSelectedItems] firstObject];
    ListingCollectionViewCell *cell = (ListingCollectionViewCell *)[self.collectionViewOutlet cellForItemAtIndexPath:selectedIndexPath];
    return cell.postedImageOutlet;
}


#pragma mark -
#pragma mark - Welcome Screen

-(void)showWelcomeMessageForBrowsing
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:mSignupFirstTime]){
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:mSignupFirstTime];
        [[NSUserDefaults standardUserDefaults]synchronize];
    self.tabBarController.tabBar.userInteractionEnabled = NO;
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
        
    StartBrowsingViewController *browsingVC = [story instantiateViewControllerWithIdentifier:@"StartBrowsingStoryboardId"];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    browsingVC.callbackToActivateTab = ^(BOOL activate){
        self.tabBarController.tabBar.userInteractionEnabled = activate;
    };
    [self presentViewController:browsingVC animated:YES completion:nil];
    }
    else {
        self.tabBarController.tabBar.userInteractionEnabled = YES;

    }

}

#pragma mark -
#pragma mark - Tab Bar animation -

/* pass a param to describe the state change, an animated flag and a completion block matching UIView animations completion*/
- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)(BOOL))completion {
    
    // bail if the current state matches the desired state
    if ([self tabBarIsVisible] == visible) return (completion)? completion(YES) : nil;
    
    // get a frame calculation ready
    CGRect frame = self.tabBarController.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = (visible)? -height : height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.3 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        self.tabBarController.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    } completion:completion];
}

//Getter to know the current state
- (BOOL)tabBarIsVisible {
    return self.tabBarController.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame);
}

#pragma mark -
#pragma mark - ScrollView Delegate -

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.mainScrollViewOutlet]) {
        searchLastScrollContentOffset = scrollView.contentOffset ;
    } else {
        scrollView = self.collectionViewOutlet;
        _currentOffset = self.collectionViewOutlet.contentOffset.y;
        lastScrollContentOffset = scrollView.contentOffset ;
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.mainScrollViewOutlet]) {
        CGPoint offset = scrollView.contentOffset;
        self.movingDividerLeadingConstraint.constant = scrollView.contentOffset.x/2;
        if(offset.x == 0 ) {
            
            self.postButtonOutlet.selected = YES;
            self.peopleButtonOutlet.selected = NO;
            vSearchBar.text = @"";
            
        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)) {
            
            self.postButtonOutlet.selected = NO;
            self.peopleButtonOutlet.selected = YES;
            vSearchBar.text = @"";
        }
        scrollView.contentOffset = offset;
    } else {
        if (self.isScreenDidAppear)
        {
            [vSearchBar setShowsCancelButton:NO];
            dispatch_async(dispatch_get_main_queue(),^{
                if (lastScrollContentOffset.y > scrollView.contentOffset.y) {
                    // scroll up
                    self.bottomConstraintOfSellButton.constant = 60;
                    [self setTabBarVisible:YES animated:YES completion:nil];
                    [[self navigationController] setNavigationBarHidden:NO animated:YES];
                }
                
                else if (lastScrollContentOffset.y < scrollView.contentOffset.y) {
                    // sroll down
                    self.bottomConstraintOfSellButton.constant = 15;
                    [self setTabBarVisible:NO animated:YES completion:nil];
                    [[self navigationController] setNavigationBarHidden:YES animated:YES];
                }
            });
        }
    }
}


#pragma mark -
#pragma mark - Notification Methods -

-(void)sellingPostAgain:(NSNotification *)noti
{
    
    
}

#pragma mark -
#pragma mark - Product Edited Notification -

-(void)UpdatePostOnEditing:(NSNotification *)noti
{
    [self requesForProductListings];
}

#pragma mark -
#pragma mark - Product Removed Notification -

-(void)deletePostFromNotification:(NSNotification *)noti {
    NSString *updatepostId = flStrForObj(noti.object[@"data"][@"postId"]);
    for (int i=0; i <explorePostsresponseData.count;i++) {
        
        if ([flStrForObj(explorePostsresponseData[i][@"postId"]) isEqualToString:updatepostId])
        {
            [explorePostsresponseData removeObjectAtIndex:i];
            [self reloadCollectionView] ;
            break;
        }
    }
}


#pragma mark -
#pragma mark - Product Added Notification -

-(void)addNewPost :(NSNotification *)noti
{
    NSMutableArray *newPost = (NSMutableArray *)noti.object[0];
    [explorePostsresponseData insertObject:newPost atIndex:0];
    
    self.collectionViewOutlet.backgroundView = nil ;
     [self reloadCollectionView] ;
    
}

#pragma mark -
#pragma mark - Ads Campaign Notification


/**
 Notify the method in App delegate to open the ads campaign
 
 @param noti details of campaign in NSdictionary format.
 */
-(void)postNotificationToAppdelegate:(NSNotification *)noti
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:mTriggerCampaign object:noti.object];
}

#pragma mark -
#pragma mark - Check Permission For Location -


/**
 This method is invoked after checking the permission for location services.
 On the basis of location services further server is requested for Listings.
 @param value Bool Value.
 */
-(void)allowPermission:(BOOL)value
{
    if(value)
    {
        //To get the current location
        getLocation = [GetCurrentLocation sharedInstance];
        [getLocation getLocation];
        getLocation.delegate = self;
        self.flagForLocation = YES;
    }
    else
    {
        [self requesForProductListings];
    }
    
}

#pragma mark-
#pragma mark - ProductDetails Delegate -


/**
 If product is removed.This delegate method get invoked.

 @param indexpath indexpath row.
 */
-(void)productIsRemovedForIndex:(NSIndexPath *)indexpath
{
    [explorePostsresponseData removeObjectAtIndex:indexpath.row];
     [self reloadCollectionView] ;
}

#pragma mark-
#pragma mark -  Share On Instagram -


/**
 Once Product is successfully posted.This method get invoked to ask the permission to share on instagram.
 @param postUrl url to share.
 */

-(void)ShareOnInstagram:(NSString *)postUrl
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram:app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        NSString *path = [Helper instagramSharing:postUrl];
        _dic = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:path]];
        _dic.UTI = @"com.instagram.exclusivegram";
        _dic.delegate = nil;
        NSString *sharedMessage = [NSString stringWithFormat:NSLocalizedString(sharedVia, sharedVia),@" %@",APP_NAME];
        _dic.annotation = [NSDictionary dictionaryWithObject:sharedMessage forKey:@"InstagramCaption"];
        [_dic presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
    }
    else
    {
        [Helper showAlertWithTitle:NSLocalizedString(alertMessage,alertMessage) Message:NSLocalizedString(noInstagramAccountAlertMessage,noInstagramAccountAlertMessage) viewController:self];
    }
}

#pragma mark -
#pragma mark - Pending Notifications -

-(void)getnotificationCount
{
    if(![[Helper userToken] isEqualToString:mGuestToken]) {
        NSDictionary *requestDict = @{
                                      @"token":[Helper userToken]
                                      };
        [WebServiceHandler getUsernNotificationCount:requestDict andDelegate:self];
    }
}


#pragma mark -
#pragma mark - Get Categories -

/**
 Prefectch The Category List From  Server for Product Filters.
 */
-(void)getCategoriesListFromServer
{
NSDictionary *requestDict = @{
                              mLimit : @"50",
                              moffset :@"0"
                              };
[WebServiceHandler getCategories:requestDict andDelegate:self];
}

#pragma mark -
#pragma mark - reload CollectionView -


/**
 Reload collectionView without Animation.
 */
-(void)reloadCollectionView{
    [UIView animateWithDuration:0 animations:^{
            [_collectionViewOutlet reloadData];
    }];
}

#pragma mark -
#pragma mark - Sorting New -

- (void)getSorting:(NSString *)selectedSorting {
    
    if ([selectedSorting isEqualToString:@""]) {
        selectedSorting = sortingByNewestFirst;
    }
    
    self.DefaultSort = selectedSorting;
    [btnSortBy setTitle:self.DefaultSort forState:UIControlStateNormal];
    
    [self handleSortByString:self.DefaultSort];
    
    [explorePostsresponseData removeAllObjects];
    [self reloadCollectionView] ;
    [avForCollectionView startAnimating];
    
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    [self applyFiltersOnProductsWithIndex:self.currentIndex];
    
    
}

#pragma mark -
#pragma mark - Sorting Price -

-(void)getSortPrice:(NSString *)min maxPrice:(NSString *)max {
    
    self.strSortMin = min;
    self.strSortMax = max;
    
    [self handleSortByString:self.DefaultSort];
    
    if ([self.strSortMin isEqualToString:@""] && [self.strSortMax isEqualToString:@""]) {
        [btnPriceSort setTitle:[NSString stringWithFormat:@"Price: Any"] forState:UIControlStateNormal];
    } else if (![self.strSortMin isEqualToString:@""] && ![self.strSortMax isEqualToString:@""]) {
        [btnPriceSort setTitle:[NSString stringWithFormat:@"Price: $%@ - $%@",min,max] forState:UIControlStateNormal];
    } else if (![self.strSortMin isEqualToString:@""]) {
        [btnPriceSort setTitle:[NSString stringWithFormat:@"Price: More than $%@",min] forState:UIControlStateNormal];
    } else if (![self.strSortMax isEqualToString:@""]) {
        [btnPriceSort setTitle:[NSString stringWithFormat:@"Price: Under $%@",max] forState:UIControlStateNormal];
    }
    
    minPrice = self.strSortMin;
    maxPrice = self.strSortMax;
    
    [explorePostsresponseData removeAllObjects];
    [self reloadCollectionView] ;
    [avForCollectionView startAnimating];
    
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    [self applyFiltersOnProductsWithIndex:self.currentIndex];
    
}

@end
