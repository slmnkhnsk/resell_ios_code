//
//  ListingCollectionViewCell.m

//
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ListingCollectionViewCell.h"
#import "TinderGenericUtility.h"
#import "UIImageView+WebCache.h"

@implementation ListingCollectionViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.contentView.backgroundColor =[UIColor clearColor];
    self.postedImageOutlet.contentMode = UIViewContentModeScaleAspectFill;
    self.imageForShowVideoOrNot.hidden = YES;
    [self layoutIfNeeded];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.postedImageOutlet.image = nil;
    self.postedImageOutlet.clipsToBounds = YES ;
    
}


-(void)ShowFeaturedLabel:(NSArray *)dataArray forIndexPath :(NSIndexPath *)indexPath
{
    if(dataArray.count > 0){
    NSString *isPromotedValue = flStrForObj(dataArray[indexPath.row][@"isPromoted"]);
    BOOL isPromoted = [isPromotedValue boolValue];
    
    if(isPromoted)
    {
        self.featuredView.hidden = NO ;
    }
    else
    {
        self.featuredView.hidden = YES ;
    }
    }
}

-(void)loadImageForCell:(NSString *)imageUrl
{
    
    
SDWebImageManager *manager = [SDWebImageManager sharedManager];

    [self.postedImageOutlet setImageWithURL:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    // request image
   
    NSURL *imageURL = [NSURL URLWithString:imageUrl];
    if([manager diskImageExistsForURL:[NSURL URLWithString:imageUrl]]){
        dispatch_async(dispatch_get_main_queue(), ^{
        [self.postedImageOutlet setImage: [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:imageUrl]];
                   });
    }
    else
   {
//      [manager downloadImageWithURL:imageURL options: 0 progress:nil completed:
//       ^(UIImage *image , NSError *error, SDImageCacheType cacheType,BOOL finished,NSURL *imageURL){
//           dispatch_async(dispatch_get_main_queue(),^{
//           [UIView transitionWithView:self.postedImageOutlet duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//               [self.postedImageOutlet setImage:image];
//           }completion:NULL];
//           });}
//            ];
//       

        [self.postedImageOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@""] options:0 completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType,NSURL *imageURL){
            dispatch_async(dispatch_get_main_queue(),^{
                [UIView transitionWithView:self.postedImageOutlet duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    [self.postedImageOutlet setImage:image];
                }completion:NULL];
                           });
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
   
//        [UIView transitionWithView:self.postedImageOutlet
//                          duration:0.5
//                           options:UIViewAnimationOptionTransitionCrossDissolve
//                        animations:^{
//                            self.postedImageOutlet.alpha = 1.0 ;
//                        }
//                        completion:NULL];

        }
}


@end
