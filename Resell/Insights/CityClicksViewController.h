//
//  CityClicksViewController.h
//  Resell
//
//  Created by Rahul Sharma on 08/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityClicksViewController : UIViewController
{
    NSArray *dataArray ;
}
@property (strong, nonatomic) IBOutlet UIView *sectionHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *labelForSectionHeader;
@property (strong,nonatomic)NSString *countrySName, *countryFullName,*postId;
@property (strong, nonatomic) IBOutlet UITableView *cityClicksTableview;

@end
