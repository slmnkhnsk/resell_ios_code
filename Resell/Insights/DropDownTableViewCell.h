//
//  DropDownTableViewCell.h
//  Resell
//
//  Created by Rahul Sharma on 04/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropDownTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *dropDOwnOptions;

@end
