//
//  DropDownTableViewCell.m
//  Resell
//
//  Created by Rahul Sharma on 04/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "DropDownTableViewCell.h"

@implementation DropDownTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
