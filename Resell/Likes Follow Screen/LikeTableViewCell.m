//
//  LikeTableViewCell.m

//
//  Created by Rahul Sharma on 4/19/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "LikeTableViewCell.h"

@implementation LikeTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self.followButtonOutlet setBackgroundColor:mBaseColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
