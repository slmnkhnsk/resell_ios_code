//
//  LikeViewController.h

//
//  Created by Rahul Sharma on 4/19/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LikeTableViewCell.h"
@interface LikeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property NSString *navigationTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak,nonatomic) NSString *getdetailsDetailsOfUserName;
@property NSString *postId;
@property NSString *postType;
@property (weak, nonatomic) IBOutlet UIView *progressIndicatorView;
@property (nonatomic) NSInteger currentIndex,paging;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UILabel *labelForBackgroundMessage;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageToShow;

@end
