//
//  AskPermissionViewController.m

//
//  Created by Rahul Sharma on 03/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AskPermissionViewController.h"

@interface AskPermissionViewController ()<CLLocationManagerDelegate,GetCurrentLocationDelegate >

@end

@implementation AskPermissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.allowButtonOutlet setBackgroundColor:mBaseColor];
    
    if(self.locationPermission)
    {
        self.titleOfPermission.text =  Localized(allowLocationTitle) ;
        
        self.messageBelowTitleLabel.text = Localized(allowNotificationMessage);
        
        [self.allowButtonOutlet setTitle:Localized(allowLocationButtonTitle) forState:UIControlStateNormal];
        
    }
    else
    {
        self.titleOfPermission.text = Localized(allowNotificationTitle) ;
        self.messageBelowTitleLabel.text = Localized(allowNotificationMessage);
        
        [self.allowButtonOutlet setTitle:Localized(allowNotificationButtonTitle) forState:UIControlStateNormal];
    }
    
    [self.btnOK setTitleColor:mBaseColor forState:UIControlStateNormal];
    
    [self setViewDesign];
}

#pragma mark - Set View Design

- (void)setViewDesign
{
    [self.titleOfPermission setTextColor:mBaseColor];
    [self.messageBelowTitleLabel setTextColor:mBaseColor];
    [self.alertBackView setBackgroundColor:mBaseColor];
    [self.btnOK setBackgroundColor:mBaseColor];
    [self.btnOK setTitleColor:mBaseWhiteColor forState:UIControlStateNormal];
    [self.saperatorView setBackgroundColor:mBaseWhiteColor];
    [self.buttonSaperatorView setBackgroundColor:mBaseWhiteColor];
    [self.secondButtonDashView setBackgroundColor:mBaseWhiteColor];
    [self.firstLineView setBackgroundColor:mBaseWhiteColor];
    [self.secondLineView setBackgroundColor:mBaseWhiteColor];
    [self.thirdLineView setBackgroundColor:mBaseWhiteColor];
    
    [self.allowButtonOutlet setTitleColor:[UIColor darkGrayColor] forState:UIControlStateDisabled];
    [self.allowButtonOutlet setTitleColor:mBaseWhiteColor forState:UIControlStateNormal];
    [self.allowButtonOutlet setBackgroundColor:mBaseColor];
}

#pragma mark -
#pragma mark - Status Bar

-(BOOL)prefersStatusBarHidden
{
    return YES ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)yesNotifyMeButtonAction:(id)sender {
    
    if(self.LocationPrivacy)
    {
        [self actionWhenLocationDisabled];
        
        return ;
    }
    
    
    if(!self.locationEnable)
    {
        UIAlertController *alertForLocation = [UIAlertController alertControllerWithTitle:Localized(allowLocationAlertTitle) message:Localized(allowLocationAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionForOk = [UIAlertAction actionWithTitle:Localized(alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
            [self.permissionDelegate allowPermission:NO];
            
        }];
        
        UIAlertAction *actionForsettings = [UIAlertAction actionWithTitle:Localized(allowLocationSettingsActionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.distanceFilter = kCLDistanceFilterNone;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            
        }];
        
        [alertForLocation addAction:actionForsettings];
        [alertForLocation addAction:actionForOk];
        [self presentViewController:alertForLocation animated:YES completion:nil];
    }
    
  else
  {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if  ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
  }

}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusDenied) {
        [self.permissionDelegate allowPermission:NO];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
  if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
         [self.permissionDelegate allowPermission:YES];
      [self dismissViewControllerAnimated:NO completion:nil];
    }
}



- (IBAction)okayButtonAction:(id)sender {
    
    if(self.LocationPrivacy)
    {
        [self actionWhenLocationDisabled];
        
        return ;
    }
    
    if(!self.locationEnable)
    {
        UIAlertController *alertForLocation = [UIAlertController alertControllerWithTitle:Localized(allowLocationAlertTitle) message:Localized(allowLocationAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionForOk = [UIAlertAction actionWithTitle:Localized(alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
            [self.permissionDelegate allowPermission:NO];
            
        }];
        
        UIAlertAction *actionForsettings = [UIAlertAction actionWithTitle:Localized(allowLocationSettingsActionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.distanceFilter = kCLDistanceFilterNone;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            
        }];
        
        [alertForLocation addAction:actionForsettings];
        [alertForLocation addAction:actionForOk];
        [self presentViewController:alertForLocation animated:YES completion:nil];
    }
    
    else
    {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if  ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
  }
}

- (IBAction)cancelButtonAction:(id)sender {
    
 [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)actionWhenLocationDisabled {
    UIAlertController *alertForLocation = [UIAlertController alertControllerWithTitle:Localized(allowLocationAlertTitle) message:NSLocalizedString(allowLocationAlertMessage, allowLocationAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionForOk = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
        
        [self.permissionDelegate allowPermission:NO];
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }];
    
    UIAlertAction *actionForsettings = [UIAlertAction actionWithTitle:NSLocalizedString(allowLocationSettingsActionTitle, allowLocationSettingsActionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    [alertForLocation addAction:actionForsettings];
    [alertForLocation addAction:actionForOk];
    [self presentViewController:alertForLocation animated:YES completion:nil];
}



@end
