//
//  ProductDetails.h
//  Resell
//
//  Created by Rahul Sharma on 02/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductDetails : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)response;
+(NSMutableArray *) arrayOfProducts :(NSDictionary *)responseData ;

/**
 Product name title property set during posting.
 */
@property (nonatomic,copy) NSString *productName;


/**
 Category type for product.
 */
@property (nonatomic,copy) NSString *category;


/**
 Click count is basically all views for product.
 when member see product click count get increse.
 backend saves lat, long if available.
 */
@property (nonatomic,copy) NSString *clickCount;


/**
 Condition of the product set during posting.
 */
@property (nonatomic,copy) NSString *condition;


/**
 Currency set by user for his product.
 */
@property (nonatomic,copy) NSString *currency;

/**
 Price set by owner for offer.
 */
@property (nonatomic,copy) NSString *price  ;

/**
 Product description given by product owner.
 As it is optional during posting so can be empty.
 */
@property (nonatomic,copy) NSString *productDescription ;


/**
 All the Urls of product images in string format.
 Urls will be accrding to number of images uploaded.
 */
@property (nonatomic,copy) NSString  *mainUrl,*imageUrl1 ,*imageUrl2,*imageUrl3, *imageUrl4;

/**
 Thumbnail Url in string format for main image of product.
 */
@property (nonatomic,copy) NSString  *thumbnailImageUrl ;


/**
 Number of likes in string format to show as in button title of UIButton.
 On clicking like button likes count can be increase / decrease.
 */
@property (nonatomic,copy) NSString *likes;

/**
 Profile pic Urls in string format.
 profile pic contains the string value of user profile pic url.
 Members profile pic url is in memberProfilePicUrl.
 */
@property (nonatomic,copy) NSString *profilePicUrl  ;


/**
 City refrence of product posted from.
 */
@property (nonatomic,copy) NSString *city;

/**
 Country small name for product posting country.
 */
@property (nonatomic,copy) NSString *countrySname;

/**
 User properties Like name, username , full name , id , chatId set fro server response.
 */
@property (nonatomic,copy) NSString *userFullName ;
@property (nonatomic,copy) NSString *username;
@property (nonatomic,copy) NSString *userId , *userMqttId ;


/**
 Bool value for negotiable , follow status, likeStatus and sold.
 */
@property (nonatomic) BOOL negotiable , followRequestStatus ,likeStatus ,sold;


/**
 Post id and posted time.
 */
@property (nonatomic,copy) NSString *postedOn , *postId ;

/**
 Place is basically address for product location.
 */
@property (nonatomic,copy) NSString *place ;

/**
 Lattitude and longitude to show the location of product in map.
 */
@property (nonatomic)double latitude , longitude ;


/**
 Member who has posted the product properties like username , fullname, id..
 */
@property (nonatomic,copy) NSString *membername ,*memberFullName,*memberId , *memberMqttId, *memberProfilePicUrl ;

/**
 Number of images related to product count.
 */
@property NSInteger code ,imageCount;

/**
 LikedByuser conatains the name and profile pics of users who has like the product.
 response array is array contains all details properties of product.
 */
@property (nonatomic, strong)NSMutableArray *likedByUsers ,*responseArray;


/**
 Message show the status of response comes from server.
 */
@property (nonatomic,copy) NSString *message , *postedByUserName;



                

@end
