//
//  NetworkStatusShowingView.m
//  UBER
//
//  Created by Nabeel Gulzar on 03/12/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "NetworkStatusShowingView.h"

@implementation NetworkStatusShowingView

static NetworkStatusShowingView *showNetworkStatusView = nil;

+ (id)sharedInstance {
    
    if (!showNetworkStatusView) {
        
        showNetworkStatusView  = [[self alloc] init];
    }
    
    return showNetworkStatusView;
}

-(id)init
{
    self = [super init];
    if (self) {
        // Initialization code

        [self addViewToShowNetworkStatus];
    }
    return self;
}


-(void)addViewToShowNetworkStatus {
    
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    
    UIView *showStatus = [[UIView alloc]initWithFrame:CGRectMake(0, 64, frontWindow.frame.size.width, 44)];
    showStatus.tag = 1000;
    showStatus.backgroundColor = [UIColor whiteColor];
    UILabel *msg = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, showStatus.frame.size.width, 44)];
    msg.backgroundColor = [UIColor clearColor];
    msg.textAlignment = NSTextAlignmentCenter;
    msg.text = @"Could not connect to the server";
    msg.textColor = [UIColor whiteColor];
    [showStatus addSubview:msg];
    [frontWindow addSubview:showStatus];
    
    // timer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(hide) userInfo:nil repeats:NO];
}

-(void)hide
{
    
    [UIView animateWithDuration:1.0
                          delay:0.2
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                        
                        
                     }
                     completion:^(BOOL finished){
                         
                         UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
                         UIView *showStatus = [frontWindow viewWithTag:1000];
                         [showStatus removeFromSuperview];
                         showNetworkStatusView = nil;
                     }];
    
}


+(void)removeViewShowingNetworkStatus {
    
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *showStatus = [frontWindow viewWithTag:1000];
    [showStatus removeFromSuperview];
    showNetworkStatusView = nil;
    
}


@end
