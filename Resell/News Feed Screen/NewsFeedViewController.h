//
//  NewsFeedViewController.h

//
//  Created by Govind on 10/3/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCRecordSession.h"


@interface NewsFeedViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableViewOutlet;
@property (weak, nonatomic) IBOutlet UIView *viewWhenNoPostsOutlet;
@property (strong, nonatomic) IBOutlet UILabel *notificationLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnFindFriends;

- (IBAction)likeButtonAction:(id)sender;

- (IBAction)numberOfLikesButtonAction:(id)sender;


#define messageWhenNoPosts  @"User and his followers have not posted anything"
@end
