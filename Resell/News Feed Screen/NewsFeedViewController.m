
//  NewsFeedViewController.m
//  Created by Ajay Thakur on 28/09/17
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "NewsFeedViewController.h"
#import "ProductImageTableViewCell.h"
#import "LikeCommentTableViewCell.h"
#import "SVPullToRefresh.h"
#import "FontDetailsClass.h"
#import "UIImageView+WebCache.h"
#import "UIImage+GIF.h"
#import "UserProfileViewController.h"
#import "CommentsViewController.h"
#import "LikeViewController.h"
#import "PGDiscoverPeopleViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ActivityViewController.h"
#import "LikersCollectionViewCell.h"
#import "ProductDetailsViewController.h"
#import "ProductDetails.h"
#import "SectionHeaderTableViewCell.h"

#import "NSString+FontAwesome.h"

@import FirebaseInstanceID;

@interface NewsFeedViewController ()<WebServiceHandlerDelegate,UIActionSheetDelegate,UITableViewDelegate,UITableViewDataSource,CLUploaderDelegate,UIGestureRecognizerDelegate,CLUploaderDelegate,ZoomTransitionProtocol,GetCurrentLocationDelegate>
{
    UIRefreshControl *refreshControl;
    NSDictionary *cloundinaryCreditinals;
    NSString *thumbNailUrl ,*userName;
    NSString *mainUrl;
    UILabel *errorMessageLabelOutlet;
    NSIndexPath *selectedCellIndexPathForActionSheet;
    UIProgressView *customprogressView;
    NSMutableArray *arrayOfProfilePicsInNewsFeed , *arrayOfProfilePicsForEvent;
    GetCurrentLocation *getLocation;
    ProductDetails *product ;
}
@property (nonatomic)double currentLat, currentLong;
@property (nonatomic, retain) UIDocumentInteractionController *dic;
@property (strong,nonatomic) NSMutableArray *dataArray;
@property int currentIndex,paging;
@property NSInteger cellDisplayedIndex;
@property NSInteger presentCellIndex;
@property bool classIsAppearing , newsFeedLikesRefresh;
@property (nonatomic, strong) ZoomInteractiveTransition * transition;
@end

@implementation NewsFeedViewController


-(void)viewDidLoad {
    [super viewDidLoad];
     self.notificationLabel.hidden = YES;
    self.transition = [[ZoomInteractiveTransition alloc] initWithNavigationController:self.navigationController];
    self.viewWhenNoPostsOutlet.hidden = NO;
    [CommonMethods setNegativeSpacingforNavigationItem:self.navigationItem andExtraBarItem:nil] ;
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    self.notificationLabel.layer.borderColor = mBaseColor2.CGColor;
    arrayOfProfilePicsInNewsFeed = [NSMutableArray new];
    arrayOfProfilePicsForEvent = [NSMutableArray new];
    cloundinaryCreditinals =[[NSUserDefaults standardUserDefaults]objectForKey:cloudinartyDetails];
    _dataArray = [NSMutableArray new];
    [self addingRefreshControl];
    [self customeError];
    [self creatingNotificationForUpdatingLikeDetails];
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];  [HomePI showPIOnView:self.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
    NSString *token = [[FIRInstanceID instanceID] token];
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:mdeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self setNavigationBar];
    [self setRightBarButtonItems];
    
    [self.btnFindFriends setBackgroundColor:mBaseColor];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if(self.tabBarController.tabBar.hidden)
    {
        self.tabBarController.tabBar.hidden = NO;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    if(![Helper userName].length){
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        userName = [ud objectForKey:@"newRegisterUsername"];
    }
    else
        userName = [Helper userName];
    
     if (_dataArray.count == 0) {
         self.currentIndex = 0;
        [self serviceRequestingForPosts:self.currentIndex];
    }
    
    _classIsAppearing = YES;
    [self getnotificationCount];
}


/**
 Clear cache memory on warning.
 */
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}




-(void)getnotificationCount
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:@"" On:self.notificationLabel];
    
    NSDictionary *requestDict = @{
                                  @"token":[Helper userToken]
                                  };
    [WebServiceHandler getUsernNotificationCount:requestDict andDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:true];
    _classIsAppearing = NO ;
}


-(void)setNavigationBar
{
    self.navigationController.navigationBar.translucent = NO ;

}

- (IBAction)bellNotificationButtonAction:(id)sender {
    
    
    
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        self.notificationLabel.hidden = YES;
        UINavigationController *nav = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    else{
         self.notificationLabel.hidden = YES;
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
        
        ActivityViewController *activityVC  = [story instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        
    }
}

#pragma mark - Set Bar Buttom Items

- (void)setRightBarButtonItems
{
    NSString *iconString = [NSString stringWithFormat:@"%@", ([NSString fontAwesomeIconStringForEnum:FAUsers])];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:iconString];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kFontAwesomeFamilyName size:20] range:NSMakeRange(0, 1)];
    
    UIButton *enterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [enterButton setBackgroundColor:[UIColor clearColor]];
    [enterButton setAttributedTitle:attributedString forState:UIControlStateNormal];
    [enterButton addTarget:self action:@selector(findFriendsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [enterButton setFrame:CGRectMake(0, 0, 30, 30)];
    
    UIBarButtonItem *enternceButton = [[UIBarButtonItem alloc] initWithCustomView:enterButton];
    [self.navigationItem setRightBarButtonItem:enternceButton];
}


/*---------------------------------------------------------*/
#pragma mark - Pull To refresh
/*---------------------------------------------------------*/
-(void)addingRefreshControl {
    refreshControl = [[UIRefreshControl alloc]init];
    [self.tableViewOutlet addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
}
-(void)refreshData:(id)sender {
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    [self serviceRequestingForPosts:self.currentIndex];
}

/*---------------------------------------------------------*/
#pragma mark - Notifications Method
/*---------------------------------------------------------*/

/**
 Delete post based on Notification
 
 @param noti notification object
 */
-(void)deletePostFromNotificationInNewsFeed:(NSNotification *)noti {
    NSString *updatepostId = flStrForObj(noti.object[@"data"][@"postId"]);
    for (int i=0; i <self.dataArray.count;i++) {
        
        if ([flStrForObj(self.dataArray[i][@"postId"]) isEqualToString:updatepostId])
        {
            //NSUInteger atSection = [selectedCellIndexPathForActionSheet section];
            selectedCellIndexPathForActionSheet = [NSIndexPath indexPathForRow:0 inSection:i];
            [self removeRelatedDataOfDeletePost:i];
            [self.tableViewOutlet beginUpdates];
            [self.tableViewOutlet deleteSections:[NSIndexSet indexSetWithIndex:selectedCellIndexPathForActionSheet.section] withRowAnimation:UITableViewRowAnimationNone];
            [self.tableViewOutlet endUpdates];
            
            break;
            
        }
    }
}
- (void)removeRelatedDataOfDeletePost:(NSInteger )atSection {
    
    [self.dataArray removeObjectAtIndex:atSection];
    
    if (self.dataArray.count == 0) {
        self.viewWhenNoPostsOutlet.hidden = NO;
    }
}


/**
 Remove observer on memory deallocating.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:mDeletePostNotifiName];
    [[NSNotificationCenter defaultCenter]removeObserver:@"updatePostDetails"];
    [[NSNotificationCenter defaultCenter]removeObserver:@"clickCount"];
    [[NSNotificationCenter defaultCenter]removeObserver:@"likesEvent"];
    
}


/**
 Notifications Method.
 */
-(void)creatingNotificationForUpdatingLikeDetails {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostFromNotificationInNewsFeed:) name:mDeletePostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDetails:) name:@"updatePostDetails" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateClickCount:) name:@"clickCount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLikesEvent:) name:@"likesEvent" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePostsForFollowUnfollow:) name:@"updatedFollowStatus" object:nil];
}



/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Update Posts on Follow/Unfollow
/*-----------------------------------------------*/

-(void)updatePostsForFollowUnfollow:(NSNotification *)noti {
    [self refreshData:self];
}



/**
 Handle event for likes

 @param noti notification object is dictionary.
 */
-(void)handleLikesEvent:(NSNotification *)noti
{
    _newsFeedLikesRefresh = YES ;
    arrayOfProfilePicsForEvent = noti.object[@ "arrayOfProfilePics"];
    product = noti.object[@"productDetails"];
    NSIndexPath* rowToReload = noti.object[@"indexPath"];
    NSIndexPath* secondRowToreload  = [NSIndexPath indexPathForRow:1 inSection:rowToReload.section];
    NSArray* rowsToReload = [NSArray arrayWithObjects:secondRowToreload, nil];
    [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
   
    if (product.likeStatus) {
       [[self.dataArray objectAtIndex:rowToReload.section] setObject:@"1" forKey:@"likeStatus"];
    }
    else {
         [[self.dataArray objectAtIndex:rowToReload.section] setObject:@"0" forKey:@"likeStatus"];
    }
    NSString *updatedNumberOfLikes = product.likes ;
    [[self.dataArray objectAtIndex:rowToReload.section] setObject:updatedNumberOfLikes forKey:@"likes"];
    
}


/**
 Update clicks on each successfull view.

 @param noti notification object is dictionary.
 */
-(void)updateClickCount:(NSNotification *)noti
{
    ProductDetails * prod = noti.object[@"productDetails"];
    product.clickCount = prod.clickCount ;
    
    NSIndexPath* rowToReload = noti.object[@"indexPath"];
    NSIndexPath* secondRowToreload  = [NSIndexPath indexPathForRow:1 inSection:rowToReload.section];
    NSArray* rowsToReload = [NSArray arrayWithObjects:secondRowToreload, nil];
    [[self.dataArray objectAtIndex:rowToReload.section] setObject:prod.clickCount forKey:@"clickCount"];
    [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}


-(void)updatedCommentsdata:(NSNotification *)noti {
    //check the postId and Its Index In array.
    if (!_classIsAppearing) {
        NSString *updatepostId = flStrForObj(noti.object[@"newCommentsData"][@"data"][0][@"postId"]);
        for (int i=0; i <self.dataArray.count;i++) {
            if ([flStrForObj(self.dataArray[i][@"postId"]) isEqualToString:updatepostId]) {
                //NSInteger updateCellNumber  = [noti.object[@"newCommentsData"][@"selectedCell"] integerValue];
                NSString *updatedComment = flStrForObj(noti.object[@"newCommentsData"][@"data"][0][@"commenTs"]);
                
                [[self.dataArray objectAtIndex:i] setObject:updatedComment forKey:@"comments"];
                NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:2 inSection:i];
                NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload,nil];
                [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
        }
    }
}

-(void)updateDetails:(NSNotification *)noti {
    //check the postId and Its Index In array.
    if (!_classIsAppearing) {
        
        NSString *updatepostId = flStrForObj(noti.object[@"profilePicUrl"][@"data"][0][@"postId"]);
        
        for (int i=0; i <self.dataArray.count;i++) {
            
            if ([flStrForObj(self.dataArray[i][@"postId"]) isEqualToString:updatepostId])
            {
                //  updating the new data and reloading particular section.(row is constant)
                // row 1 is likebutton.
                
                if ([flStrForObj(noti.object[@"profilePicUrl"][@"message"]) isEqualToString:@"unliked the post"]) {
                    //notification for unlike a post
                    //so update like status to zero.
                    
                    [[self.dataArray objectAtIndex:i] setObject:@"0" forKey:@"likeStatus"];
                }
                else {
                    [[self.dataArray objectAtIndex:i] setObject:@"1" forKey:@"likeStatus"];
                }
                
                NSString *updatedNumberOfLikes = flStrForObj(noti.object[@"profilePicUrl"][@"data"][0][@"likes"]);
                [[self.dataArray objectAtIndex:i] setObject:updatedNumberOfLikes forKey:@"likes"];
                _dataArray[i][@"likes"] = noti.object[@"profilePicUrl"][@"data"][0][@"likes"];
                
                NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:1 inSection:i];
                NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
                [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                
                break;
            }
        }
    }
}




-(void)serviceRequestingForPosts :(NSInteger )index {
    //service requesting
    NSDictionary *requestDict = @{
                                  mauthToken:flStrForObj([Helper userToken]),
                                  moffset:flStrForObj([NSNumber numberWithInteger:index* mPagingValue]),
                                  mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue])
                                  };
    [WebServiceHandler getPostsInHOmeScreen:requestDict andDelegate:self];
}

//   navigation bar back button

/*---------------------------------------------------*/
#pragma
#pragma mark -  Table view data source and delegates
/*---------------------------------------------------*/


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section +5 == self.dataArray.count && self.paging != self.currentIndex  && self.cellDisplayedIndex!=indexPath.section) {
        self.cellDisplayedIndex = indexPath.section ;
        [self serviceRequestingForPosts:self.currentIndex];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return _dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row ==0) {
        ProductImageTableViewCell *cell = (ProductImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ProductImageCell" forIndexPath:indexPath];
        NSString *urlString = self.dataArray[indexPath.section][@"mainUrl"];
        cell.url = urlString;
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([flStrForObj(self.dataArray[indexPath.section][@"postsType"]) isEqualToString:@"0"]) {
                cell.postType = @"0";
                [cell.imageViewOutlet setHidden:NO];
                [cell setUrl:urlString];
                [cell setPlaceHolderUrl:_dataArray[                                                                                                                                                                                                     indexPath.row][@"mainUrl"]];
                [cell loadImageForCell];
                cell.imageViewOutlet.tag = 5000 +indexPath.section;
            }
        });
        return cell;
    }
    else {
        [arrayOfProfilePicsInNewsFeed removeAllObjects];
        NSInteger likeCount = [flStrForObj(self.dataArray[indexPath.section][@"likes"]) integerValue];
        if(likeCount >5)
            likeCount = 5;
        
            for(int j=0;j<likeCount;j++){
            @try
            {
                NSString *userNamel = flStrForObj(self.dataArray[indexPath.section][@"likedByUsers"][j][@"likedByUsers"]);
                NSString *profilePicOfUser = flStrForObj(self.dataArray[indexPath.section][@"likedByUsers"][j][@"profilePicUrl"]);
                NSMutableDictionary  *tempDict = [[NSMutableDictionary alloc] init];
                [tempDict setValue:userNamel forKey:@"likedByUsers"];
                [tempDict setValue:profilePicOfUser forKey:@"profilePicUrl"];
                [arrayOfProfilePicsInNewsFeed addObject:tempDict];
            }
                // To handle any exception on hiting Like button continuousle.
                   @catch (NSException *exception)
                   {
                   }
                   @finally
                   {
                   }
            }
        
        LikeCommentTableViewCell *cell = (LikeCommentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"LikeTableViewNewsFeedCell"];
        ProductDetails *productDetails = [[ProductDetails alloc]initWithDictionary:self.dataArray[indexPath.section]];
        
        if(_newsFeedLikesRefresh)
        {
          _newsFeedLikesRefresh = NO ;
            productDetails = product ;
            [arrayOfProfilePicsInNewsFeed removeAllObjects];
            [arrayOfProfilePicsInNewsFeed addObjectsFromArray:arrayOfProfilePicsForEvent];
        }
        cell.product = productDetails;
        cell.arrayOfProfilePics = arrayOfProfilePicsInNewsFeed;
        cell.index = indexPath;
        cell.collectionViewCellID = mCollectionViewCellIDInNewsFeed;
        cell.navController = self.navigationController;
        [cell setPropertiesOfOutletscheckFlag:NO];
        [cell.viewCountButton setTitle:flStrForObj(self.dataArray[indexPath.section][@"clickCount"]) forState:UIControlStateNormal];
        [cell setPrice:flStrForObj(self.dataArray[indexPath.section][@"price"]) setCurrency:flStrForObj(self.dataArray[indexPath.section][@"currency"]) andSetTitle:flStrForObj(self.dataArray[indexPath.section][@"productName"])];
        
        if(productDetails.likeStatus){
            [cell.likeButtonOutlet setTitle:productDetails.likes forState:UIControlStateSelected];
            cell.likeButtonOutlet.layer.borderColor = mBaseColor2.CGColor;
            cell.likeButtonOutlet.selected = YES;
        }
        else{
            [cell.likeButtonOutlet setTitle:productDetails.likes forState:UIControlStateNormal];
            cell.likeButtonOutlet.selected = NO;
            cell.likeButtonOutlet.layer.borderColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:.7].CGColor;
        }

        cell.likeButtonOutlet.tag = indexPath.section;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row ==0)
        return self.view.frame.size.width;
    else
        return 120;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        ProductImageTableViewCell *cell = (ProductImageTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
        
        ProductDetailsViewController *newView = [story instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
        newView.hidesBottomBarWhenPushed = YES;
        newView.product = [[ProductDetails alloc]initWithDictionary:self.dataArray[indexPath.section]];
        newView.postId = flStrForObj(self.dataArray[indexPath.section][@"postId"]);
        newView.movetoRowNumber =indexPath.item;
        newView.dataFromHomeScreen = YES;
        newView.isNewsFeedSocial = YES ;
        newView.indexPath = indexPath ;
        newView.imageFromHome = cell.imageViewOutlet.image;
        newView.currentCity = getLocation.currentCity;
        newView.countryShortName = getLocation.countryShortCode ;
        newView.currentLattitude = [NSString stringWithFormat:@"%lf",self.currentLat];
        newView.currentLongitude = [NSString stringWithFormat:@"%lf",self.currentLong];
        [self.navigationController pushViewController:newView animated:YES];
    }
}



- (void)stopAnimation {
    __weak NewsFeedViewController *weakSelf = self;
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.tableViewOutlet.pullToRefreshView stopAnimating];
        [weakSelf.tableViewOutlet.infiniteScrollingView stopAnimating];
    });
}

/*---------------------------------------*/
#pragma
#pragma mark - Reloading TableView
/*---------------------------------------*/

-(void)animateButton:(UIButton *)likeButton {
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    [ani setDuration:0.2];
    [ani setRepeatCount:1];
    [ani setFromValue:[NSNumber numberWithFloat:1.0]];
    [ani setToValue:[NSNumber numberWithFloat:0.5]];
    [ani setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[likeButton layer] addAnimation:ani forKey:@"zoom"];
}

-(void)reloadRowToShowNewNumberOfLikes:(NSInteger )reloadRowAtSection {
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:1 inSection:reloadRowAtSection];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
}

/*---------------------------------------*/
#pragma
#pragma mark - Button Actions
/*---------------------------------------*/

- (IBAction)likeButtonAction:(id)sender {
    UIButton *likeButton = (UIButton *)sender;
    // adding animation for selected button
    
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        [self animateButton:likeButton];
        if(likeButton.selected) {
            likeButton.selected = NO;
            [[self.dataArray objectAtIndex:likeButton.tag] setObject:@"0" forKey:@"likeStatus"];
            NSInteger newNumberOfLikes = [self.dataArray[likeButton.tag][@"likes"] integerValue];
            newNumberOfLikes --;
            [likeButton setTitle:[NSString stringWithFormat:@"%ld",(long)newNumberOfLikes] forState:UIControlStateNormal];
            [[self.dataArray objectAtIndex:likeButton.tag] setObject:[@(newNumberOfLikes) stringValue] forKey:@"likes"];
            [arrayOfProfilePicsInNewsFeed removeAllObjects];
            [arrayOfProfilePicsInNewsFeed addObjectsFromArray:self.dataArray[likeButton.tag][@"likedByUsers"]];
            for(int j=0;j<arrayOfProfilePicsInNewsFeed.count;j++)
            {
                if([flStrForObj(arrayOfProfilePicsInNewsFeed[j][@"likedByUsers"]) isEqualToString:userName])    // remove ProfilePic if userName has liked the post.
                    [self.dataArray[likeButton.tag][@"likedByUsers"] removeObjectAtIndex:j];
            }
            // Notify to favorites
            NSDictionary *dicForUnlike = @{
                                           @"notificationForLike":@"0",
                                           @"postdetails":self.dataArray[likeButton.tag]
                                           };
            [[NSNotificationCenter defaultCenter] postNotificationName:mfavoritePostNotifiName object:dicForUnlike];
            [self reloadRowToShowNewNumberOfLikes:likeButton.tag];
            [self unlikeAPost:flStrForObj(self.dataArray[likeButton.tag][@"postId"]) postType:flStrForObj(self.dataArray[likeButton.tag][@"postsType"])];
        }
        else  {
            likeButton.selected = YES;
            [[self.dataArray objectAtIndex:likeButton.tag] setObject:@"1" forKey:@"likeStatus"];
            NSInteger newNumberOfLikes = [self.dataArray[likeButton.tag][@"likes"] integerValue];
            newNumberOfLikes ++;
            [likeButton setTitle:[NSString stringWithFormat:@"%ld",(long)newNumberOfLikes] forState:UIControlStateSelected];
            NSDictionary *dic = [NSMutableDictionary new];
            [dic setValue:flStrForObj(self.dataArray[likeButton.tag][@"profilePicUrl"]) forKey:@"profilePicUrl"]; // add Profile pic of user If liked he post.
            [dic setValue:userName forKey:@"likedByUsers"];
            [self.dataArray[likeButton.tag][@"likedByUsers"] insertObject:dic atIndex:0];
            [[self.dataArray objectAtIndex:likeButton.tag] setObject:[@(newNumberOfLikes) stringValue] forKey:@"likes"];
            NSDictionary *dicForlike = @{
                                         @"notificationForLike":@"1",
                                         @"postdetails":self.dataArray[likeButton.tag]
                                         };
            [[NSNotificationCenter defaultCenter] postNotificationName:mfavoritePostNotifiName object:dicForlike];
            [self reloadRowToShowNewNumberOfLikes:likeButton.tag];
            [self likeAPost:flStrForObj(self.dataArray[likeButton.tag][@"postId"]) postType:flStrForObj(self.dataArray[likeButton.tag][@"postsType"])];
        }
        
    }
    
    
}


-(IBAction)CommentButtonAction:(id)sender {
    
    UIButton *commentButton = (UIButton *)sender;
    
    CommentsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"commentsStoryBoardId"];
    newView.postId =  flStrForObj(self.dataArray[commentButton.tag][@"postId"]);
    newView.postCaption =  flStrForObj(self.dataArray[commentButton.tag][@"postCaption"]);
    newView.postType = flStrForObj(self.dataArray[commentButton.tag][@"postsType"]);
    newView.imageUrlOfPostedUser =  flStrForObj(self.dataArray[commentButton.tag][@"profilePicUrl"]);
    newView.selectedCellIs =commentButton.tag;
    newView.userNameOfPostedUser = flStrForObj(self.dataArray[commentButton.tag][@"postedByUserName"]);
    newView.commentingOnPostFrom = @"ToHomeScreen";
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)numberOfLikesButtonAction:(id)sender {
    UIButton *listOflikes = (UIButton *)sender;
    LikeViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"likeStoryBoardId"];
    newView.navigationTitle = @"Likers";
    newView.postId =  flStrForObj(self.dataArray[listOflikes.tag][@"postId"]);
    newView.postType = flStrForObj(self.dataArray[listOflikes.tag][@"postsType"]);
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)moreButtonAction:(id)sender {
    
    selectedCellIndexPathForActionSheet = [self.tableViewOutlet indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
}

- (IBAction)viewAllCommentsButtonAction:(id)sender {
    UIButton *allCommentButton = (UIButton *)sender;
    CommentsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"commentsStoryBoardId"];
    newView.postId =  flStrForObj(self.dataArray[allCommentButton.tag][@"postId"]);
    newView.postCaption =  flStrForObj(self.dataArray[allCommentButton.tag][@"postCaption"]);
    newView.postType = flStrForObj(self.dataArray[allCommentButton.tag][@"postsType"]);
    newView.imageUrlOfPostedUser =  flStrForObj(self.dataArray[allCommentButton.tag][@"profilePicUrl"]);
    newView.selectedCellIs =allCommentButton.tag;
    newView.userNameOfPostedUser = flStrForObj(self.dataArray[allCommentButton.tag][@"postedByUserName"]);
    newView.commentingOnPostFrom = @"ToHomeScreen";
    [self.navigationController pushViewController:newView animated:YES];
}


/*----------------------------------------------------------------------------*/
#pragma
#pragma mark - tableview Header Buttons And Actions.
/*----------------------------------------------------------------------------*/


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{  return 60;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    static NSString *CellIdentifier = @"sectionHeaderCell";
    SectionHeaderTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [headerView setHeaderForSectionWithIndex:section data:self.dataArray];
       return headerView;
}


-(void)customeError
{
    errorMessageLabelOutlet = [[UILabel alloc]initWithFrame:CGRectMake(0, -80, [UIScreen mainScreen].bounds.size.width, 50)];
    errorMessageLabelOutlet.backgroundColor = [UIColor colorWithRed:108/255.0f green:187/255.0f blue:79/255.0f alpha:1.0];
    errorMessageLabelOutlet.textColor = [UIColor whiteColor];
    errorMessageLabelOutlet.textAlignment = NSTextAlignmentCenter;
    [errorMessageLabelOutlet setHidden:YES];
    [self.view addSubview:errorMessageLabelOutlet];
}

#pragma mark-Error Alert
-(void)showingErrorAlertfromTop:(NSString *)message {
    [errorMessageLabelOutlet setHidden:NO];
    
    [errorMessageLabelOutlet setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
    [self.view layoutIfNeeded];
    errorMessageLabelOutlet.text = message;
    
    /**
     *  changing the error message view position if user enter  wrong number
     */
    
    [UIView animateWithDuration:0.4 animations:
     ^ {
         
         [self.view layoutIfNeeded];
     }];
    
    int duration = 2; // duration in seconds
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4 animations:
         ^ {
             [errorMessageLabelOutlet setFrame:CGRectMake(0, -100, [UIScreen mainScreen].bounds.size.width, 100)];
             [errorMessageLabelOutlet setHidden:YES];
             [self.view layoutIfNeeded];
         }];
    });
}

/*---------------------------------------------------*/
#pragma mark
#pragma mark - converting video to thumbnail image
/*---------------------------------------------------*/

-(UIImage *)gettingThumbnailImage :(NSString *)url {
    NSURL *videoURl = [NSURL fileURLWithPath:url];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURl options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    
    UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
    return img;
}

- (void)makeMyProgressBarMoving {
    if (customprogressView.progress < 1) {
        //it will progress of 0.016666666666 for every sec.(value for 60 secs.)(1/60)
        [customprogressView setProgress:0.1 + customprogressView.progress animated:YES];
    }
    else if (customprogressView.progress == 1){
        [customprogressView setProgress:0 animated:NO];
    }
}


- (IBAction)headerProfileButtonACtion:(id)sender {
    UIButton *selectedHeaderButton = (UIButton *)sender;
    NSInteger selectedIndex = selectedHeaderButton.tag % 10000;
    [self openProfileOfUsername:flStrForObj(self.dataArray[selectedIndex][@"membername"])];
}


/*-----------------------------------------------------------------------------*/
#pragma
#pragma mark - REQUESTING SERVICES .
/*------------------------------------------------------------------------------*/


-(void)likeAPost:(NSString *)postId postType:(NSString *)postType {
    NSDictionary *requestDict = @{
                        mauthToken:flStrForObj([Helper userToken]),
                        mpostid:postId,
                        mLabel:@"Photo"
                        };
    [WebServiceHandler likeAPost:requestDict andDelegate:self];
}

-(void)unlikeAPost:(NSString *)postId postType:(NSString *)postType {
   NSDictionary *requestDict = @{
                        mauthToken:flStrForObj([Helper userToken]),
                        mpostid:postId,
                        mLabel:@"Photo",
                        mUserName: userName
                        };
    [WebServiceHandler unlikeAPost:requestDict andDelegate:self];
}


/*----------------------------------------------------------------------*/
#pragma mark
#pragma mark - WebServiceDelegate(Response)
/*----------------------------------------------------------------------*/

//handling response.

-(void)internetIsNotAvailable:(RequestType)requsetType
{
    [refreshControl endRefreshing];
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    if(!self.dataArray.count)
    {
    self.tableViewOutlet.backgroundView = [Helper showMessageForNoInternet:YES forView:self.view];
    self.viewWhenNoPostsOutlet.hidden = YES;
    self.tableViewOutlet.hidden = NO;
    }
}

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [refreshControl endRefreshing];
    if (error) {
        
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
        return ;
        }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    if (requestType == RequestTypegetPostsInHOmeScreen ) {
        self.tableViewOutlet.backgroundView = nil ;
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
                self.viewWhenNoPostsOutlet.hidden = YES;
                self.tableViewOutlet.hidden = NO;
                
                if(self.currentIndex == 0) {
                    [_dataArray removeAllObjects];
                    [_dataArray  addObjectsFromArray:response[@"data"]];
                }
                else {
                    [_dataArray  addObjectsFromArray:response[@"data"]];
                }
                self.currentIndex ++;
                self.paging ++;
                [self stopAnimation];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableViewOutlet reloadData];
                 });
            }
                break;
            case 204 :
            {
                if(_dataArray.count && self.currentIndex == 0)
                {
                    self.currentIndex = 0 ;
                    [_dataArray removeAllObjects];
                    self.viewWhenNoPostsOutlet.hidden = NO;
                    self.tableViewOutlet.hidden = YES;
                }
            }
                break ;
            default:
                break;
        }
    }
    else if (requestType == RequestTypeLikeAPost) {
        
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
            }
                break;
                default:
                break;
        }
    }
    else if (requestType == RequestTypeUnlikeAPost) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updatePostDetails" object:[NSDictionary dictionaryWithObject:responseDict forKey:@"profilePicUrl"]];
            }
                break;
                default:
                break;
        }
    }else   if (requestType == RequestTypeunseenNotificationCount) {
        
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
             }
                break;
            
        }
    }
}


- (IBAction)followPeopleButtonAction:(id)sender {
    PGDiscoverPeopleViewController *postsVc = [self.storyboard instantiateViewControllerWithIdentifier:mDiscoverPeopleVcSI];
    [self.navigationController pushViewController:postsVc animated:YES];
}

- (IBAction)showAllLikersButtonAction:(id)sender {
    
    UIButton *moreButton = (UIButton *)sender;
    NSInteger tag = moreButton.tag%1000;
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        LikeViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"likeStoryBoardId"];
        newView.navigationTitle = @"Likes";
        newView.postId =  flStrForObj(self.dataArray[tag][@"postId"]);
        newView.postType = flStrForObj(self.dataArray[tag][@"postsType"]);
        [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)findFriendsButtonAction:(id)sender {
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
    
    PGDiscoverPeopleViewController *newView = [story instantiateViewControllerWithIdentifier:@"discoverPeopleStoryBoardId"];
    [self.navigationController pushViewController:newView animated:YES];
    
}


-(void)likePostFromDoubleTap:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    NSIndexPath *selectedCellForLike = [self.tableViewOutlet indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
    //animating the like button.
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    [ani setDuration:0.2];
    [ani setRepeatCount:1];
    [ani setFromValue:[NSNumber numberWithFloat:1.0]];
    [ani setToValue:[NSNumber numberWithFloat:0.5]];
    [ani setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[selectedButton layer] addAnimation:ani forKey:@"zoom"];
    //checking if the post is already liked or not .if it is already liked no need to perform anything otherwise we need to perform like action.
    NSString *likeStatus =  [NSString stringWithFormat:@"%@", self.dataArray[selectedCellForLike.section][@"likeStatus"]];
    if ([likeStatus  isEqualToString:@"0"]) {
        selectedButton.selected = YES;
        [[self.dataArray objectAtIndex:selectedButton.tag] setObject:@"1" forKey:@"likeStatus"];
        NSInteger newNumberOfLikes = [self.dataArray[selectedButton.tag][@"likes"] integerValue];
        newNumberOfLikes ++;
        [[self.dataArray objectAtIndex:selectedButton.tag] setObject:[@(newNumberOfLikes) stringValue] forKey:@"likes"];
        [self reloadRowToShowNewNumberOfLikes:selectedButton.tag];
        [self likeAPost:flStrForObj(self.dataArray[selectedButton.tag][@"postId"]) postType:flStrForObj(self.dataArray[selectedButton.tag][@"postsType"])];
    }
}

/**
 *  This method will let the media link generate in main thread
 *
 *  @param feed media path
 */

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL* ) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

/*------------------------------------------------------*/
#pragma
#pragma mark - Show Tags On Image.
/*------------------------------------------------------*/
- (void)buttonClicked:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    [self openProfileOfUsername:selectedButton.titleLabel.text];
}

-(void)openProfileOfUsername:(NSString *)selectedUserName {
    UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
    newView.checkingFriendsProfile = YES;
    newView.checkProfileOfUserNmae = selectedUserName;
    newView.ProductDetails = YES;
    [self.navigationController pushViewController:newView animated:YES];
}


#pragma mark - ZoomTransitionProtocol

-(UIView *)viewForZoomTransition:(BOOL)isSource {
    NSIndexPath *selectedIndexPath = [self.tableViewOutlet indexPathForSelectedRow];
    ProductImageTableViewCell *cell = (ProductImageTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:selectedIndexPath];
    return cell.imageViewOutlet;
}


#pragma mark
#pragma mark - Location Delegate -

- (void)updatedLocation:(double)latitude and:(double)longitude
{
 
    self.currentLat = latitude ;
    self.currentLong = longitude ;
    
}



@end
