
//  ProductImageTableViewCell.h
//  Created by Ajay Thakur on 16/09/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProductImageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *VideoBackgroundViewOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewOutlet;
@property (nonatomic,strong) NSString *url;
@property (nonatomic,strong) NSString *placeHolderUrl;
@property (nonatomic,strong) UIImage *placeHolderImage;
@property NSString *postType;

-(void)loadImageForCell;

@end



