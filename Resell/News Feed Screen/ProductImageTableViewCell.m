
//  ProductImageTableViewCell.m
//  InstaVideoPlayerExample
//  Created by Rahul Sharma on 22/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.

#import "ProductImageTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "FontDetailsClass.h"
#import "UserProfileViewController.h"

@interface ProductImageTableViewCell ()

@end

@implementation ProductImageTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.imageViewOutlet.image = nil ;
}

- (void)buttonAction {
    
    
}

-(void)loadImageForCell
{
    NSURL *imageURL = [NSURL URLWithString:_url];
    if([[SDWebImageManager sharedManager] diskImageExistsForURL:[NSURL URLWithString:_url]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.imageViewOutlet setImage: [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:_url]];
        });
    }
    else
    {
        self.imageViewOutlet.alpha = 0 ;
        [self.imageViewOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@""] options:SDWebImageProgressiveDownload completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType,NSURL *imageURL){
            dispatch_async(dispatch_get_main_queue(),^{
                self.imageViewOutlet.image = image;});
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
        [UIView transitionWithView:self.imageViewOutlet duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            self.imageViewOutlet.alpha = 1.0;
        } completion:NULL];
    }

}



@end
