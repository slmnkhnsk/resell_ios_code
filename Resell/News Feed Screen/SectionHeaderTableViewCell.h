//
//  SectionHeaderTableViewCell.h
//  Resell
//
//  Created by Rahul Sharma on 28/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AlignmentOfLabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeStampLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageOutlet;

@property (weak, nonatomic) IBOutlet UIButton *headerProfileButtonOulet;


-(void)setHeaderForSectionWithIndex:(NSInteger)index  data :(NSArray *)_dataArray;
@end
