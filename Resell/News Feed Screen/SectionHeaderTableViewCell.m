//
//  SectionHeaderTableViewCell.m
//  Resell
//
//  Created by Rahul Sharma on 28/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SectionHeaderTableViewCell.h"

@implementation SectionHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setHeaderForSectionWithIndex:(NSInteger)index  data :(NSArray *)_dataArray
{
    [self.userProfileImageOutlet sd_setImageWithURL:[NSURL URLWithString:flStrForObj(_dataArray[index][@"memberProfilePicUrl"])] placeholderImage:[UIImage imageNamed:@"defaultpp"]];
    self.userNameLabel.text = flStrForObj(_dataArray[index][@"memberFullName"]);
    NSString *timeStamp = [Helper convertEpochToNormalTimeInshort:_dataArray[index][@"postedOn"]];
    self.timeStampLabel.text = timeStamp;
    self.headerProfileButtonOulet.tag  = index ;
}
@end
