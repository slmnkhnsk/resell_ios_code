

//
//  Created by Rahul Sharma on 19/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtpScreenViewController : UIViewController <WebServiceHandlerDelegate,UITextFieldDelegate>
{
    NSString *receivedOTP;
    NSString *getCodeFromTextfields;
}

@property(nonatomic) NSString *numb;
@property (nonatomic)BOOL  updateNumber;
// UIOutlets

@property (weak, nonatomic) IBOutlet UIButton *backButtonOutLet;

@property (weak, nonatomic) IBOutlet UILabel *phnNumbLabel;
@property (strong, nonatomic) IBOutlet UIButton *continueButtonOutlet;

@property (strong, nonatomic) IBOutlet UILabel *enterLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descLabel;

@property (strong, nonatomic) IBOutlet UIView *firstDigitBottomLineView;
@property (strong, nonatomic) IBOutlet UIView *secondDigitBottomLineView;
@property (strong, nonatomic) IBOutlet UIView *thirdDigitBottomLineView;
@property (strong, nonatomic) IBOutlet UIView *fourthDigitBottomLineView;


@property (strong,nonatomic) NSDictionary *paramsDict;

//button actions

- (IBAction)continueButtonAction:(id)sender;


//ACTIVITY view.
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityViewOutlet;

- (IBAction)textFieldValueChangedAction:(id)sender;
- (IBAction)tapToDismissKeyboard:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *otpTextfield1;
@property (strong, nonatomic) IBOutlet UITextField *otpTextfield2;
@property (strong, nonatomic) IBOutlet UITextField *otpTextfield3;
@property (strong, nonatomic) IBOutlet UITextField *otpTextfield4;
- (IBAction)backButtonAction:(id)sender;
- (IBAction)resendButtonAction:(id)sender;

@end
