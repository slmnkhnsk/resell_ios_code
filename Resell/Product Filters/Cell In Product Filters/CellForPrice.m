//
//  CellForPrice.m

//
//  Created by Rahul Sharma on 03/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForPrice.h"
#import "CurrencySelectVC.h"

#define  ACCEPTABLE_CHARACTERSFORPRICE @"1234567890$₹."

@implementation CellForPrice 

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
     self.textField.layer.borderColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f].CGColor;
    
}


-(void)setObjectsForIndex :(NSIndexPath *)index minPrice:(NSString *)minPrice maxPrice:(NSString *)maxPrice currencyCode:(NSString *)currencyCode
{
    switch (index.row) {
        case 0: self.labelForTitle.text = NSLocalizedString(filterCurrencyTitle, filterCurrencyTitle) ;
            self.dropDownImage.hidden = self.labelForCurrency.hidden = self.currencyButtonOutlet.hidden = NO;
            self.textField.hidden = YES;
            self.labelForCurrency.text = currencyCode ; 
            break;
        case 1: self.labelForTitle.text = NSLocalizedString(filterPriceFrom, filterPriceFrom);
            self.textField.hidden = NO;
            self.textField.tag = 1;
            if(minPrice.length > 0)
            {
                self.textField.text = minPrice ;
            }
            else
            {
                self.textField.text = @"";
                self.textField.layer.borderColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f].CGColor;
            }
            self.dropDownImage.hidden = self.labelForCurrency.hidden = self.currencyButtonOutlet.hidden = YES;

            break;
        case 2: self.labelForTitle.text = NSLocalizedString(filterPriceTo, filterPriceTo);
            self.textField.tag = 2;
            self.textField.hidden = NO;
            
            if(maxPrice.length > 0)
            {
                self.textField.text = maxPrice ;
            }
            else
            {
                self.textField.text = @"";
                 self.textField.layer.borderColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f].CGColor;
            }
            self.dropDownImage.hidden = self.labelForCurrency.hidden = self.currencyButtonOutlet.hidden = YES;
            break;
        default:
            break;
    }

    
}

#pragma mark - TextField Delegate Method

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.textField.layer.borderColor = mBaseColor.CGColor;
    
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
            if ([arrayOfString count] > 2 ) {
                return NO;
            }
            else {
                //checking number of characters after dot.
                NSRange range = [newString rangeOfString:@"."];
                if ([newString containsString:@"."] && range.location == (newString.length - 4))
                    return NO;
                else
                    return YES;
            }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(self.textField.text.length==0)
    {
        self.textField.layer.borderColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f].CGColor;
    }
    
    switch (textField.tag) {
        case 1:
        {
            if(self.callBackForFromPrice)
            {  self.callBackForFromPrice(self.textField.text); }
        }
            break;
        case 2:
        {
            if(self.callBackFortoPrice)
            {  self.callBackFortoPrice(self.textField.text); }

        }
        default:
            break;
    }

    [self.textField resignFirstResponder];
}


#pragma mark - Currency Button Action

- (IBAction)currencyButtonAction:(id)sender {
    
    CurrencySelectVC *currencySelectVC = [[UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil] instantiateViewControllerWithIdentifier:mCurrencyStoryboardID];
    currencySelectVC.previousSelection = self.labelForCurrency.text ;
    [currencySelectVC setDelegate:self];
    
    [ self.navigationController pushViewController :currencySelectVC animated:YES];
    
}

#pragma mark - CurrencyDelegate Method
-(void) country:(CurrencySelectVC *)country didChangeValue:(id)value{
    [country setDelegate:nil];
    NSDictionary *countryDict = value;
    self.currencyCode =[NSString stringWithFormat:@"%@" ,[countryDict objectForKey:CURRENCY_CODE]];
    self.labelForCurrency.text = self.currencyCode;
    self.currencySymbol = [NSString stringWithFormat:@"%@",[countryDict objectForKey:CURRENCY_SYMBOL]];
    if(_callbackForCurrency){
    self.callbackForCurrency(self.currencyCode , self.currencySymbol);
    }
}

@end
