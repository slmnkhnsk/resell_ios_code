//
//  CollectionCategoriesCell.m

//
//  Created by Rahul Sharma on 02/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#define mCategoryNameResponseKey       @"name"
#define mCategoryActiveImageKey        @"activeimage"
#define mcategoryDeactiveImageKey      @"deactiveimage"

#import "CollectionCategoriesCell.h"

@implementation CollectionCategoriesCell

- (void)awakeFromNib {
    [super awakeFromNib];
  }

-(void)setValuesWithArray:(NSArray *)arrayOfCategory indexPath:(NSIndexPath *)index andIconsArray:(NSArray *)arrayOficons
{
    if(index.row!=arrayOfCategory.count)
    {
        self.labelForCategory.text = flStrForObj(arrayOfCategory[index.row][mCategoryNameResponseKey]);
        self.labelForCategory.text = [self.labelForCategory.text capitalizedString];
         [self.imageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfCategory[index.row][mcategoryDeactiveImageKey])] placeholderImage:[UIImage imageNamed:@""]];
    }
    else
    {
        self.labelForCategory.text = @"";
        self.imageView.image = nil;
    }
}
@end
