
//
//  Created by Rahul Sharma on 31/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

/* Ajay Thakur */

#import "FilterViewController.h"
#import "CellForLocation.h"
#import "CellForCategories.h"
#import "CellForDistance.h"
#import "CellForSorting.h"
#import "CellForPrice.h"
#import "Helper.h"
#import "PinAddressController.h"
#import "CurrencySelectVC.h"

#define mDefaultColor   [UIColor colorWithRed:72/255.0f green:72/255.0f blue:72/255.0f alpha:1.0f]
@interface FilterViewController ()<CurrencyDelegate>
@end

@implementation FilterViewController
{   NSMutableArray *arrayOfCategories,*arrayOfCategoryList;
    NSArray *arrayOfSections;
    NSArray *arrayOfCells;
    NSArray *arrayOfSortingBy;
    NSArray *arrayOfPostWithIn;
    UITapGestureRecognizer *tap;
    NSInteger i;
    NSString *price,*distance , *tempCurrency;
    UITapGestureRecognizer *gestureRecognizer;
}

#pragma mark-
#pragma mark - ViewController LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    i=0;
    arrayOfCategories = [[NSMutableArray alloc]init];
    
    arrayOfSections=[[NSArray alloc]initWithObjects:filterLocationSectionTitle,filterCategorySectionTitle,filterDistanceSectionTitle/*,filterSortBySectionTitle*/,filterPostedWithInSectionTitle/*,filterPriceSectionTitle*/, nil];
    
    arrayOfCells=[[NSArray alloc]initWithObjects:@"cellForLocation",@"cellForCategories",@"cellForSlider"/*,@"cellForSort"*/,@"cellForSort"/*,@"cellForPrice"*/,nil];
    
    arrayOfSortingBy=[[NSArray alloc]initWithObjects:sortingByNewestFirst,sortingByClosestFirst,sortingByPriceLowToHigh,sortingByPriceHighToLow,nil];
    
    arrayOfPostWithIn = [[NSArray alloc]initWithObjects:postedWithLast24h,postedWithLast7d,postedWithLast30d,postedWithAllProducts,nil];
    
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setSeparatorColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1]];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = NSLocalizedString(navTitleForFilterScreen, navTitleForFilterScreen);
    [self createNavLeftButtonandCreateNavRightButton];
    
    arrayOfCategoryList =  [[[NSUserDefaults standardUserDefaults] objectForKey:mKeyForSavingCategoryList] mutableCopy];
    
    gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    gestureRecognizer.enabled = NO;
    [self.tableView addGestureRecognizer:gestureRecognizer];
    
    [self.applyButtonOutlet setBackgroundColor:mBaseColor];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveAccKeyboardInFilter:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveToOriginalInFilter) name:UIKeyboardWillHideNotification object:nil];
    
    
    self.navigationController.navigationBar.hidden = NO;
    
    if(self.checkArrayOfFilters){
        self.checkArrayOfFilters = NO;
        for(int j=0;j<self.arrayOfSelectedFilters.count;j++)
        {
            if([_arrayOfSelectedFilters[j][@"typeOfFilter"] isEqualToString:@"category"])
                [arrayOfCategories addObject:_arrayOfSelectedFilters[j][@"value"]];
            else if([self.arrayOfSelectedFilters[j][@"typeOfFilter"]isEqualToString:@"sortBy"])
                self.sortBy = self.arrayOfSelectedFilters[j][@"value"];
            else if([self.arrayOfSelectedFilters[j][@"typeOfFilter"]isEqualToString:@"postedWithin"])
                self.postedWithin = self.arrayOfSelectedFilters[j][@"value"];
            else if ([self.arrayOfSelectedFilters[j][@"price"]isEqualToString:@"price"])
                price = self.arrayOfSelectedFilters[j][@"value"];
            else if([self.arrayOfSelectedFilters[j][@"typeOfFilter"]isEqualToString:@"distance"])
            {
                self.distnce = (unsigned)[self.arrayOfSelectedFilters[j][@"value"]integerValue];
            }
            else if([self.arrayOfSelectedFilters[j][@"typeOfFilter"]isEqualToString:@"locationName"])
                self.selectedLocation = self.arrayOfSelectedFilters[j][@"value"];
            
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark - TableView DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrayOfSections.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        case 1:
        case 2:
            return 1;
            break;
        //case 3:
        case 3:
            return 4;
            break;
        //case 5:
        //    return 3;
        //    break;
        default:
            return 0;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:{
            CellForLocation *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            if(self.selectedLocation.length>5)
            {
                cell.LabelForChangeLocationTitle.hidden = YES;
                cell.selectedLocLab.hidden = NO;
                cell.selectedLocLab.text = self.selectedLocation;
                self.lattitude = _lattitude ;
                self.longitude=  _longitude ;
            }
            return cell;
        }
            break;
            
        case 1:{
            CellForCategories *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            cell.arrayOfDefaults = [[NSMutableArray alloc]init];
            cell.arrayOfCategoryList = [NSMutableArray new];
            cell.arrayOfCategoryList = arrayOfCategoryList;
            cell.arrayOfDefaults = arrayOfCategories;
            [cell.collectionviewCategory reloadData];
            cell.callBackFilter=^(NSMutableString *category, NSURL *imageUrl)
            {
                if ([arrayOfCategories containsObject:category]) {
                    [arrayOfCategories removeObject:category];
                }
                else
                    [arrayOfCategories addObject:category];
            };
            
            return cell;
        }
            
        case 2:{
            CellForDistance *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            [cell setValuesForSlider:_distnce andLeadingConstraint:self.leadingConstraint];
            cell.callBackForDistance =^(int dist, NSInteger leadingConstraint)
            {
                self.distnce = dist ;
                self.leadingConstraint = leadingConstraint ;
            };
            return cell;
        }
        /*case 3:{
            CellForSorting *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            if([[arrayOfSortingBy objectAtIndex:indexPath.row] isEqualToString:self.sortBy ])
            {
                cell.imageMark.hidden=NO;
                cell.labelForSortBy.textColor = mBaseColor;
            }
            else{
                cell.imageMark.hidden=YES;
                cell.labelForSortBy.textColor = mDefaultColor;
                
            }
            
            cell.labelForSortBy.text = NSLocalizedString([arrayOfSortingBy objectAtIndex:indexPath.row],[arrayOfSortingBy objectAtIndex:indexPath.row]);
            return cell;
        }*/
            
        case 3:{
            CellForSorting *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            if([[arrayOfPostWithIn objectAtIndex:indexPath.row] isEqualToString:self.postedWithin])
            {
                cell.imageMark.hidden=NO;
                cell.labelForSortBy.textColor = mBaseColor;
            }
            else
            {
                cell.imageMark.hidden=YES;
                cell.labelForSortBy.textColor = mDefaultColor;
                if((!self.arrayOfSelectedFilters.count )&& (indexPath.row==3)&& (!self.postedWithin.length))
                {
                    cell.imageMark.hidden=NO;
                    cell.labelForSortBy.textColor = mBaseColor;
                }
            }
            cell.labelForSortBy.text = NSLocalizedString([arrayOfPostWithIn objectAtIndex:indexPath.row], [arrayOfPostWithIn objectAtIndex:indexPath.row]);
            return cell;
        }
            
        /*case 5:{
            CellForPrice *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            cell.navigationController = self.navigationController;
            
            
            [cell setObjectsForIndex:indexPath minPrice:self.minPrce maxPrice:self.maxPrce currencyCode:_currencyCode];
            
            cell.callBackForFromPrice =^(NSString *fromPrice)
            {
                self.minPrce = fromPrice;
            };
            cell.callBackFortoPrice = ^(NSString *toPrice)
            {
                self.maxPrce = toPrice;
            };
            
            cell.callbackForCurrency = ^(NSString *currCode, NSString *currSymbol)
            {
                _currencyCode = currCode;
                _currencySymbol = currSymbol;
                
            };
            
            
            return cell;
        }*/
            
        default:
        {
            UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
            return cell1;
        }
            break;
            
    }
}

#pragma mark-
#pragma mark - TableView Delegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1){
        
        int count = (unsigned)arrayOfCategoryList.count ;
        if(!count)
        {return 0; }
        
        if(count % 2)
        { count += 1;}
        return 50 *(count/2);
    }
    else if(indexPath.section == 2)
        return 90;
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}


/**
 This is Header for sections.
 @return view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView=nil;
    UIView *subView=nil;
    UIView *divider=nil;
    CGFloat width =tableView.frame.size.width;
    sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,width ,50)];
    sectionView.backgroundColor=[UIColor colorWithRed:249/255.0 green:246/255.0 blue:255/255.0 alpha:1];
    
    divider=[[UIView alloc]initWithFrame:CGRectMake(0, 0, width,0.5)];
    divider.backgroundColor=[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1];
    [sectionView addSubview:divider];
    subView=[[UIView alloc]initWithFrame:CGRectMake(0, 0.5, tableView.frame.size.width,48)];
    subView.backgroundColor=[UIColor colorWithRed:249/255.0 green:246/255.0 blue:255/255.0 alpha:1];
    
    AlignmentOfLabel* lableIn=[[AlignmentOfLabel alloc]initWithFrame:CGRectMake(15, sectionView.center.y, sectionView.frame.size.width - 30, 15)];
    [Helper setToLabel:lableIn Text:[arrayOfSections objectAtIndex:section] WithFont:@"Roboto-medium" FSize:13 Color:[UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1]];
    [subView addSubview:lableIn];
    [sectionView addSubview:subView];
    divider=[[UIView alloc]initWithFrame:CGRectMake(0, 49.5, width,0.5)];
    divider.backgroundColor=[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1];
    [sectionView addSubview:divider];
    return sectionView;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
        PinAddressController *pinAddressVC = [story instantiateViewControllerWithIdentifier:mPinAddressStoryboardID];
        pinAddressVC.navigationItem.title = NSLocalizedString(navTitleForChangeLocation, navTitleForChangeLocation) ;
        pinAddressVC.lat = self.lattitude;
        pinAddressVC.longittude = self.longitude;
        pinAddressVC.isFromFilters = YES ;
        pinAddressVC.callBackForLocation=^(NSDictionary *locDict)
        {
            CellForLocation *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.selectedLocLab.hidden = NO;
            cell.LabelForChangeLocationTitle.hidden = YES;
            cell.selectedLocLab.text = locDict[@"address"];
            self.lattitude = [locDict[@"lat"] doubleValue];
            self.longitude= [locDict[@"long"] doubleValue];
            _selectedLocation = locDict[@"address"];
        };
        [self.navigationController pushViewController:pinAddressVC animated:YES];
    }
    else if (indexPath.section==3)
    {
        CellForSorting *cell=[tableView cellForRowAtIndexPath:indexPath];
        for(i=0;i<arrayOfSortingBy.count;i++)
        {
            NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:3 ];
            CellForSorting *cell=[tableView cellForRowAtIndexPath:ind];
            if(i!=indexPath.row)
            {
                cell.imageMark.hidden=YES;
                cell.labelForSortBy.textColor = mDefaultColor;
            }
        }
        if(cell.imageMark.hidden){
            cell.imageMark.hidden=NO;
            cell.labelForSortBy.textColor = mBaseColor;
            self.sortBy =[arrayOfSortingBy objectAtIndex:indexPath.row];
        }
        else
        {
            self.sortBy = @"";
            cell.imageMark.hidden=YES;
            cell.labelForSortBy.textColor = mDefaultColor;
        }
    }
    else if(indexPath.section==4)
    {
        CellForSorting *cell=[tableView cellForRowAtIndexPath:indexPath];
        for(i=0;i<arrayOfPostWithIn.count;i++)
        {
            NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:4 ];
            CellForSorting *cell=[tableView cellForRowAtIndexPath:ind];
            if(i!=indexPath.row){
                cell.imageMark.hidden=YES;
                cell.labelForSortBy.textColor = mDefaultColor;
                self.postedWithin = @"";
            }
            
        }
        if(cell.imageMark.hidden){
            cell.labelForSortBy.textColor = mBaseColor;
            cell.imageMark.hidden=NO;
            if(indexPath.row!=3)
                self.postedWithin =[arrayOfPostWithIn objectAtIndex:indexPath.row];
        }
        
    }
    
}

-(void)requestForFilter {
    NSInteger max = [self.maxPrce integerValue];
    NSInteger min = [self.minPrce integerValue];
    if(!self.currencySymbol.length)
    {
        self.currencySymbol = tempCurrency ;
    }
    
    if((self.maxPrce.length >0 && self.minPrce.length >0) && min > max)
    {
        [Helper showAlertWithTitle:nil Message:NSLocalizedString(minPriceAlertMessage, minPriceAlertMessage) viewController:self];
    }
    else
    {
        if(self.maxPrce.length >0 || self.minPrce.length >0)
        {
            if((self.minPrce.length > 0) && !(self.maxPrce.length > 0))
            {
                NSString *fromCost = [[NSString stringWithFormat:@"%@ ",_currencySymbol]stringByAppendingString:self.minPrce];
                price = [[NSString stringWithFormat:@"%@",@"From "] stringByAppendingString:[NSString stringWithFormat:@"%@",fromCost]];
            }
            else if(!(self.minPrce.length > 0) && (self.maxPrce.length > 0))
            {
                NSString *fromCost = [[NSString stringWithFormat:@"%@ ",_currencySymbol]stringByAppendingString:self.maxPrce];
                price = [[NSString stringWithFormat:@"%@",@"To "] stringByAppendingString:[NSString stringWithFormat:@"%@",fromCost]];
            }
            else
            {
                NSString *fromCost = [[NSString stringWithFormat:@"%@ ",_currencySymbol]stringByAppendingString:self.minPrce];
                NSString *toCost  =  [[NSString stringWithFormat:@"%@ ",_currencySymbol]stringByAppendingString:self.maxPrce];
                price = [[[NSString stringWithFormat:@"%@",fromCost]stringByAppendingString:@" - "]stringByAppendingString:toCost]  ;
            }
        }
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        
        [dic setValue:arrayOfCategories forKey:@"category"];
        if(self.sortBy.length){[dic setValue:self.sortBy forKey:@"sortBy"];}
        
        if(self.postedWithin.length){ [dic setValue:self.postedWithin forKey:@"postedWithin"];}
        
        [dic setValue:price forKey:@"price"];
        
        if(self.distnce != 0){
            
            [dic setValue:[[NSString stringWithFormat:@"%ld",(long)self.distnce]stringByAppendingString:@" Km"] forKey:@"distance"];
        }
        // leading constraint for moving label
        [dic setValue:[NSString stringWithFormat:@"%ld",(long)self.leadingConstraint] forKey:@"leadingConstraint"];
        
        if(self.selectedLocation.length > 5)
        {
            [dic setValue:self.selectedLocation forKey:@"locationName"];
            [dic setValue:[NSString stringWithFormat:@"%f",self.lattitude] forKey:@"lat"];
            [dic setValue:[NSString stringWithFormat:@"%f",self.longitude] forKey:@"long"];
        }
        
        [self.delegate getFilteredItems:dic miniPrice:(NSString *)_minPrce maxPrice:(NSString *)_maxPrce curncyCode :self.currencyCode curncySymbol:self.currencySymbol anddistance:(int)_distnce];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}



#pragma mark Apply Filters

- (IBAction)applyFiltersButtonAction:(id)sender {
    if ([CLLocationManager locationServicesEnabled]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
            [self actionWhenLocationDisabled];
        }
        else {
            [self requestForFilter];
        }
    }
    else {
        [self actionWhenLocationDisabled];
    }
}


-(void)actionWhenLocationDisabled {
    UIAlertController *alertForLocation = [UIAlertController alertControllerWithTitle:NSLocalizedString(allowLocationAlertTitle, allowLocationAlertTitle) message:NSLocalizedString(allowLocationAlertMessage, allowLocationAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionForOk = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
    }];
    
    UIAlertAction *actionForsettings = [UIAlertAction actionWithTitle:NSLocalizedString(allowLocationSettingsActionTitle, allowLocationSettingsActionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    [alertForLocation addAction:actionForsettings];
    [alertForLocation addAction:actionForOk];
    [self presentViewController:alertForLocation animated:YES completion:nil];
}


- (IBAction)navCancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)createNavLeftButtonandCreateNavRightButton {
    [self.navCancelButtonOutlet addTarget:self action:@selector(navBarLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navCancelButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
    
    UIBarButtonItem *navLeft = [[UIBarButtonItem alloc]initWithCustomView:self.navCancelButtonOutlet];
    self.navigationItem.leftBarButtonItem = navLeft;
    
    [self.navResetButtonOutlet addTarget:self action:@selector(navBarRightButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navResetButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
    
    UIBarButtonItem *navRight = [[UIBarButtonItem alloc]initWithCustomView:self.navResetButtonOutlet];
    self.navigationItem.rightBarButtonItem = navRight;
}

- (void)navBarRightButtonAction {
    [self.view endEditing:YES];
    self.minPrce = @"";
    self.maxPrce = @"";
    _distnce = 0;
    self.leadingConstraint = 40;
    self.currencyCode = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
    self.currencySymbol = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey: NSLocaleCurrencySymbol]];
    tempCurrency = self.currencySymbol;
    [self.arrayOfSelectedFilters removeAllObjects];
    [arrayOfCategories removeAllObjects];
    self.sortBy = self.postedWithin = @"";
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    CellForLocation *cell = [self.tableView cellForRowAtIndexPath:index];
    cell.LabelForChangeLocationTitle.hidden = NO;
    cell.selectedLocLab.hidden = YES;
    self.lattitude = 0;
    self.longitude=  0;
    _selectedLocation = nil;
    
    [self.tableView reloadData];
}



- (void)navBarLeftButtonAction{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.
 
 @param notification post by Notificationcentre.
 */
-(void)veiwMoveAccKeyboardInFilter :(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0,kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    gestureRecognizer.enabled = YES;
    
}

/**
 This method will move view back to original frame.
 */
-(void)veiwMoveToOriginalInFilter
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
}


-(void)hideKeyboard
{
    [self.view endEditing:YES];
    gestureRecognizer.enabled = NO ;
}



@end

