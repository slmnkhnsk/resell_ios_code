//
//  ProductLocationController.h

//
//  Created by Rahul Sharma on 17/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface ProductLocationController : UIViewController
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
- (IBAction)showMyLoc:(id)sender;
@property (nonatomic) double latti,longi;
@end
