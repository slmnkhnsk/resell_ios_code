//
//  ProductLocationController.m

//
//  Created by Rahul Sharma on 17/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ProductLocationController.h"

@interface ProductLocationController ()

@end

@implementation ProductLocationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpMapView:_latti andLongitude:_longi];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpMapView:(double )latitude andLongitude:(double )longi{
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude                                                                 longitude:longi zoom:12];
    [self.mapView animateToCameraPosition:camera];
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = kGMSTypeNormal;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.settings.zoomGestures = YES;
    self.mapView.settings.tiltGestures = NO;
    self.mapView.settings.rotateGestures = NO;
    self.mapView.userInteractionEnabled=NO;
    
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(latitude, longi);
    marker.title = @"hello";
    marker.snippet = @"india";
    marker.map = self.mapView;
    
}


- (IBAction)showMyLoc:(id)sender {
}
@end
