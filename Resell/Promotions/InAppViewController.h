//
//  InAppViewController.h
//  Datum
//
//  Created by rahul Sharma on 10/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InAppViewController : UIViewController

#pragma mark -
#pragma mark - Properties

@property (strong,nonatomic) NSString *productName, *currency , *imageUrl, *price ,*postId;
#pragma mark -
#pragma mark - Navigation Bar

- (IBAction)navCancelButtonAction:(id)sender;


#pragma mark -
#pragma mark - Product Section
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *currencyLabel;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;


#pragma mark -
#pragma mark - TableView


@property (strong, nonatomic) IBOutlet UIView *sectionHeaderForTableview;


@property (strong, nonatomic) IBOutlet UITableView *plansTableView;


#pragma mark -
#pragma mark - Purchase Button

@property (strong, nonatomic) IBOutlet UIButton *purchaseButtonOutlet;


@end
