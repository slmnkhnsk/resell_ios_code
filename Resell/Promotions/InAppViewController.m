

//
//  InAppViewController.m
//  Datum
//
//  Created by rahul Sharma on 10/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "InAppViewController.h"
#import "RageIAPHelper.h"
#import "PromotionsPlanTableViewCell.h"

@interface InAppViewController ()<UITableViewDataSource , UITableViewDelegate , WebServiceHandlerDelegate >
{
    NSMutableArray *arrayOfPlans;
    NSInteger selectedRow , planSelection;
    NSString *productKey , *planId;
}

@property (nonatomic,strong) NSArray *keyArray;
@property (nonatomic,strong) NSArray *products;

@end

@implementation InAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    planSelection = 0;
    selectedRow = -1;
    arrayOfPlans = [NSMutableArray new];
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:self.imageUrl] placeholderImage:[UIImage imageNamed:@""]];
    self.priceLabel.text = self.price ;
    self.productNameLabel.text = self.productName ;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:self.currency];
    NSString *currencySymbol = [NSString stringWithFormat:@"%@",[locale displayNameForKey:NSLocaleCurrencySymbol value:self.currency]];
    self.currencyLabel.text = currencySymbol ;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    
    self.keyArray = @[INAPP_KEY_100_Clicks,INAPP_KEY_200_Clicks,INAPP_KEY_300_Clicks,INAPP_KEY_500_Clicks];
    
    productKey = self.keyArray[0] ;
    
  //  expiredValue = [[NSUserDefaults standardUserDefaults] objectForKey:kInAppExpired];

    
    NSDictionary *requestDic = @{mauthToken : [Helper userToken] };
    
    [WebServiceHandler getPromotionPlans:requestDic andDelegate:self];
    ProgressIndicator *progress = [ProgressIndicator sharedInstance];
    [progress showPIOnView:self.view withMessage:@"Loading.."];
    
    [self.btnCancel setTitleColor:mBaseColor forState:UIControlStateNormal];
    [self.purchaseButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
}

/**
 *  It is to check products and send service request acording to the product identifier.
 *
 *  @param notification notification for product purchase.
 */
- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [self.products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            NSLog(@"idx %lu",(unsigned long)idx);
            NSLog(@"identifier %@",product.productIdentifier);
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi showPIOnView:self.view withMessage:@"Buying..."];
            
            NSDictionary *requestDic = @{
                                         mpostid : flStrForObj(self.postId),
                                         mPromoPlanId : flStrForObj(planId),
                                         mauthToken : [Helper userToken]
                                         };
            
            [WebServiceHandler PurchasePromoPlans:requestDic andDelegate:self];
            return ;
        }
    }];
}
/**
 *  Get products
 *
 *  @param block block
 */
- (void)getProductsblock:(void(^)(NSArray *products , NSError *error))block {
    
    self.products = [[NSArray alloc] init];
    
    [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            
            block(products,nil);
        }
    }];
}



#pragma mark-
#pragma mark - Webservice Delegate

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Error" message:[error localizedDescription] actionTitle:@"Ok"];
        mPresentAlertController;
    }
    
    if(requestType == RequestForPromoPlans)
    {
        switch ([response[@"code"] integerValue]) {
            case 200:
            {
                arrayOfPlans = response[@"data"];
                [self.plansTableView reloadData];
            }
                break;
            case 204 :
                break;
            default:
                break;
        }

    }
    
    
    if(requestType == RequestForPurchasePlans)
    {
        switch ([response[@"code"] integerValue]) {
            case 200:
            {
             [[NSNotificationCenter defaultCenter]postNotificationName:mUpdatePromotedPost object:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
                break;
            case 204 :
                break;
            default:
                break;
        }
        
    }
    

}

#pragma mark-
#pragma mark - Tableview Datasource & Delegate-

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrayOfPlans.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PromotionsPlanTableViewCell *plansCell = [tableView dequeueReusableCellWithIdentifier:@"promotionsPlansCell"];
    [plansCell setViewsWithData:arrayOfPlans forIndex:indexPath.row];
    return plansCell ;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedRow != -1 && indexPath.row!= selectedRow)
    {
        NSIndexPath *ind =[NSIndexPath indexPathForRow:selectedRow inSection:0 ];
        PromotionsPlanTableViewCell *cell=[tableView cellForRowAtIndexPath:ind];
         [cell updateViewForActiveState:NO];
         selectedRow = -1;
    }
    
    for(int i=0;i<arrayOfPlans.count;i++)
    {
        NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:0 ];
        PromotionsPlanTableViewCell *cell=[tableView cellForRowAtIndexPath:ind];
        if(i==indexPath.row && indexPath.row != selectedRow)
        {
            [cell updateViewForActiveState:YES];
        }
        else if (indexPath.row != selectedRow)
        {
             [cell updateViewForActiveState:NO];
            
        }
    }
    planSelection = indexPath.row ;
    productKey = self.keyArray[indexPath.row];
    planId = flStrForObj(arrayOfPlans[indexPath.row][@"planId"]);
    self.purchaseButtonOutlet.enabled = YES ;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    self.sectionHeaderForTableview.frame = CGRectMake(0, 0, self.view.frame.size.width,30 );
    
    return self.sectionHeaderForTableview;
}



- (IBAction)navCancelButtonAction:(id)sender {
    
  [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark - Purchase Button

- (IBAction)purchaseButtonAction:(id)sender {
    ProgressIndicator *progress = [ProgressIndicator sharedInstance];
    [progress showPIOnView:self.view withMessage:@"Connecting.."];
    
    [RageIAPHelper sharedInstance].callBack = ^(NSInteger plan)
    {
      [[ProgressIndicator sharedInstance] hideProgressIndicator];
        self.purchaseButtonOutlet.enabled = YES ;
        planSelection = plan ;
    };
    
    if(!planId.length)
    {
        productKey = self.keyArray[planSelection];
        planId = flStrForObj(arrayOfPlans[planSelection][@"planId"]);
       
    }
    
    self.purchaseButtonOutlet.enabled = NO ;
    [self getProductsblock:^(NSArray *products, NSError *error)
     {
    self.products = products;
    SKProduct *product;
    for(SKProduct *p in products){
        if ([p.productIdentifier isEqualToString:productKey]) {
            product = p;
            [[RageIAPHelper sharedInstance] buyProduct:product forPlanselection:planSelection];
            break;
        }
    }

     }];
     }




@end
