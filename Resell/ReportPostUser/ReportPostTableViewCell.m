//
//  ReportPostTableViewCell.m

//
//  Created by Rahul_Sharma on 02/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ReportPostTableViewCell.h"

@implementation ReportPostTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.markImageView setImage:[UIImage imageNamed:@"filter_select_button_black"]];
    self.viewForAdditionalNote.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


/**
 Set properties for outlets.
 */
-(void)setPropertiesForObjectForIndexPath:(NSIndexPath *)indexPath forSelected:(NSInteger)selectedIndex

{
    if(indexPath.row == selectedIndex)
    {
        self.viewForAdditionalNote.hidden = NO;
        self.markImageView.hidden = NO;
        self.reportTypeLabel.textColor = mBaseColor ;
    }
    else
    {
    self.markImageView.hidden = YES;
    self.viewForAdditionalNote.hidden = YES;
    self.reportTypeLabel.textColor = [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1.0f];
    }
}

@end
