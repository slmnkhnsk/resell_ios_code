//
//  ReportPostViewController.h

//
//  Created by Rahul_Sharma on 02/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetails.h"

@interface ReportPostViewController : UIViewController

@property (nonatomic, assign) NSInteger selectedRow;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *soldByUserNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleForDescription;

@property (strong, nonatomic) IBOutlet UIButton *submitButtonOutlet;

@property (strong,nonatomic) NSString *postID,*reasonForPost,*userName,*fullName,*profilePicUrl;

- (IBAction)submitButtonAction:(id)sender;

@property (nonatomic)BOOL user;
@property ProductDetails *postdetails;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorOutlet;
- (IBAction)tapToDismissKeyboard:(id)sender;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityInadicatorOtlet;


@end
