//
//  SearchLocationController.h

//
//  Created by Rahul Sharma on 24/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKitWebServiceHandler.h"
@interface SearchLocationController : UIViewController

typedef void (^updateLocation)(NSDictionary *locationDict);

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *searchTable;
@property(nonatomic)float currLat,currLongi;
@property NSArray* predictLocations;
@property (nonatomic,copy)updateLocation callBackForLocation;
@end

