//
//  SearchLocationController.m

//
//  Created by Rahul Sharma on 24/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SearchLocationController.h"
#import "PredictLocationTableCell.h"
#import <GoogleMaps/GoogleMaps.h>
@interface SearchLocationController ()<UISearchDisplayDelegate>{
    NSString *searchAPI,*nearByAPI ,*APIKey,*fullLocation;
    NSString *locName;
    float locLat,locLong;
    
}
@end

@implementation SearchLocationController
- (void)viewDidLoad {
    [super viewDidLoad];
    searchAPI=@"https://maps.googleapis.com/maps/api/place/autocomplete/json";
    nearByAPI=@"https://maps.googleapis.com/maps/api/place/nearbysearch/json";
    APIKey=@"AIzaSyAFsgyUx-wUDdH9mpDni_MFGywIVRqldpA";
    [self searchService:@""];
    self.navigationItem.title = @"Search Location";
    
    [self.searchBar setTintColor:mBaseColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    _searchBar.text = @"";
    [_searchBar becomeFirstResponder];
}

- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    //[_searchBar resignFirstResponder];
}

#pragma mark -
#pragma mark Search Bar Delegates
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:NO];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:NO];
    return YES;
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [searchBar setShowsCancelButton:NO animated:NO];
    [searchBar resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self searchService:searchText];
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

#pragma mark - WebService

/**
 *  Service To Search
 */
-(void)searchService:(NSString* )searchString
{
    RestKitWebServiceHandler * restKit = [RestKitWebServiceHandler sharedInstance];
    NSString* loc=[NSString stringWithFormat:@"%f,%f",locLat,locLong];
    NSDictionary *queryParams = [NSDictionary dictionaryWithObjectsAndKeys:searchString,@"input",loc,@"location",@"500",@"radius",@"prominence",@"rankby",APIKey,@"key", nil];
    NSString* API;
    if (searchString.length) {
        API=searchAPI;
    }else{
        API=nearByAPI;
    }
    
   // NSLog(@"\n\n%@\%@",API,queryParams);
    [restKit composeRequestWithMethodGET:API
                                 paramas:queryParams
                            onComplition:^(BOOL success, NSDictionary *response){
                                
                                if (success) { //handle success responsea
                                    [self guestLoginSeviceResponse:(NSDictionary*)response api:API];
                                }
                                else{//error
                                    
                                }
                            }];
    
}

-(void)guestLoginSeviceResponse:(NSDictionary *)array api:(NSString*)API
{
   // NSLog(@"%@",array);
    if ([array[@"status"]isEqualToString:@"OK"]) {
        if (API==searchAPI) {
            _predictLocations = array[@"predictions"];
        }else{
            _predictLocations = array[@"results"];
        }
        [_searchTable reloadData];
    }
    
}
#pragma mark - Tableview Delegate & Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_predictLocations.count>10) {
        return 10;
    }
    return _predictLocations.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PredictLocationTableCell *cell =(PredictLocationTableCell*) [self.searchTable dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[PredictLocationTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSDictionary* dict=_predictLocations[indexPath.row];
    if (dict[@"description"]) {
        cell.descriptionLabel.text = dict[@"description"];
    }else{
        cell.descriptionLabel.text = dict[@"name"];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIButton *btn = [[UIButton alloc]init];
    btn.tag = indexPath.row;
    [self selectCell:btn];
}


-(void)selectCell:(UIButton*)btn{
    NSDictionary* dict=_predictLocations[btn.tag];
    fullLocation=[[NSString alloc]init];
    if (dict[@"description"]) {
        fullLocation = dict[@"description"];
    }else{
        fullLocation= dict[@"name"];
    }
    NSString*placeid=dict[@"reference"];
    RestKitWebServiceHandler * restKit = [RestKitWebServiceHandler sharedInstance];
    NSDictionary *queryParams = [NSDictionary dictionaryWithObjectsAndKeys:placeid,@"reference",APIKey,@"key", nil];
    
    
   // NSLog(@"%@",queryParams);
    [restKit composeRequestWithMethodGET:@"https://maps.googleapis.com/maps/api/place/details/json"
                                 paramas:queryParams
                            onComplition:^(BOOL success, NSDictionary *response){
                                
                                if (success) { //handle success response
                                    [self getAddressAndUpdateUI:(NSDictionary*)response];
                                }
                                else{//error
//                                    [progressIndicator hideProgressIndicator];
                                }
                            }];
    
}
/**
 *  Get address from google
 *  Update the UI
 *  @param place Dictionary of place
 */
- (void)getAddressAndUpdateUI:(NSDictionary *)placein {
//    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
//    [progressIndicator hideProgressIndicator];
    NSDictionary* place=placein[@"result"];
   // NSLog(@"%@",place);
    
    if (!place) {
        return;
    }
    NSDictionary *locationDict = [place valueForKey:@"geometry"][@"location"];
    locLat = [locationDict[@"lat"] floatValue];
    locLong = [locationDict[@"lng"] floatValue];
    locName = [place valueForKey:@"name"];
  //  NSLog(@"%@",locName);
    NSArray *addressArray = [place valueForKey:@"address_components"];
    NSString *countryShortName = addressArray[addressArray.count - 1][@"short_name"];

    NSDictionary * dict = [NSDictionary new];
    dict = @{@"address": fullLocation,
             @"lat" :locationDict[@"lat"],
             @"long":locationDict[@"lng"],
             @"city" :locName,
             @"countryShortName" : countryShortName
             };
    self.callBackForLocation(dict);
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)current:(id)sender {
    locName=@"Current Location";
      locLat = _currLat;
      locLong = _currLongi;
    if (locLat==0||locLong ==0) {
        locLat = 13.0178200;
        locLong = 77.5919200;
    }
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(locLat,locLong) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
       // NSLog(@"%@\n%@",response,error);
        if (response) {
            GMSAddress* addressObj=[[response results]objectAtIndex:0];
            locName=addressObj.locality;
        }else{
            locName=@"";
        }
        [self.navigationController popViewControllerAnimated:YES];
        //        [self dismissViewControllerAnimated:YES completion:nil];
    }];

}
@end
