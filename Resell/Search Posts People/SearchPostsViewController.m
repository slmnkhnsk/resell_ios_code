
//
//  SearchPostsViewController.m

//
//  Created by Rahul Sharma on 17/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SearchPostsViewController.h"
#import "TopTableViewCell.h"
#import "PeopleTableViewCell.h"
#import "UserProfileViewController.h"
#import "ProductDetailsViewController.h"


@interface SearchPostsViewController () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate,WebServiceHandlerDelegate>
{
    CGPoint lastScrollContentOffset;
    NSMutableArray *topResponseData,*peopleData;
   // UIGestureRecognizer *gestureRecognizer ;
}

@end

@implementation SearchPostsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    topResponseData = peopleData = [NSMutableArray new];
    self.postButtonOutlet.selected = YES;
    // Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(navTitleForSearch, navTitleForSearch) ;
    self.searchBarOutlet.placeholder = NSLocalizedString(searchPosts, searchPosts);
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mCloseButton normalState:mCloseButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navBack) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
    
    [self.postButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.peopleButtonOutlet setTitleColor:[mBaseColor colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    [self.movingDividerOutlet setBackgroundColor:mBaseColor];
    [self.topTabView setBackgroundColor:mNavigationBarColor];
    
//    gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
//    gestureRecognizer.enabled = NO;
//    [self.postTableView addGestureRecognizer:gestureRecognizer];
//    [self.peopleTableView addGestureRecognizer:gestureRecognizer];
//    gestureRecognizer.enabled = NO;

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.searchBarOutlet.text = self.searchString;
    
    if (![self.searchBarOutlet.text isEqualToString:@""]) {
        NSString *encodedString =  [self.searchBarOutlet.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        
        if(self.peopleButtonOutlet.selected){
            if (![self.searchBarOutlet.text isEqualToString:@""]) {
                [self requestForPeople:encodedString];
            }
            else {
                [peopleData removeAllObjects];
                [self.peopleTableView reloadData];
            }
        }
        else
        {
            
            if(![self.searchBarOutlet.text isEqualToString:@""]) {
                [self requestForPosts:encodedString];
            }
            else {
                [topResponseData removeAllObjects];
                [self.postTableView reloadData];
            }
        }
    }
    
    //[_searchBarOutlet becomeFirstResponder];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveAccKeyboardInSearch) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveToOriginalInSearch) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
   
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [self.view endEditing:YES];
    
}


-(void)navBack
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*---------------------------------------------------------*/
#pragma mark - searchbar delegates
/*---------------------------------------------------------*/

#pragma UIsearchbardelegate


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{

    if (self.postButtonOutlet.selected)
    {
        self.searchBarOutlet.placeholder = NSLocalizedString(searchPosts, searchPosts);
        
    }
    else {
        self.searchBarOutlet.placeholder = NSLocalizedString(searchPeople, searchPeople);
        
    }
    return YES;
}



- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar {
    
    [self performSegueWithIdentifier:@"addContactToDiscoverPeopleSegue" sender:nil];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSString *encodedString =  [searchText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    
    if(self.peopleButtonOutlet.selected){
    if (![searchText isEqualToString:@""]) {
        [self requestForPeople:encodedString];
    }
    else {
        [peopleData removeAllObjects];
        [self.peopleTableView reloadData];
    }
    }
    else
    {
        
    if(![searchText isEqualToString:@""]) {
        [self requestForPosts:encodedString];
    }
    else {
        [topResponseData removeAllObjects];
        [self.postTableView reloadData];
    }
    }
}


-(void)requestForPeople:(NSString *)searchText {
    
    if([[Helper userToken]isEqualToString:mGuestToken])
    {
        NSDictionary *requestDict = @{
                                      mMember :searchText,
                                      moffset:@"0",
                                      mlimit:@"40"
                                      };
        [WebServiceHandler getSearchPeopleForGuestUser:requestDict andDelegate:self];
        
    }
    else{
    NSDictionary *requestDict = @{
                                  @"userNameToSearch" :searchText,
                                  mauthToken :flStrForObj([Helper userToken]),
                                  moffset:@"0",
                                  mlimit:@"40"
                                  };
    [WebServiceHandler getSearchPeople:requestDict andDelegate:self];
    }
}

-(void)requestForPosts:(NSString *)searchText {
    NSDictionary *requestDict = @{
                                  mauthToken :flStrForObj([Helper userToken]),
                                  mProductName:searchText,
                                  moffset:@"0",
                                  mlimit:@"20"
                                  
                                  };
    [WebServiceHandler getSearchForPosts:requestDict andDelegate:self];
}



/*------------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*--------------------------------------------------------*/
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    //handling response.
    if (error) {
      return;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    //response for hashtagsuggestion api.
        if (requestType == RequestTypeGetSearchForPosts ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [self handlingPostsResponseData:responseDict];
            }
                break;
            case 19031:
            case 19032: {
                [self errorAlert:responseDict[@"message"]];
            }
        }
    }
    
    if (requestType == RequestTypeGetSearchPeople) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [self hadnlingPeopleApiResponse:responseDict];
            }
                break;
            case 19031:
            case 19032: {
                [self errorAlert:responseDict[@"message"]];
            }
        }
        
    }
    
    if (requestType == RequestTypegetUserSearchHistory) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
               // NSLog(@"searched history is :%@",responseDict);
            }
                break;
        }
    }
    
    if (requestType == RequestTypeAddToSearchHistory) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
               // NSLog(@"added key to searched history is :%@",responseDict);
            }
                break;
        }
    }
    
}

- (void)errorAlert:(NSString *)message {
    //alert for failure response.
    UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Message" message:message actionTitle:@"Ok"];
    [self presentViewController:controller animated:YES completion:nil];
}



-(void)hadnlingPeopleApiResponse:(NSDictionary *)peopleDic {
    if (peopleDic) {
        peopleData =[[NSMutableArray alloc] init];
        peopleData = peopleDic[@"data"];
        [self.peopleTableView reloadData];
    }
    else {
        peopleData = nil;
        [self.peopleTableView reloadData];
    }
}


-(void)handlingPostsResponseData:(NSDictionary *)postsData {
    if (postsData) {
        topResponseData =[[NSMutableArray alloc] init];
        topResponseData = postsData[@"data"];
        [self.postTableView reloadData];
    }
    else {
        topResponseData = nil;
        [self.postTableView reloadData];
    }
}



/*--------------------------------------------------*/
#pragma mark - Scrollview Delegate
/*-------------------------------------------------*/
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    lastScrollContentOffset = scrollView.contentOffset ;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.mainScrollViewOutlet]) {
        CGPoint offset = scrollView.contentOffset;
        self.movingDividerLeadingConstraint.constant = scrollView.contentOffset.x/2;
        if(offset.x == 0 ) {
            
            self.postButtonOutlet.selected = YES;
            self.peopleButtonOutlet.selected = NO;
            self.searchBarOutlet.placeholder = NSLocalizedString(searchPosts, searchPosts);
            self.searchBarOutlet.text = @"";

        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)) {

            self.postButtonOutlet.selected = NO;
            self.peopleButtonOutlet.selected = YES;
            self.searchBarOutlet.placeholder = NSLocalizedString(searchPeople, searchPeople);
            self.searchBarOutlet.text = @"";
        }
        scrollView.contentOffset = offset;
    }
}

/*-------------------------------------------------------------------------*/
#pragma mark - tableview Delegate and data source.
/*------------------------------------------------------------------------*/


// top tableview  (posts)         -       10(tag)
//peopleTableView      -       20(tag)



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 10)
        return topResponseData.count;
    else if (tableView.tag == 20)
        return peopleData.count;
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 10 ) {
        TopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:mTopTableViewCellID  forIndexPath:indexPath];
        [cell setValuesInCellWithUsername:flStrForObj(topResponseData[indexPath.row][@"productName"]) fullName:flStrForObj(topResponseData[indexPath.row][@"category"]) andProfileImage:flStrForObj( topResponseData[indexPath.row][@"thumbnailImageUrl"])];
        return cell;
    }
    else  if (tableView.tag == 20 )
    {
        PeopleTableViewCell *peopleCell = [tableView dequeueReusableCellWithIdentifier:mPeopleTableViewCellID forIndexPath:indexPath];
        [peopleCell setValuesInCellWithUsername:flStrForObj(peopleData[indexPath.row][@"username"]) fullName:flStrForObj(peopleData[indexPath.row][@"fullName"]) andProfileImage:flStrForObj(peopleData[indexPath.row][@"profilePicUrl"])];
        
        return peopleCell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 10) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ProductDetailsViewController *newView = [story instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
        newView.hidesBottomBarWhenPushed = YES;
        newView.postId = flStrForObj(topResponseData[indexPath.row][@"postId"]);
        NSArray *data = [NSArray new];
        data = topResponseData[indexPath.row];
        newView.movetoRowNumber = indexPath.item;
        newView.noAnimation = YES;
        newView.hidesBottomBarWhenPushed = YES ;
        [self.navigationController pushViewController:newView animated:YES];
        }
    else
    {
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UserProfileViewController *newView = [story instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
        newView.checkProfileOfUserNmae = flStrForObj(peopleData[indexPath.row][@"username"]);
        newView.checkingFriendsProfile = YES;
        newView.ProductDetails = YES;
        [self.navigationController pushViewController:newView animated:YES];

    }
  
}


- (IBAction)postsButtonAction:(id)sender {
    
    self.postButtonOutlet.selected = YES;
    self.peopleButtonOutlet.selected = NO;
    
    [self.postButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.peopleButtonOutlet setTitleColor:[mBaseColor colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    
    self.movingDividerLeadingConstraint.constant = self.mainScrollViewOutlet.contentOffset.x/2;
    CGRect frame = self.mainScrollViewOutlet.bounds;
    frame.origin.x = 0 * CGRectGetWidth(self.view.frame);
    [self.mainScrollViewOutlet scrollRectToVisible:frame animated:YES];
    
}

- (IBAction)peopleButtonAction:(id)sender {
    
    self.postButtonOutlet.selected = NO;
    self.peopleButtonOutlet.selected = YES;
    
    [self.postButtonOutlet setTitleColor:[mBaseColor colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    [self.peopleButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    
    CGRect frame = self.mainScrollViewOutlet.bounds;
    frame.origin.x = 1 * CGRectGetWidth(self.view.frame);
    [self.mainScrollViewOutlet scrollRectToVisible:frame animated:YES];
    self.movingDividerLeadingConstraint.constant = self.mainScrollViewOutlet.contentOffset.x/2;
    
}


//-(void)hideKeyboard
//{
//    [self.view endEditing:YES];
//}
//
//
//#pragma mark - Move View
//
///**
// This method will move view by evaluating keyboard height.
// 
// @param notification post by Notificationcentre.
// */
//-(void)veiwMoveAccKeyboardInSearch {
//
//    gestureRecognizer.enabled = YES;
//    
//}
//
///**
// This method will move view back to original frame.
// */
//-(void)veiwMoveToOriginalInSearch
//{
//
//    gestureRecognizer.enabled = NO;
//}

@end
