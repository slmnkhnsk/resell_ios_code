//
//  SuccessFullyPostedViewController.m

//
//  Created by Rahul Sharma on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SuccessFullyPostedViewController.h"

@interface SuccessFullyPostedViewController ()

@end

@implementation SuccessFullyPostedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(_isTwitterSharing)
    {
        [self.activityLoaderOutlet startAnimating];
        [self shareLinkOnTwitter];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return YES ;
}

-(void)shareLinkOnTwitter
{
  //  NSString * postLink = [SHARE_LINK  stringByAppendingString:[NSString stringWithFormat:@"%@",self.postId]];
    NSString *mediaLink =[Helper getWebLinkForFeed:SHARE_LINK];
    NSString *posttext = [NSString stringWithFormat:@"Shared via @Resell %@",mediaLink];
    NSArray *items = @[posttext];
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    // and present it
    [Helper presentActivityController:controller forViewController:self];
    [self.activityLoaderOutlet stopAnimating];
}


- (IBAction)okaButtonAction:(id)sender {
    
    if(self.postingDelegate)
    {
        [_postingDelegate postingListedSuccessfully];
    }
    
}

- (IBAction)closeButtonAction:(id)sender {
    
    if(self.postingDelegate)
    {
        [_postingDelegate postingListedSuccessfully];
    }
    
    
}
@end

