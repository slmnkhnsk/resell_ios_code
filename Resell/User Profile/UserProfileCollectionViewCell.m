//
//  UserProfileCollectionViewCell.m

//
//  Created by Rahul Sharma on 3/30/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "UserProfileCollectionViewCell.h"

@implementation UserProfileCollectionViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderWidth= 0.5f;
    self.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.contentView.backgroundColor =[UIColor clearColor];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.postedImagesOutlet.image = nil ;
}

-(void)setImageObjectWithData:(NSArray *)dataArray andIndex :(NSIndexPath *)indexPath
{
    NSString *isPromotedValue = flStrForObj(dataArray[indexPath.row][@"isPromoted"]);
    BOOL isPromoted = [isPromotedValue boolValue];
    
    if(isPromoted)
    {
        self.featuredView.hidden = NO ;
    }
    else
    {
        self.featuredView.hidden = YES ;
    }
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];

    NSURL *imageURL = dataArray[indexPath.row][@"mainUrl"];
    if([manager diskImageExistsForURL:[NSURL URLWithString:dataArray[indexPath.row][@"mainUrl"]]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            if(dataArray.count >= indexPath.row +1){
            [self.postedImagesOutlet setImage: [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:dataArray[indexPath.row][@"mainUrl"]]];
            }
        });
    }
    else
    {
        [self.postedImagesOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@""] options:0 completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType,NSURL *imageURL){
            dispatch_async(dispatch_get_main_queue(),^{
                [UIView transitionWithView:self.postedImagesOutlet duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    [self.postedImagesOutlet setImage:image];
                }completion:NULL];
            });
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;

        
    }

}

@end
