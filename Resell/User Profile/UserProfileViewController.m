

//
//  UserProfileViewController.m
//
//  Created by Rahul Sharma on 3/30/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "UserProfileViewController.h"
#import "UserProfileCollectionViewCell.h"
#import "LikeViewController.h"
#import "FBLoginHandler.h"
#import "UIImageView+AFNetworking.h"
#import "EditProfileViewController.h"
#import "FontDetailsClass.h"
#import "OptionsViewController.h"
#import "PGDiscoverPeopleViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ProductDetailsViewController.h"
#import "EditProductViewController.h"
#import "LikeCommentTableViewCell.h"
#import "SVPullToRefresh.h"
#import <MessageUI/MessageUI.h>
#import "ReportPostViewController.h"
#import "PaypalViewController.h"
#import "UpdatePhoneNumberVC.h"
#import "UpDatingEmailViewController.h"
#import "CameraViewController.h"
#import "RFQuiltLayout.h"

/* MACROS */

#define mEditProfile                @"EDIT PROFILE"
#define mlikeStoryBoardId           @"likeStoryBoardId"
#define mDiscoverPeopleImage        @"profile_add_user_icon_off"
#define mSettingButtonImage         @"profile_setting_button"
#define mEditProfileImage           @"editprofile"
#define mResponseKeyForProfileDetails   @"profileData"


@interface UserProfileViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,WebServiceHandlerDelegate,UIGestureRecognizerDelegate,SDWebImageManagerDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,ZoomTransitionProtocol,UIAlertViewDelegate, FBLoginHandlerDelegate, GIDSignInDelegate , GIDSignInUIDelegate, payPalDelegate,RFQuiltLayoutDelegate>
{
    UserProfileCollectionViewCell *collectionViewCell;
    UIRefreshControl *refreshControl;
    NSString *ProfilePicUrl,*USERNAME , *fbloggedUserAccessToken, *googlePlusUserAccessToken, *googlePlusId,*paypalUrl  ;
    BOOL checkingOwnProfile, refresh;
    BOOL visitingFirstTime;
    UIActivityIndicatorView *av;
    CGFloat heightOfTheRow ,ratio;
    NSInteger pageNumber;
    UIButton *navNextButton, *navCancelButton;
    NSMutableArray *soldPostData,*sellingPostData,*favouritePostData,*profiledetailsOfUser;
    bool neccessaryToshowFollowRequestMessage;
    BOOL classIsAppearing, soldPostServiceCall ;
    UIView *navBackView,*dividerView;
    
    CALayer *selectedTabBottomLine;
    CALayer *selectedStickyTabBottomLine;
}
@property (nonatomic, strong) ZoomInteractiveTransition * transition;
@end

@implementation UserProfileViewController

/*-----------------------------------------------*/
#pragma mark -
#pragma mark - viewController LifeCycle
/*-----------------------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    self.transition = [[ZoomInteractiveTransition alloc] initWithNavigationController:self.navigationController];
    self.sellingCurrentIndex = 0, self.soldCurrentIndex = 0 , self.favouritesCurrentIndex = 0 ;
    self.sellingPaging = 1, self.soldPaging = 1 , self.favouritePaging = 1 ;
    self.sellingDisplayedIndex = -1 , self.soldDisplayedIndex = -1 , self.favouriteDisplayedIndex = -1 ;
    sellingPostData = [[NSMutableArray alloc]init];
    soldPostData = [[NSMutableArray alloc]init];
    favouritePostData = [[NSMutableArray alloc]init];
    profiledetailsOfUser   = [[NSMutableArray alloc]init];
    self.contactButtonOutlet.layer.borderColor = mBaseColor.CGColor ;
    //    [self createActivityViewInNavbar];
    [self RFQuiltLayoutIntialization];
    [self handlingWebUrlFromBio];
    [self tapGestureForWebsiteUrl];
    
    if(![Helper userName].length){
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        USERNAME = [ud objectForKey:@"newRegisterUsername"];
    }
    else
    {
        USERNAME = [Helper userName];
    }
     self.automaticallyAdjustsScrollViewInsets = YES;
    [self addingRefreshControl];
    [self notiFicationForUpdatingStatus];
    [self createNavLeftButton];
    classIsAppearing = YES;
    self.profilePhotoOutlet.layer.borderColor = [UIColor whiteColor].CGColor;
    
    CGFloat navHeight = [UIApplication sharedApplication].statusBarFrame.size.height +  self.navigationController.navigationBar.frame.size.height ;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    navBackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, navHeight)];
    navBackView.backgroundColor=[UIColor clearColor];
    dividerView = [[UIView alloc]initWithFrame:CGRectMake(0, navHeight - 0.5, self.view.frame.size.width,0.5)];
    dividerView.backgroundColor = [UIColor clearColor] ;
    [navBackView addSubview:dividerView];
    [self.view addSubview:navBackView];
    [self GetProfileDetails];
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
    [HomePI showPIOnView:self.view withMessage:@"Loading..."];
    [self requestForPostsWithIndex:self.sellingCurrentIndex withSoldType:@"0"];
    self.paypalOutlet.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    if(self.checkingFriendsProfile && ![[Helper userName]isEqualToString:self.checkProfileOfUserNmae])
    {
        [self dontShowFavouritesOfOtherUsers];
    }
    else {
        [self showFaviouriteButton];
    }
        if (@available(iOS 11.0, *)) {
            [self.scrollView setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
        } else {
            // Fallback on earlier versions

        }
     if([[RTL sharedInstance] isRTL]){
     self.slidingScrollView.transform = CGAffineTransformMakeRotation(M_PI);
     self.productSectionContentView.transform = CGAffineTransformMakeRotation(M_PI);
     }
    
    [self setViewDesign];
}


-(void)viewWillAppear:(BOOL)animated
{
    if(self.tabBarController.tabBar.hidden && !self.hidesBottomBarWhenPushed)
    {
        self.tabBarController.tabBar.hidden = NO;
    }
    
      [self PaypalLinkIsSaved:[[NSUserDefaults standardUserDefaults]objectForKey:payPalLink]];
    [self gettingDetailsOfUser];
    [self createNavSettingButton];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    classIsAppearing = NO;
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

/**
 Clear cache memory on warning.
 */
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self clearImageCache];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

/**
 Deallocate memory again.
 */
-(void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:@"updatedFollowStatus"];
//    [[NSNotificationCenter defaultCenter] removeObserver:@"updatedFollowStatus"];
//    [[NSNotificationCenter defaultCenter] removeObserver:@"updatedFollowStatus"];
    [[NSNotificationCenter defaultCenter] removeObserver:mSavePayPalNotify];
    [[NSNotificationCenter defaultCenter] removeObserver:@"updatedFollowStatus"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
     [self clearImageCache];
}


-(void)tapGestureForWebsiteUrl {
    UITapGestureRecognizer *labelrecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(webUrlTappedAction:)];
    labelrecog.numberOfTapsRequired = 1;
    [self.webSiteUrlLabelOutlet addGestureRecognizer:labelrecog];
    self.webSiteUrlLabelOutlet.userInteractionEnabled = YES;
}


#pragma mark - RFQuiltLayout Intialization

/**
 This method will create an object for RFQuiltLayout.
 */
-(void)RFQuiltLayoutIntialization
{
    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
    layout.direction = UICollectionViewScrollDirectionVertical;
    if(self.view.frame.size.width == 375)
        layout.blockPixels = CGSizeMake( 37,31);
    else if(self.view.frame.size.width == 414)
    {
        layout.blockPixels = CGSizeMake( 102,31);
    }

    else
        layout.blockPixels = CGSizeMake( 79,31);
    self.sellingCollectionView.collectionViewLayout = layout;
    layout.delegate=self;
    
    RFQuiltLayout* soldLayout = [[RFQuiltLayout alloc]init];
    soldLayout.direction = UICollectionViewScrollDirectionVertical;
    if(self.view.frame.size.width == 375)
        soldLayout.blockPixels = CGSizeMake( 37,31);
    else if(self.view.frame.size.width == 414)
    {
        soldLayout.blockPixels = CGSizeMake( 102,31);
    }

    else
        soldLayout.blockPixels = CGSizeMake( 79,31);
    self.soldCollectionView.collectionViewLayout = soldLayout;
    soldLayout.delegate=self;
    
    RFQuiltLayout* favLayout = [[RFQuiltLayout alloc]init];
    favLayout.direction = UICollectionViewScrollDirectionVertical;
    if(self.view.frame.size.width == 375)
        favLayout.blockPixels = CGSizeMake( 37,31);
    else if(self.view.frame.size.width == 414)
    {
        favLayout.blockPixels = CGSizeMake( 102,31);
    }

    else
        favLayout.blockPixels = CGSizeMake( 79,31);
    self.favouritesCollectionView.collectionViewLayout = favLayout;
    favLayout.delegate=self;
    
}

/*------------------------------------*/
#pragma mark
#pragma mark - Set View Design
/*------------------------------------*/

- (void)setViewDesign
{
    [self.sellingButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
    [self.soldButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
    [self.favoriteButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
    [self.stickySoldButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
    [self.stickySellingButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
    [self.stickyFavouriteButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
    
    [self.sellingButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.soldButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.favoriteButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.stickySoldButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.stickySellingButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.stickyFavouriteButtonOutlet setTitleColor:mBaseColor forState:UIControlStateSelected];
    [self.contactButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
    
    [self.sectionHeaderForCollectionView setBackgroundColor:mBaseWhiteColor];
    [self.stickySectionHeaderForCV setBackgroundColor:mBaseWhiteColor];
    [self.startDiscoveringOutlet setBackgroundColor:mBaseColor];
    [self.noPostButtonOutlet setBackgroundColor:mBaseColor];
 
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.sectionHeaderForCollectionView.frame.size.height-1, [UIScreen mainScreen].bounds.size.width, 1.0f);
    bottomBorder.backgroundColor = mNavLineColor.CGColor;
    [self.sectionHeaderForCollectionView.layer addSublayer:bottomBorder];
    
    CALayer *bottomStickyBorder = [CALayer layer];
    bottomStickyBorder.frame = CGRectMake(0.0f, self.stickySectionHeaderForCV.frame.size.height-1, [UIScreen mainScreen].bounds.size.width, 1.0f);
    bottomStickyBorder.backgroundColor = mNavLineColor.CGColor;
    [self.stickySectionHeaderForCV.layer addSublayer:bottomStickyBorder];
    
    [self.viewAboveSectionHeaderOfCV setBackgroundColor:mNavLineColor];
    
    selectedTabBottomLine = [CALayer layer];
    selectedTabBottomLine.frame = CGRectMake(5.0f, self.sellingButtonOutlet.bounds.size.height - 2, ([UIScreen mainScreen].bounds.size.width / 3) - 10, 2.0f);
    selectedTabBottomLine.backgroundColor = mBaseColor.CGColor;
    [self.sellingButtonOutlet.layer addSublayer:selectedTabBottomLine];
    
    selectedStickyTabBottomLine = [CALayer layer];
    selectedStickyTabBottomLine.frame = CGRectMake(5.0f, self.sellingButtonOutlet.bounds.size.height - 2, ([UIScreen mainScreen].bounds.size.width / 3) - 10, 2.0f);
    selectedStickyTabBottomLine.backgroundColor = mBaseColor.CGColor;
    [self.stickySellingButtonOutlet.layer addSublayer:selectedStickyTabBottomLine];
}

/*------------------------------------*/
#pragma mark
#pragma mark - Notification Methods
/*------------------------------------*/

// selling Data.

-(void)deletePostFromNotification:(NSNotification *)noti {
    
    NSString *updatepostId = flStrForObj(noti.object[@"data"][@"postId"]);
    for (int i=0; i < sellingPostData.count;i++) {
        
        if ([flStrForObj(sellingPostData [i][@"postId"]) isEqualToString:updatepostId])
        {
            NSInteger numberOfPosts = [self.numberOfPostsString integerValue];
            numberOfPosts--;
            if (numberOfPosts >=0) {
                self.numberOfPostsString = [NSString stringWithFormat:@"%ld",(long)numberOfPosts ];
            }
            else {
                self.numberOfPostsString = 0;
            }
            
            [self updatePostButtonTitle];
//            [self.sellingCollectionView reloadData];
            [self.sellingCollectionView performBatchUpdates:^{
                [sellingPostData removeObjectAtIndex:i];
                NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
                [self.sellingCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            } completion:^(BOOL finished) {
            }];
            
            if(!sellingPostData.count)
            {
                [self showingMessageForCollectionViewBackgroundforCase:0];
            }
            break ;
        }
    }
}

/**
 Add/Remove products to/from favourites on like/unlike
 
 @param noti notification object which have the response of added/removed product.
 */
-(void)maintainFavoritesFromNotification:(NSNotification *)noti
{
    NSMutableArray *newFavouritedata = (NSMutableArray *)noti.object[@"postdetails"];
    NSString *needtoaddAsFavourite = flStrForObj(noti.object[@"notificationForLike"]);
    if ([needtoaddAsFavourite isEqualToString:@"1"]) {
        //add post to favourites
        self.favouritesCollectionView.backgroundView = nil;
        if(!favouritePostData) {
            favouritePostData  = [[NSMutableArray alloc] init];
        }
        [favouritePostData insertObject:newFavouritedata atIndex:0];
    }
    else
    {
        //if  post is exist then remove from favourites
        NSString *updatepostId = flStrForObj(noti.object[@"postdetails"][@"postId"]);
        for (int i=0; i <favouritePostData.count;i++) {
            if ([flStrForObj(favouritePostData[i][@"postId"]) isEqualToString:updatepostId])
            {
                    [self.favouritesCollectionView performBatchUpdates:^{
                         [favouritePostData removeObjectAtIndex:i];
                        NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
                        [self.favouritesCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
                    } completion:^(BOOL finished) {
                    }];

                
                break;
            }
        }
        
        if(!favouritePostData.count){
            [self showingMessageForCollectionViewBackgroundforCase:2];
        }
    }
    [self.favouritesCollectionView reloadData];
    [self calculateCollectionViewHeightForTag:2];
    
}


/**
 Add product to Sold data and remove from selling on uploading any product.
 
 @param noti notification object which have the details of sold product.
 */
-(void)changeSellingToSoldNotification:(NSNotification *)noti
{
    NSString *updatepostId = flStrForObj(noti.object[@"data"][@"postId"]);
    for (int i=0; i < sellingPostData.count;i++) {
        if ([flStrForObj(sellingPostData [i][@"postId"]) isEqualToString:updatepostId])
        {
                [sellingPostData removeObjectAtIndex:i];
            
                [self.sellingCollectionView reloadData];
            
            [soldPostData insertObject:noti.object[@"data"] atIndex:0];
            
            if(!sellingPostData.count)
            {   [sellingPostData removeAllObjects];
                [self showingMessageForCollectionViewBackgroundforCase:0];
                [self.sellingCollectionView reloadData];
            }
            break ;
        }
    }
    
    
    
}



/**
 Add selling product on uploading any new product.
 
 @param noti notification object whaich have the response of added product.
 */
-(void)maintainSellingDataFromNotification:(NSNotification *)noti
{
    NSMutableArray *newSellingData = (NSMutableArray *)noti.object[0];
    
    if(!sellingPostData) {
        sellingPostData  = [[NSMutableArray alloc] init];
    }
    [sellingPostData insertObject:newSellingData atIndex:0];
    
    
    if(self.sellingButtonOutlet.selected || self.stickySellingButtonOutlet)
    {
        if(!(sellingPostData.count > 0)){[self showingMessageForCollectionViewBackgroundforCase:0]; }
        
        else {self.sellingCollectionView.backgroundView = nil;}
        
        [self.sellingCollectionView reloadData];
        [self calculateCollectionViewHeightForTag:0];
    }
}



/**
 Selling sold product again.
 */
-(void)sellingAgainPost :(NSDictionary *)postDetails
{
    NSString *updatepostId = flStrForObj(postDetails[@"data"][@"postId"]);
    for (int i=0; i < soldPostData.count;i++) {
        if ([flStrForObj(soldPostData [i][@"postId"]) isEqualToString:updatepostId])
        {
            [self.soldCollectionView performBatchUpdates:^{
                 [soldPostData removeObjectAtIndex:i];
                NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
                [self.soldCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            } completion:^(BOOL finished) {
            }];
            [sellingPostData insertObject:postDetails[@"data"] atIndex:0];
            
            break ;
        }
    }
    
    if(!soldPostData.count)
    {
        [self showingMessageForCollectionViewBackgroundforCase:1];
    }
    
}

/**
 Update Post Data on editing Notification Method.
 
 @param noti notification object.
 */
-(void)updatePostDetailsOnEditing:(NSNotification *)noti
{
    NSString *updatepostId = flStrForObj(noti.object[@"data"][0][@"postId"]);
    for (int i=0; i < sellingPostData.count;i++) {
        if ([flStrForObj(sellingPostData [i][@"postId"]) isEqualToString:updatepostId])
        {
            [sellingPostData replaceObjectAtIndex:i withObject:noti.object[@"data"][0]];
            [self.sellingCollectionView performBatchUpdates:^{
                NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
                [self.sellingCollectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            } completion:^(BOOL finished) {
            }];
            break ;
        }
    }
    
}


-(void)savePayPalLink:(NSNotification *)noti
{
    [self PaypalLinkIsSaved:noti.object];
}


-(void)sendNewFollowStatusThroughNotification:(NSString *)userName andNewStatus:(NSString *)newFollowStatus {
    
    
    
    NSDictionary *newFollowDict = @{@"newFollowStatus"     :flStrForObj(newFollowStatus),
                                    @"userName"            :flStrForObj(userName),
                                    };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedFollowStatus" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"newUpdatedFollowData"]];
}



/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Notification Observers
/*-----------------------------------------------*/



-(void)notiFicationForUpdatingStatus {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProfilePicUrl:) name:@"updateProfilePic" object:nil];
    if(self.checkingFriendsProfile) {
        //for others profile
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollwoStatus:) name:@"updatedFollowStatus" object:nil];
    }
    else {
        //for own profile
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostFromNotification:) name:mDeletePostNotifiName object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(maintainFavoritesFromNotification:)name:mfavoritePostNotifiName object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(maintainSellingDataFromNotification:)name:mSellingPostNotifiName object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeSellingToSoldNotification:) name:mSellingToSoldNotification object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updatePostDetailsOnEditing:) name:mUpdatePostDataNotification object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(savePayPalLink:) name:mSavePayPalNotify object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshData:) name:mUpdatePromotedPost object:nil];
    }
}


-(void)gettingDetailsOfUser {
    if(self.checkingFriendsProfile) {
        //  self.navigationItem.title
        USERNAME = self.checkProfileOfUserNmae;
    } else {
        // self.navigationItem.title = USERNAME;
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      };
        [WebServiceHandler getUserProfileDetails:requestDict andDelegate:self];
    }
}


/*-----------------------------------------------*/
#pragma mark -
#pragma mark - creatingNavBarButtons.
/*-----------------------------------------------*/

- (void)createNavSettingButton {
    navNextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    if (checkingOwnProfile) {
        
        if(_checkingFriendsProfile)
        {
            
            navNextButton.hidden = YES;
            self.editButtonOutlet.hidden = YES ;
            self.navigationItem.rightBarButtonItem = nil ;
        }
        else{
            // it will show settings button.
            NSString *settingImageName;
            
            if (ratio>1)
            {
                settingImageName = mSettingsON;
            }
            else
            {
                settingImageName = mSettingsOFF;
            }
            [navNextButton setImage:[UIImage imageNamed:settingImageName]forState:UIControlStateNormal];
            
            [navNextButton setImage:[UIImage imageNamed:settingImageName]forState:UIControlStateSelected];
            
            [navNextButton addTarget:self action:@selector(SettingButtonAction:)forControlEvents:UIControlEventTouchUpInside];
            
            [navNextButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
            
            [navNextButton setFrame:CGRectMake(0,0,40,40)];
            
            UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
            UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                               initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                               target:nil action:nil];
            negativeSpacer.width = -14;// it was -6 in iOS 6
            [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton,self.editBarButtonItem, nil] animated:NO];
        }
        
        
    }
    else
    {
        // it will show settings button.
        [navNextButton setImage:[UIImage imageNamed:mReport_Icon]forState:UIControlStateNormal];
        
        [navNextButton setImage:[UIImage imageNamed:mReport_Icon]forState:UIControlStateSelected];
        
        [navNextButton addTarget:self action:@selector(reportUserAction:)forControlEvents:UIControlEventTouchUpInside];
        
        [navNextButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        
        [navNextButton setFrame:CGRectMake(0,0,40,40)];
        
        UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -14;// it was -6 in iOS 6
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton,nil] animated:NO];
    }
}

-(void)addingRefreshControl {
    refreshControl = [[UIRefreshControl alloc]init];
    [self.scrollView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
}

-(void)refreshData:(id)sender {
    
    refresh = YES;
    [self GetProfileDetails];
    if(self.sellingButtonOutlet.selected )
    {   self.sellingCurrentIndex = 0 ;
        checkingOwnProfile = NO ;
        [self requestForPostsWithIndex:self.sellingCurrentIndex withSoldType:@"0"];
    }
    else if (self.soldButtonOutlet.selected )
    {   self.soldCurrentIndex = 0 ;
        checkingOwnProfile = NO ;
        soldPostServiceCall = YES ;
        [self requestForPostsWithIndex:self.soldCurrentIndex withSoldType:@"1"];
    }
    else if(self.favoriteButtonOutlet.selected){
        self.favouritesCurrentIndex = 0 ;
        [self requestForFavouritesWithIndex:self.favouritesCurrentIndex];
    }
    
    refresh = NO;
}


//method for creating activityview in  navigation bar right.
- (void)createActivityViewInNavbar {
    av = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [av setFrame:CGRectMake(0,0,50,30)];
    [self.view addSubview:av];
    av.tag  = 1;
    [av startAnimating];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:av];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}


- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

/*-----------------------------------------------------*/
#pragma mark -
#pragma mark - collectionView Delegates and DataSource.
/*-----------------------------------------------------*/

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (collectionView.tag) {
        case 0:
            
            if (indexPath.row +12 == sellingPostData.count && self.sellingPaging!= self.sellingCurrentIndex && self.sellingDisplayedIndex != indexPath.row) {
                self.sellingDisplayedIndex = indexPath.row ;
                 soldPostServiceCall = NO ;
                [self requestForPostsWithIndex:self.sellingCurrentIndex withSoldType:@"0"];
            }
            break;
        case 1:
            if (indexPath.row +12 == soldPostData.count && self.soldPaging!= self.soldCurrentIndex && self.soldDisplayedIndex != indexPath.row) {
                soldPostServiceCall = YES ;
                self.soldDisplayedIndex = indexPath.row ;
                [self requestForPostsWithIndex:self.soldCurrentIndex withSoldType:@"1"];

            }
            break ;
        default:
            
            if (indexPath.row +12 == favouritePostData.count && self.favouritePaging!= self.favouritesCurrentIndex && self.favouriteDisplayedIndex != indexPath.row) {
                self.favouriteDisplayedIndex = indexPath.row ;
                [self requestForFavouritesWithIndex:self.favouriteDisplayedIndex];
            }
            break;
    }
}


/**
 *  declaring numberOfSectionsInCollectionView
 *
 *  @param collectionView declaring numberOfSectionsInCollectionView in collection view.
 *
 *  @return number of sctions in collection view here it is 1.
 */

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

/**
 *  declaring numberOfItemsInSection
 *  @param collectionView declaring numberOfItemsInSection in collection view.
 *  @param section    here only one section.
 *  @return number of items in collection view here it is 100.
 */

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    switch (collectionView.tag) {
        case 0:
        {
            return sellingPostData.count ;
        }
            break;
        case 1:
        {
            return soldPostData.count ;
        }
            break;
        default:
        {
            return favouritePostData.count ;
        }
            break;
    }
}

/**
 *  implementation of collection view cell
 *  @param collectionView collectionView has only image view
 *  @return implemented cell will return.
 */

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item == 0) {
        [self calculateCollectionViewHeightForTag:collectionView.tag];
    }
    switch (collectionView.tag) {
        case 0:
        {
            collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"sellingCellIdentifier" forIndexPath:indexPath];
            [collectionViewCell setImageObjectWithData:sellingPostData andIndex:indexPath];
            return collectionViewCell;
        }
            break;
        case 1:
        {
            collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"soldCellIdentifier" forIndexPath:indexPath];
            [collectionViewCell setImageObjectWithData:soldPostData andIndex:indexPath];
            return collectionViewCell;
            
        }
            break;
        default:
        {
            collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"favCellIdentifier" forIndexPath:indexPath];
            [collectionViewCell setImageObjectWithData:favouritePostData andIndex:indexPath];
            return collectionViewCell;
            
        }
            break;
    }
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.view.frame)/3,CGRectGetWidth(self.view.frame)/3);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (collectionView.tag) {
        case 0:
        {
            if(!self.checkingFriendsProfile)
            {
                
                EditProductViewController *editVC = [self.storyboard instantiateViewControllerWithIdentifier:mEditItemStoryBoardID];
                editVC.memberPostDetails = sellingPostData[indexPath.row] ;
                editVC.hidesBottomBarWhenPushed = YES;
                editVC.showInsights = YES ;
                editVC.isPromoted =  [flStrForObj(sellingPostData[indexPath.row][@"isPromoted"])boolValue] ;
                [self.navigationController pushViewController:editVC animated:YES];
                
            }
            else
            {
                {
                    UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.sellingCollectionView cellForItemAtIndexPath:indexPath];
                    
                    UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
                    
                    ProductDetailsViewController *newView = [story instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
                    newView.imageFromHome  = cell.postedImagesOutlet.image ;
                    newView.postId = flStrForObj(sellingPostData[indexPath.row][@"postId"]);
                    newView.movetoRowNumber =indexPath.item;
                    newView.dataFromHomeScreen = YES;
                    newView.hidesBottomBarWhenPushed = YES ;
                    newView.product = [[ProductDetails alloc]initWithDictionary:sellingPostData[indexPath.row]];
                    newView.indexPath = indexPath ;
                    [self.navigationController pushViewController:newView animated:YES];
                }
                
            }
        }
            break;
        case 1:
        {
            if(!self.checkingFriendsProfile)
            {
                UIAlertController *alertController  = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(sellProductAgain, sellProductAgain) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *yes = [UIAlertAction actionWithTitle:NSLocalizedString(alertYes, alertYes) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    NSDictionary *requestDic = @{ mpostid : flStrForObj(soldPostData[indexPath.row][@"postId"]),
                                                  mtype : @"0",
                                                  mauthToken : [Helper userToken]
                                                  };
                    [ WebServiceHandler markAsSelling:requestDic andDelegate:self];
                    
                }];
                UIAlertAction *no = [UIAlertAction actionWithTitle:NSLocalizedString(alertNo, alertNo) style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:yes];
                [alertController addAction:no];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
            break;
        default:
        {
            {
                if ([USERNAME isEqualToString:mGuestToken] || self.favoriteButtonOutlet.selected ||_ProductDetails){
                    UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.favouritesCollectionView cellForItemAtIndexPath:indexPath];
                    
                UIStoryboard *story = [UIStoryboard storyboardWithName:mMainStoryBoard bundle:nil];
                    
                    ProductDetailsViewController *newView = [story instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
                    newView.imageFromHome  = cell.postedImagesOutlet.image ;
                    newView.hidesBottomBarWhenPushed = YES;
                    newView.postId = flStrForObj(favouritePostData[indexPath.row][@"postId"]);
                    newView.movetoRowNumber =indexPath.item;
                    newView.dataFromHomeScreen = YES;
                    newView.product = [[ProductDetails alloc]initWithDictionary:favouritePostData[indexPath.row]];
                    [self.navigationController pushViewController:newView animated:YES];
                }
            }
        }
            break;
    }
    
}

#pragma mark –
#pragma mark – RFQuiltLayoutDelegate

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger width,width2;
    NSInteger height,height2;
    
    switch (collectionView.tag) {
        case 0:
        {
            if(self.view.frame.size.width == 375){
                width = 5;
                height2 = [flStrForObj(sellingPostData[indexPath.row][@"containerHeight"]) integerValue]/30;
                width2 =  [flStrForObj(sellingPostData[indexPath.row][@"containerWidth"]) integerValue]/37;
                
                if (width2 == 0)
                    width2 = 10;
                
                if (height2 == 0)
                    height2 = 10;
                    
                height = (width * height2)/width2;
            }
            else{
                width = 2;
                height2 = [flStrForObj(sellingPostData[indexPath.row][@"containerHeight"]) integerValue]/30;
                if(self.view.frame.size.width == 414)
                {
                    width2 =  [flStrForObj(sellingPostData[indexPath.row][@"containerWidth"]) integerValue]/102;
                }
                else
                {
                    width2 =  [flStrForObj(sellingPostData[indexPath.row][@"containerWidth"]) integerValue]/79;
                }

                if (width2 == 0)
                    width2 = 10;
                
                if (height2 == 0)
                    height2 = 10;
                
                height = (width * height2)/width2;
            }
            return CGSizeMake(width,height);
        }
            break;
        case 1:
        {
            if(self.view.frame.size.width == 375){
                width = 5;
                height2 = [flStrForObj(soldPostData[indexPath.row][@"containerHeight"]) integerValue]/30;
                width2 =  [flStrForObj(soldPostData[indexPath.row][@"containerWidth"]) integerValue]/37;
                height = (width * height2)/width2;
            }
            else{
                width = 2;
                height2 = [flStrForObj(soldPostData[indexPath.row][@"containerHeight"]) integerValue]/30;
                if(self.view.frame.size.width == 414)
                {
                    width2 =  [flStrForObj(soldPostData[indexPath.row][@"containerWidth"]) integerValue]/102;
                }
                else
                {
                    width2 =  [flStrForObj(soldPostData[indexPath.row][@"containerWidth"]) integerValue]/79;
                }
                
                if (width2 == 0)
                    width2 = 10;
                
                if (height2 == 0)
                    height2 = 10;

                height = (width * height2)/width2;
            }
            return CGSizeMake(width,height);
            
        }
            break;
        default:
        {
            if(self.view.frame.size.width == 375){
                width = 5;
                height2 = [flStrForObj(favouritePostData[indexPath.row][@"containerHeight"]) integerValue]/30;
                width2 =  [flStrForObj(favouritePostData[indexPath.row][@"containerWidth"]) integerValue]/37;
                
                if (width2 == 0)
                    width2 = 10;
                
                if (height2 == 0)
                    height2 = 10;
                
                height = (width * height2)/width2;
            }
            else{
                width = 2;
                height2 = [flStrForObj(favouritePostData[indexPath.row][@"containerHeight"]) integerValue]/30;
                if(self.view.frame.size.width == 414)
                {
                    width2 =  [flStrForObj(favouritePostData[indexPath.row][@"containerWidth"]) integerValue]/102;
                }
                else
                {
                    width2 =  [flStrForObj(favouritePostData[indexPath.row][@"containerWidth"]) integerValue]/79;
                }
                
                if (width2 == 0)
                    width2 = 10;
                
                if (height2 == 0)
                    height2 = 10;

                height = (width * height2)/width2;
            }
            return CGSizeMake(width,height);
        }
            break;
    }
    
    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,0,0);
}


/*-----------------------------------------------------------------------------*/
#pragma
#pragma mark - REQUESTING SERVICES .
/*------------------------------------------------------------------------------*/

-(void)GetProfileDetails
{
    if (self.checkingFriendsProfile && self.checkProfileOfUserNmae) {
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      mmemberName : self.checkProfileOfUserNmae
                                      };
        if([[Helper userToken]isEqualToString:mGuestToken])
        {
            [WebServiceHandler getProfileDetailsOfMemberForGuest:requestDict andDelegate:self];
        }
        else
            
            [WebServiceHandler getProfileDetailsOfMember :requestDict andDelegate:self];
    }
    
    else
    {
        
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      };
        
        [WebServiceHandler getProfileDetailsOfUser :requestDict andDelegate:self];
    }
}



/**
 Request Selling Posts
 
 */
-(void)requestForPostsWithIndex:(NSInteger )offestIndex withSoldType :(NSString *)sold{
    
    if (self.checkingFriendsProfile && self.checkProfileOfUserNmae) {
        self.navigationController.navigationItem.hidesBackButton =  YES;
        // Requesting For member posts Api.(to get the details of another user)(passing "token" and respective memberusername as parameter)
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      mmemberName :self.checkProfileOfUserNmae,
                                      mlimit:mLimitValue,
                                      moffset: [NSNumber numberWithInteger:20 *offestIndex],
                                      @"sold" : sold
                                      };
        if([[Helper userToken]isEqualToString:mGuestToken])
        {
            [WebServiceHandler getMemberPostsForGuest:requestDict andDelegate:self];
        }
        else
        {
            [WebServiceHandler getMemberPosts:requestDict andDelegate:self];
        }
        if ([self.checkProfileOfUserNmae isEqualToString:[Helper userName]])
        {
            checkingOwnProfile =  YES;
        }
        else
        {
            checkingOwnProfile =  NO;
        }
    }
    else {
        
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      moffset : [NSNumber numberWithInteger:20 *offestIndex],
                                      mlimit : mLimitValue,
                                      @"sold" : sold
                                      };
        
        [WebServiceHandler getUserPosts:requestDict andDelegate:self];
        checkingOwnProfile = YES;
        
    }
}

/**
 Request Favourite Posts
 
 */
-(void)requestForFavouritesWithIndex:(NSInteger )offsetIndex
{
    NSDictionary *requestDict = @{
                                  mauthToken :flStrForObj([Helper userToken]),
                                  mmemberName :USERNAME,
                                  mlimit:mLimitValue,
                                  moffset:[NSNumber numberWithInteger:20 *offsetIndex]
                                  };
    [WebServiceHandler RequestTypePostsLikedByUser:requestDict andDelegate:self];
}



-(void)requestForAcceptFollowOrDeny:(NSString *)followAction {
    
    //   action  ----> 1 : accept]
    
    NSDictionary *requestDict = @{
                                  mauthToken :flStrForObj([Helper userToken]),
                                  mmembername:_checkProfileOfUserNmae,
                                  mfollowAction:followAction
                                  };
    [WebServiceHandler accceptFollowRequest:requestDict andDelegate:self];
    
}

/*---------------------------------------*/
#pragma
#pragma mark - Button Actions
/*---------------------------------------*/

-(void)handlingWebUrlFromBio{
    self.biodataLabelOutlet.userInteractionEnabled = YES;
    self.biodataLabelOutlet.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        [self openWebPage:string];
    };
}

-(void)openProfileOfUsername:(NSString *)selectedUserName {
    UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
    newView.checkingFriendsProfile = YES;
    newView.checkProfileOfUserNmae = selectedUserName;
    [self.navigationController pushViewController:newView animated:YES];
}



-(void)showingProgressindicator {
    //showing progress indicator and requesting for posts.
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];  [HomePI showPIOnView:self.view withMessage:@"Deleting.."];
}





- (IBAction)sellingButtonAction:(id)sender {
    
    if(!_sellingButtonOutlet.selected){
    
        [self.sellingButtonOutlet.layer addSublayer:selectedTabBottomLine];
        [self.stickySellingButtonOutlet.layer addSublayer:selectedStickyTabBottomLine];
        
        CGRect frame = self.slidingScrollView.bounds;
        frame.origin.x = 0;
        [self.slidingScrollView scrollRectToVisible:frame animated:YES];
        
        self.soldButtonOutlet.selected = self.stickySoldButtonOutlet.selected = self.favoriteButtonOutlet.selected  =
        self.stickyFavouriteButtonOutlet.selected = NO;
        self.sellingButtonOutlet.selected = self.stickySellingButtonOutlet.selected = YES;
        self.movingDividerLeadingConstraint.constant = self.stickyDividerLeadingConstraint.constant = 5;
        if(sellingPostData.count>0)
        {
            self.sellingCollectionView.backgroundView = nil;
        }
        else
        {
            [self showingMessageForCollectionViewBackgroundforCase:0];
        }
        [self.sellingCollectionView reloadData];
    }
    
    [self calculateCollectionViewHeightForTag:0];
    
}


- (IBAction)soldButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:mGuestToken])
    {
        [self presentLoginForGuestUsers];
    }

    else if (!self.soldButtonOutlet.selected && !self.stickySoldButtonOutlet.selected){
        
        [self.soldButtonOutlet.layer addSublayer:selectedTabBottomLine];
        [self.stickySoldButtonOutlet.layer addSublayer:selectedStickyTabBottomLine];
        
        CGRect frame = self.slidingScrollView.bounds;
        frame.origin.x = CGRectGetWidth(self.view.frame);
        [self.slidingScrollView scrollRectToVisible:frame animated:YES];
        
            self.sellingButtonOutlet.selected = self.stickySellingButtonOutlet.selected = self.favoriteButtonOutlet.selected = self.stickyFavouriteButtonOutlet.selected = NO;
            self.soldButtonOutlet.selected = self.stickySoldButtonOutlet.selected = YES;
            self.movingDividerLeadingConstraint.constant = self.stickyDividerLeadingConstraint.constant = 5+ self.sellingButtonOutlet.frame.size.width;
            if(soldPostData.count>0)
            {
                self.soldCollectionView.backgroundView = nil;
            }
            else
            {
                if(!refresh){
                    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
                    [HomePI showPIOnView:self.view withMessage:@"Loading..."];
                }

                [self showingMessageForCollectionViewBackgroundforCase:1];
                soldPostServiceCall = YES;
                self.soldCurrentIndex = 0;
                [self requestForPostsWithIndex:self.soldCurrentIndex withSoldType:@"1"];
            }
        
        [self.soldCollectionView reloadData];
    }
    
    [self calculateCollectionViewHeightForTag:1];
}

- (IBAction)favoriteButtonAction:(id)sender {
    
    if(!self.favoriteButtonOutlet.selected && !self.stickyFavouriteButtonOutlet.selected)
    {
        [self.favoriteButtonOutlet.layer addSublayer:selectedTabBottomLine];
        [self.stickyFavouriteButtonOutlet.layer addSublayer:selectedStickyTabBottomLine];
        
        CGRect frame = self.slidingScrollView.bounds;
        frame.origin.x = CGRectGetWidth(self.view.frame) *2;
        [self.slidingScrollView scrollRectToVisible:frame animated:YES];
        self.sellingButtonOutlet.selected = self.stickySellingButtonOutlet.selected = self.soldButtonOutlet.selected = self.stickySoldButtonOutlet.selected = NO;
        self.movingDividerLeadingConstraint.constant = self.stickyDividerLeadingConstraint.constant = 5+ self.soldButtonOutlet.frame.size.width + self.sellingButtonOutlet.frame.size.width ;
        self.favoriteButtonOutlet.selected = self.stickyFavouriteButtonOutlet.selected = YES;
        if(favouritePostData.count>0)
        {
            self.favouritesCollectionView.backgroundView = nil;
        }
        else
        {
            if(!refresh){
                ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
                [HomePI showPIOnView:self.view withMessage:@"Loading..."];
            }

            self.favouritesCurrentIndex = 0;
            [self requestForFavouritesWithIndex:self.favouritesCurrentIndex];
        }
        
        [self.favouritesCollectionView reloadData];
    }
    
    [self calculateCollectionViewHeightForTag:2];
    
}



- (IBAction)postsButtonAction:(id)sender {
    //    if (self.collectionViewHeight.constant > 100)
    //        [self.scrollView setContentOffset:CGPointMake(0, 50 ) animated:YES];
    //    [self sellingButtonAction:self];
}

- (IBAction)followingButtonAction:(id)sender {
    if([[Helper userToken] isEqualToString:mGuestToken])
        [self presentLoginForGuestUsers];
    else{
        if (self.checkProfileOfUserNmae) {
            //Opening LikeViewController With navigation title as FOLLOWERS.
            LikeViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mlikeStoryBoardId];
            newView.navigationTitle = NSLocalizedString(navTitleForFollowing, navTitleForFollowing);
            newView.getdetailsDetailsOfUserName = self.checkProfileOfUserNmae;
            [self.navigationController pushViewController:newView animated:YES];    }
        else {
            //Opening LikeViewController With navigation title as FOLLOWING.
            LikeViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mlikeStoryBoardId];
             newView.navigationTitle = NSLocalizedString(navTitleForFollowing, navTitleForFollowing);
            [self.navigationController pushViewController:newView animated:YES];
        }
    }
}

- (IBAction)followersButtonAction:(id)sender {
    if([[Helper userToken] isEqualToString:mGuestToken])
        [self presentLoginForGuestUsers];
    
    else{
        if (self.checkProfileOfUserNmae) {
            //Opening LikeViewController With navigation title as FOLLOWERS.
            LikeViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mlikeStoryBoardId];
            newView.navigationTitle = NSLocalizedString(navTitleForFollowers, navTitleForFollowers);
            newView.getdetailsDetailsOfUserName = self.checkProfileOfUserNmae;
            [self.navigationController pushViewController:newView animated:YES];
        }
        else {
            //Opening LikeViewController With navigation title as FOLLOWERS.
            LikeViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mlikeStoryBoardId];
            newView.navigationTitle = NSLocalizedString(navTitleForFollowers, navTitleForFollowers);
            [self.navigationController pushViewController:newView animated:YES];
        }
    }
}


/**
 Edit Profile Button and Follow/Following Button for other users.
 
 @param sender edit button outlet.
 */
- (IBAction)editProfileButtonAction:(id)sender {
    if([[Helper userToken] isEqualToString:mGuestToken])
        [self presentLoginForGuestUsers];
    else{
        //if user checking his own profile then we need to show edit profile button otherwise follow/following relation.
        [self performSegueWithIdentifier:@"editProfileSegue" sender:nil];
    }
}

/**
 Action on clicking conntact button.
 
 @param sender self
 */
- (IBAction)contactButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:mGuestToken])
        [self presentLoginForGuestUsers];
    else{
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        //    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Get Direction" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        //        [self getDirection];
        //    }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(actionsheetTitleForCall, actionsheetTitleForCall) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self callNumber:self.phoneNumberOfMember];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(actionsheetEmailOptionTitle, actionsheetEmailOptionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self showMailPanel];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // OK button tapped.
            
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
    
}


/**
 Method to call on registered contact of User.
 */
-(void)callNumber :(NSString *)num
{
    
    NSString *callThisNumber = [@"tel://" stringByAppendingString:num];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callThisNumber]];
}


/**
 This method get invoked on choosing email option in contact.
 */
-(void)showMailPanel{
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
        [mailCont setSubject:NSLocalizedString(emailSubject, emailSubject)];
        [mailCont setToRecipients:[NSArray arrayWithObject:flStrForObj(self.emailOfMember)]];
        [mailCont setMessageBody:@" " isHTML:NO];
        mailCont.navigationBar.tintColor = [UIColor blackColor];
        [mailCont.navigationBar setBackgroundColor:[UIColor whiteColor]];
        mailCont.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:mBaseColor};
        [self presentViewController:mailCont animated:YES completion:^{
        }];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}



- (void)SettingButtonAction:(UIButton *)sender {
    navNextButton.enabled  = NO;
    [self performSegueWithIdentifier:@"settingButtonToOptionsSegue" sender:nil];
}

-(void)reportUserAction:(UIButton *)sender
{
    
    if(![[Helper userToken] isEqualToString:mGuestToken])
    {
        ReportPostViewController *reportVc = [self.storyboard instantiateViewControllerWithIdentifier:@"reprtPostVc"];
        reportVc.fullName =  flStrForObj(profiledetailsOfUser[0][@"fullName"]);
        reportVc.user = YES;
        reportVc.userName = flStrForObj(profiledetailsOfUser[0][@"username"]);;
        reportVc.profilePicUrl = flStrForObj(profiledetailsOfUser[0][@"profilePicUrl"]);
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:reportVc];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else
    {
        if([[Helper userToken] isEqualToString:mGuestToken])
        {
            [self presentLoginForGuestUsers];
        }
    }
    
}


/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Follow Button
/*-----------------------------------------------*/

- (IBAction)followButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:@"guestUser"])
    {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
    else
    {
        
        CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [ani setDuration:0.1];
        [ani setRepeatCount:1];
        [ani setFromValue:[NSNumber numberWithFloat:1.0]];
        [ani setToValue:[NSNumber numberWithFloat:0.9]];
        [ani setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[self.followButtonOutlet layer] addAnimation:ani forKey:@"zoom"];
        
        //actions for when the account is public.
        if ([self.followButtonOutlet.titleLabel.text isEqualToString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
            [Helper showUnFollowAlert:self.profilePhotoOutlet.image and:self.userNameLabelOutlet.text viewControllerReference:self onComplition:^(BOOL isUnfollow)
             {
                 if(isUnfollow)
                 {
                     [self unfollowAction];
                 }
             }];
        }
        else {
            
            [self increaseTheFollowersCount:flStrForObj(self.numberOfFollowersString)];
            [self sendNewFollowStatusThroughNotification:flStrForObj(self.checkProfileOfUserNmae) andNewStatus:@"1"];
            
            [self.followButtonOutlet makeButtonAsFollowing];
            
            //passing parameters.
            NSDictionary *requestDict = @{muserNameTofollow     :self.checkProfileOfUserNmae,
                                          mauthToken            :flStrForObj([Helper userToken]),
                                          mdeviceToken          :[Helper deviceToken]
                                          };
            //requesting the service and passing parametrs.
            [WebServiceHandler follow:requestDict andDelegate:self];
        }
    }
}

/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Increase Follow Count
/*-----------------------------------------------*/

/**
 This method will increase Follow count on following.
 
 @param followersCount Follower count in string format.
 */
-(void)increaseTheFollowersCount:(NSString *)followersCount {
    NSInteger numberOfFollowers = [followersCount integerValue];
    numberOfFollowers ++;
    if (numberOfFollowers < 0 || numberOfFollowers ==0) {
        self.numberOfFollowersString = @"0";
    }
    else {
        self.numberOfFollowersString = flStrForInt(numberOfFollowers);
    }
    
    [self updateFollowerButtonTitle];
}

/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Decrease Follow Count
/*-----------------------------------------------*/

/**
 This method will decrease Follow count on unfollowing.
 
 @param followersCount Follower count in string format.
 */

-(void)decreaseFollowersCount:(NSString *)followersCount {
    NSInteger numberOfFollowers = [followersCount integerValue];
    
    numberOfFollowers --;
    
    if (numberOfFollowers < 0 || numberOfFollowers ==0) {
        self.numberOfFollowersString = @"0";
        //        self.followersButtonOutlet.enabled = NO;
    }
    else {
        self.numberOfFollowersString = flStrForInt(numberOfFollowers);
        //        self.followersButtonOutlet.enabled = YES;
    }
    
    [self updateFollowerButtonTitle];
}


/*------------------------------------*/
#pragma mark
#pragma mark - Update Profile Pic
/*------------------------------------*/

-(void)updateProfilePicUrl:(NSNotification *)noti {
    
    UserDetails *user = noti.object ;
    _webSiteUrlLabelOutlet.text = user.website ;
    _biodataLabelOutlet.text = user.bio ;
    _userNameLabelOutlet.text = user.fullName ;
    USERNAME= user.username ;
    ProfilePicUrl = user.profilePicUrl ;
    
    if ([ProfilePicUrl isEqualToString:@"defaultUrl"]) {
        _profilePhotoOutlet.image = [UIImage imageNamed:@"defaultpp"];
        self.topImageView.image = [UIImage imageNamed:@"profile_default bg"];
        self.topImageView.alpha = 0.7;
    }
    else {
        [self.profilePhotoOutlet setImageWithURL:[NSURL URLWithString:ProfilePicUrl] placeholderImage:[UIImage imageNamed:@"defaultpp"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.topImageView sd_setImageWithURL:[NSURL URLWithString:ProfilePicUrl]];
        
        self.topImageView.alpha = 0.5;
    }
    
}


/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Update Follow Status
/*-----------------------------------------------*/

-(void)updateFollwoStatus:(NSNotification *)noti {
    //check the postId and Its Index In array.
    NSString *userName = flStrForObj(noti.object[@"newUpdatedFollowData"][@"userName"]);
    NSString *foolowStatusRespectToUser = noti.object[@"newUpdatedFollowData"][@"newFollowStatus"];
    if ([  USERNAME/*self.navigationItem.title*/ isEqualToString:userName]) {
        if ([foolowStatusRespectToUser isEqualToString:@"1"]) {
            [self.followButtonOutlet makeButtonAsFollowing];        }
        else
        {
            [self.followButtonOutlet makeButtonAsFollow];
        }
    }
}

/*------------------------------------*/
#pragma mark
#pragma mark - navigation bar back button
/*------------------------------------*/

- (void)createNavLeftButton {
    
    navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    navCancelButton.layer.shadowOffset = CGSizeMake(0, -2);
    navCancelButton.layer.shadowRadius = 2;
    navCancelButton.layer.shadowOpacity = 0.2;
    [navCancelButton rotateButton];
    //checking if user checking his own profile or not.
    if (!self.checkingFriendsProfile) {
        //For DiscoverPeople
        [navCancelButton setImage:[UIImage imageNamed:mDiscoverPeopleImage]
                         forState:UIControlStateNormal];
        [navCancelButton setImage:[UIImage imageNamed:mDiscoverPeopleImage]
                         forState:UIControlStateSelected];
        [navCancelButton addTarget:self
                            action:@selector(discoverPeopleButtonAction)
                  forControlEvents:UIControlEventTouchUpInside];
        [navCancelButton setFrame:CGRectMake(0,0,40,40)];
        UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -14;// it was -6 in iOS 6
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
        
    }
    else {
        [navCancelButton setImage:[UIImage imageNamed:@"item backButton"]
                         forState:UIControlStateNormal];
        [navCancelButton setImage:[UIImage imageNamed:@"item backButton"]
                         forState:UIControlStateSelected];
        [navCancelButton addTarget:self
                            action:@selector(backButtonClicked)
                  forControlEvents:UIControlEventTouchUpInside];
        [navCancelButton setFrame:CGRectMake(0,0,40,40)];
        UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -14;// it was -6 in iOS 6
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    }
}

-(void)discoverPeopleButtonAction {
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:mHomeStoryBoard bundle:nil];
    
    PGDiscoverPeopleViewController *newView = [story instantiateViewControllerWithIdentifier:@"discoverPeopleStoryBoardId"];
    [self.navigationController pushViewController:newView animated:YES];
}

-(void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

/*------------------------------------*/
#pragma mark
#pragma mark - Update Member Details
/*------------------------------------*/

-(void) updateMemberDetails:(NSArray *)profiledetails {
    
    [profiledetailsOfUser addObjectsFromArray:profiledetails] ;
    ProfilePicUrl = flStrForObj(profiledetails[0][@"profilePicUrl"]);
    
    if(!USERNAME)
        USERNAME = flStrForObj(profiledetails[0][@"username"]);
    
    NSString *userfollowingstatus =  [NSString stringWithFormat:@"%@",profiledetails[0][@"followStatus"]];
    if (checkingOwnProfile)     {
        self.viewForContactAndFollow.hidden = YES;
        self.heightConstraintForContactView.constant = 30;
        self.heightConstraintForPostView.constant = -30;
    }
    else {
        self.viewForContactAndFollow.hidden = NO;
        self.heightConstraintForContactView.constant = 30;
        if ([userfollowingstatus isEqualToString:@"1"]) {
            [self.followButtonOutlet makeButtonAsFollowing];
        }
        else
        {
            [self.followButtonOutlet makeButtonAsFollow];
        }
    }
    _webSiteUrlLabelOutlet.text = flStrForObj(profiledetails[0][@"website"]);
    _biodataLabelOutlet.text = flStrForObj(profiledetails[0][@"bio"]);
    _userNameLabelOutlet.text = flStrForObj(profiledetails[0][@"fullName"]);
    self.phoneNumberOfMember = [[NSMutableString alloc]initWithString:flStrForObj(profiledetails[0][@"phoneNumber"])];
    self.emailOfMember = flStrForObj(profiledetails[0][@"email"]);
    
    if(_checkingFriendsProfile){
        
        self.numberOfPostsString = [NSString stringWithFormat:@"%@",flStrForObj( profiledetails[0][@"posts"])];
        [self updatePostButtonTitle];
        
        self.numberOfFollowersString = [NSString stringWithFormat:@"%@",flStrForObj( profiledetails[0][@"followers"])];
        [self updateFollowerButtonTitle];
        
        self.numberOfFollowingString = [NSString stringWithFormat:@"%@",flStrForObj( profiledetails[0][@"following"])];
        [self updateFollowingButtonTitle];
    }
    
    if ([ProfilePicUrl isEqualToString:@"defaultUrl"]|| !ProfilePicUrl.length) {
        _profilePhotoOutlet.image = [UIImage imageNamed:@"defaultpp"];
        self.topImageView.image = [UIImage imageNamed:@"profile_default bg"];
        self.topImageView.alpha = 0.7;
    }
    else {
        [self.profilePhotoOutlet setImageWithURL:[NSURL URLWithString:ProfilePicUrl] placeholderImage:[UIImage imageNamed:@"defaultpp"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.topImageView sd_setImageWithURL:[NSURL URLWithString:ProfilePicUrl]];
        self.topImageView.alpha = 0.5;
        
    }
    NSString *facebookVerified = [NSString stringWithFormat:@"%@",profiledetails[0][@"facebookVerified"]];
    NSString *googleVerified = [NSString stringWithFormat:@"%@",profiledetails[0][@"googleVerified"]];
    NSString *emailVerified = [NSString stringWithFormat:@"%@",profiledetails[0][@"emailVerified"]];
    paypalUrl = [NSString stringWithFormat:@"%@",flStrForObj(profiledetails[0][@"paypalUrl"])];
    [[NSUserDefaults standardUserDefaults]setObject:paypalUrl forKey:payPalLink];
    self.verifiedByView.hidden = NO ;
    
    [self checkVerificationForCase:0 andState:facebookVerified];
    [self checkVerificationForCase:1 andState:googleVerified];
    [self checkVerificationForCase:2 andState:emailVerified];
    [self checkVerificationForCase:3 andState:paypalUrl];
    
    // [self changeTheTopcontentViewHeight];
}

-(void)checkVerificationForCase:(NSInteger )caseNum andState:(NSString *)state
{
    switch(caseNum)
    {
        case 0:{
            if([state containsString:@"0"])
            {
                if(checkingOwnProfile){self.facebookButtonOutlet.enabled = YES; }
                
                else {self.facebookButtonOutlet.enabled = NO; }
                self.facebookVerifyOutlet.image = [UIImage imageNamed:@"facebook unverified"];
            }
            else
            {   self.facebookButtonOutlet.enabled = NO;
                self.facebookVerifyOutlet.image = [UIImage imageNamed:@"facebook verified"];
            }
            
        }
            break;
        case 1:{
            if([state containsString:@"0"])
            {
                if(checkingOwnProfile){self.googlePlusButtonOutlet.enabled = YES; }
                else {self.googlePlusButtonOutlet.enabled = NO; }
                self.googleVerifyOutlet.image = [UIImage imageNamed:@"g+ unverified"];
            }
            else
            {
                self.googlePlusButtonOutlet.enabled = NO;
                self.googleVerifyOutlet.image = [UIImage imageNamed:@"g+ verified"];
            }
        }
            break;
        case 2:{
            if([state containsString:@"0"])
            {
                if(checkingOwnProfile){self.emailButtonOutLet.enabled = YES; }
                else {self.emailButtonOutLet.enabled = NO; }
                self.emailVerifyOutlet.image = [UIImage imageNamed:@"email unverified"];
            }
            else
            {
                self.emailButtonOutLet.enabled = NO;
                self.emailVerifyOutlet.image = [UIImage imageNamed:@"email verified"];
            }
            
        }
            break;
        case 3:{
            if(state.length < 2)
            {
                if(checkingOwnProfile)
                {
                    self.paypalOutlet.enabled = YES;
                }
                
                else
                {
                    self.paypalOutlet.enabled = NO;
                }
                
                [self.paypalOutlet setImage:[UIImage imageNamed:@"paypal unverified"] forState:UIControlStateNormal];
                self.paypalOutlet.layer.borderWidth = 0.5;
                self.paypalOutlet.layer.borderColor = [UIColor lightGrayColor].CGColor;
                [self.paypalOutlet setTitle:@"Paypal.Me" forState:UIControlStateNormal];
                
            }
            else
            {
                if(checkingOwnProfile)
                {
                    self.paypalOutlet.enabled = YES;
                }
                else
                {   self.paypalOutlet.hidden = YES ;
                    self.paypalOutlet.enabled = NO;
                    self.paypalVerifyImageOutlet.hidden = NO;
                    self.paypalVerifyImageOutlet.image = [UIImage imageNamed:@"paypal verified"];
                }
                
                [self.paypalOutlet setImage:[UIImage imageNamed:@"paypal verified"] forState:UIControlStateNormal];
                self.paypalOutlet.layer.borderWidth = 0;
                self.paypalOutlet.layer.borderColor = [UIColor clearColor].CGColor;
                [self.paypalOutlet setTitle:@"" forState:UIControlStateNormal];
                
            }
            
        }
            break;
    }
}

#pragma mark - PostView Buttons

- (void)updatePostButtonTitle
{
    self.postButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.postButton.titleLabel.textAlignment = NSTextAlignmentCenter;

    NSString *postString = [NSString stringWithFormat:@"%@\nPosts", self.numberOfPostsString];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:postString];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:22] range:NSMakeRange(0, self.numberOfPostsString.length)];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(self.numberOfPostsString.length, 6)];
    [attributeString addAttribute:NSForegroundColorAttributeName value:mBaseColor range:NSMakeRange(0, postString.length)];
    
    [self.postButton setAttributedTitle:attributeString forState:UIControlStateNormal];
}

- (void)updateFollowerButtonTitle
{
    self.followersButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.followersButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    NSString *followerString = [NSString stringWithFormat:@"%@\nFollowers", self.numberOfFollowersString];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:followerString];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:22] range:NSMakeRange(0, self.numberOfFollowersString.length)];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(self.numberOfFollowersString.length, 10)];
    [attributeString addAttribute:NSForegroundColorAttributeName value:mBaseColor range:NSMakeRange(0, followerString.length)];
    
    [self.followersButton setAttributedTitle:attributeString forState:UIControlStateNormal];
}

- (void)updateFollowingButtonTitle
{
    self.followingButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.followingButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    NSString *followingString = [NSString stringWithFormat:@"%@\nFollowing", self.numberOfFollowingString];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:followingString];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:22] range:NSMakeRange(0, self.numberOfFollowingString.length)];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(self.numberOfFollowingString.length, 10)];
    [attributeString addAttribute:NSForegroundColorAttributeName value:mBaseColor range:NSMakeRange(0, followingString.length)];
    
    [self.followingButton setAttributedTitle:attributeString forState:UIControlStateNormal];
}


/*-----------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*----------------------------------------------------*/
-(void)internetIsNotAvailable:(RequestType)requsetType {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [av stopAnimating];
    [refreshControl endRefreshing];
}

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [av stopAnimating];
    [refreshControl endRefreshing];
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    switch (requestType) {
        case RequestTypemakeUserProfileDetails:
        {
            
            [self updateDetailsOfUser:response];
        }
            break;
        case RequestTypeProfileDetails:
        {
            [self updateMemberDetails:response[@"data"]];
        }
            break;
            //user Selling Post/Sold Posts details request.
        case RequestTypeGetPosts:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self stopAnimation];
                    if(soldPostServiceCall)
                    {
                        soldPostServiceCall = NO;
                        [self handlingResponseForSoldPosts:response];
                    }
                    else
                    {
                        [self handlingResponseForRequestPosts:response];
                        
                    }
                }
                    break;
                case 204:{
                    [self stopAnimation];
                    if(soldPostServiceCall)
                    {
                        soldPostServiceCall = NO;
                        [self handlingResponseForSoldPosts:response];
                    }
                    else
                    {
                        [self handlingResponseForRequestPosts:response];
                    }
                    
                }
                    break;
                default :
                    break;
            }
        }
            break;
            
        case RequestTypePostsLikedByUser:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self handlingResponseForFavourites:response];
                }
                    break;
                    
                case 204:{
                    [self handlingResponseForFavourites:response];
                }
                    break;
                default:
                    break;
            }
        }
            break;
            
            
        case RequestToVerifyWithFacebook:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    self.facebookVerifyOutlet.image = [UIImage imageNamed:@"facebook verified"];
                    
                    self.facebookButtonOutlet.enabled = NO ;
                }
                    break;
                case 409 :{
                    [self ShowAlertWithMessage:NSLocalizedString(facebookAccountAlreadyLinked, facebookAccountAlreadyLinked)];
                    
                    break;
                default:
                    break;
                }
            }
        }
            break;
        case RequestToVerifyWithGooglePlus:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    
                    self.googleVerifyOutlet.image = [UIImage imageNamed:@"g+ verified"];
                    self.googlePlusButtonOutlet.enabled = NO;
                }
                    break;
                case 409 :{
                    [self ShowAlertWithMessage:NSLocalizedString(googleAccountAlreadyLinked, googleAccountAlreadyLinked)];
                    
                }
                    break;
                default:
                    break;
            }
        }
            break;
            
        case RequestToMarkAsSelling:
        {
            switch ([response[@"code"] integerValue]) {
                case 200: {
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:mSellingAgainNotification object:[NSDictionary dictionaryWithObject:response[@"data"] forKey:@"postDetails"]];
                    
                    [self sellingAgainPost:response];
                }
                    break;
                    
                default:
                    break;
            }
        }
            
            break;
        default:
            break;
            
            
        }
    
}



- (void)ShowAlertWithMessage:(NSString *)message {
    [Helper showAlertWithTitle:nil Message:message viewController:self];
}



-(void)updateDetailsOfUser:(id)response {
    //    self.navigationItem.title = flStrForObj(response[@"data"][0][@"username"]);
    
    NSString *checkNull;
    
    checkNull = flStrForObj(response[@"data"][0][@"followingCount"]);
    if(checkNull.length){ self.numberOfFollowingString = checkNull;}
    else { self.numberOfFollowingString = @"" ;}
    
    [self updateFollowingButtonTitle];
    
    checkNull = nil ;
    checkNull = flStrForObj(response[@"data"][0][@"followerCount"]);
    if(checkNull.length){ self.numberOfFollowersString = checkNull;}
    else { self.numberOfFollowersString = @"" ;}
    
    [self updateFollowerButtonTitle];
    
    checkNull = nil ;
    checkNull = self.numberOfPostsString = flStrForObj(response[@"data"][0][@"totalPosts"]);
    if(checkNull.length){ self.numberOfPostsString = checkNull;}
    else { self.numberOfPostsString = @"" ;}
    
    [self updatePostButtonTitle];
    
    NSString *emailVerified = [NSString stringWithFormat:@"%@",flStrForObj(response[@"data"][0][@"emailVerified"])];
    if (emailVerified.length > 0)
    {
    [self checkVerificationForCase:2 andState:emailVerified];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"editProfileSegue"]) {
        
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        EditProfileViewController *controller = (EditProfileViewController *)navController.topViewController;
        controller.necessarytocallEditProfile = YES;
        controller.pushingVcFrom = @"ProfileScreen";
        controller.profilepic = _profilePhotoOutlet.image;
    }
    if ([segue.identifier isEqualToString:@"settingButtonToOptionsSegue"]) {
        OptionsViewController *optionsVc  = [segue destinationViewController];
        optionsVc.token = flStrForObj([Helper userToken]);
        optionsVc.delegate=self;
    }
    

}

-(void)clearImageCache{
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearDisk];
}

/*-----------------------------------------------------*/
#pragma mark -
#pragma mark - CustomActionSheet
/*----------------------------------------------------*/

-(void)unfollowAction {
    
    if ([self.followButtonOutlet.titleLabel.text isEqualToString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
        [self decreaseFollowersCount:flStrForObj(self.numberOfFollowersString)];
    }
    [self sendNewFollowStatusThroughNotification:flStrForObj(self.checkProfileOfUserNmae) andNewStatus:@"2"];
    
    [self.followButtonOutlet makeButtonAsFollow];
    //passing parameters.
    NSDictionary *requestDict = @{muserNameToUnFollow     : self.checkProfileOfUserNmae ,
                                  mauthToken            :flStrForObj([Helper userToken]),
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler unFollow:requestDict andDelegate:self];
}


- (void)buttonClicked:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
    newView.checkProfileOfUserNmae = selectedButton.titleLabel.text;
    newView.checkingFriendsProfile = YES;
    [self.navigationController pushViewController:newView animated:YES];
}


/*-----------------------------------------------*/
#pragma mark -
#pragma mark - WebUrl Tap Action
/*-----------------------------------------------*/
/**
 This method get invoked on tapping website url.
 */
- (IBAction)webUrlTappedAction:(id)sender {
    [self openWebPage:self.webSiteUrlLabelOutlet.text];
}

-(void)openWebPage:(NSString *)selectedUrl{
    UIAlertController *alertController  =[UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(alertMessageToOpenSafari, alertMessageToOpenSafari) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(alertYes, alertYes) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *urlstr = [@"http://"    stringByAppendingString:selectedUrl];
        NSURL *url = [NSURL URLWithString:urlstr];
        if (![[UIApplication sharedApplication] openURL:url]) {
            UIAlertController *controller = [CommonMethods showAlertWithTitle:nil message:NSLocalizedString(invalidUrl, invalidUrl) actionTitle:NSLocalizedString(alertOk, alertOk)];
            mPresentAlertController;}
    }];
    UIAlertAction *noButton = [UIAlertAction actionWithTitle:NSLocalizedString(alertNo, alertNo) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:yesButton];
    [alertController addAction:noButton];
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark -
#pragma mark - Present Login -
/**
 This method will invoked for guest users and present login/signup screen.
 */
-(void)presentLoginForGuestUsers
{
    UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
    [self presentViewController:navigationController animated:YES completion:nil];
}


#pragma mark -
#pragma mark - CollectionView BackgroundView For Empty Posts -


/**
 This method is invoked to show custom background view for collectionview
 in case if product list is empty for any of section.

 @param type integer tag for sections.(selling/Sold/Favourites).
 */
-(void)showingMessageForCollectionViewBackgroundforCase:(NSInteger)type
{
    switch (type) {
        case 0:
        {
            self.startDiscoveringOutlet.hidden = YES;
            self.imageForBackGroundView.image = [UIImage imageNamed:@"empty_selling"];
            self.label1ForBackgroundView.text = NSLocalizedString(emptyPostsText, emptyPostsText);
            self.label2ForBackgroundView.text = NSLocalizedString(emptySellingPosts, emptySellingPosts);
            if(self.ProductDetails)
            {
                self.label2ForBackgroundView.hidden = self.noPostButtonOutlet.hidden = YES ;
                
            }
            else
            {
                self.label2ForBackgroundView.hidden = self.noPostButtonOutlet.hidden = NO ;
            }
            [self.noPostButtonOutlet addTarget:self action:@selector(startSellingAction) forControlEvents:UIControlEventTouchUpInside];
            UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.sellingCollectionView.frame.size.width,self.sellingCollectionView.frame.size.height)];
            self.backgroundViewForNoPost.frame = CGRectMake(0, 30,self.view.frame.size.width, 150);
            [backGroundViewForNoPost addSubview:self.backgroundViewForNoPost];
            self.sellingCollectionView.backgroundView = backGroundViewForNoPost;
        }
            
            break;
        case 1:
        {
            self.imageForBackGroundView.image = [UIImage imageNamed:@"empty_selling.png"];
            self.label1ForBackgroundView.text = NSLocalizedString(emptyPostsText, emptyPostsText);
            self.startDiscoveringOutlet.hidden = self.label2ForBackgroundView.hidden = self.noPostButtonOutlet.hidden = YES ;
            UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.soldCollectionView.frame.size.width,self.soldCollectionView.frame.size.height)];
            self.backgroundViewForNoPost.frame = CGRectMake(0, 30,self.view.frame.size.width, 150);
            [backGroundViewForNoPost addSubview:self.backgroundViewForNoPost];
            self.soldCollectionView.backgroundView = backGroundViewForNoPost;
        }
            break;
        case 2:
        {
            self.noPostButtonOutlet.hidden = YES ;
            self.startDiscoveringOutlet.hidden = self.label2ForBackgroundView.hidden = NO ;
            self.imageForBackGroundView.image = [UIImage imageNamed:@"empty_favourite"];
            self.label1ForBackgroundView.text = NSLocalizedString(emptyFavouritesText1, emptyFavouritesText1);
            self.label2ForBackgroundView.text = NSLocalizedString(emptyFavouritesText2, emptyFavouritesText2);
            [self.startDiscoveringOutlet addTarget:self action:@selector(startBrowsingProducts) forControlEvents:UIControlEventTouchUpInside];
            UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.favouritesCollectionView.frame.size.width,self.favouritesCollectionView.frame.size.height)];
            self.backgroundViewForNoPost.frame = CGRectMake(0, 30,self.view.frame.size.width, 150);
            [backGroundViewForNoPost addSubview:self.backgroundViewForNoPost];
            self.favouritesCollectionView.backgroundView = backGroundViewForNoPost;
        }
            break;
            
        default:
            break;
    }
}

-(void)startSellingAction
{
    CameraViewController *cameraVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CameraStoryBoardID"];
    cameraVC.sellProduct = TRUE;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraVC];
    
    [self presentViewController:nav animated:NO completion:nil];
    
}

-(void)startBrowsingProducts
{
    [self.tabBarController setSelectedIndex:0];
}


- (void)stopAnimation {
    __weak UserProfileViewController    *weakSelf = self;
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.scrollView.pullToRefreshView stopAnimating];
        [weakSelf.scrollView.infiniteScrollingView stopAnimating];
    });
}

/*---------------------------------------*/
#pragma
#pragma mark - Calculate CollectionView Height
/*---------------------------------------*/

/**
 Calculate height based on content.
 */
- (void)calculateCollectionViewHeightForTag :(NSInteger )tag {
    
    if (self.checkingFriendsProfile && ![[Helper userName]isEqualToString:self.checkProfileOfUserNmae]) {
        self.slidingScrollView.contentSize =  CGSizeMake(self.view.frame.size.width *2,self.slidingScrollView.contentSize.height ) ;
    }
    switch (tag) {
        case 0:
        {
            self.collectionViewHeight.constant = self.sellingCollectionView.contentSize.height;
            if(self.collectionViewHeight.constant < self.view.frame.size.height)
            {
                self.collectionViewHeight.constant =  self.view.frame.size.height - 100;
            }
        }
            break;
        case 1:
        {
            self.collectionViewHeight.constant = self.soldCollectionView.contentSize.height;
            if(self.collectionViewHeight.constant < self.view.frame.size.height)
            {
                self.collectionViewHeight.constant =  self.view.frame.size.height - 100;
            }
        }
            break;
            
        case 2:
        {
            self.collectionViewHeight.constant = self.favouritesCollectionView.contentSize.height;
            if(self.collectionViewHeight.constant < self.view.frame.size.height)
            {
                self.collectionViewHeight.constant =  self.view.frame.size.height - 100;
            }
        }
            break;
        default:
            
            break;
    }
}

/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Handling Response
/*-----------------------------------------------*/

-(void)handlingResponseForRequestPosts :(NSDictionary *)response
{
    if(self.sellingCurrentIndex == 0){
        [sellingPostData removeAllObjects];
    }
    [sellingPostData addObjectsFromArray:response[@"data"]];
    if(sellingPostData.count > 0)
    {
        self.sellingCollectionView.backgroundView = nil;
        [self.sellingCollectionView reloadData];
        [self calculateCollectionViewHeightForTag:0];
        self.sellingCurrentIndex++;
        self.sellingPaging ++ ;
    }
    else{
        [self calculateCollectionViewHeightForTag:0];
        [self showingMessageForCollectionViewBackgroundforCase:0];
    }
    
}


-(void)handlingResponseForSoldPosts :(NSDictionary *)response
{
    if(self.soldCurrentIndex == 0){
        [soldPostData removeAllObjects];
    }
    [soldPostData addObjectsFromArray:response[@"data"]];
    if(soldPostData.count > 0)
    {
        self.soldCollectionView.backgroundView = nil;
        [self.soldCollectionView reloadData];
        [self calculateCollectionViewHeightForTag:1];
        self.soldCurrentIndex++;
        self.favouritePaging ++ ;
        
    }
    else {
        [self.soldCollectionView reloadData];
        [self showingMessageForCollectionViewBackgroundforCase:1];
    }
    
}

-(void)handlingResponseForFavourites :(NSDictionary *)response
{
    if(self.favouritesCurrentIndex == 0){
        [favouritePostData removeAllObjects];
    }
    [favouritePostData addObjectsFromArray:response[@"data"]];
    if(favouritePostData.count > 0)
    {
        self.favouritesCollectionView.backgroundView = nil;
        [self.favouritesCollectionView reloadData];
        [self calculateCollectionViewHeightForTag:2];
        self.favouritesCurrentIndex++;
        self.favouritePaging++ ;
    }
    else if(self.favoriteButtonOutlet.selected){
        [self showingMessageForCollectionViewBackgroundforCase:2];
    }
    
}



/*-----------------------------------------------*/
#pragma mark -
#pragma mark - ScrollView Delegate. -
/*-----------------------------------------------*/

- (void)scrollViewDidScroll:(UIScrollView*)scrollView {
    
    if (self.checkingFriendsProfile && ![[Helper userName]isEqualToString:self.checkProfileOfUserNmae]) {
        self.slidingScrollView.contentSize =  CGSizeMake(self.view.frame.size.width *2,self.slidingScrollView.contentSize.height ) ;
    }
    if(scrollView == self.slidingScrollView)
    {
        CGPoint offset = self.slidingScrollView.contentOffset;
        if(self.checkingFriendsProfile && ![[Helper userName]isEqualToString:self.checkProfileOfUserNmae])
        {
            if([[Helper userToken] isEqualToString:mGuestToken])
            {
                [self presentLoginForGuestUsers];
            }
            else
            {
            self.movingDividerLeadingConstraint.constant = self.stickyDividerLeadingConstraint.constant = scrollView.contentOffset.x/2;
            }
        }
        else
        {
            self.movingDividerLeadingConstraint.constant = self.stickyDividerLeadingConstraint.constant = scrollView.contentOffset.x/3;
        }
        
        
        if(offset.x ==0 )
        {
            
        [self sellingButtonAction:self.sellingButtonOutlet];
        }
        
        else if(offset.x == CGRectGetWidth(self.view.frame))
        {
            
        [self soldButtonAction:self.soldButtonOutlet];
        }
        
        else if(offset.x == CGRectGetWidth(self.view.frame)*2)
        {
        [self favoriteButtonAction:self.favoriteButtonOutlet];
        }
    }
    
    else if(scrollView == self.scrollView)
    {
        [self offsetDidUpdate:scrollView.contentOffset];
        CGRect frame ;
        if(self.checkingFriendsProfile)
        {
            frame = CGRectMake(0, 230, self.view.frame.size.width,50);
        }
        else
        {
            frame = CGRectMake(0, 200, self.view.frame.size.width,50);
        }
        
        if(!CGRectIntersectsRect(self.scrollView.bounds ,frame ))
        {
            self.stickySectionHeaderForCV.hidden = NO ;
            self.sectionHeaderForCollectionView.hidden = YES;
        }
        else
        {
            self.stickySectionHeaderForCV.hidden = YES ;
            self.sectionHeaderForCollectionView.hidden = NO;
        }
        
    }
}

- (void)offsetDidUpdate:(CGPoint)newOffset
{
    if (newOffset.y < 0.2 && newOffset.y!=0)
    {
        
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, newOffset.y/2);
        
        CGFloat scaleFactor = (300 - (newOffset.y/3)) /300;
        
        CGAffineTransform translateAndZoom = CGAffineTransformScale(translate, scaleFactor, scaleFactor);
        
        float alpha = -newOffset.y/100.0;
        if(alpha < 0.5)
        {
            self.topImageView.alpha = 0.5;
        }
        else
        {
            self.topImageView.alpha = alpha;
        }
        //        self.avterView.alpha = 1-alpha;
        
        self.profilePhotoOutlet.alpha = 1 - alpha;
        
        self.topImageView.transform = translateAndZoom;
        self.profilePhotoOutlet.transform = translateAndZoom;
        
    }
    
    else
    {
        self.topImageView.alpha = 0.5;
        
        CGFloat movedOffset = -_scrollView.contentOffset.y;
        
        ratio= -((movedOffset)/150) ;
        navBackView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:ratio];
        if (ratio>1) {
            navBackView.backgroundColor = [UIColor whiteColor];
            dividerView.backgroundColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f];
            self.navigationItem.title = flStrForObj(USERNAME);
            self.topImageView.hidden = YES;
            if(!self.checkingFriendsProfile)
            {
                [self buttonName:navNextButton imageNameForONStatus:mSettingsON];
                [self buttonName:navCancelButton imageNameForONStatus:mAddPeopleIconON];
                [self buttonName:self.editButtonOutlet imageNameForONStatus:mEditProfileON];
            }
            else
            {
                [self buttonName:navCancelButton imageNameForONStatus:mNavigationBackButtonImageName];
                [self buttonName:navNextButton imageNameForONStatus:mMoreON];
            }
            
        }else{
            self.topImageView.hidden = NO ;
            navBackView.backgroundColor = dividerView.backgroundColor = [UIColor clearColor];
            self.navigationItem.title =@"";
            if(!self.checkingFriendsProfile)
            {
                [self buttonName:navNextButton imageNameForOFFStatus:mSettingsOFF];
                [self buttonName:navCancelButton imageNameForOFFStatus:mAddPeopleIconOFF];
                [self buttonName:self.editButtonOutlet imageNameForONStatus:mEditProfileOFF];
            }
            else
            {
                [self buttonName:navCancelButton imageNameForOFFStatus:@"item backButton"];
                [self buttonName:navNextButton imageNameForOFFStatus:@"item_Report"];
                
            }
        }
        
        
        
    }
}



-(void)buttonName:(UIButton *)buttonName imageNameForONStatus :(NSString *)imageName
{
    [[buttonName layer] setShadowOpacity:0.0];
    [[buttonName layer] setShadowRadius:0.0];
    [[buttonName layer] setShadowColor:[UIColor clearColor].CGColor];
    [buttonName setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
}

-(void)buttonName:(UIButton *)buttonName imageNameForOFFStatus :(NSString *)imageName
{
    [[buttonName layer] setShadowColor:[UIColor blackColor].CGColor];
    buttonName.layer.shadowOffset = CGSizeMake(0, -2);
    buttonName.layer.shadowRadius = 5;
    buttonName.layer.shadowOpacity = 0.5;
    [buttonName setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}


#pragma mark -
#pragma mark - Verification Action Methods -

- (IBAction)phoneNumberVerfyAction:(id)sender {
    UpdatePhoneNumberVC *updatePhoneVC = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyPhoneNumber"];
    updatePhoneVC.verify = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:updatePhoneVC];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

- (IBAction)facebookVerfyAction:(id)sender {
    //request for fb.
    FBLoginHandler *handler = [FBLoginHandler sharedInstance];
    [handler loginWithFacebook:self];
    [handler setDelegate:self];
}


- (IBAction)googlePlusVerifiAction:(id)sender {
    
    GIDSignIn *signin = [GIDSignIn sharedInstance];
    [signin signOut];
    signin.shouldFetchBasicProfile = true;
    signin.delegate = self;
    signin.uiDelegate = self;
    [signin signIn];
}

- (IBAction)mailVerificationAction:(id)sender {
    
    UpDatingEmailViewController *updateEmailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyEmail"];
    updateEmailVC.verifyEmail = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:updateEmailVC];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}


/*--------------------------------------------*/
#pragma mark
#pragma mark - facebook handler -
/*--------------------------------------------*/

/**
 Facebook handler get call on success of facebook service.
 
 @param userInfo user information in dictionary.
 */
- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo {
    
    
    fbloggedUserAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    
    
    NSDictionary *requestDict = @{mFacebookAccessToken :fbloggedUserAccessToken ,
                                  mauthToken  :flStrForObj([Helper userToken]),
                                  };
    [WebServiceHandler verifyWithFacebook:requestDict andDelegate:self];
    //    self.fbLoginDetails = userInfo;
}


/**
 If facebook handler get unsuccessful rsponse.

 @param error errorDescription
 */
- (void)didFailWithError:(NSError *)error {
    [Helper showAlertWithTitle:nil Message:NSLocalizedString(facebookVerificationFailed, facebookVerificationFailed) viewController:self];
}

#pragma mark -
#pragma mark - GIDSignInDelegate -

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    if (error == nil) {
        
        googlePlusUserAccessToken = user.authentication.accessToken;
        googlePlusId = user.userID ;
        
        NSDictionary *requestDict = @{mauthToken : flStrForObj([Helper userToken]),
                                      mGooglePlusId :user.userID,
                                      mFacebookAccessToken : googlePlusUserAccessToken
                                      };
        [WebServiceHandler verifyWithGooglePlus:requestDict andDelegate:self];
        
        
    } else {
        
    }
}



#pragma mark -
#pragma mark - ZoomTransitionProtocol -

/**
 Animation method.This method open product details screen by taking snapshot for product image.
 and show animation transition.

 @param isSource Boll value Yes if transition from source to destination.

 @return return image for transition.
 */
-(UIView *)viewForZoomTransition:(BOOL)isSource
{
    if(self.sellingButtonOutlet.selected || self.stickySellingButtonOutlet.selected)
    {
        NSIndexPath *selectedIndexPath = [[self.sellingCollectionView indexPathsForSelectedItems] firstObject];
        UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.sellingCollectionView cellForItemAtIndexPath:selectedIndexPath];
        
        return cell.postedImagesOutlet;
        
    }
    else if(self.stickyFavouriteButtonOutlet.selected || self.favoriteButtonOutlet.selected)
    {
        NSIndexPath *selectedIndexPath = [[self.favouritesCollectionView indexPathsForSelectedItems] firstObject];
        UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.favouritesCollectionView cellForItemAtIndexPath:selectedIndexPath];
        return cell.postedImagesOutlet;
        
    }
    else
    {
        NSIndexPath *selectedIndexPath = [[self.soldCollectionView indexPathsForSelectedItems] firstObject];
        UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.soldCollectionView cellForItemAtIndexPath:selectedIndexPath];
        return cell.postedImagesOutlet;
        
    }
}


/**
 PayPal button action method.
 This method redirect user to paypal controller screen for linking/unlinking paypal.

 @param sender paypal button outlet.
 */
- (IBAction)paypalAction:(id)sender {
    PaypalViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mpaypalView];
    newView.paypalCallback = self;
    newView.paypalLink = paypalUrl;
    newView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:newView animated:YES];
    
}

#pragma mark -
#pragma PayPal callback delegate -

/**
 Delegate Method from Paypal controller.
 Update Status of Paypal Button on the basis of paypal link saved/unlink.

 @param paypalLink payPal link saved/unlink from paypal controller.
 */
-(void)PaypalLinkIsSaved:(NSString *)paypalLink
{
   if(paypalLink.length)
   {
       [self.paypalOutlet setImage:[UIImage imageNamed:@"paypal verified"] forState:UIControlStateNormal];
       self.paypalOutlet.layer.borderWidth = 0;
       self.paypalOutlet.layer.borderColor = [UIColor clearColor].CGColor;
       [self.paypalOutlet setTitle:@"" forState:UIControlStateNormal];
       paypalUrl = paypalLink ;
   }
   else
   {
       [self.paypalOutlet setImage:[UIImage imageNamed:@"paypal unverified"] forState:UIControlStateNormal];
       self.paypalOutlet.layer.borderWidth = 0.5;
       self.paypalOutlet.layer.borderColor = [UIColor lightGrayColor].CGColor;
       [self.paypalOutlet setTitle:@"Paypal.Me" forState:UIControlStateNormal];
       paypalUrl = @"";
   }
    [[NSUserDefaults standardUserDefaults]setObject:paypalUrl forKey:payPalLink ];
}

#pragma mark -
#pragma mark - Dont Show Favourites -

/**
 Dont Show Favorite Products option if user is checking his
 own profile.
 */
-(void)dontShowFavouritesOfOtherUsers {
    CGFloat widthOfButton = self.view.frame.size.width/2;
    self.sellingButtonWidth.constant = self.stickySellingWidth.constant = widthOfButton;
    self.soldWidth.constant = self.stickySoldWidth.constant = widthOfButton;
    self.favButtonWidthConstraint.constant = self.stickyFavWidth.constant = 0;
    self.movingDividerWidth.constant = self.stickyMovingDividerWidth.constant = widthOfButton - 10;
    
}


/**
 Show Favorite Products option if user is checking his
 own profile.
 */
-(void)showFaviouriteButton {
    CGFloat widthOfButton = self.view.frame.size.width/3;
    self.sellingButtonWidth.constant = self.stickySellingWidth.constant = widthOfButton;
    self.soldWidth.constant = self.stickySoldWidth.constant = widthOfButton;
    self.favButtonWidthConstraint.constant = self.stickyFavWidth.constant = widthOfButton;
    self.movingDividerWidth.constant = self.stickyMovingDividerWidth.constant = widthOfButton - 10;
    self.slidingScrollView.contentSize =  CGSizeMake(self.view.frame.size.width *3,self.slidingScrollView.contentSize.height ) ;
}

@end
