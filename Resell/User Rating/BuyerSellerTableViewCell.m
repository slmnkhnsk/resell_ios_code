//
//  BuyerSellerTableViewCell.m

//
//  Created by Rahul Sharma on 26/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "BuyerSellerTableViewCell.h"

@implementation BuyerSellerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.selectButtonOutlet setTitleColor:mBaseColor forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
