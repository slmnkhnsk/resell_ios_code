//
//  SelectBuyerSellerViewController.h

//
//  Created by Rahul Sharma on 26/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBuyerSellerViewController : UIViewController

@property (nonatomic,strong) NSMutableArray *responseDict;
@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end
