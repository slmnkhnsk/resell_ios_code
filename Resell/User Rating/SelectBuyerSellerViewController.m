//
//  SelectBuyerSellerViewController.m

//
//  Created by Rahul Sharma on 26/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SelectBuyerSellerViewController.h"
#import "BuyerSellerTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"
#import "StarScreenViewController.h"

@interface SelectBuyerSellerViewController ()<UITableViewDelegate,UITableViewDataSource,WebServiceHandlerDelegate>

@end

@implementation SelectBuyerSellerViewController
{
    NSInteger dataIndex;
    int tagButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Select Buyer To Rate";
    [self createNavLeftButton];
    // Do any additional setup after loading the view.
}
-(void)createNavLeftButton
{
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

-(void)navLeftButtonAction
{
      [self.navigationController popViewControllerAnimated:YES];;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    tagButton = 0;
    if(section == 0)
    return _responseDict.count ;
    else
        return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BuyerSellerTableViewCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:@"ListCell"forIndexPath:indexPath];
    [cell.selectButtonOutlet.layer setBorderColor: mBaseColor.CGColor];
    cell.selectButtonOutlet.tag = tagButton++;
    [cell.selectButtonOutlet addTarget:self action:@selector(selectedUserButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    NSDictionary * data = [_responseDict objectAtIndex:indexPath.row];
    if(indexPath.section == 1 )
    {
        cell.widthImage.constant = 0;
        cell.nameLabel.text = @"Sold it Somewhere else";
        cell.addressLabel.text = @"I didn't sell it on Resell";
        return cell;
    }
    cell.nameLabel.text = [NSString stringWithFormat:@"%@",data[@"buyername"]];
    cell.addressLabel.text = [[NSString stringWithFormat:@"Price Offered %@",data[@"currency"]]stringByAppendingString:[NSString stringWithFormat:@" %@",data[@"offerPrice"]]];
    NSString *imageURL1 =[NSString stringWithFormat:@"%@",data[@"buyerProfilePicUrl"]];
        NSURL *imageUrl =[NSURL URLWithString:imageURL1];
        [cell.memberPic sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"defaultpp"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ToStarView"]) {
        StarScreenViewController *vc = [segue destinationViewController];
        vc.activityResponse = [_responseDict objectAtIndex:dataIndex] ;
    }
} 


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)selectedUserButtonClicked:(UIButton*)sender
{
    if (sender.tag < _responseDict.count)
    {
        dataIndex = sender.tag ;
        [self performSegueWithIdentifier:@"ToStarView" sender:nil];
    }
    else
    {
        ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
        [HomePI showPIOnView:self.view withMessage:@"Submiting..."];
        NSDictionary *data = [_responseDict objectAtIndex:0];
        NSDictionary * dict = @{@"postId":[NSString stringWithFormat:@"%@",data[@"postId"]],
                                @"type":@"0",
                                @"token" : [Helper userToken]
                                };
        [WebServiceHandler soldSomeWhere:dict andDelegate:self];
    }
}

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    NSDictionary *responseDict = (NSDictionary*)response;
    
    if (requestType == RequestTypesoldElseWhere) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200:
                [[ProgressIndicator sharedInstance] hideProgressIndicator];
                [[NSNotificationCenter defaultCenter]postNotificationName:mSellingToSoldNotification object:[NSDictionary dictionaryWithObject:response[@"data"] forKey:@"data"]];
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
                
            case 409:
            {
                [[ProgressIndicator sharedInstance] hideProgressIndicator];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Already Sold."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
                break;
                
            default:
                break;
        }
    }
}
@end
