//
//  StarScreenViewController.h

//
//  Created by Rahul Sharma on 27/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarScreenViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *submitRatingOut;

@property (nonatomic) NSDictionary *responseDict;
@property (nonatomic) NSDictionary *activityResponse;

- (IBAction)SubmitButton:(id)sender;
@property BOOL ratiingForSeller;
@end
