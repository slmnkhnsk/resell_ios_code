//
//  StarTableViewCell.h

//
//  Created by Rahul Sharma on 27/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *sellerPic;

@property (strong, nonatomic) IBOutlet UILabel *starLabel;
@property (strong, nonatomic) IBOutlet UIButton *starOne;
@property (strong, nonatomic) IBOutlet UIButton *starTwo;
@property (strong, nonatomic) IBOutlet UIButton *starThree;
@property (strong, nonatomic) IBOutlet UIButton *starFour;
@property (strong, nonatomic) IBOutlet UIButton *starFive;
@property (nonatomic) int ratingValue;

- (IBAction)starActionButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *notLabel;
@property (weak, nonatomic) IBOutlet UILabel *yesLabel;


@end
