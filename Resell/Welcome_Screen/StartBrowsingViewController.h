//
//  StartBrowsingViewController.h

//
//  Created by Rahul Sharma on 29/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^activateTabBar)(BOOL activate);

@interface StartBrowsingViewController : UIViewController
@property (nonatomic,strong)activateTabBar callbackToActivateTab ;
@property (weak, nonatomic) IBOutlet UIButton *btnStartBrowsing;

- (IBAction)startBrowsingButtonAction:(id)sender;

@end
