//
//  StartBrowsingViewController.m

//
//  Created by Rahul Sharma on 29/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "StartBrowsingViewController.h"

@interface StartBrowsingViewController ()

@end

@implementation StartBrowsingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    [self.btnStartBrowsing setBackgroundColor:mBaseColor];
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)startBrowsingButtonAction:(id)sender {
    
    if(self.callbackToActivateTab)
    {
        self.callbackToActivateTab (YES);
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
