//
//  UserProfileViewController.h
//
//  Created by Rahul Sharma on 3/30/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserProfileCollectionViewCell.h"
#import "UserProfileViewTableViewCell.h"
#import "KILabel.h"


@interface UserProfileViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource, UIScrollViewDelegate>
@property bool  checkingFriendsProfile;
@property (strong,nonatomic) NSString *checkProfileOfUserNmae;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editBarButtonItem;
@property (strong, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfPostsLabelOutlet;
@property (weak, nonatomic) IBOutlet UILabel *numberOfFollowersLabelOutlet;
@property (weak, nonatomic) IBOutlet UILabel *numberOfFollowingLabelOutlet;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabelOutlet;
@property (strong,nonatomic)  NSMutableString *phoneNumberOfMember;
@property (weak,nonatomic) NSString *emailOfMember;
@property (weak, nonatomic) IBOutlet UILabel *webSiteUrlLabelOutlet;
@property (nonatomic)NSInteger sellingCurrentIndex,soldCurrentIndex,favouritesCurrentIndex,sellingPaging,soldPaging,favouritePaging,sellingDisplayedIndex,soldDisplayedIndex,favouriteDisplayedIndex ;
@property BOOL ProductDetails;
@property (weak, nonatomic) IBOutlet UIView *topContentView;
@property (weak, nonatomic) IBOutlet UIView *verifiedByView;
@property (weak, nonatomic) IBOutlet UIButton *paypalOutlet;

@property (strong, nonatomic) IBOutlet UIButton *facebookButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *googlePlusButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *emailButtonOutLet;
@property (strong, nonatomic) IBOutlet UIButton *mobileButtonOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *facebookVerifyOutlet;

@property (strong, nonatomic) IBOutlet UIImageView *googleVerifyOutlet;

@property (strong, nonatomic) IBOutlet UIImageView *emailVerifyOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *mobileVerifyOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *paypalVerifyImageOutlet;

@property (strong, nonatomic) IBOutlet UIView *viewForContactAndFollow;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintForContactView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintForPostView;

/**
 *  profile photo imageView outlet.
 */
@property (weak, nonatomic) IBOutlet UIImageView *profilePhotoOutlet;
/**
 *  collectionView and tableView heightOutlet and its acommon for both collection and table view.
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;

/**
 *  outlet for scrollView.
 */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

/**
 *  outlet for collectionView.
 */
@property (weak, nonatomic) IBOutlet KILabel *biodataLabelOutlet;
@property (strong, nonatomic) IBOutlet UIButton *followButtonOutlet;


@property (strong, nonatomic) IBOutlet UIButton *contactButtonOutlet;

- (IBAction)favoriteButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *stickySellingButtonOutlet;

@property (strong, nonatomic) IBOutlet UIButton *stickySoldButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *stickyFavouriteButtonOutlet;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *stickyDividerLeadingConstraint;

@property (weak, nonatomic) IBOutlet UIButton *favoriteButtonOutlet;

/**
 *  collectionView button outlet
 */

@property (weak, nonatomic) IBOutlet UIButton *sellingButtonOutlet;

/**
 *  tableView button outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *soldButtonOutlet;

//Button Actions

- (IBAction)followingButtonAction:(id)sender;
- (IBAction)followersButtonAction:(id)sender;
- (IBAction)postsButtonAction:(id)sender;
- (IBAction)editProfileButtonAction:(id)sender;
/**
 *  button action for collectionView.
 *
 *  @param sender it will open collection view.
 */
- (IBAction)sellingButtonAction:(id)sender;
/**
 *  button action for tableView.
 *
 *  @param sender it will open table view.
 */
- (IBAction)soldButtonAction:(id)sender;

/**
 *  button action for tableView.
 *
 *  @param sender it will open map view.
 */
- (IBAction)contactButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *movingDividerLeadingConstraint;
- (IBAction)followButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *editButtonOutlet;
@property (strong, nonatomic) IBOutlet UIView *sectionHeaderForCollectionView;
@property (strong, nonatomic) IBOutlet UIView *viewAboveSectionHeaderOfCV;
@property (strong, nonatomic) IBOutlet UIView *stickySectionHeaderForCV;
@property (strong, nonatomic) IBOutlet UIView *backgroundViewForNoPost;

@property (strong, nonatomic) IBOutlet UILabel *label2ForBackgroundView;
@property (strong, nonatomic) IBOutlet UIImageView *imageForBackGroundView;
@property (strong, nonatomic) IBOutlet UILabel *label1ForBackgroundView;
@property (strong, nonatomic) IBOutlet UIButton *noPostButtonOutlet;

@property (strong, nonatomic) IBOutlet UIButton *startDiscoveringOutlet;

// Scrolling Horizontal Objects

@property (strong, nonatomic) IBOutlet UIView *sellingViewInScrollview;

@property (strong, nonatomic) IBOutlet UICollectionView *sellingCollectionView;

@property (strong, nonatomic) IBOutlet UIView *soldViewInScrollview;
@property (strong, nonatomic) IBOutlet UICollectionView *soldCollectionView;
@property (strong, nonatomic) IBOutlet UIView *favouritesViewInScrollview;
@property (strong, nonatomic) IBOutlet UICollectionView *favouritesCollectionView;
@property (strong, nonatomic) IBOutlet UIScrollView *slidingScrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sellingButtonWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *movingDividerWidth;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *favButtonWidthConstraint;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *soldWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *stickyMovingDividerWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *stickySellingWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *stickySoldWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *stickyFavWidth;
@property (weak, nonatomic) IBOutlet UIView *productSectionContentView;

@end
