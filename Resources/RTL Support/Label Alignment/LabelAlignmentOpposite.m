//
//  LabelAlignmentOpposite.m
//  Resell
//
//  Created by Rahul Sharma on 03/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "LabelAlignmentOpposite.h"

@implementation LabelAlignmentOpposite

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setAlignmentForLabel];
}
- (void)setAlignmentForLabel {
    
    if([[RTL sharedInstance] isRTL]) {
        [self setTextAlignment:NSTextAlignmentLeft];
    }
    else {
        [self setTextAlignment:NSTextAlignmentRight];
    }
}


@end
