//
//  SearchBarAlignment.m
//  Resell
//
//  Created by Rahul Sharma on 02/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SearchBarAlignment.h"

@implementation SearchBarAlignment

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setTextAlignmentForSearchBarField];
}

- (void)setTextAlignmentForSearchBarField {
    
    if([[RTL sharedInstance] isRTL]) {
        UITextField *textFieldInsideSearchBar = [self valueForKey:@"searchField"];
        textFieldInsideSearchBar.textAlignment = NSTextAlignmentRight ;
    }
    else
    {
        UITextField *textFieldInsideSearchBar = [self valueForKey:@"searchField"];
        textFieldInsideSearchBar.textAlignment = NSTextAlignmentLeft ;
    }
}
@end
